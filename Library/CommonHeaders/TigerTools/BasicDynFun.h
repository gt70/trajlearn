/*	Basci dyn function
Optimal control imbedded
Make it pure, avoid any global parameters
*/
#ifndef BASICDYNFUN_H
#define BASICDYNFUN_H
#include "TigerTools/Integrator.h"
namespace basicdynfun {
	/***EE case***/
	//Dyn fun
	//hpara:m0, Tmax, Ispg0, mu
	int TOPTDynFun(double t, const double* x, double* dx, const double* hpara);
	//Ham fun
	//hpara:m0, Tmax, Ispg0, mu
	double TOPTHamFun(double t, const double *x, double lmd0, const double *hpara);
	//Dynamic function for fuel optimal solver with logarithmic homotopy
	//para should contain:lmd0, eps, T, Isp, mu
	int FOPTDynFun(double t, const double* x, double* dx, const double* hpara);
	int QFOPTDynFun(double t, const double* x, double* dx, const double* hpara);

	//H for fuel optimal with logarithmic homotopy
	//para contains: lmd0, eps, T, Isp, mu
	double FOPTHamFun(double t, const double *x, const double *hpara);
	double QFOPTHamFun(double t, const double *x, const double *hpara);

	//H for fuel optimal with logarithmic homotopy
	//para contains: lmd0, eps, T, Isp, mu
	double FOPTHamFunMOne(double t, const double *x, const double *hpara);
	double QFOPTHamFunMOne(double t, const double *x, const double *hpara);

	//Dynamic function for whose rho contains no 1.
	//para contains:lmd0, eps, T, IG, mu
	int FOPTDynFunType2(double t, const double* x, double* dx, const double* hpara);
	//H for other type
	double FOPTHamFunType2(double t, const double *x, const double *hpara);

	//Bang control law
	//para contains: lmd0, (eps), T, Isp, mu
	//Thrust on, int
	int ThrustDyn(double t, const double *x, double *dx, const double *para);

	//Off control law, which is quite simple
	//para contains:lmd0, (eps), T, Isp, mu
	int CoastDyn(double t, const double *x, double *dx, const double *para);

	//Hamilton with bang-off-bang structure
	//para contains:lmd0, (eps), T, Isp, mu
	double BangHamFun(double t, const double *x, const double *para);

	//Generate rho, which is not affected by u
	//para contains:lmd0, (eps), T, Isp, mu
	double rho(const double *x, const double *lmd, const double *para);
	double drhodt(const double *x, const double *lmd, const double *para);
	void rhodrhodtgen(const double *x, const double *lmd, double *rho, double *drhodt, const double *para);

	int SFun(double t, const double *x, const double *para, double &fval, double &fgrad, int calcflag);

	class ODE45Integrator;
	//An function to help integrate from t0 to tf with accurate switch detection
	int BangInt(double *state, double t0, double tf, const double *para, FILE *fp, TigerIntegrator::ODE45Integrator *FODE45, double IStep = 1e-1, double TimeTol = 1e-10);

	/****RV case***/
	//Dynamic function for fuel optimal solver with logarithmic homotopy described with RV
	//para should contain:lmd0, eps, T, Isp, mu
	int RVFOPTDyn(double t, const double* x, double* dx, const double* para);
	/*
	Function to obtain H
	para:lmd0, m0, t0, T, IG, mu
	*/
	double RVTOPTHam(double t, const double *x, const double *para);
	//para should contain:lmd0, eps, T, Isp, mu
	double RVFOPTHam(double t, const double *x, const double *para);
	template<class T>double RVFOPTRho(T m, T lmdm, T *lmdv, double *para) {
		const double lmd0 = para[0], IG = para[3];
		return 1 - lmdm / lmd0 - IG*V_Norm2(lmdv, 3) / m / lmd0;
	}
	/***Adjoint tran***/
	int AdjointTranEEToRV(const double *ee, const double *lmdee, double *lmdrv, const double mu);
	int AdjointTranRVToEE(const double *ee, const double *lmdrv, double *lmdee, const double mu);
	//use for GTOC to generate required formatted thrust
	void ControlLawGen(double *Thrust, const double *x, const double lmd0, const double epsilon, const double T, const double IG, const double mu);
}
#endif