#include "TigerMinpackSolver.h"
#include "../MinPack/cminpack.h"
#include <iostream>
#include "stdlib.h"
namespace TigerMinpackSolver{
//Init
	TigerMinpackSolver::TigerMinpackSolver(const int n, const int nprint_) {
	dim = n;
	xtol = 1e-10;
	nprint = nprint_;
	maxfevno = 2000;
	fvecnorm = 0;
	work = new double[n*(3*n + 13)/2];
	if(work == NULL){
		std::cout << "MinpackSolver allocate error\n";
		exit(0);
	}
	fjac = NULL;
}
int TigerMinpackSolver::Solve(SolveFun fun, double *x, double *fvec, const double *para){
	int flag = hybrd1(fun, dim, x, fvec, para, work, xtol, nprint, maxfevno);
	return flag;
}
int TigerMinpackSolver::Solveas(SolveFun fun, double *x, double *fvec, const double *para){
	int flag = hybrd1_as(fun, dim, x, fvec, para, work, xtol, nprint, maxfevno);
	return flag;
}
int TigerMinpackSolver::Solve(DBSolveFun fun, double *x, double *fvec, const double *para) {
	int flag = hybrd1((SolveFun)fun, dim, x, fvec, para, work, xtol, nprint, maxfevno);
	return flag;
}
int TigerMinpackSolver::Solve(SolveJac fun, double *x, double *fvec, const double *para){
	if(fjac == NULL){
		fjac = new double[dim*dim];
		if(fjac == NULL){
			std::cout << "MinpackSolver allocate error\n";
			exit(0);
		}
	}
	int flag = hybrj1(fun, dim, x, fvec, para, fjac, work, xtol, nprint, maxfevno);
	return flag;
}

int TigerMinpackSolver::Solvefac(SolveJac fun, double *x, double *fvec, const double *para, double fac){
	if(fjac == NULL){
		fjac = new double[dim*dim];
		if(fjac == NULL){
			std::cout << "MinpackSolver allocate error\n";
			exit(0);
		}
	}
	int flag = hybrj1_fac(fun, dim, x, fvec, para, fjac, work, xtol, nprint, maxfevno, fac);
	return flag;
}

int TigerMinpackSolver::Solveas(SolveJac fun, double *x, double *fvec, const double *para){
	if(fjac == NULL){
		fjac = new double[dim*dim];
		if(fjac == NULL){
			std::cout << "MinpackSolver allocate error\n";
			exit(0);
		}
	}
	int flag = hybrj1_as(fun, dim, x, fvec, para, fjac, work, xtol, nprint, maxfevno);
	return flag;
}

void TigerMinpackSolver::setDim(const int n) {
	if(dim < n){
		delete[] work;
		work = new double[n*(3*n + 13)/2];
		if(work == NULL){
			std::cout << "MinpackSolver allocate error\n";
			exit(0);
		}
	}
	dim = n;
}

void TigerMinpackSolver::setXtol(const double tol) {
	xtol = tol;
}
void TigerMinpackSolver::setNprint(const int nprint_) {
	nprint = nprint_;
}
void TigerMinpackSolver::setMaxfevno(const int maxfevno_) {
	maxfevno = maxfevno_;
}
TigerMinpackSolver::~TigerMinpackSolver(){
	delete[] work;
	if(fjac)
		delete[] fjac;
}
}
