/*  Dev by tiger at 2014/12/16
*/
#include "InitFile.h"
#include <iostream>
#include <fstream>
#include "stdio.h"
#include "stdlib.h"
using namespace std;
namespace TigerConfig {
	void Config::init(const char* filename) {
		ifstream infile(filename);
		if (infile.fail()) {
			cout << "Read " << filename << " error\n";
			exit(0);
		}
		string str;
		while (!infile.eof()) {
			getline(infile, str);
			//Check if comment
			if (str[0] == '#')
				continue;
			//Find key
			if (str.find(':') != string::npos) {
				str.erase(str.end() - 1);//erase ":"
				string key(str);
				//get vector<string>
				vector<string> vstr;
				string currentstr;
				while (!infile.eof()) {
					//read unitl ":" found
					int place = infile.tellg();
					getline(infile, currentstr);
					if (currentstr[0] == '#')//if comment skip
						continue;
					if (currentstr.find(':') != string::npos) {
						infile.seekg(place);
						break;
					}
					vstr.push_back(currentstr);
					if (infile.fail())
						break;
				}
				imap.insert(make_pair(key, vstr));
				if (infile.fail()) {
					infile.close();
					return;
				}
			}
			if (infile.fail()) {
				infile.close();
				return;
			}
		}
	}
	void Config::getIntArray(const string &key, int *iA) {
		int len = imap[key].size();
		for (int id = 0; id != len; id++)
			sscanf(imap[key][id].c_str(), "%d", iA + id);
	}
	void Config::getDoubleArray(const string &key, double *iA) {
		int len = imap[key].size();
		for (int id = 0; id != len; id++)
			sscanf(imap[key][id].c_str(), "%lf", iA + id);
	}
	void Config::getIntArray(const string &key, int *iA, int len) {
		for (int id = 0; id != len; id++)
			sscanf(imap[key][id].c_str(), "%d", iA + id);
	}
	void Config::getDoubleArray(const string &key, double *iA, int len) {
		for (int id = 0; id != len; id++)
			sscanf(imap[key][id].c_str(), "%lf", iA + id);
	}
	int Config::getLength(const string &key) {
		return imap[key].size();
	}
}

