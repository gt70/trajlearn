#ifndef _MATHCONSTANT_H_
#define _MATHCONSTANT_H_
#include <cmath>
//Original Header
//extern double IspPg0NU, lmd0, TmaxNU, mhEarthNU, lmd0;
const double DPI = 3.1415926535897932384626433832795;
const double D2PI = 6.283185307179586476925286766559;
const double D2R = 0.017453292519943295769236907684886;
const double R2D = 57.295779513082320876798154814105;
//const double MJD0 = 59215.0;
#endif
