/*	Basci dyn function
	Optimal control imbedded
	Make it pure, avoid any global parameters
*/
#include "BasicDynFun.h"
#include "TigerTools.h"
#include "Tools/VecMat.h"
#include "Tools/OrbitFun.h"
#include <cmath>
namespace basicdynfun {
	/*********EE Case**********/
	//Dyn fun
	//hpara:m0, Tmax, Ispg0, mu
	int TOPTDynFun(double t, const double* x, double* dx, const double* hpara) {
		const double m0 = hpara[0], Tmax = hpara[1], Ispg0 = hpara[2], mu = hpara[3];
		double m = m0 - t*Tmax / Ispg0;
		const double P = x[0], ex = x[1], ey = x[2], hx = x[3], hy = x[4], L = x[5];
		const double *lmd = x + 6, lmdm = x[11];
		const double t1 = 0.1e1 / mu;
		const double t3 = sqrt(t1 * P);
		const double t5 = cos(L);
		const double t7 = sin(L);
		const double t9 = t5 * ex + t7 * ey + 0.1e1;
		const double t10 = 0.1e1 / t9;
		const double t12 = 0.1e1 / m;
		const double t13 = t12 * Tmax;
		const double t14 = t3 * lmd[1];
		const double t16 = t3 * lmd[2];
		const double t18 = -t7 * t14 + t5 * t16;
		const double t19 = t18 * t18;
		const double t20 = t3 * lmd[0];
		const double t21 = t10 * P;
		const double t24 = ex + t5;
		const double t26 = t10 * t24 + t5;
		const double t28 = ey + t7;
		const double t30 = t10 * t28 + t7;
		const double t32 = -t26 * t14 - t30 * t16 - 0.2e1 * t21 * t20;
		const double t33 = t32 * t32;
		const double t34 = t10 * lmd[1];
		const double t37 = t7 * hx - t5 * hy;
		const double t38 = t37 * t3;
		const double t41 = t10 * lmd[2];
		const double t44 = t10 * lmd[3];
		const double t45 = hx * hx;
		const double t46 = hy * hy;
		const double t47 = t45 + t46 + 0.1e1;
		const double t48 = t47 * t3 / 0.2e1;
		const double t49 = t5 * t48;
		const double t51 = t10 * lmd[4];
		const double t52 = t7 * t48;
		const double t54 = t10 * lmd[5];
		const double t56 = -ex * t38 * t41 + ey * t38 * t34 - t38 * t54 - t49 * t44 - t52 * t51;
		const double t57 = t56 * t56;
		const double t59 = sqrt(t19 + t33 + t57);
		const double t60 = 0.1e1 / t59;
		const double t61 = t32 * t60;
		const double t62 = t61 * t13;
		const double t65 = t7 * t3;
		const double t67 = t60 * t12;
		const double t68 = t18 * t67;
		const double t69 = t68 * Tmax * t65;
		const double t72 = t32 * t67;
		const double t74 = t3 * t10;
		const double t75 = ey * t37;
		const double t77 = t56 * t60;
		const double t78 = t77 * t13;
		const double t81 = t5 * t3;
		const double t83 = t68 * Tmax * t81;
		const double t87 = ex * t37;
		const double t97 = 0.1e1 / P;
		const double t99 = sqrt(t97 * mu);
		const double t100 = t9 * t9;
		const double t101 = t100 * t99;
		const double t104 = t78 * t37 * t74;
		const double t106 = 0.1e1 / t3;
		const double t118 = t1 * t18 * t67;
		const double t123 = t1 * t32 * t67;
		const double t125 = t106 * t10;
		const double t128 = t1 * t77 * t13;
		const double t142 = t47 * t106 / 0.2e1;
		const double t153 = P * P;
		const double t161 = Tmax * t37;
		const double t170 = 0.1e1 / t100;
		const double t172 = t170 * P * t20;
		const double t177 = t170 * t24;
		const double t183 = t3 * t170;
		const double t184 = t75 * t183;
		const double t186 = t5 * t77 * t13;
		const double t194 = t87 * t183;
		const double t198 = t170 * lmd[3];
		const double t200 = t5 * t5;
		const double t202 = t56 * t67;
		const double t205 = t170 * lmd[4];
		const double t206 = t52 * t205;
		const double t208 = t9 * t99;
		const double t212 = t161 * t183;
		const double t228 = t7 * t77 * t13;
		const double t232 = t170 * t28;
		const double t241 = t49 * t198;
		const double t244 = t7 * t7;
		const double t259 = t202 * ey * Tmax;
		const double t263 = t202 * ex * Tmax;
		const double t265 = hx * t3;
		const double t277 = hy * t3;
		const double t287 = -t7 * ex + t5 * ey;
		const double t299 = t287 * t77 * t13;
		const double t303 = t5 * hx + t7 * hy;
		dx[0] = 0.2e1 * t62 * t10 * P * t3;
		dx[1] = t72 * Tmax * t26 * t3 - t78 * t75 * t74 + t69;
		dx[2] = t72 * Tmax * t30 * t3 + t78 * t87 * t74 - t83;
		dx[3] = t78 * t5 * t47 * t74 / 0.2e1;
		dx[4] = t78 * t7 * t47 * t74 / 0.2e1;
		dx[5] = t97 * t101 + t104;
		dx[6] = -t1 * t61 * t13 * t21 * t106 * lmd[0] - 0.2e1 * t62 * t10 * t20 - (t118 * Tmax * t7 * t106 + t123 * Tmax * t26 * t106 - t128 * t75 * t125) * lmd[1] / 0.2e1 - (-t118 * Tmax * t5 * t106 + t123 * Tmax * t30 * t106 + t128 * t87 * t125) * lmd[2] / 0.2e1 - t128 * t5 * t142 * t44 / 0.2e1 - t128 * t7 * t142 * t51 / 0.2e1 - (-mu / t153 / P * t100 / t99 / 0.2e1 - 0.1e1 / t153 * t101 + t1 * t56 * t67 * t161 * t125 / 0.2e1) * lmd[5];
		dx[7] = 0.2e1 * t5 * t61 * t13 * t172 - (t72 * Tmax * (-t5 * t177 + t10) * t3 + t186 * t184) * lmd[1] - (-t62 * t5 * t170 * t28 * t3 - t186 * t194 + t104) * lmd[2] + t202 * Tmax * t200 * t48 * t198 + t186 * t206 - (-t5 * t56 * t67 * t212 + 0.2e1 * t5 * t97 * t208) * lmd[5];
		dx[8] = 0.2e1 * t7 * t61 * t13 * t172 - (-t62 * t7 * t170 * t24 * t3 + t228 * t184 - t104) * lmd[1] - (t72 * Tmax * (-t7 * t232 + t10) * t3 - t228 * t194) * lmd[2] + t228 * t241 + t202 * Tmax * t244 * t48 * t205 - (-t7 * t56 * t67 * t212 + 0.2e1 * t7 * t97 * t208) * lmd[5];
		dx[9] = -t186 * t265 * t44 - t228 * t265 * t51 + t259 * t65 * t34 - t263 * t65 * t41 - t78 * t65 * t54;
		dx[10] = -t186 * t277 * t44 - t228 * t277 * t51 - t259 * t81 * t34 + t263 * t81 * t41 + t78 * t81 * t54;
		dx[11] = 0.2e1 * t287 * t61 * t13 * t172 - (t83 + t72 * Tmax * (-t7 * t10 - t287 * t177 - t7) * t3 + t299 * t184 - t78 * ey * t303 * t74) * lmd[1] - (t69 + t72 * Tmax * (t5 * t10 - t287 * t232 + t5) * t3 - t299 * t194 + t78 * ex * t303 * t74) * lmd[2] + t299 * t241 + t228 * t48 * t44 + t299 * t206 - t186 * t48 * t51 - (-t287 * t56 * t67 * t212 + 0.2e1 * t287 * t97 * t208 + t78 * t303 * t74) * lmd[5];
		return 1;
	}
	//Ham fun
	//hpara:m0, Tmax, Ispg0, mu
	double TOPTHamFun(double t, const double *x, double lmd0, const double *hpara) {
		const double m0 = hpara[0], Tmax = hpara[1], Ispg0 = hpara[2], mu = hpara[3];
		double m = m0 - t*Tmax / Ispg0;
		const double P = x[0], ex = x[1], ey = x[2], hx = x[3], hy = x[4], L = x[5];
		const double *lmd = x + 6, lmdm = x[11];
		const double t3 = sqrt(P / mu);
		const double t5 = cos(L);
		const double t7 = sin(L);
		const double t9 = t5 * ex + t7 * ey + 0.1e1;
		const double t10 = 0.1e1 / t9;
		const double t12 = t10 * P * t3 * lmd[0];
		const double t13 = 0.1e1 / m;
		const double t14 = t13 * Tmax;
		const double t15 = t3 * lmd[1];
		const double t17 = t3 * lmd[2];
		const double t19 = -t7 * t15 + t5 * t17;
		const double t20 = t19 * t19;
		const double t24 = t5 + t10 * (ex + t5);
		const double t28 = t7 + t10 * (ey + t7);
		const double t30 = -t24 * t15 - t28 * t17 - 0.2e1 * t12;
		const double t31 = t30 * t30;
		const double t35 = t7 * hx - t5 * hy;
		const double t36 = t35 * t3;
		const double t42 = t10 * lmd[3];
		const double t43 = hx * hx;
		const double t44 = hy * hy;
		const double t46 = (t43 + t44 + 0.1e1) * t3 / 0.2e1;
		const double t49 = t10 * lmd[4];
		const double t54 = -ex * t36 * t10 * lmd[2] + ey * t36 * t10 * lmd[1] - t36 * t10 * lmd[5] - t5 * t46 * t42 - t7 * t46 * t49;
		const double t55 = t54 * t54;
		const double t57 = sqrt(t20 + t31 + t55);
		const double t58 = 0.1e1 / t57;
		const double t65 = t58 * t13;
		const double t66 = t19 * t65;
		const double t70 = t30 * t65;
		const double t72 = t3 * t10;
		const double t76 = t54 * t58 * t14;
		const double t93 = t65 * t54;
		const double t100 = 0.1e1 / P;
		const double t102 = sqrt(t100 * mu);
		const double t103 = t9 * t9;
		const double t110 = 0.2e1 * t30 * t58 * t14 * t12 + (t70 * Tmax * t24 * t3 + t66 * Tmax * t7 * t3 - t76 * ey * t35 * t72) * lmd[1] + (t70 * Tmax * t28 * t3 - t66 * Tmax * t5 * t3 + t76 * ex * t35 * t72) * lmd[2] + t93 * Tmax * t5 * t46 * t42 + t93 * Tmax * t7 * t46 * t49 + (t100 * t103 * t102 + t76 * t35 * t72) * lmd[5] + lmd0;
		return t110;
	}

	//Dynamic function for fuel optimal solver with logarithmic homotopy
	//para should contain:lmd0, eps, T, Isp, mu
	int FOPTDynFun(double t, const double* x, double* dx, const double* hpara) {
		const double lmd0 = hpara[0], epsilon = hpara[1], Tmax = hpara[2], Ispg0 = hpara[3], mu = hpara[4];
		const double P = x[0], ex = x[1], ey = x[2], hx = x[3], hy = x[4], L = x[5], m = x[6];
		const double *lmd = x + 7, lmdm = x[13];
		const double t1 = 0.1e1 / mu;
		const double t3 = sqrt(t1 * P);
		const double t5 = cos(L);
		const double t7 = sin(L);
		const double t9 = t5 * ex + t7 * ey + 0.1e1;
		const double t10 = 0.1e1 / t9;
		const double t11 = Tmax * t10;
		const double t13 = 0.1e1 / m;
		const double t14 = epsilon * t13;
		const double t15 = t3 * lmd[1];
		const double t17 = t3 * lmd[2];
		const double t19 = -t7 * t15 + t5 * t17;
		const double t20 = t19 * t19;
		const double t21 = t3 * lmd[0];
		const double t22 = t10 * P;
		const double t25 = ex + t5;
		const double t27 = t10 * t25 + t5;
		const double t29 = ey + t7;
		const double t31 = t10 * t29 + t7;
		const double t33 = -t27 * t15 - t31 * t17 - 0.2e1 * t22 * t21;
		const double t34 = t33 * t33;
		const double t35 = t10 * lmd[1];
		const double t38 = t7 * hx - t5 * hy;
		const double t39 = t38 * t3;
		const double t42 = t10 * lmd[2];
		const double t45 = t10 * lmd[3];
		const double t46 = hx * hx;
		const double t47 = hy * hy;
		const double t48 = t46 + t47 + 0.1e1;
		const double t49 = t48 * t3 / 0.2e1;
		const double t50 = t5 * t49;
		const double t52 = t10 * lmd[4];
		const double t53 = t7 * t49;
		const double t55 = t10 * lmd[5];
		const double t57 = -ex * t39 * t42 + ey * t39 * t35 - t39 * t55 - t50 * t45 - t53 * t52;
		const double t58 = t57 * t57;
		const double t60 = sqrt(t20 + t34 + t58);
		const double t62 = 0.1e1 / lmd0;
		const double t64 = t62 * t13 * t60 * Ispg0;
		const double t65 = t62 * lmdm;
		const double t68 = pow(0.1e1 - t64 - t65, 0.2e1);
		const double t69 = epsilon * epsilon;
		const double t72 = sqrt(t68 + 0.4e1 * t69);
		const double t74 = 0.1e1 / (0.1e1 - t64 - t65 + 0.2e1 * epsilon + t72);
		const double t75 = 0.1e1 / t60;
		const double t76 = t75 * t74;
		const double t78 = t33 * t76 * t14;
		const double t81 = t7 * t3;
		const double t82 = t13 * Tmax;
		const double t84 = t74 * epsilon;
		const double t85 = t19 * t75;
		const double t86 = t85 * t84;
		const double t87 = t86 * t82 * t81;
		const double t90 = t33 * t75;
		const double t91 = t90 * t84;
		const double t93 = t3 * t10;
		const double t95 = Tmax * ey * t38;
		const double t97 = t57 * t76;
		const double t98 = t97 * t14;
		const double t101 = t5 * t3;
		const double t103 = t86 * t82 * t101;
		const double t108 = Tmax * ex * t38;
		const double t113 = Tmax * t5 * t48 / 0.2e1;
		const double t118 = Tmax * t7 * t48 / 0.2e1;
		const double t122 = 0.1e1 / P;
		const double t124 = sqrt(t122 * mu);
		const double t125 = t9 * t9;
		const double t126 = t125 * t124;
		const double t128 = Tmax * t38;
		const double t130 = t98 * t128 * t93;
		const double t137 = 0.1e1 / t3;
		const double t141 = t74 * t14;
		const double t142 = t1 * t90;
		const double t152 = t1 * t85 * t84;
		const double t156 = t142 * t84;
		const double t158 = t137 * t10;
		const double t160 = t57 * t75;
		const double t161 = t1 * t160;
		const double t162 = t161 * t141;
		const double t184 = P * P;
		const double t192 = t13 * t128;
		const double t199 = 0.1e1 / t125;
		const double t202 = Tmax * t199 * P * t21;
		const double t207 = t199 * t25;
		const double t213 = t3 * t199;
		const double t214 = t95 * t213;
		const double t215 = t5 * t160;
		const double t216 = t215 * t141;
		const double t225 = t108 * t213;
		const double t229 = t199 * lmd[3];
		const double t230 = t5 * t5;
		const double t234 = t97 * epsilon * t82;
		const double t237 = t199 * lmd[4];
		const double t239 = t118 * t3 * t237;
		const double t242 = t9 * t124;
		const double t245 = t192 * t213;
		const double t260 = t7 * t160;
		const double t261 = t260 * t141;
		const double t265 = t199 * t29;
		const double t275 = t113 * t3 * t229;
		const double t278 = t7 * t7;
		const double t296 = hx * t3;
		const double t313 = hy * t3;
		const double t326 = -t7 * ex + t5 * ey;
		const double t337 = t326 * t160;
		const double t338 = t337 * t141;
		const double t342 = t5 * hx + t7 * hy;
		const double t383 = m * m;
		dx[0] = 0.4e1 * t78 * t11 * P * t3;
		dx[1] = 0.2e1 * t91 * t82 * t27 * t3 - 0.2e1 * t98 * t95 * t93 + 0.2e1 * t87;
		dx[2] = 0.2e1 * t91 * t82 * t31 * t3 + 0.2e1 * t98 * t108 * t93 - 0.2e1 * t103;
		dx[3] = 0.2e1 * t98 * t113 * t93;
		dx[4] = 0.2e1 * t98 * t118 * t93;
		dx[5] = t122 * t126 + 0.2e1 * t130;
		dx[6] = -0.2e1 * Tmax / Ispg0 * t84;
		dx[7] = -0.2e1 * t142 * t141 * Tmax * t22 * t137 * lmd[0] - 0.4e1 * t78 * t11 * t21 - (t152 * t82 * t7 * t137 + t156 * t82 * t27 * t137 - t162 * t95 * t158) * lmd[1] - (-t152 * t82 * t5 * t137 + t156 * t82 * t31 * t137 + t162 * t108 * t158) * lmd[2] - t162 * t113 * t137 * t45 - t162 * t118 * t137 * t52 - (-mu / t184 / P * t125 / t124 / 0.2e1 - 0.1e1 / t184 * t126 + t161 * t84 * t192 * t158) * lmd[5];
		dx[8] = 0.4e1 * t5 * t90 * t141 * t202 - 0.2e1 * (t91 * t82 * (-t5 * t207 + t10) * t3 + t216 * t214) * lmd[1] - 0.2e1 * (-t78 * Tmax * t5 * t199 * t29 * t3 - t216 * t225 + t130) * lmd[2] + 0.2e1 * t234 * t230 * t49 * t229 + 0.2e1 * t216 * t239 - 0.2e1 * (t5 * t122 * t242 - t215 * t84 * t245) * lmd[5];
		dx[9] = 0.4e1 * t7 * t90 * t141 * t202 - 0.2e1 * (-t78 * Tmax * t7 * t199 * t25 * t3 + t261 * t214 - t130) * lmd[1] - 0.2e1 * (t91 * t82 * (-t7 * t265 + t10) * t3 - t261 * t225) * lmd[2] + 0.2e1 * t261 * t275 + 0.2e1 * t234 * t278 * t49 * t237 - 0.2e1 * (t7 * t122 * t242 - t260 * t84 * t245) * lmd[5];
		dx[10] = -0.2e1 * t98 * Tmax * t81 * t55 - 0.2e1 * t234 * ex * t81 * t42 + 0.2e1 * t234 * ey * t81 * t35 - 0.2e1 * t234 * t5 * t296 * t45 - 0.2e1 * t234 * t7 * t296 * t52;
		dx[11] = 0.2e1 * t98 * Tmax * t101 * t55 + 0.2e1 * t234 * ex * t101 * t42 - 0.2e1 * t234 * ey * t101 * t35 - 0.2e1 * t234 * t5 * t313 * t45 - 0.2e1 * t234 * t7 * t313 * t52;
		dx[12] = 0.4e1 * t326 * t90 * t141 * t202 - 0.2e1 * (t103 + t91 * t82 * (-t7 * t10 - t326 * t207 - t7) * t3 + t338 * t214 - t98 * Tmax * ey * t342 * t93) * lmd[1] - 0.2e1 * (t87 + t91 * t82 * (t5 * t10 - t326 * t265 + t5) * t3 - t338 * t225 + t98 * Tmax * ex * t342 * t93) * lmd[2] + 0.2e1 * t338 * t275 + 0.2e1 * t234 * t53 * t45 + 0.2e1 * t338 * t239 - 0.2e1 * t234 * t50 * t52 - 0.2e1 * (t98 * Tmax * t342 * t93 + t326 * t122 * t242 - t337 * t84 * t245) * lmd[5];
		dx[13] = -0.2e1 / t383 * t60 * Tmax * t84;
		return 1;
	}

	//H for fuel optimal with logarithmic homotopy
	//para contains: lmd0, eps, T, Isp, mu
	double FOPTHamFun(double t, const double *x, const double *hpara) {
		const double lmd0 = hpara[0], epsilon = hpara[1], T = hpara[2], IG = hpara[3], mu = hpara[4];
		const double P = x[0], ex = x[1], ey = x[2], hx = x[3], hy = x[4], L = x[5], m = x[6];
		const double *lmd = x + 7, lmdm = x[13];
		const double t3 = sqrt(P / mu);
		const double t4 = t3 * lmd[0];
		const double t5 = cos(L);
		const double t7 = sin(L);
		const double t9 = t5 * ex + t7 * ey + 0.1e1;
		const double t10 = 0.1e1 / t9;
		const double t11 = t10 * P;
		const double t14 = 0.1e1 / m;
		const double t15 = epsilon * t14;
		const double t16 = t3 * lmd[1];
		const double t18 = t3 * lmd[2];
		const double t20 = -t7 * t16 + t5 * t18;
		const double t21 = t20 * t20;
		const double t26 = t5 + t10 * (ex + t5);
		const double t30 = t7 + t10 * (ey + t7);
		const double t32 = -0.2e1 * t11 * t4 - t26 * t16 - t30 * t18;
		const double t33 = t32 * t32;
		const double t37 = t7 * hx - t5 * hy;
		const double t38 = t37 * t3;
		const double t45 = hx * hx;
		const double t46 = hy * hy;
		const double t48 = (t45 + t46 + 0.1e1) * t3 / 0.2e1;
		const double t50 = t5 * t48 * t10 * lmd[3];
		const double t53 = t7 * t48 * t10 * lmd[4];
		const double t56 = -ex * t38 * t10 * lmd[2] + ey * t38 * t10 * lmd[1] - t38 * t10 * lmd[5] - t50 - t53;
		const double t57 = t56 * t56;
		const double t59 = sqrt(t21 + t33 + t57);
		const double t61 = 0.1e1 / lmd0;
		const double t63 = t14 * t61 * t59 * IG;
		const double t64 = t61 * lmdm;
		const double t67 = pow(0.1e1 - t63 - t64, 0.2e1);
		const double t68 = epsilon * epsilon;
		const double t71 = sqrt(t67 + 0.4e1 * t68);
		const double t73 = 0.1e1 / (0.1e1 - t63 - t64 + 0.2e1 * epsilon + t71);
		const double t74 = 0.1e1 / t59;
		const double t75 = t74 * t73;
		const double t81 = t14 * T;
		const double t83 = t73 * epsilon;
		const double t85 = t20 * t74 * t83;
		const double t90 = t32 * t74 * t83;
		const double t92 = t3 * t10;
		const double t96 = t56 * t75;
		const double t97 = t96 * t15;
		const double t114 = t96 * epsilon * t81;
		const double t119 = 0.1e1 / P;
		const double t121 = sqrt(t119 * mu);
		const double t122 = t9 * t9;
		const double t133 = 0.1e1 / IG;
		const double t138 = 0.2e1 * t83;
		const double t142 = log(0.2e1 * (0.1e1 - t138) * t83);
		const double t147 = 0.4e1 * t32 * t75 * t15 * T * t11 * t4 + 0.2e1 * (-t97 * T * ey * t37 * t92 + t90 * t81 * t26 * t3 + t85 * t81 * t7 * t3) * lmd[1] + 0.2e1 * (t97 * T * ex * t37 * t92 + t90 * t81 * t30 * t3 - t85 * t81 * t5 * t3) * lmd[2] + 0.2e1 * t114 * t50 + 0.2e1 * t114 * t53 + (0.2e1 * t97 * T * t37 * t92 + t119 * t122 * t121) * lmd[5] - 0.2e1 * t133 * T * t73 * lmdm * epsilon + (-t142 * epsilon + t138) * t133 * lmd0 * T;
		return t147;
	}

	//H for fuel optimal but minus lmd*Ldot
	//para contains: lmd0, eps, T, Isp, mu
	double FOPTHamFunMOne(double t, const double *x, const double *hpara) {
		const double lmd0 = hpara[0], epsilon = hpara[1], T = hpara[2], IG = hpara[3], mu = hpara[4];
		const double P = x[0], ex = x[1], ey = x[2], hx = x[3], hy = x[4], L = x[5], m = x[6];
		const double *lmd = x + 7, lmdm = x[13];
		const double t3 = sqrt(P / mu);
		const double t4 = t3 * lmd[0];
		const double t5 = cos(L);
		const double t7 = sin(L);
		const double t10 = 0.1e1 / (t5 * ex + t7 * ey + 0.1e1);
		const double t11 = t10 * P;
		const double t14 = 0.1e1 / m;
		const double t15 = epsilon * t14;
		const double t16 = t3 * lmd[1];
		const double t18 = t3 * lmd[2];
		const double t20 = -t7 * t16 + t5 * t18;
		const double t21 = t20 * t20;
		const double t26 = t5 + t10 * (ex + t5);
		const double t30 = t7 + t10 * (ey + t7);
		const double t32 = -0.2e1 * t11 * t4 - t26 * t16 - t30 * t18;
		const double t33 = t32 * t32;
		const double t37 = t7 * hx - t5 * hy;
		const double t38 = t37 * t3;
		const double t45 = hx * hx;
		const double t46 = hy * hy;
		const double t48 = (t45 + t46 + 0.1e1) * t3 / 0.2e1;
		const double t50 = t5 * t48 * t10 * lmd[3];
		const double t53 = t7 * t48 * t10 * lmd[4];
		const double t54 = t10 * lmd[5];
		const double t56 = -ex * t38 * t10 * lmd[2] + ey * t38 * t10 * lmd[1] - t38 * t54 - t50 - t53;
		const double t57 = t56 * t56;
		const double t59 = sqrt(t21 + t33 + t57);
		const double t61 = 0.1e1 / lmd0;
		const double t63 = t14 * t61 * t59 * IG;
		const double t64 = t61 * lmdm;
		const double t67 = pow(0.1e1 - t63 - t64, 0.2e1);
		const double t68 = epsilon * epsilon;
		const double t71 = sqrt(t67 + 0.4e1 * t68);
		const double t73 = 0.1e1 / (0.1e1 - t63 - t64 + 0.2e1 * epsilon + t71);
		const double t74 = 0.1e1 / t59;
		const double t75 = t74 * t73;
		const double t81 = t14 * T;
		const double t83 = t73 * epsilon;
		const double t85 = t20 * t74 * t83;
		const double t90 = t32 * t74 * t83;
		const double t92 = t3 * t10;
		const double t96 = t56 * t75;
		const double t97 = t96 * t15;
		const double t114 = t96 * epsilon * t81;
		const double t125 = 0.1e1 / IG;
		const double t130 = 0.2e1 * t83;
		const double t134 = log(0.2e1 * (0.1e1 - t130) * t83);
		const double t139 = 0.4e1 * t32 * t75 * t15 * T * t11 * t4 + 0.2e1 * (-t97 * T * ey * t37 * t92 + t90 * t81 * t26 * t3 + t85 * t81 * t7 * t3) * lmd[1] + 0.2e1 * (t97 * T * ex * t37 * t92 + t90 * t81 * t30 * t3 - t85 * t81 * t5 * t3) * lmd[2] + 0.2e1 * t114 * t50 + 0.2e1 * t114 * t53 + 0.2e1 * t97 * T * t38 * t54 - 0.2e1 * t125 * T * t73 * lmdm * epsilon + (-t134 * epsilon + t130) * t125 * lmd0 * T;
		return t139;
	}

	//Dynamic function for whose rho contains no 1.
	//para contains:lmd0, eps, T, IG, mu
	int FOPTDynFunType2(double t, const double* x, double* dx, const double* hpara) {
		const double lmd0 = hpara[0], epsilon = hpara[1], T = hpara[2], IG = hpara[3], mu = hpara[4];
		const double P = x[0], ex = x[1], ey = x[2], hx = x[3], hy = x[4], L = x[5], m = x[6];
		const double *lmd = x + 7, lmdm = x[13];
		double t1 = 0.1e1 / mu;
		double t3 = sqrt(t1 * P);
		double t5 = cos(L);
		double t7 = sin(L);
		double t9 = t5 * ex + t7 * ey + 0.1e1;
		double t10 = 0.1e1 / t9;
		double t11 = T * t10;
		double t13 = 0.1e1 / m;
		double t14 = epsilon * t13;
		double t15 = t3 * lmd[1];
		double t17 = t3 * lmd[2];
		double t19 = -t7 * t15 + t5 * t17;
		double t20 = t19 * t19;
		double t21 = t3 * lmd[0];
		double t22 = t10 * P;
		double t25 = ex + t5;
		double t27 = t10 * t25 + t5;
		double t29 = ey + t7;
		double t31 = t10 * t29 + t7;
		double t33 = -t27 * t15 - t31 * t17 - 0.2e1 * t22 * t21;
		double t34 = t33 * t33;
		double t35 = t10 * lmd[1];
		double t38 = t7 * hx - t5 * hy;
		double t39 = t38 * t3;
		double t42 = t10 * lmd[2];
		double t45 = t10 * lmd[3];
		double t46 = hx * hx;
		double t47 = hy * hy;
		double t48 = t46 + t47 + 0.1e1;
		double t49 = t48 * t3 / 0.2e1;
		double t50 = t5 * t49;
		double t52 = t10 * lmd[4];
		double t53 = t7 * t49;
		double t55 = t10 * lmd[5];
		double t57 = -ex * t39 * t42 + ey * t39 * t35 - t39 * t55 - t50 * t45 - t53 * t52;
		double t58 = t57 * t57;
		double t60 = sqrt(t20 + t34 + t58);
		double t62 = 0.1e1 / lmd0;
		double t64 = t13 * t62 * t60 * IG;
		double t65 = t62 * lmdm;
		double t68 = pow(-t64 - t65, 0.2e1);
		double t69 = epsilon * epsilon;
		double t72 = sqrt(t68 + 0.4e1 * t69);
		double t74 = 0.1e1 / (-t64 - t65 + 0.2e1 * epsilon + t72);
		double t75 = 0.1e1 / t60;
		double t76 = t75 * t74;
		double t78 = t33 * t76 * t14;
		double t81 = t7 * t3;
		double t82 = t13 * T;
		double t84 = t74 * epsilon;
		double t85 = t19 * t75;
		double t86 = t85 * t84;
		double t87 = t86 * t82 * t81;
		double t90 = t33 * t75;
		double t91 = t90 * t84;
		double t93 = t3 * t10;
		double t95 = T * ey * t38;
		double t97 = t57 * t76;
		double t98 = t97 * t14;
		double t101 = t5 * t3;
		double t103 = t86 * t82 * t101;
		double t108 = T * ex * t38;
		double t113 = T * t5 * t48 / 0.2e1;
		double t118 = T * t7 * t48 / 0.2e1;
		double t122 = 0.1e1 / P;
		double t124 = sqrt(t122 * mu);
		double t125 = t9 * t9;
		double t126 = t125 * t124;
		double t128 = T * t38;
		double t130 = t98 * t128 * t93;
		double t137 = 0.1e1 / t3;
		double t141 = t74 * t14;
		double t142 = t1 * t90;
		double t152 = t1 * t85 * t84;
		double t156 = t142 * t84;
		double t158 = t137 * t10;
		double t160 = t57 * t75;
		double t161 = t1 * t160;
		double t162 = t161 * t141;
		double t184 = P * P;
		double t192 = t13 * t128;
		double t199 = 0.1e1 / t125;
		double t202 = T * t199 * P * t21;
		double t207 = t199 * t25;
		double t213 = t3 * t199;
		double t214 = t95 * t213;
		double t215 = t5 * t160;
		double t216 = t215 * t141;
		double t225 = t108 * t213;
		double t229 = t199 * lmd[3];
		double t230 = t5 * t5;
		double t234 = t97 * epsilon * t82;
		double t237 = t199 * lmd[4];
		double t239 = t118 * t3 * t237;
		double t242 = t9 * t124;
		double t245 = t192 * t213;
		double t260 = t7 * t160;
		double t261 = t260 * t141;
		double t265 = t199 * t29;
		double t275 = t113 * t3 * t229;
		double t278 = t7 * t7;
		double t296 = hx * t3;
		double t313 = hy * t3;
		double t326 = -t7 * ex + t5 * ey;
		double t337 = t326 * t160;
		double t338 = t337 * t141;
		double t342 = t5 * hx + t7 * hy;
		double t383 = m * m;
		dx[0] = 0.4e1 * t78 * t11 * P * t3;
		dx[1] = 0.2e1 * t91 * t82 * t27 * t3 - 0.2e1 * t98 * t95 * t93 + 0.2e1 * t87;
		dx[2] = 0.2e1 * t91 * t82 * t31 * t3 + 0.2e1 * t98 * t108 * t93 - 0.2e1 * t103;
		dx[3] = 0.2e1 * t98 * t113 * t93;
		dx[4] = 0.2e1 * t98 * t118 * t93;
		dx[5] = t122 * t126 + 0.2e1 * t130;
		dx[6] = -0.2e1 * T / IG * t84;
		dx[7] = -0.2e1 * t142 * t141 * T * t22 * t137 * lmd[0] - 0.4e1 * t78 * t11 * t21 - (t152 * t82 * t7 * t137 + t156 * t82 * t27 * t137 - t162 * t95 * t158) * lmd[1] - (-t152 * t82 * t5 * t137 + t156 * t82 * t31 * t137 + t162 * t108 * t158) * lmd[2] - t162 * t113 * t137 * t45 - t162 * t118 * t137 * t52 - (-mu / t184 / P * t125 / t124 / 0.2e1 - 0.1e1 / t184 * t126 + t161 * t84 * t192 * t158) * lmd[5];
		dx[8] = 0.4e1 * t5 * t90 * t141 * t202 - 0.2e1 * (t91 * t82 * (-t5 * t207 + t10) * t3 + t216 * t214) * lmd[1] - 0.2e1 * (-t78 * T * t5 * t199 * t29 * t3 - t216 * t225 + t130) * lmd[2] + 0.2e1 * t234 * t230 * t49 * t229 + 0.2e1 * t216 * t239 - 0.2e1 * (t5 * t122 * t242 - t215 * t84 * t245) * lmd[5];
		dx[9] = 0.4e1 * t7 * t90 * t141 * t202 - 0.2e1 * (-t78 * T * t7 * t199 * t25 * t3 + t261 * t214 - t130) * lmd[1] - 0.2e1 * (t91 * t82 * (-t7 * t265 + t10) * t3 - t261 * t225) * lmd[2] + 0.2e1 * t261 * t275 + 0.2e1 * t234 * t278 * t49 * t237 - 0.2e1 * (t7 * t122 * t242 - t260 * t84 * t245) * lmd[5];
		dx[10] = -0.2e1 * t98 * T * t81 * t55 - 0.2e1 * t234 * ex * t81 * t42 + 0.2e1 * t234 * ey * t81 * t35 - 0.2e1 * t234 * t5 * t296 * t45 - 0.2e1 * t234 * t7 * t296 * t52;
		dx[11] = 0.2e1 * t98 * T * t101 * t55 + 0.2e1 * t234 * ex * t101 * t42 - 0.2e1 * t234 * ey * t101 * t35 - 0.2e1 * t234 * t5 * t313 * t45 - 0.2e1 * t234 * t7 * t313 * t52;
		dx[12] = 0.4e1 * t326 * t90 * t141 * t202 - 0.2e1 * (t103 + t91 * t82 * (-t7 * t10 - t326 * t207 - t7) * t3 + t338 * t214 - t98 * T * ey * t342 * t93) * lmd[1] - 0.2e1 * (t87 + t91 * t82 * (t5 * t10 - t326 * t265 + t5) * t3 - t338 * t225 + t98 * T * ex * t342 * t93) * lmd[2] + 0.2e1 * t338 * t275 + 0.2e1 * t234 * t53 * t45 + 0.2e1 * t338 * t239 - 0.2e1 * t234 * t50 * t52 - 0.2e1 * (t98 * T * t342 * t93 + t326 * t122 * t242 - t337 * t84 * t245) * lmd[5];
		dx[13] = -0.2e1 / t383 * t60 * T * t84;
		return 1;
	}

	//H for other type
	double FOPTHamFunType2(double t, const double *x, const double *hpara) {
		const double lmd0 = hpara[0], epsilon = hpara[1], T = hpara[2], IG = hpara[3], mu = hpara[4];
		const double P = x[0], ex = x[1], ey = x[2], hx = x[3], hy = x[4], L = x[5], m = x[6];
		const double *lmd = x + 7, lmdm = x[13];
		double t3 = sqrt(P / mu);
		double t4 = t3 * lmd[0];
		double t5 = cos(L);
		double t7 = sin(L);
		double t9 = t5 * ex + t7 * ey + 0.1e1;
		double t10 = 0.1e1 / t9;
		double t11 = t10 * P;
		double t14 = 0.1e1 / m;
		double t15 = epsilon * t14;
		double t16 = t3 * lmd[1];
		double t18 = t3 * lmd[2];
		double t20 = -t7 * t16 + t5 * t18;
		double t21 = t20 * t20;
		double t26 = t5 + t10 * (ex + t5);
		double t30 = t7 + t10 * (ey + t7);
		double t32 = -0.2e1 * t11 * t4 - t26 * t16 - t30 * t18;
		double t33 = t32 * t32;
		double t37 = t7 * hx - t5 * hy;
		double t38 = t37 * t3;
		double t45 = hx * hx;
		double t46 = hy * hy;
		double t48 = (t45 + t46 + 0.1e1) * t3 / 0.2e1;
		double t50 = t5 * t48 * t10 * lmd[3];
		double t53 = t7 * t48 * t10 * lmd[4];
		double t56 = -ex * t38 * t10 * lmd[2] + ey * t38 * t10 * lmd[1] - t38 * t10 * lmd[5] - t50 - t53;
		double t57 = t56 * t56;
		double t59 = sqrt(t21 + t33 + t57);
		double t61 = 0.1e1 / lmd0;
		double t63 = t14 * t61 * t59 * IG;
		double t64 = t61 * lmdm;
		double t67 = pow(-t63 - t64, 0.2e1);
		double t68 = epsilon * epsilon;
		double t71 = sqrt(t67 + 0.4e1 * t68);
		double t73 = 0.1e1 / (-t63 - t64 + 0.2e1 * epsilon + t71);
		double t74 = 0.1e1 / t59;
		double t75 = t74 * t73;
		double t81 = t14 * T;
		double t83 = t73 * epsilon;
		double t85 = t20 * t74 * t83;
		double t90 = t32 * t74 * t83;
		double t92 = t3 * t10;
		double t96 = t56 * t75;
		double t97 = t96 * t15;
		double t114 = t96 * epsilon * t81;
		double t119 = 0.1e1 / P;
		double t121 = sqrt(t119 * mu);
		double t122 = t9 * t9;
		double t133 = 0.1e1 / IG;
		double t143 = log(0.2e1 * (0.1e1 - 0.2e1 * t83) * t83);
		double t146 = 0.4e1 * t32 * t75 * t15 * T * t11 * t4 + 0.2e1 * (-t97 * T * ey * t37 * t92 + t90 * t81 * t26 * t3 + t85 * t81 * t7 * t3) * lmd[1] + 0.2e1 * (t97 * T * ex * t37 * t92 + t90 * t81 * t30 * t3 - t85 * t81 * t5 * t3) * lmd[2] + 0.2e1 * t114 * t50 + 0.2e1 * t114 * t53 + (0.2e1 * t97 * T * t37 * t92 + t119 * t122 * t121) * lmd[5] - 0.2e1 * t133 * T * t73 * lmdm * epsilon - t143 * epsilon * t133 * lmd0 * T;
		return t146;

	}
	/**Bang control law**/
	//para contains: lmd0, (eps), T, Isp, mu
	//Thrust on, int
	int ThrustDyn(double t, const double *x, double *dx, const double *para) {
		const double Tmax = para[2], Ispg0 = para[3], mu = para[4];
		const double P = x[0], ex = x[1], ey = x[2], hx = x[3], hy = x[4], L = x[5], m = x[6];
		const double *lmd = x + 7, lmdm = x[13];
		double t1 = 0.1e1 / mu;
		double t3 = sqrt(P * t1);
		double t5 = cos(L);
		double t7 = sin(L);
		double t9 = t5 * ex + t7 * ey + 0.1e1;
		double t10 = 0.1e1 / t9;
		double t12 = 0.1e1 / m;
		double t13 = t12 * Tmax;
		double t14 = t3 * lmd[1];
		double t16 = t3 * lmd[2];
		double t18 = -t7 * t14 + t5 * t16;
		double t19 = t18 * t18;
		double t20 = t3 * lmd[0];
		double t21 = t10 * P;
		double t24 = ex + t5;
		double t26 = t10 * t24 + t5;
		double t28 = ey + t7;
		double t30 = t10 * t28 + t7;
		double t32 = -t26 * t14 - t30 * t16 - 0.2e1 * t21 * t20;
		double t33 = t32 * t32;
		double t34 = t10 * lmd[1];
		double t37 = t7 * hx - t5 * hy;
		double t38 = t37 * t3;
		double t41 = t10 * lmd[2];
		double t44 = t10 * lmd[3];
		double t45 = hx * hx;
		double t46 = hy * hy;
		double t47 = t45 + t46 + 0.1e1;
		double t48 = t47 * t3 / 0.2e1;
		double t49 = t5 * t48;
		double t51 = t10 * lmd[4];
		double t52 = t7 * t48;
		double t54 = t10 * lmd[5];
		double t56 = -ex * t38 * t41 + ey * t38 * t34 - t38 * t54 - t49 * t44 - t52 * t51;
		double t57 = t56 * t56;
		double t59 = sqrt(t19 + t33 + t57);
		double t60 = 0.1e1 / t59;
		double t61 = t32 * t60;
		double t62 = t61 * t13;
		double t65 = t7 * t3;
		double t67 = t60 * t12;
		double t68 = t18 * t67;
		double t69 = t68 * Tmax * t65;
		double t72 = t32 * t67;
		double t74 = t3 * t10;
		double t75 = ey * t37;
		double t77 = t56 * t60;
		double t78 = t77 * t13;
		double t81 = t5 * t3;
		double t83 = t68 * Tmax * t81;
		double t87 = ex * t37;
		double t97 = 0.1e1 / P;
		double t99 = sqrt(t97 * mu);
		double t100 = t9 * t9;
		double t101 = t100 * t99;
		double t104 = t78 * t37 * t74;
		double t108 = 0.1e1 / t3;
		double t120 = t1 * t18 * t67;
		double t125 = t1 * t32 * t67;
		double t127 = t108 * t10;
		double t130 = t1 * t77 * t13;
		double t144 = t47 * t108 / 0.2e1;
		double t155 = P * P;
		double t163 = Tmax * t37;
		double t172 = 0.1e1 / t100;
		double t174 = t172 * P * t20;
		double t179 = t172 * t24;
		double t185 = t3 * t172;
		double t186 = t75 * t185;
		double t188 = t5 * t77 * t13;
		double t196 = t87 * t185;
		double t200 = t172 * lmd[3];
		double t202 = t5 * t5;
		double t204 = t56 * t67;
		double t207 = t172 * lmd[4];
		double t208 = t52 * t207;
		double t210 = t9 * t99;
		double t214 = t163 * t185;
		double t230 = t7 * t77 * t13;
		double t234 = t172 * t28;
		double t243 = t49 * t200;
		double t246 = t7 * t7;
		double t261 = t204 * ey * Tmax;
		double t265 = t204 * ex * Tmax;
		double t267 = hx * t3;
		double t279 = hy * t3;
		double t289 = -t7 * ex + t5 * ey;
		double t301 = t289 * t77 * t13;
		double t305 = t5 * hx + t7 * hy;
		double t341 = m * m;
		dx[0] = 0.2e1 * t62 * t10 * t3 * P;
		dx[1] = t72 * Tmax * t26 * t3 - t78 * t75 * t74 + t69;
		dx[2] = t72 * Tmax * t30 * t3 + t78 * t87 * t74 - t83;
		dx[3] = t78 * t5 * t47 * t74 / 0.2e1;
		dx[4] = t78 * t7 * t47 * t74 / 0.2e1;
		dx[5] = t97 * t101 + t104;
		dx[6] = -Tmax / Ispg0;
		dx[7] = -t1 * t61 * t13 * t21 * t108 * lmd[0] - 0.2e1 * t62 * t10 * t20 - (t120 * Tmax * t7 * t108 + t125 * Tmax * t26 * t108 - t130 * t75 * t127) * lmd[1] / 0.2e1 - (-t120 * Tmax * t5 * t108 + t125 * Tmax * t30 * t108 + t130 * t87 * t127) * lmd[2] / 0.2e1 - t130 * t5 * t144 * t44 / 0.2e1 - t130 * t7 * t144 * t51 / 0.2e1 - (-mu / t155 / P * t100 / t99 / 0.2e1 - 0.1e1 / t155 * t101 + t1 * t56 * t67 * t163 * t127 / 0.2e1) * lmd[5];
		dx[8] = 0.2e1 * t5 * t61 * t13 * t174 - (t72 * Tmax * (-t5 * t179 + t10) * t3 + t188 * t186) * lmd[1] - (-t62 * t5 * t172 * t28 * t3 - t188 * t196 + t104) * lmd[2] + t204 * Tmax * t202 * t48 * t200 + t188 * t208 - (-t5 * t56 * t67 * t214 + 0.2e1 * t5 * t97 * t210) * lmd[5];
		dx[9] = 0.2e1 * t7 * t61 * t13 * t174 - (-t62 * t7 * t172 * t24 * t3 + t230 * t186 - t104) * lmd[1] - (t72 * Tmax * (-t7 * t234 + t10) * t3 - t230 * t196) * lmd[2] + t230 * t243 + t204 * Tmax * t246 * t48 * t207 - (-t7 * t56 * t67 * t214 + 0.2e1 * t7 * t97 * t210) * lmd[5];
		dx[10] = -t188 * t267 * t44 - t230 * t267 * t51 + t261 * t65 * t34 - t265 * t65 * t41 - t78 * t65 * t54;
		dx[11] = -t188 * t279 * t44 - t230 * t279 * t51 - t261 * t81 * t34 + t265 * t81 * t41 + t78 * t81 * t54;
		dx[12] = 0.2e1 * t289 * t61 * t13 * t174 - (t83 + t72 * Tmax * (-t7 * t10 - t289 * t179 - t7) * t3 + t301 * t186 - t78 * ey * t305 * t74) * lmd[1] - (t69 + t72 * Tmax * (t5 * t10 - t289 * t234 + t5) * t3 - t301 * t196 + t78 * ex * t305 * t74) * lmd[2] + t301 * t243 + t230 * t48 * t44 + t301 * t208 - t188 * t48 * t51 - (-t289 * t56 * t67 * t214 + 0.2e1 * t289 * t97 * t210 + t78 * t305 * t74) * lmd[5];
		dx[13] = -0.1e1 / t341 * Tmax * t59;
		return 1;
	}

	//Off control law, which is quite simple
	//para contains:lmd0, (eps), T, Isp, mu
	int CoastDyn(double t, const double *x, double *dx, const double *para) {
		const double mu = para[4];
		const double P = x[0], ex = x[1], ey = x[2], L = x[5];
		const double *lmd = x + 7, lmdm = x[13];
		const double t1 = 0.1e1 / P;
		const double t3 = sqrt(t1 * mu);
		const double t4 = cos(L);
		const double t6 = sin(L);
		const double t8 = t4 * ex + t6 * ey + 0.1e1;
		const double t9 = t8 * t8;
		const double t10 = t9 * t3;
		const double t14 = P * P;
		const double t24 = t3 * lmd[5];
		const double t25 = t1 * t8;
		dx[0] = 0;
		dx[1] = 0;
		dx[2] = 0;
		dx[3] = 0;
		dx[4] = 0;
		dx[5] = (t1 * t10);
		dx[6] = 0;
		dx[7] = -((-mu / t14 / P * t9 / t3 / 0.2e1 - 0.1e1 / t14 * t10) * lmd[5]);
		dx[8] = -(0.2e1 * t4 * t25 * t24);
		dx[9] = -(0.2e1 * t6 * t25 * t24);
		dx[10] = 0;
		dx[11] = 0;
		dx[12] = -(0.2e1 * (-t6 * ex + t4 * ey) * t25 * t24);
		dx[13] = 0;
		return 1;
	}

	//Hamilton with bang-off-bang structure
	//para contains:lmd0, (eps), T, Isp, mu
	double BangHamFun(double t, const double *x, const double *para) {
		const double lmd0 = para[0], Tmax = para[2], Ispg0 = para[3], mu = para[4];
		const double P = x[0], ex = x[1], ey = x[2], hx = x[3], hy = x[4], L = x[5], m = x[6];
		const double *lmd = x + 7, lmdm = x[13];
		const double t3 = sqrt(P / mu);
		const double t5 = cos(L);
		const double t7 = sin(L);
		const double t9 = t5 * ex + t7 * ey + 0.1e1;
		const double t10 = 0.1e1 / t9;
		const double t12 = t10 * P * t3 * lmd[0];
		const double t13 = 0.1e1 / m;
		const double t14 = t13 * Tmax;
		const double t15 = t3 * lmd[1];
		const double t17 = t3 * lmd[2];
		const double t19 = -t7 * t15 + t5 * t17;
		const double t20 = t19 * t19;
		const double t24 = t5 + t10 * (ex + t5);
		const double t28 = t7 + t10 * (ey + t7);
		const double t30 = -t24 * t15 - t28 * t17 - 0.2e1 * t12;
		const double t31 = t30 * t30;
		const double t35 = t7 * hx - t5 * hy;
		const double t36 = t35 * t3;
		const double t43 = hx * hx;
		const double t44 = hy * hy;
		const double t46 = (t43 + t44 + 0.1e1) * t3 / 0.2e1;
		const double t48 = t5 * t46 * t10 * lmd[3];
		const double t51 = t7 * t46 * t10 * lmd[4];
		const double t54 = -ex * t36 * t10 * lmd[2] + ey * t36 * t10 * lmd[1] - t36 * t10 * lmd[5] - t48 - t51;
		const double t55 = t54 * t54;
		const double t57 = sqrt(t20 + t31 + t55);
		const double t59 = 0.1e1 / lmd0;
		const double t65 = (0.0e0 < -t13 * t59 * t57 * Ispg0 - t59 * lmdm + 0.1e1 ? 0 : 1);
		const double t66 = 0.1e1 / t57;
		const double t67 = t66 * t65;
		const double t74 = t65 * t13;
		const double t76 = t19 * t66 * t74;
		const double t81 = t30 * t66 * t74;
		const double t83 = t3 * t10;
		const double t87 = t54 * t67 * t14;
		const double t104 = 0.1e1 / P;
		const double t106 = sqrt(t104 * mu);
		const double t107 = t9 * t9;
		const double t118 = 0.1e1 / Ispg0;
		const double t124 = 0.2e1 * t30 * t67 * t14 * t12 + (t81 * Tmax * t24 * t3 + t76 * Tmax * t7 * t3 - t87 * ey * t35 * t83) * lmd[1] + (t81 * Tmax * t28 * t3 - t76 * Tmax * t5 * t3 + t87 * ex * t35 * t83) * lmd[2] + t87 * t48 + t87 * t51 + (t54 * t66 * t74 * Tmax * t35 * t83 + t104 * t107 * t106) * lmd[5] - t118 * Tmax * t65 * lmdm + t65 * t118 * lmd0 * Tmax;
		return t124;
	}

	//Generate rho, which is not affected by u
	//para contains:lmd0, (eps), T, Isp, mu
	double rho(const double *x, const double *lmd, const double *para) {
		const double lmd0 = para[0], Ispg0 = para[3], mu = para[4];
		const double P = x[0], ex = x[1], ey = x[2], hx = x[3], hy = x[4], L = x[5], m = x[6];
		const double lmdm = lmd[6];
		const double t3 = sqrt(P / mu);
		const double t4 = t3 * lmd[1];
		const double t5 = sin(L);
		const double t7 = t3 * lmd[2];
		const double t8 = cos(L);
		const double t11 = pow(-t5 * t4 + t8 * t7, 0.2e1);
		const double t16 = 0.1e1 / (t8 * ex + t5 * ey + 0.1e1);
		const double t29 = pow(-0.2e1 * t16 * P * t3 * lmd[0] - (t8 + t16 * (ex + t8)) * t4 - (t5 + t16 * (ey + t5)) * t7, 0.2e1);
		const double t34 = (t5 * hx - t8 * hy) * t3;
		const double t41 = hx * hx;
		const double t42 = hy * hy;
		const double t44 = (t41 + t42 + 0.1e1) * t3 / 0.2e1;
		const double t53 = pow(-ex * t34 * t16 * lmd[2] + ey * t34 * t16 * lmd[1] - t5 * t44 * t16 * lmd[4] - t8 * t44 * t16 * lmd[3] - t34 * t16 * lmd[5], 0.2e1);
		const double t55 = sqrt(t11 + t29 + t53);
		const double t57 = 0.1e1 / lmd0;
		const double t62 = 0.1e1 - 0.1e1 / m * t57 * t55 * Ispg0 - t57 * lmdm;
		return t62;
	}

	double drhodt(const double *x, const double *lmd, const double *para) {
		const double lmd0 = para[0], Ispg0 = para[3], mu = para[4];
		const double P = x[0], ex = x[1], ey = x[2], hx = x[3], hy = x[4], L = x[5], m = x[6];
		const double lmdm = lmd[6];
		double t1 = sqrt(P / mu);
		double t2 = sin(L);
		double t3 = cos(L);
		double t4 = t1 * (t2 * lmd[1] - t3 * lmd[2]);
		double t5 = ex * t3 + ey * t2 + 0.1e1;
		double t6 = 0.1e1 / t5;
		double t7 = (ex + t3) * t6;
		double t8 = t7 + t3;
		double t9 = (ey + t2) * t6;
		double t10 = t9 + t2;
		double t11 = -2;
		double t12 = t11 * lmd[0] * t1 * P;
		double t13 = t12 * t6 - t1 * (t10 * lmd[2] + t8 * lmd[1]);
		double t14 = hx * t2 - hy * t3;
		double t15 = lmd[2] * ex;
		double t16 = lmd[1] * ey;
		double t17 = t6 * t14;
		double t18 = lmd[3] * t3;
		double t19 = lmd[4] * t2;
		double t20 = t6 * t1;
		double t21 = t20 * (hx * hx + hy * hy + 0.1e1);
		double t22 = -t21 * (t19 + t18) / 0.2e1 - t17 * t1 * (-t16 + lmd[5] + t15);
		double t23 = pow(t13, 0.2e1) + pow(t22, 0.2e1) + pow(t4, 0.2e1);
		double t24 = ex * t2 - ey * t3;
		t15 = t16 - lmd[5] - t15;
		t16 = 0.1e1 / P;
		double t25 = mu * t16;
		double t26 = sqrt(t25);
		double t27 = pow(t5, 0.2e1);
		t23 = 0.1e1 / lmd0 / m * pow(t23, -0.1e1 / 0.2e1);
		return t11 * t23 * lmd[5] * Ispg0 * (t20 * t13 * P * t27 * pow(t16, 0.2e1) * (-sqrt(t25) / 0.2e1 - t26) + (t5 * (t2 * (t17 * ex * t22 + t10 * t13 + t3 * t4) * t1 - t3 * (t17 * ey * t22 - t13 * t8 + t2 * t4) * t1) - t14 * t1 * t22 * t24) * t16 * t26) - t23 * Ispg0 * (t13 * (t12 * pow(t6, 0.2e1) * t24 - t1 * (lmd[1] * (t6 * (t7 * t24 - t2) - t2) + lmd[2] * (t6 * (t9 * t24 + t3) + t3))) + t22 * (t20 * (t15 * (hx * t3 + hy * t2) + t17 * t15 * t24) - t21 * (-t2 * lmd[3] + t3 * lmd[4] + (t19 + t18) * t6 * t24) / 0.2e1) + t4 * t1 * (t2 * lmd[2] + t3 * lmd[1])) * t26 * t27 * t16;

	}

	void rhodrhodtgen(const double *x, const double *lmd, double *rho, double *drhodt, const double *para) {
		const double lmd0 = para[0], Ispg0 = para[3], mu = para[4];
		const double P = x[0], ex = x[1], ey = x[2], hx = x[3], hy = x[4], L = x[5], m = x[6];
		const double lmdm = lmd[6];
		double t1 = sqrt(P / mu);
		double t2 = sin(L);
		double t3 = cos(L);
		double t4 = t1 * (t2 * lmd[1] - t3 * lmd[2]);
		double t5 = ex * t3 + ey * t2 + 0.1e1;
		double t6 = 0.1e1 / t5;
		double t7 = (ex + t3) * t6;
		double t8 = t7 + t3;
		double t9 = (ey + t2) * t6;
		double t10 = t9 + t2;
		double t11 = -2;
		double t12 = t11 * lmd[0] * t1 * P;
		double t13 = t12 * t6 - t1 * (t10 * lmd[2] + t8 * lmd[1]);
		double t14 = hx * t2 - hy * t3;
		double t15 = lmd[2] * ex;
		double t16 = lmd[1] * ey;
		double t17 = t6 * t14;
		double t18 = lmd[3] * t3;
		double t19 = lmd[4] * t2;
		double t20 = t6 * t1;
		double t21 = t20 * (hx * hx + hy * hy + 0.1e1);
		double t22 = -t21 * (t19 + t18) / 0.2e1 - t17 * t1 * (-t16 + lmd[5] + t15);
		double t23 = pow(t13, 0.2e1) + pow(t22, 0.2e1) + pow(t4, 0.2e1);
		double t24 = pow(t23, -0.1e1 / 0.2e1);
		double t25 = 0.1e1 / m;
		double t26 = 0.1e1 / lmd0;
		double t27 = ex * t2 - ey * t3;
		t15 = t16 - lmd[5] - t15;
		t16 = 0.1e1 / P;
		double t28 = mu * t16;
		double t29 = sqrt(t28);
		double t30 = pow(t5, 0.2e1);
		double t31 = t26 * t25 * t24;
		*rho = -t26 * (Ispg0 * t23 * t24 * t25 + lmdm) + 0.1e1;
		*drhodt = t11 * t31 * lmd[5] * Ispg0 * (t20 * t13 * P * t30 * pow(t16, 0.2e1) * (-sqrt(t28) / 0.2e1 - t29) + (t5 * (t2 * (t17 * ex * t22 + t10 * t13 + t3 * t4) * t1 - t3 * (t17 * ey * t22 - t13 * t8 + t2 * t4) * t1) - t14 * t1 * t22 * t27) * t16 * t29) - t31 * Ispg0 * (t13 * (t12 * pow(t6, 0.2e1) * t27 + t1 * (lmd[1] * (t6 * (-t7 * t27 + t2) + t2) - lmd[2] * (t6 * (t9 * t27 + t3) + t3))) + t22 * (t20 * (t15 * (hx * t3 + hy * t2) + t17 * t15 * t27) - t21 * (-t2 * lmd[3] + t3 * lmd[4] + (t19 + t18) * t6 * t27) / 0.2e1) + t4 * t1 * (t2 * lmd[2] + t3 * lmd[1])) * t29 * t30 * t16;

	}

	int SFun(double t, const double *x, const double *para, double &fval, double &fgrad, int calcflag) {
		const double *lmd = x + 7;
		if (calcflag == 0) {
			fval = basicdynfun::rho(x, lmd, para);
			return 1;
		}
		else if (calcflag == 1) {
			basicdynfun::rhodrhodtgen(x, lmd, &fval, &fgrad, para);
			return 1;
		}
		return 1;
	}
	class ODE45Integrator;
	//An function to help integrate from t0 to tf with accurate switch detection
	int BangInt(double *state, double t0, double tf, const double *para, FILE *fp, TigerIntegrator::ODE45Integrator *FODE45, double IStep, double TimeTol) {
		//Init ode start point
		double tin = t0, tout = tf;
		//Use Rho to judge starting
		bool ThrustLeg = basicdynfun::rho(state, state + 7, para) < 0;
		int eventflag = 0;
		int eventdir = ThrustLeg ? 1 : -1;
		int flag = 0;
		while (1) {
			if (ThrustLeg) {
				flag = FODE45->Integrate(basicdynfun::ThrustDyn, state, para, basicdynfun::SFun, para, eventflag, eventdir, tin, tout, fp, IStep);
				//SFun(tout, xx, para, fval, fgrad, 1);
			}
			else {
				flag = FODE45->Integrate(basicdynfun::CoastDyn, state, para, basicdynfun::SFun, para, eventflag, eventdir, tin, tout, fp, IStep);
				//SFun(tout, xx, para, fval, fgrad, 1);
			}
			if (fp)	fprintf(fp, "\n");
			if (fabs(tout - tf) < TimeTol)		break;
			ThrustLeg = !ThrustLeg;
			eventdir = -eventdir;
			tin = tout;
			tout = tf;
		}
		return 0;
	}

	/*************RV Case***********/
	/*
	Dynamic equations to be integrated to obatain time-optimal control
	para:lmd0, m0, t0, T, IG, mu
	*/
	int RVTOPTDyn(double t, const double* x, double* dx, const double* para) {
		const double *r = x, *v = x + 3, *lmdr = x + 6, *lmdv = x + 9;
		const double lmd0 = para[0], m0 = para[1], t0 = para[2], T = para[3], IG = para[4], mu = para[5];
		const double m = m0 - (t - t0) * T / IG;
		const double t1 = pow(r[0], 0.2e1);
		const double t2 = pow(r[1], 0.2e1);
		const double t3 = pow(r[2], 0.2e1);
		double t4 = t3 + t1 + t2;
		double t5 = pow(t4, -0.5e1 / 0.2e1);
		t4 = t4 * t5;
		double t6 = pow(lmdv[0], 0.2e1) + pow(lmdv[1], 0.2e1) + pow(lmdv[2], 0.2e1);
		const double t7 = mu * t4;
		t6 = T / m * pow(t6, -0.1e1 / 0.2e1);
		const double t8 = 0.3e1 * t5;
		const double t9 = lmdv[2] * r[2];
		const double t10 = lmdv[1] * r[1];
		t5 = mu * t5;
		const double t11 = lmdv[0] * r[0];
		dx[0] = v[0];
		dx[1] = v[1];
		dx[2] = v[2];
		dx[3] = -t6 * lmdv[0] - t7 * r[0];
		dx[4] = -t6 * lmdv[1] - t7 * r[1];
		dx[5] = -t6 * lmdv[2] - t7 * r[2];
		dx[6] = -lmdv[0] * mu * (t8 * t1 - t4) - 0.3e1 * t5 * r[0] * (t9 + t10);
		dx[7] = -lmdv[1] * mu * (t8 * t2 - t4) - 0.3e1 * t5 * r[1] * (t9 + t11);
		dx[8] = -lmdv[2] * mu * (t8 * t3 - t4) - 0.3e1 * t5 * r[2] * (t10 + t11);
		dx[9] = -lmdr[0];
		dx[10] = -lmdr[1];
		dx[11] = -lmdr[2];
		return 1;
	}
	/*
	Function to obtain H
	para:lmd0, m0, t0, T, IG, mu
	*/
	double RVTOPTHam(double t, const double *x, const double *para) {
		const double *r = x, *v = x + 3, *lmdr = x + 6, *lmdv = x + 9;
		const double lmd0 = para[0], m0 = para[1], t0 = para[2], T = para[3], IG = para[4], mu = para[5];
		const double m = m0 - (t - t0) * T / IG;
		const double t4 = pow(r[0], 0.2e1);
		const double t5 = pow(r[1], 0.2e1);
		const double t6 = pow(r[2], 0.2e1);
		const double t7 = t4 + t5 + t6;
		const double t8 = sqrt(t7);
		const double t11 = 0.1e1 / t8 / t7 * mu;
		const double t14 = T / m;
		const double t15 = pow(lmdv[0], 0.2e1);
		const double t16 = pow(lmdv[1], 0.2e1);
		const double t17 = pow(lmdv[2], 0.2e1);
		const double t19 = sqrt(t15 + t16 + t17);
		const double t20 = 0.1e1 / t19;
		return lmdr[0] * v[0] + lmdr[1] * v[1] + lmdr[2] * v[2] + lmd0 + (-lmdv[0] * t20 * t14 - r[0] * t11) * lmdv[0] + (-lmdv[1] * t20 * t14 - r[1] * t11) * lmdv[1] + (-lmdv[2] * t20 * t14 - r[2] * t11) * lmdv[2];
	}
	/*
	Dynamical functions for fuel optimal control
	para: lmd0, eps, T, IG, mu
	*/
	int RVFOPTDyn(double t, const double* x, double* dx, const double* para) {
		const double *r = x, *v = x + 3, m = x[6], *lmdr = x + 7, *lmdv = x + 10, lmdm = x[13];
		const double lmd0 = para[0], epsilon = para[1], T = para[2], IG = para[3], mu = para[4];
		double t1 = pow(r[1], 0.2e1);
		double t2 = pow(r[0], 0.2e1);
		double t3 = pow(r[2], 0.2e1);
		double t4 = t3 + t1 + t2;
		double t5 = pow(t4, -0.5e1 / 0.2e1);
		t4 = t4 * t5;
		double t6 = pow(lmdv[0], 0.2e1) + pow(lmdv[1], 0.2e1) + pow(lmdv[2], 0.2e1);
		double t7 = pow(t6, -0.1e1 / 0.2e1);
		t6 = t6 * t7;
		double t8 = 0.1e1 / m;
		double t9 = (IG * t6 * t8 + lmdm) / lmd0;
		double t10 = 0.1e1 - t9;
		t10 = (4 * epsilon * epsilon) + pow(t10, 0.2e1);
		double t11 = 2 * epsilon;
		t9 = 0.1e1 + sqrt(t10) - t9 + t11;
		t9 = 0.1e1 / t9;
		t9 = t11 * t9 * T;
		t7 = t9 * t8 * t7;
		t10 = mu * t4;
		t11 = (0.3e1 * t5);
		double t12 = lmdv[2] * r[2];
		double t13 = lmdv[1] * r[1];
		t5 = mu * t5;
		double t14 = lmdv[0] * r[0];
		dx[0] = v[0];
		dx[1] = v[1];
		dx[2] = v[2];
		dx[3] = -t10 * r[0] - t7 * lmdv[0];
		dx[4] = -t10 * r[1] - t7 * lmdv[1];
		dx[5] = -t10 * r[2] - t7 * lmdv[2];
		dx[6] = -t9 / IG;
		dx[7] = -lmdv[0] * mu * (t11 * t2 - t4) - 0.3e1 * t5 * r[0] * (t12 + t13);
		dx[8] = -lmdv[1] * mu * (t11 * t1 - t4) - 0.3e1 * t5 * r[1] * (t14 + t12);
		dx[9] = -lmdv[2] * mu * (t11 * t3 - t4) - 0.3e1 * t5 * r[2] * (t14 + t13);
		dx[10] = -lmdr[0];
		dx[11] = -lmdr[1];
		dx[12] = -lmdr[2];
		dx[13] = -t9 * t6 * pow(t8, 0.2e1);
		return 1;
	}
	/*
	Dynamical functions for fuel optimal control
	para: lmd0, eps, T, IG, mu
	*/
	double RVFOPTHam(double t, const double *x, const double *para) {
		const double *r = x, *v = x + 3, m = x[6], *lmdr = x + 7, *lmdv = x + 10, lmdm = x[13];
		const double lmd0 = para[0], epsilon = para[1], T = para[2], IG = para[3], mu = para[4];
		const double t2 = 0.1e1 / IG;
		const double t3 = pow(lmdv[0], 0.2e1);
		const double t4 = pow(lmdv[1], 0.2e1);
		const double t5 = pow(lmdv[2], 0.2e1);
		const double t7 = sqrt(t3 + t4 + t5);
		const double t9 = 0.1e1 / lmd0;
		const double t10 = 0.1e1 / m;
		const double t12 = t10 * t9 * t7 * IG;
		const double t13 = t9 * lmdm;
		const double t16 = pow(0.1e1 - t12 - t13, 0.2e1);
		const double t17 = epsilon * epsilon;
		const double t20 = sqrt(t16 + 0.40e1 * t17);
		const double t22 = 0.1e1 / (0.1e1 - t12 - t13 + 0.20e1 * epsilon + t20);
		const double t23 = t22 * epsilon;
		const double t24 = 0.20e1 * t23;
		const double t28 = log(0.20e1 * (0.1e1 - t24) * t23);
		const double t36 = pow(r[0], 0.2e1);
		const double t37 = pow(r[1], 0.2e1);
		const double t38 = pow(r[2], 0.2e1);
		const double t39 = t36 + t37 + t38;
		const double t40 = sqrt(t39);
		const double t43 = 0.1e1 / t40 / t39 * mu;
		const double t45 = T * t23;
		const double t47 = 0.1e1 / t7 * t10;
		const double t70 = (-t28 * epsilon + t24) * t2 * lmd0 * T + lmdr[0] * v[0] + lmdr[1] * v[1] + lmdr[2] * v[2] + (-r[0] * t43 - 0.20e1 * lmdv[0] * t47 * t45) * lmdv[0] + (-r[1] * t43 - 0.20e1 * lmdv[1] * t47 * t45) * lmdv[1] + (-r[2] * t43 - 0.20e1 * lmdv[2] * t47 * t45) * lmdv[2] - 0.20e1 * t2 * T * t22 * lmdm * epsilon;
		return t70;
	}


	//Generate Transpose of Jabobian Matrix, RV=f(EE), Mat = (\p RV \p EE)^T

	int JBEEtoRVGen(const double *x, double *jb, const double mu) {
		double P = x[0], f = x[1], g = x[2], h = x[3], k = x[4], L = x[5];
		double t1 = h * h;
		double t2 = k * k;
		double t3 = 0.1e1 + t1 + t2;
		double t4 = cos(L);
		double t5 = sin(L);
		double t6 = f * t4 + g * t5 + 0.1e1;
		double t7 = 0.1e1 + t1 - t2;
		double t8 = 0.2e1 * t5;
		double t9 = t8 * h * k;
		double t10 = t4 * t7 + t9;
		t1 = 0.1e1 - t1 + t2;
		t2 = 0.2e1 * t4;
		double t11 = t2 * h * k;
		double t12 = t1 * t5 + t11;
		double t13 = h * t5 - k * t4;
		double t14 = 0.1e1 / P;
		double t15 = mu * t14;
		double t16 = f + t4;
		double t17 = g + t5;
		double t18 = h * t16;
		double t19 = 0.2e1 * t18 * k - t17 * t7;
		double t20 = h * t17;
		double t21 = -0.2e1 * t20 * k + t1 * t16;
		t18 = k * t17 + t18;
		t6 = 0.1e1 / t6;
		t3 = 0.1e1 / t3;
		t14 = pow(t14, 0.2e1);
		double t22 = t3 * pow(t15, -0.1e1 / 0.2e1);
		double t23 = t3 * t6;
		double t24 = -t22 / 0.2e1;
		t15 = sqrt(t15);
		double t25 = 0.2e1 * t3 * t15;
		double t26 = t25 * h;
		double t27 = t26 * k;
		double t28 = P * t3 * pow(t6, 0.2e1);
		double t29 = t28 * t12;
		double t30 = t28 * t10;
		t15 = t3 * t15;
		double t31 = h * t4 + k * t5;
		double t32 = t3 * t10;
		double t33 = t23 * P;
		double t34 = t3 * t12;
		double t35 = -0.4e1 * t3;
		double t36 = t35 * t13;
		t20 = k * t16 - t20;
		double t37 = t3 * t19;
		t3 = t3 * t21;
		t35 = t35 * t18;
		double t38 = f * t5 - g * t4;
		jb[0 * 6 + 0] = t23 * t10;
		jb[0 * 6 + 1] = t23 * t12;
		jb[0 * 6 + 2] = 0.2e1 * t23 * t13;
		jb[0 * 6 + 3] = t24 * t19 * mu * t14;
		jb[0 * 6 + 4] = t24 * t21 * mu * t14;
		jb[0 * 6 + 5] = -t22 * t18 * mu * t14;
		jb[1 * 6 + 0] = -t30 * t4;
		jb[1 * 6 + 1] = -t29 * t4;
		jb[1 * 6 + 2] = -t28 * t2 * t13;
		jb[1 * 6 + 3] = t27;
		jb[1 * 6 + 4] = t15 * t1;
		jb[1 * 6 + 5] = t26;
		jb[2 * 6 + 0] = -t30 * t5;
		jb[2 * 6 + 1] = -t29 * t5;
		jb[2 * 6 + 2] = -t28 * t8 * t13;
		jb[2 * 6 + 3] = -t15 * t7;
		jb[2 * 6 + 4] = -t27;
		jb[2 * 6 + 5] = t25 * k;
		jb[3 * 6 + 0] = 0.2e1 * t33 * (-t32 * h + t31);
		jb[3 * 6 + 1] = -0.2e1 * t33 * (t34 * h + t13);
		jb[3 * 6 + 2] = t33 * (t36 * h + t8);
		jb[3 * 6 + 3] = -0.2e1 * t15 * (t37 * h - t20);
		jb[3 * 6 + 4] = -0.2e1 * t15 * (t3 * h + t18);
		jb[3 * 6 + 5] = t15 * (t35 * h + 0.2e1 * t16);
		jb[4 * 6 + 0] = 0.2e1 * t33 * (-t32 * k + t13);
		jb[4 * 6 + 1] = 0.2e1 * t33 * (-t34 * k + t31);
		jb[4 * 6 + 2] = t33 * (t36 * k - t2);
		jb[4 * 6 + 3] = -0.2e1 * t15 * (t37 * k - t18);
		jb[4 * 6 + 4] = -0.2e1 * t15 * (t3 * k - t20);
		jb[4 * 6 + 5] = t15 * (t35 * k + 0.2e1 * t17);
		jb[5 * 6 + 0] = t33 * (t6 * t10 * t38 - t5 * t7 + t11);
		jb[5 * 6 + 1] = t33 * (t6 * t12 * t38 + t1 * t4 - t9);
		jb[5 * 6 + 2] = 0.2e1 * t33 * (t6 * t13 * t38 + t31);
		jb[5 * 6 + 3] = -t15 * t10;
		jb[5 * 6 + 4] = -t15 * t12;
		jb[5 * 6 + 5] = -t25 * t13;
		return 1;
	}

	//Transform adjoint variables
	int AdjointTranEEToRV(const double *ee, const double *lmdee, double *lmdrv, const double mu) {
		double JB[36];
		JBEEtoRVGen(ee, JB, mu);
		for (int i = 0; i < 6; i++) {
			lmdrv[i] = lmdee[i];
		}
		GaussRow(6, 1, JB, 6, lmdrv, 1);
		//M_Multi(lmdrv, JB, lmdee, 6, 1, 6);
		return 1;
	}

	//Transform adjoint variables
	int AdjointTranRVToEE(const double *ee, const double *lmdrv, double *lmdee, const double mu) {
		double JB[36];
		JBEEtoRVGen(ee, JB, mu);
		M_Multi(lmdee, JB, lmdrv, 6, 1, 6);
		return 1;
	}

	//EE CASE GENERATE CONTROL LAW
	void lmd2Control(double *Thrust, const double *x, const double lmd0, const double epsilon, const double T, const double IG, const double mu) {
		double P = x[0], ex = x[1], ey = x[2], hx = x[3], hy = x[4], L = x[5], m = x[6];
		const double *lmd = x + 7, lmdm = x[13];
		double t1 = sqrt(P / mu);
		double t2 = sin(L);
		double t3 = cos(L);
		double t4 = t1 * (t2 * lmd[1] - t3 * lmd[2]);
		double t5 = ex * t3 + ey * t2 + 0.1e1;
		t5 = 0.1e1 / t5;
		double t6 = -0.2e1 * lmd[0] * t1 * P * t5 - t1 * (lmd[1] * ((ex + t3) * t5 + t3) + lmd[2] * (t5 * (ey + t2) + t2));
		t1 = -t1 * t5 * (hx *hx + hy *hy + 1) * (t2 * lmd[4] + t3 * lmd[3]) / 0.2e1 - t5 * (hx * t2 - hy * t3) * t1 * (lmd[2] * ex - lmd[1] * ey + lmd[5]);
		t2 = t1 *t1 + t4 * t4 + t6 * t6;
		t3 = pow(t2 , (-0.1e1 / 0.2e1));
		t2 = (lmdm + IG * t2 * t3 / m) / lmd0;
		t5 = 0.1e1 - t2;
		t5 = (4 * epsilon * epsilon) + t5 * t5;
		double t7 = 2 * epsilon;
		t2 = t7 + 0.1e1 - t2 + sqrt(t5);
		t2 = 0.1e1 / t2;
		t2 = t7 * t2 * t3;
		Thrust[0] = T *(-t2 * t4);
		Thrust[1] = T * t2 * t6;
		Thrust[2] = T * t2 * t1;
	}

	void STMGen(const double *ee, double *stm) {
		int flag = 0;
		double rv[6];
		ee2rv(flag, rv, ee, 1.0);
		double normr = V_Norm2(rv, 3);
		double dirx[3], dirz[3], diry[3];
		for (size_t i = 0; i < 3; i++) {
			dirx[i] = rv[i] / normr;
		}
		V_Cross(dirz, rv, rv + 3);
		double dirznorm = V_Norm2(dirz, 3);
		for (size_t i = 0; i < 3; i++) {
			dirz[i] /= dirznorm;
		}
		V_Cross(diry, dirz, dirx);
		for (int i = 0; i < 3; i++) {
			stm[3 * i] = dirx[i];
			stm[3 * i + 1] = diry[i];
			stm[3 * i + 2] = dirz[i];
		}
	}
	//Generate thrust in 
	void ControlLawGen(double *Thrust, const double *x, const double lmd0, const double epsilon, const double T, const double IG, const double mu) {
		double tmplaw[3], stm[9];
		lmd2Control(tmplaw, x, lmd0, epsilon, T, IG, mu);
		STMGen(x, stm);
		M_Multi(Thrust, stm, tmplaw, 3, 1, 3);
	}
}