#ifndef BASICDYNFUNMAX_H
#define BASICDYNFUNMAX_H
namespace basicdynfunmax {
	int FOPTDynFunMax(double t, const double* x, double* dx, const double* hpara);
	double FOPTHamFunMOne(double t, double *x, const double *para);
	int CoastDyn(double t, const double* x, double* dx, const double* hpara);
	int ThrustDyn(double t, const double* x, double* dx, const double* hpara);
	double rho(const double *x, const double *lmd, const double *para);
	void rhodrhodtgen(const double *x, const double *lmd, double *rho, double *drhodt, const double *para);
}
#endif