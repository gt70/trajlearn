/*  Dev by tiger at 2014/12/16
*  A class for reading data from file
*  use a map(string, vector<string>) to associate
*/
#ifndef CONFIG_H
#define CONFIG_H
#include <map>
#include <vector>
#include <string>
using namespace std;
namespace TigerConfig {
	typedef vector<string> vstr;
	typedef map<string, vstr> IniMap;
	class Config {
	private:
		IniMap imap;
	public:
		//Config(const char[]);
		void init(const char[]);
		void getIntArray(const string &key, int *);
		void getDoubleArray(const string &key, double *);
		void getIntArray(const string &key, int *, int);
		void getDoubleArray(const string &key, double *, int);
		int getLength(const string &key);
	};
}

#endif
