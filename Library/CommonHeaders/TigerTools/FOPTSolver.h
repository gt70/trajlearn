/*	
	Class to help solve fuel optimal transfer
	central mu, Tmax, Isp are included
*/
#ifndef FOPTSOLVER_H
#define FOPTSOLVER_H
#include "TigerMinpackSolver.h"
#include "Integrator.h"
#include <string>
namespace foptsolver {
	class FOPTSolver {
	public:
		TigerMinpackSolver::TigerMinpackSolver *MSolver;
		TigerIntegrator::ODE45Integrator *ODE45;
		//para contains:state0(7), statef(7), trantime
		double para[15];
		double x[8];
		double fvec[8];
		double mf;
		std::string filename;
		double mu;
		double Tmax;
		double Ispg0;
		double epsilon;
		int output;

		//construction function
		FOPTSolver() {
			MSolver = new TigerMinpackSolver::TigerMinpackSolver(8, 0);
			ODE45 = new TigerIntegrator::ODE45Integrator(14);
		}
		FOPTSolver(const double mu, const double Tmax, const double Ispg0, const double eps) {
			MSolver = new TigerMinpackSolver::TigerMinpackSolver(8, 0);
			ODE45 = new TigerIntegrator::ODE45Integrator(14);
			this->mu = mu;
			this->Tmax = Tmax;
			this->Ispg0 = Ispg0;
			output = 0;
			epsilon = eps;
		}
		~FOPTSolver() {
			delete MSolver;
			delete ODE45;
		}
		void setMu(const double mu) {
			this->mu = mu;
		}
		void setTmax(const double Tmax) {
			this->Tmax = Tmax;
		}
		void setIspg0(const double Ispg0) {
			this->Ispg0 = Ispg0;
		}
		void setNprint(const int print) {
			MSolver->setNprint(print);
		}
		void setOutput(const int output) {
			this->output = output;
		}
		void setEps(const double eps) {
			this->epsilon = eps;
		}
		bool succeed() {
			double norm = 0;
			for (int i = 0; i < 8; i++) {
				norm += fvec[i] * fvec[i];
			}
			return sqrt(norm) < 1e-6;
		}
	};
}
#endif