/*
Used to transform adjoint variables from EE type to RV type
*/
#include <cmath>
#include "TigerTools/TigerTools.h"
#include "Tools/VecMat.h"

/*
Evaluate Jacobian matrix of R with respect to x
only R is taken into consideration
*/
void JbGen(const double *x, double *jb) {
	const double P = x[0], f = x[1], g = x[2], h = x[3], k = x[4], L = x[5];
	double t1 = k * k;
	double t2 = h * h;
	double t3 = 0.1e1 + t1 + t2;
	double t4 = cos(L);
	double t5 = sin(L);
	double t6 = f * t4 + g * t5 + 0.1e1;
	double t7 = 0.1e1 - t1 + t2;
	double t8 = 0.2e1 * t5;
	double t9 = t8 * h * k;
	double t10 = t4 * t7 + t9;
	t1 = 0.1e1 + t1 - t2;
	t2 = 0.2e1 * t4;
	double t11 = t2 * h * k;
	double t12 = t1 * t5 + t11;
	double t13 = h * t5 - k * t4;
	t6 = 0.1e1 / t6;
	t3 = 0.1e1 / t3;
	double t14 = t3 * t6;
	double t15 = pow(t6, 0.2e1);
	double t16 = P * t3 * t15;
	double t17 = t16 * t12;
	double t18 = t16 * t10;
	double t19 = h * t4 + k * t5;
	double t20 = t3 * t10;
	double t21 = t14 * P;
	double t22 = t3 * t12;
	double t23 = -0.4e1 * t3 * t13;
	double t24 = f * t5 - g * t4;
	jb[0 * 3 + 0] = t14 * t10;
	jb[0 * 3 + 1] = t14 * t12;
	jb[0 * 3 + 2] = 0.2e1 * t3 * t6 * t13;
	jb[1 * 3 + 0] = -t18 * t4;
	jb[1 * 3 + 1] = -t17 * t4;
	jb[1 * 3 + 2] = -t2 * P * t3 * t15 * t13;
	jb[2 * 3 + 0] = -t18 * t5;
	jb[2 * 3 + 1] = -t17 * t5;
	jb[2 * 3 + 2] = -t16 * t8 * t13;
	jb[3 * 3 + 0] = -0.2e1 * t21 * (t20 * h - t19);
	jb[3 * 3 + 1] = -0.2e1 * t21 * (t22 * h + t13);
	jb[3 * 3 + 2] = t21 * (t23 * h + t8);
	jb[4 * 3 + 0] = -0.2e1 * t21 * (t20 * k - t13);
	jb[4 * 3 + 1] = -0.2e1 * t21 * (t22 * k - t19);
	jb[4 * 3 + 2] = t21 * (t23 * k - t2);
	jb[5 * 3 + 0] = t21 * (t6 * t10 * t24 - t5 * t7 + t11);
	jb[5 * 3 + 1] = t21 * (t6 * t12 * t24 + t1 * t4 - t9);
	jb[5 * 3 + 2] = 0.2e1 * t21 * (t6 * t13 * t24 + t19);
	return;
}

