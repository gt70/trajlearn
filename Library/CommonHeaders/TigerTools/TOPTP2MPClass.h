/*	Header file for two primary classes
 * 	Ability for parallel calculation has not been tested yet
 */
#ifndef TOPTP2MPCLASS_H
#define TOPTP2MPCLASS_H
#include "Integrator.h"
#include "TigerMinpackSolver.h"
#include "../Tools/VecMat.h"
//#include "../Tools/Constant.h"
namespace optsolver{
	class TSolver{
	public:
		TSolver(){
			ODE45 = new TigerIntegrator::ODE45Integrator(12);
			MinpackSolver = new TigerMinpackSolver::TigerMinpackSolver(8, 0);
		};
		~TSolver() {
			delete ODE45;
			delete MinpackSolver;
		}
		int Solve(double *x0, const double *coe0, const double *coef, const double m0);
		void GenRandom(double *x0, const double guesstime);
		int SolveUntil(const int miniter, const int maxiter, double *bestx, const double guess, const double *coe0, const double *coef, const double m0);
		static int DynFunMP(double t, const double* x, double* dx, const double* dfpara);
		static double HamP2MPMP(double t, const double* x, double lam0, const double* hpara);
		bool Succeed(){return V_Norm2(fvec, 8) < 1e-6;};
	
		double ee0[6];
		double eef[6];
		double m0;
		double fvec[8];
		int output;
		TigerIntegrator::ODE45Integrator *ODE45;//One ODE solver, with 12 dimension allowed
		TigerMinpackSolver::TigerMinpackSolver *MinpackSolver;//Nonlinear solver, with lmd0

		static double Tmax;
		static double Ispg0;
		static double mu;

		static void setTmax(const double T){Tmax = T;};
		static void setIspg0(const double IG){Ispg0 = IG;};
		static void setMu(const double Mu){mu = Mu;};
	};
}
#endif
