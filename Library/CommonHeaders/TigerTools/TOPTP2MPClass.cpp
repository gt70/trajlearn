/*	Dev by tiger at 06.04.2015
 * 	Class, including familiar timeoptimal for rendezvous using EE
 */
#include "Integrator.h"
#include "TigerMinpackSolver.h"
#include "../Tools/Constant.h"
#include "../Tools/VecMat.h"
#include "../Tools/OrbitFun.h" 
#include <cmath>
#include "TOPTP2MPClass.h"
double L0dt2Lt(double L0, double dt, const double* ee);
void MatMD(double * M, double& D6, double * dM, double* dD6, double* ddM, double* ddD6, const double* ee, int step);
namespace optsolver{
int TSolver::DynFunMP(double t, const double* x, double* dx, const double* hpara){
	double m = hpara[0] - t*Tmax/Ispg0;
	const double P = x[0], ex = x[1], ey = x[2], hx = x[3], hy = x[4], L = x[5];
	const double *lmd = x + 6, lmdm = x[11];
	const double t1 = 0.1e1 / mu;
	const double t3 = sqrt(t1 * P);
	const double t5 = cos(L);
	const double t7 = sin(L);
	const double t9 = t5 * ex + t7 * ey + 0.1e1;
	const double t10 = 0.1e1 / t9;
	const double t12 = 0.1e1 / m;
	const double t13 = t12 * Tmax;
	const double t14 = t3 * lmd[1];
	const double t16 = t3 * lmd[2];
	const double t18 = -t7 * t14 + t5 * t16;
	const double t19 = t18 * t18;
	const double t20 = t3 * lmd[0];
	const double t21 = t10 * P;
	const double t24 = ex + t5;
	const double t26 = t10 * t24 + t5;
	const double t28 = ey + t7;
	const double t30 = t10 * t28 + t7;
	const double t32 = -t26 * t14 - t30 * t16 - 0.2e1 * t21 * t20;
	const double t33 = t32 * t32;
	const double t34 = t10 * lmd[1];
	const double t37 = t7 * hx - t5 * hy;
	const double t38 = t37 * t3;
	const double t41 = t10 * lmd[2];
	const double t44 = t10 * lmd[3];
	const double t45 = hx * hx;
	const double t46 = hy * hy;
	const double t47 = t45 + t46 + 0.1e1;
	const double t48 = t47 * t3 / 0.2e1;
	const double t49 = t5 * t48;
	const double t51 = t10 * lmd[4];
	const double t52 = t7 * t48;
	const double t54 = t10 * lmd[5];
	const double t56 = -ex * t38 * t41 + ey * t38 * t34 - t38 * t54 - t49 * t44 - t52 * t51;
	const double t57 = t56 * t56;
	const double t59 = sqrt(t19 + t33 + t57);
	const double t60 = 0.1e1 / t59;
	const double t61 = t32 * t60;
	const double t62 = t61 * t13;
	const double t65 = t7 * t3;
	const double t67 = t60 * t12;
	const double t68 = t18 * t67;
	const double t69 = t68 * Tmax * t65;
	const double t72 = t32 * t67;
	const double t74 = t3 * t10;
	const double t75 = ey * t37;
	const double t77 = t56 * t60;
	const double t78 = t77 * t13;
	const double t81 = t5 * t3;
	const double t83 = t68 * Tmax * t81;
	const double t87 = ex * t37;
	const double t97 = 0.1e1 / P;
	const double t99 = sqrt(t97 * mu);
	const double t100 = t9 * t9;
	const double t101 = t100 * t99;
	const double t104 = t78 * t37 * t74;
	const double t106 = 0.1e1 / t3;
	const double t118 = t1 * t18 * t67;
	const double t123 = t1 * t32 * t67;
	const double t125 = t106 * t10;
	const double t128 = t1 * t77 * t13;
	const double t142 = t47 * t106 / 0.2e1;
	const double t153 = P * P;
	const double t161 = Tmax * t37;
	const double t170 = 0.1e1 / t100;
	const double t172 = t170 * P * t20;
	const double t177 = t170 * t24;
	const double t183 = t3 * t170;
	const double t184 = t75 * t183;
	const double t186 = t5 * t77 * t13;
	const double t194 = t87 * t183;
	const double t198 = t170 * lmd[3];
	const double t200 = t5 * t5;
	const double t202 = t56 * t67;
	const double t205 = t170 * lmd[4];
	const double t206 = t52 * t205;
	const double t208 = t9 * t99;
	const double t212 = t161 * t183;
	const double t228 = t7 * t77 * t13;
	const double t232 = t170 * t28;
	const double t241 = t49 * t198;
	const double t244 = t7 * t7;
	const double t259 = t202 * ey * Tmax;
	const double t263 = t202 * ex * Tmax;
	const double t265 = hx * t3;
	const double t277 = hy * t3;
	const double t287 = -t7 * ex + t5 * ey;
	const double t299 = t287 * t77 * t13;
	const double t303 = t5 * hx + t7 * hy;
	dx[0] = 0.2e1 * t62 * t10 * P * t3;
	dx[1] = t72 * Tmax * t26 * t3 - t78 * t75 * t74 + t69;
	dx[2] = t72 * Tmax * t30 * t3 + t78 * t87 * t74 - t83;
	dx[3] = t78 * t5 * t47 * t74 / 0.2e1;
	dx[4] = t78 * t7 * t47 * t74 / 0.2e1;
	dx[5] = t97 * t101 + t104;
	dx[6] = -t1 * t61 * t13 * t21 * t106 * lmd[0] - 0.2e1 * t62 * t10 * t20 - (t118 * Tmax * t7 * t106 + t123 * Tmax * t26 * t106 - t128 * t75 * t125) * lmd[1] / 0.2e1 - (-t118 * Tmax * t5 * t106 + t123 * Tmax * t30 * t106 + t128 * t87 * t125) * lmd[2] / 0.2e1 - t128 * t5 * t142 * t44 / 0.2e1 - t128 * t7 * t142 * t51 / 0.2e1 - (-mu / t153 / P * t100 / t99 / 0.2e1 - 0.1e1 / t153 * t101 + t1 * t56 * t67 * t161 * t125 / 0.2e1) * lmd[5];
	dx[7] = 0.2e1 * t5 * t61 * t13 * t172 - (t72 * Tmax * (-t5 * t177 + t10) * t3 + t186 * t184) * lmd[1] - (-t62 * t5 * t170 * t28 * t3 - t186 * t194 + t104) * lmd[2] + t202 * Tmax * t200 * t48 * t198 + t186 * t206 - (-t5 * t56 * t67 * t212 + 0.2e1 * t5 * t97 * t208) * lmd[5];
	dx[8] = 0.2e1 * t7 * t61 * t13 * t172 - (-t62 * t7 * t170 * t24 * t3 + t228 * t184 - t104) * lmd[1] - (t72 * Tmax * (-t7 * t232 + t10) * t3 - t228 * t194) * lmd[2] + t228 * t241 + t202 * Tmax * t244 * t48 * t205 - (-t7 * t56 * t67 * t212 + 0.2e1 * t7 * t97 * t208) * lmd[5];
	dx[9] = -t186 * t265 * t44 - t228 * t265 * t51 + t259 * t65 * t34 - t263 * t65 * t41 - t78 * t65 * t54;
	dx[10] = -t186 * t277 * t44 - t228 * t277 * t51 - t259 * t81 * t34 + t263 * t81 * t41 + t78 * t81 * t54;
	dx[11] = 0.2e1 * t287 * t61 * t13 * t172 - (t83 + t72 * Tmax * (-t7 * t10 - t287 * t177 - t7) * t3 + t299 * t184 - t78 * ey * t303 * t74) * lmd[1] - (t69 + t72 * Tmax * (t5 * t10 - t287 * t232 + t5) * t3 - t299 * t194 + t78 * ex * t303 * t74) * lmd[2] + t299 * t241 + t228 * t48 * t44 + t299 * t206 - t186 * t48 * t51 - (-t287 * t56 * t67 * t212 + 0.2e1 * t287 * t97 * t208 + t78 * t303 * t74) * lmd[5];
	return 1;
}
double TSolver::HamP2MPMP(double t, const double *x, double lmd0, const double *hpara){
	double m = hpara[0] - t*Tmax/Ispg0;
	const double P = x[0], ex = x[1], ey = x[2], hx = x[3], hy = x[4], L = x[5];
	const double *lmd = x + 6, lmdm = x[11];
	const double t3 = sqrt(P / mu);
	const double t5 = cos(L);
	const double t7 = sin(L);
	const double t9 = t5 * ex + t7 * ey + 0.1e1;
	const double t10 = 0.1e1 / t9;
	const double t12 = t10 * P * t3 * lmd[0];
	const double t13 = 0.1e1 / m;
	const double t14 = t13 * Tmax;
	const double t15 = t3 * lmd[1];
	const double t17 = t3 * lmd[2];
	const double t19 = -t7 * t15 + t5 * t17;
	const double t20 = t19 * t19;
	const double t24 = t5 + t10 * (ex + t5);
	const double t28 = t7 + t10 * (ey + t7);
	const double t30 = -t24 * t15 - t28 * t17 - 0.2e1 * t12;
	const double t31 = t30 * t30;
	const double t35 = t7 * hx - t5 * hy;
	const double t36 = t35 * t3;
	const double t42 = t10 * lmd[3];
	const double t43 = hx * hx;
	const double t44 = hy * hy;
	const double t46 = (t43 + t44 + 0.1e1) * t3 / 0.2e1;
	const double t49 = t10 * lmd[4];
	const double t54 = -ex * t36 * t10 * lmd[2] + ey * t36 * t10 * lmd[1] - t36 * t10 * lmd[5] - t5 * t46 * t42 - t7 * t46 * t49;
	const double t55 = t54 * t54;
	const double t57 = sqrt(t20 + t31 + t55);
	const double t58 = 0.1e1 / t57;
	const double t65 = t58 * t13;
	const double t66 = t19 * t65;
	const double t70 = t30 * t65;
	const double t72 = t3 * t10;
	const double t76 = t54 * t58 * t14;
	const double t93 = t65 * t54;
	const double t100 = 0.1e1 / P;
	const double t102 = sqrt(t100 * mu);
	const double t103 = t9 * t9;
	const double t110 = 0.2e1 * t30 * t58 * t14 * t12 + (t70 * Tmax * t24 * t3 + t66 * Tmax * t7 * t3 - t76 * ey * t35 * t72) * lmd[1] + (t70 * Tmax * t28 * t3 - t66 * Tmax * t5 * t3 + t76 * ex * t35 * t72) * lmd[2] + t93 * Tmax * t5 * t46 * t42 + t93 * Tmax * t7 * t46 * t49 + (t100 * t103 * t102 + t76 * t35 * t72) * lmd[5] + lmd0;
	return t110;
}

//x:lam0,lam1-6,dt
//sfpara:ee0(7), eef(7), m0, flag
int ShootFunP2MPTimeOpt(int n, const double *x, double *fvec, int iflag, const void* para)
{
	const TSolver *that = (TSolver *)para;
	double x0[12] = { 0.0 };
	double dfpara[1] = { 1.0 };
	double m0;
	int ip = 0;
	double lam0 = 1.0, tf = 0.0, epsi = 1.0;
	int i;
	m0 = that->m0;
	int outflag = that->output;

	lam0 = x[6];
	V_Copy(x0 + 6, x, 6);
	tf = x[7];
	dfpara[0] = m0;
	V_Copy(x0, that->ee0, 6);

	int flag = that->ODE45->Integrate(TSolver::DynFunMP, x0, dfpara, 0.0, tf, NULL);
	if (flag<1)
		return -1;
	const double *ee1 = that->eef;
	double L1 = L0dt2Lt(ee1[5], tf, ee1);
	for (i = 0; i<5; i++)
		fvec[i] = x0[i] - ee1[i];
	fvec[5] = x0[5] - L1;
	fvec[6] = V_Norm2(x, 7) - 1.0;
	double temp = (1.0 + ee1[1] * cos(L1) + ee1[2] * sin(L1)) / ee1[0];
	double mu = that->mu;
	fvec[7] = TSolver::HamP2MPMP(tf, x0, lam0, dfpara) - x0[11] * sqrt(mu*ee1[0])*temp*temp;
	return 0;
}

void TSolver::GenRandom(double *x0, const double time){
	for(int i = 0;i < 7;i++){
		x0[i] = (double)rand()/RAND_MAX;
	}
	x0[6] += 0.5;
	x0[7] = time;
}

int TSolver::Solve(double *x0, const double *coe0, const double *coef, const double m0){
	//coetrans(tempv, coe0);
	int info = 0;
	coe2ee(info, ee0, coe0, mu);
	ee0[5] -= D2PI*floor(ee0[5]/D2PI);
	//coetrans(tempv, coe1);
	coe2ee(info, eef, coef, mu);
	eef[5] -= D2PI*floor(eef[5]/D2PI);
	//if diff too big seems that have to add 2pi for the smaller one
	if(fabs(ee0[5] - eef[5]) > DPI)
	{
		if(ee0[5] < eef[5])
			ee0[5] += D2PI;
		else
			eef[5] += D2PI;
	}
	/* Seems to be a mistake
	//set coe
	V_Copy(this->ee0, coe0, 6);
	V_Copy(this->eef, coef, 6);
	* */
	this->m0 = m0;
	info = this->MinpackSolver->Solve(ShootFunP2MPTimeOpt, x0, this->fvec, (double*)this);
	return info;
}

int TSolver::SolveUntil(const int miniter, const int maxiter, double *bestx, const double guesstime, const double *coe0, const double *coef, const double m0){
	double bestt = 10000;
	double xguess[8];
	int okiter = 0;
	bool succeed = false;
	for(int i = 0;i < maxiter;i++){
		GenRandom(xguess, guesstime);
		Solve(xguess, coe0, coef, m0);
		if(Succeed() && xguess[7] < bestt && xguess[7] > 0){
			V_Copy(bestx, xguess, 8);
			bestt = xguess[7];
			succeed = true;
			okiter++;
			if(okiter >= miniter){
				break;
			}
		}
	}
	if(succeed)
		return 1;
	else
		return -1;
}
//end of namespace optsolver
}
/*
double TSolver::Tmax = TmaxNU;
double TSolver::Ispg0 = IspPg0NU;
double TSolver::mu = mhNU;
int main(){
	srand(time(NULL));
	double coe0[6] = {3.0, 0, 0, 0, 0, 0};
	double coef[6] = {3.1, 0, 0, 0, 0, 0.05};
	//allocate
	TigerIntegrator::ODE45Integrator ODE45(12);
	ODE45.setRelTol(1e-6);
	ODE45.setAbsTol(1e-6);
	TigerMinpackSolver::TigerMinpackSolver Min(8, 0);
	Min.setXtol(1e-6);
	TSolver Solver(&ODE45, &Min);
	
	double x0[8];
	clock_t start, end;
	start = clock();
	int maxiter = 100, succeed = 0;
	double result;
	for(int i = 0;i < maxiter;i++){
		for(int j = 0;j < 7;j++)	x0[j] = (double)rand()/RAND_MAX - 0.5;
		x0[6] += 0.5;
		x0[7] = 2;
		double m0 = 0.8;
		int flag = Solver.Solve(x0, coe0, coef, m0);
		if(Solver.Succeed()){
			succeed++;
			result = x0[7];
		}
	}
	end = clock();
	cout << "Time cost : " << (double)(end - start)/CLOCKS_PER_SEC << endl;
	cout << "Succeed " << succeed << " times" << endl;
	cout << "Result = " << result << endl;
	//cout << flag << endl;
	//cout << "Time = " << x0[7] << endl;
	//cout << "mf = " << m0 - x0[7]*TmaxNU/IspPg0NU << endl;
	return 0;
}
*/
//
