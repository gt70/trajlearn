/*	Some basic operations needed is implemented here
* 	Including many things to avoid global parameters
* 	Some Classes are defined to pass parameters
*/
#include <cmath>
#include <cstdio>
#include "BasicApproximate.h"
static const double CONSTANT_PI = 3.141592653589793;
static const double CONSTANT_2PI = 2 * CONSTANT_PI;
//Planar case, with given parameters, return change of coefficient
double dacalc(const double Lambda, const double theta_e, const double theta_0, const double theta_f, const double A, const double r) {
	if (fabs(Lambda) < 1e-5)
		return 2 * A*r*(theta_f - theta_0);
	else
		return 0.2e1 * A * r / Lambda * (sin(Lambda * (theta_f - theta_e)) - sin(Lambda * (theta_0 - theta_e)));
}
double dexcalc(const double Lambda, const double theta_e, const double theta_0, const double theta_f, const double A) {
	if (fabs(1 - Lambda) < 1e-5)
		return 3 * A / 2 * (theta_f - theta_0)*cos(theta_e) + A / 2 * (sin(2 * theta_f - theta_e) - sin(2 * theta_0 - theta_e));
	else
		return 0.3e1 * A * (sin(((1 - Lambda) * theta_f + Lambda * theta_e)) + sin((-(1 - Lambda) * theta_0 - Lambda * theta_e))) / (2 - 2 * Lambda) + A * (-sin((-(1 + Lambda) * theta_f + Lambda * theta_e)) - sin(((1 + Lambda) * theta_0 - Lambda * theta_e))) / (2 + 2 * Lambda);
}
double deycalc(const double Lambda, const double theta_e, const double theta_0, const double theta_f, const double A) {
	if (fabs(1 - Lambda) < 1e-5)
		return 3 * A / 2 * (theta_f - theta_0)*sin(theta_e) - A / 2 * (cos(2 * theta_f - theta_e) - cos(2 * theta_0 - theta_e));
	else
		return 0.3e1 * A * (-cos(((1 - Lambda) * theta_f + Lambda * theta_e)) + cos((-(1 - Lambda) * theta_0 - Lambda * theta_e))) / (2 - 2 * Lambda) + A * (-cos((-(1 + Lambda) * theta_f + Lambda * theta_e)) + cos(((1 + Lambda) * theta_0 - Lambda * theta_e))) / (2 + 2 * Lambda);
}
//Update delta according to intercal and beta
//A is the planar part of total A
void DeltaUpdate(double *delta, const double Lambda, const double theta_e, const double theta0,
	const double endtheta, const double A, const double r) {
	delta[0] += dacalc(Lambda, theta_e, theta0, endtheta, A, r);
	delta[1] += dexcalc(Lambda, theta_e, theta0, endtheta, A);
	delta[2] += deycalc(Lambda, theta_e, theta0, endtheta, A);
}
//Update a revolution for planar case
void OneRevUpdate(double *delta, const double Lambda, const double theta_e, const double A, const double r) {
	//For quick return
	if (fabs(Lambda) < 1e-5) {
		delta[0] += 4 * CONSTANT_PI*A*r;
		return;
	}
	else if (fabs(1 - Lambda) < 1e-5) {
		double tmp = 3 * A*CONSTANT_PI;
		delta[1] += tmp*cos(theta_e);
		delta[2] += tmp*sin(theta_e);
		return;
	}
	else {
		double tmp = (3 * A / (1 - Lambda) - A / (1 + Lambda))*sin(Lambda*CONSTANT_PI);
		delta[0] += 4 * A*r / Lambda*sin(Lambda*CONSTANT_PI);
		delta[1] += tmp*cos(theta_e);
		delta[2] += tmp*sin(theta_e);
		return;
	}
}
//Planar case, return change of coes
//desired change is introduced by function to call this
//always return Positive Case, that is \alpha=\Lambda(\vartheta-\vartheta_e)
//A=T/m/(mu/r/r)
int PlaneCaseDelta(double *delta, const double lambda, const double theta_e, const double theta_0, const double theta_f,
	const double A, const double r) {
	if (theta_f < theta_0) {
		//printf("Planar:theta_f < theta_0\n");
		return -1;
	}
	//part
	//see what interval theta_0 and theta_f belong to
	delta[0] = 0;
	delta[1] = 0;
	delta[2] = 0;
	int i1 = floor((theta_0 - theta_e + CONSTANT_PI) / CONSTANT_2PI);
	int i2 = floor((theta_f - theta_e + CONSTANT_PI) / CONSTANT_2PI);
	//if in one interval
	if (i1 == i2) {
		DeltaUpdate(delta, lambda, theta_e, theta_0 - i1*CONSTANT_2PI, theta_f - i1*CONSTANT_2PI, A, r);
		return 1;
	}
	//if close to each other
	else {
		DeltaUpdate(delta, lambda, theta_e, theta_0 - i1*CONSTANT_2PI, theta_e + CONSTANT_PI, A, r);
		DeltaUpdate(delta, lambda, theta_e, theta_e - CONSTANT_PI, theta_f - i2*CONSTANT_2PI, A, r);
		//y = y + dycalc(A, r, lambda, theta_e, theta_0 - i1*2*pi, theta_e + pi);
		//y = y + dycalc(A, r, lambda, theta_e, theta_e - pi, theta_f - i2*2*pi);
		if (i2 - i1 > 1) {
			int k = i2 - i1 - 1;
			if (k == 1) {
				OneRevUpdate(delta, lambda, theta_e, A, r);
			}
			else {
				double diff[3] = { 0, 0, 0 };
				OneRevUpdate(diff, lambda, theta_e, A, r);
				for (int i = 0; i < 3; i++)
					delta[i] += k*diff[i];
			}
		}
		return 1;
	}
}

//Obtain bang-bang control, detect switching point
//A=T/m/(mu/r/r)
int OuterCaseDelta(double *delta, const double thetai, const double theta_0, const double thetaf, const double A) {
	double lefttheta = thetaf - theta_0;
	if (lefttheta < 0) {
		//printf("Outer:thetaf < theta0\n");
		return -1;
	}
	double theta0 = theta_0;
	//Adjust to make theta0 with in [thetai, thetai + 2*pi)
	while (theta0 - thetai >= CONSTANT_2PI)
		theta0 -= CONSTANT_2PI;
	while (theta0 - thetai < 0)
		theta0 += CONSTANT_2PI;
	//determine initial interval
	bool nowpos = false;
	if (theta0 - thetai >= CONSTANT_PI*0.5 && theta0 - thetai <= CONSTANT_PI*1.5)
		nowpos = true;
	bool jump = false;
	double endtheta, dtheta;
	while (1) {
		if (nowpos) {
			endtheta = thetai + CONSTANT_PI*1.5;
			dtheta = endtheta - theta0;
			if (dtheta > lefttheta) {
				endtheta = theta0 + lefttheta;
				jump = true;
			}
			delta[0] += (sin(endtheta) - sin(theta0));
			delta[1] += (cos(theta0) - cos(endtheta));
			if (jump)
				break;
			lefttheta = lefttheta - (endtheta - theta0);
			nowpos = !nowpos;
			theta0 = thetai - CONSTANT_PI / 2.0;
		}
		else {
			endtheta = thetai + CONSTANT_PI / 2.;
			dtheta = endtheta - theta0;
			if (dtheta > lefttheta) {
				endtheta = theta0 + lefttheta;
				jump = true;
			}
			delta[0] -= (sin(endtheta) - sin(theta0));
			delta[1] -= (cos(theta0) - cos(endtheta));
			if (jump)
				break;
			lefttheta = lefttheta - (endtheta - theta0);
			nowpos = !nowpos;
			theta0 = thetai + CONSTANT_PI / 2.0;
		}
	}
	delta[0] *= A / 2.;
	delta[1] *= A / 2.;
	return 1;
}

//plane shoot A unknown theta_f known
//x are Lambda, thetae, A, with thetaf given fixed
//para contains:theta_0, theta_f, r, ta, tex, tey, dir, brought by class EstPara
int PlaneShootA(int n, const double *x, double *dx, int iflag, const double *para) {
	const EstPara *estpara = (EstPara*)para;
	const double lambda = x[0], theta_e = x[1], A = x[2];
	for (int i = 0; i < 3; i++)
		dx[i] = 0.0;
	int flag = PlaneCaseDelta(dx, lambda, theta_e, estpara->theta0, estpara->thetaf, A, estpara->r);
	if (flag == -1)
		return -1;
	if (estpara->dir) {
		dx[0] -= estpara->da;
		dx[1] -= estpara->dex;
		dx[2] -= estpara->dey;
	}
	else {
		dx[0] += estpara->da;
		dx[1] += estpara->dex;
		dx[2] += estpara->dey;
	}
	return 1;
}
//plane shoot A known theta_f unknown
//x are Lambda, thetae, theta_f, with A given fixed
//para contains:theta_0, theta_f, r, ta, tex, tey, dir, brought by class EstPara
int PlaneShootTheta(int n, const double *x, double *dx, int iflag, const double *para) {
	const EstPara *estpara = (EstPara*)para;
	const double lambda = x[0], theta_e = x[1], theta_f = x[2];
	for (int i = 0; i < 3; i++)
		dx[i] = 0.0;
	int flag = PlaneCaseDelta(dx, lambda, theta_e, estpara->theta0, theta_f, estpara->A, estpara->r);
	if (flag == -1)
		return -1;
	if (estpara->dir) {
		dx[0] -= estpara->da;
		dx[1] -= estpara->dex;
		dx[2] -= estpara->dey;
	}
	else {
		dx[0] += estpara->da;
		dx[1] += estpara->dex;
		dx[2] += estpara->dey;
	}
	return 1;
}
//Outer case shoot A unknwon, thetaf known
//x are thetai, A
//para introduced by class EstPara
int OuterShootA(int n, const double *x, double *dx, int iflag, const double *para) {
	dx[0] = 0;
	dx[1] = 0;
	const EstPara *estpara = (EstPara*)para;
	const double thetai = x[0], A = x[1];
	int flag = OuterCaseDelta(dx, thetai, estpara->theta0, estpara->thetaf, A);
	if (flag == -1)
		return -1;
	dx[0] -= estpara->dhx;
	dx[1] -= estpara->dhy;
	return 1;
}
//Outer case shoot A knwon, thetaf unknown
//x are thetai, thetaf
//para introduced by class EstPara
int OuterShootTheta(int n, const double *x, double *dx, int iflag, const double *para) {
	dx[0] = 0;
	dx[1] = 0;
	const EstPara *estpara = (EstPara*)para;
	const double thetai = x[0], thetaf = x[1];
	int flag = OuterCaseDelta(dx, thetai, estpara->theta0, thetaf, estpara->A);
	if (flag == -1)
		return -1;
	dx[0] -= estpara->dhx;
	dx[1] -= estpara->dhy;
	return 1;
}
void MatGen(double *M1, double *M2, const double A, const double P, const double L0, const double Lf) {
	double t1 = A * (L0 - Lf);
	double t2 = sin(L0);
	double t3 = sin(Lf);
	double t4 = A * P;
	double t5 = cos(L0);
	double t6 = cos(Lf);
	double t7 = 0.2e1 * t4 * (t2 - t3);
	t4 = 0.2e1 * t4 * (t5 - t6);
	t2 = t5 * t2;
	t3 = t6 * t3;
	const double t8 = 0.3e1 / 0.4e1 * A * (t2 - t3);
	const double t9 = 0.5e1 / 0.4e1 * t1;
	t5 = A * (pow(t5, 0.2e1) - pow(t6, 0.2e1));
	t6 = 0.3e1 / 0.4e1 * t5;
	t5 = t5 / 0.16e2;
	M1[0] = 0.2e1 * t1 * P * P;
	M1[1] = t7;
	M1[2] = -t4;
	M1[3] = t7;
	M1[4] = t8 + t9;
	M1[5] = -t6;
	M1[6] = -t4;
	M1[7] = -t6;
	M1[8] = -t8 + t9;

	M2[0] = (L0 - Lf + t2 - t3) * A / 0.16e2;
	M2[1] = -t5;
	M2[2] = -t5;
	M2[3] = -(-L0 + Lf + t2 - t3) * A / 0.16e2;
}
