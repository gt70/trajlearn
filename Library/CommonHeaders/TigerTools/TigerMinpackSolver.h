#ifndef MINPACKSOLVER_H
#define MINPACKSOLVER_H
#include "stdio.h"
#include "math.h"
#include "../MinPack/cminpack.h"
namespace TigerMinpackSolver{
//Define a header file for fast integration
typedef int(*SolveFun)(int n, const double *x, double *fvec, int iflag, const double* para);
typedef int(*DBSolveFun)(int n, const double *x, double *fvec, int iflag, const void* para);
typedef int (*SolveJac)(int n, const double *x, double *fvec, double *fjac, int ldfjac, int iflag, const double* para);
class TigerMinpackSolver{
private:
	int dim;
	double *work;
	double xtol;
	int nprint;
	int maxfevno;
	double fvecnorm;
	double *fjac;
public:
	TigerMinpackSolver(const int n, const int nprint = 1);
	int Solve(SolveFun, double*, double*, const double*);
	int Solveas(SolveFun, double*, double*, const double*);
	int Solve(DBSolveFun, double*, double*, const double*);
	int Solve(SolveJac, double *, double *, const double *);
	int Solveas(SolveJac, double *, double *, const double *);
	int Solvefac(SolveJac, double *, double *, const double *, double fac);
	void setDim(const int n);
	void setXtol(const double tol);
	void setNprint(const int nprint);
	void setMaxfevno(const int maxfevno);
	~TigerMinpackSolver();
};

//Write a new class which is more advanced so I can test choice of parameters on the selected problem
//Incorporate features such as return of Jacobian (useless), select diag, select mode, select factor
//I hope this can help me to solve this problem
class MinpackSolver{
private:
	int dim_;
	double *work_, *diag_, *qtf_, *wa1_, *wa2_, *wa3_, *wa4_, *r_, *q_;
	double *x_, *fvec_;
	double xtol_, fvecnorm_, factor_, epsfcn_;
	int nprint_, maxfevno_;
	int mode_;
public:
	MinpackSolver(const int n, const int nprint = 1){
		dim_ = n;
		work_ = new double[(n*(3*n + 13))/2 + 2*n];
		diag_ = work_;
		qtf_ = work_ + n;
		wa1_ = work_ + 2*n;
		wa2_ = work_ + 3*n;
		wa3_ = work_ + 4*n;
		wa4_ = work_ + 5*n;
		r_ = work_ + 6*n;
		q_ = r_ + (n*(n + 1))/2;
		x_ = q_ + n*n;
		fvec_ = x_ + n;
		//default values
		maxfevno_ = 1000;
		nprint_ = nprint;
		xtol_ = 1e-10;
		factor_ = 1;
		mode_ = 2;
		epsfcn_ = 0;//According to hybrd1
		for(int i = 0; i < dim_; i++)
			diag_[i] = 1.0;
	};
	//Call hybrd interface to solve the problem
	int solve(SolveFun fun, const double *para){
		int ml = dim_ - 1, mu = dim_ - 1;
		int nfev = 0, ldfjac = dim_, lr = (dim_*(dim_+1))/2;
		int flag = hybrd(fun, dim_, x_, fvec_, para, xtol_, maxfevno_, ml, mu,
			epsfcn_, diag_, mode_, factor_, nprint_, &nfev, q_, ldfjac, r_, lr, qtf_,
			wa1_, wa2_, wa3_, wa4_);
		return flag;
	};
	int solve(SolveJac fun, const double *para){
		int ldfjac = dim_, nfev = 0, njev = 0;
		int lr = (dim_*(dim_+1))/2;
		int flag = hybrj(fun, dim_, x_, fvec_, para, q_, ldfjac, xtol_,
			maxfevno_, diag_, mode_, factor_, nprint_, &nfev, &njev, r_,
			lr, qtf_, wa1_, wa2_, wa3_, wa4_);
		return flag;
	};
	bool solved(double tol = 1e-5){
		double norm2 = 0;
		for(int i = 0; i < dim_; i++)
			norm2 += fvec_[i] * fvec_[i];
		return sqrt(norm2) < tol;
	};
	int funeval(SolveFun fun, const double *para){
		int iflag = 1;
		return fun(dim_, x_, fvec_, iflag, para);
	}
	int funeval(SolveJac fun, const double *para){
		int iflag = 1, ldfjac = dim_;
		return fun(dim_, x_, fvec_, q_, ldfjac, iflag, para);
	}

	void setXtol(const double tol){
		xtol_ = tol;
	};
	void setNprint(const int nprint){
		nprint_ = nprint;
	};
	void setMaxfevno(const int maxfevno){
		maxfevno_ = maxfevno;
	};
	void setMode(const int mode, const double val){
		mode_ = mode;
		for(int i = 0; i < dim_; i++)
			diag_[i] = val;
	};
	void setMode(const int mode, const double *val){
		mode_ = mode;
		for(int i = 0; i < dim_; i++)
			diag_[i] = val[i];
	};
	void setMode(const int mode, const int ind, const double val){
		mode_ = mode;
		diag_[ind] = val;
	};//set the ind mode
	void setFactor(const double fac){factor_ = fac;};
	void setX(const double *x){
		for(int i = 0; i < dim_; i++)
			x_[i] = x[i];
	};
	void getX(double *x) const {
		for(int i = 0; i < dim_; i++)
			x[i] = x_[i];
	};
	double *getX() const {return x_;};
	double *getF() const {return fvec_;};
	~MinpackSolver(){
		delete[] work_;
	}
};
}
typedef TigerMinpackSolver::TigerMinpackSolver MSolver;
typedef TigerMinpackSolver::MinpackSolver EZMSolver;
#endif
