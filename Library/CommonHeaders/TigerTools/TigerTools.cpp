#include "TigerTools.h"
#include "stdlib.h"
#include <cmath>
#include "stdio.h"
#include "VectorMatrix.h"
using namespace TigerVecMat;
int ForwardDiff(Fun fun, const int n, const int m, const double *x0, const double step,
	double *Jac, const void *inPara, void *outPara) {
	//double xnew[n], y0[m], y1[m];
	double *work = new double[2 * m + n];
	double *xnew = work, *y0 = work + n, *y1 = work + n + m;
	//Evaluate y0
	int flag = fun(n, m, x0, y0, inPara, outPara);
	if (flag != 1)	return -1;
	for (int i = 0; i < n; i++) {
		//V_Copy(xnew, x0, n);
		for (int j = 0; j < n; j++)	xnew[j] = x0[i];
		xnew[i] += step;
		flag = fun(n, m, xnew, y1, inPara, outPara);
		if (flag != 1)	return -1;
		//Allocate to row major Jac
		for (int row = 0; row < m; row++) {
			Jac[row*n + i] = (y1[row] - y0[row]) / step;
		}
	}
	delete[] work;
	return 1;
}
/* complex interface for Gauss elimination for sloving Ax=b
* nA is dimension of A, nb is colomn of b
* A is pointer to first element of matrix A, lda is leading column of A
* b is pointer to first element of b, ldb is leading column of b
*/
int GaussRow(int nA, int nb, double *A, int lda, double *b, int ldb) {
	//establish an array to store order
	int *Order = new int[nA];
	if (Order == NULL) {
		printf("Memory allocate error\n");
		return -1;
	}
	for (int i = 0; i < nA; i++)
		Order[i] = i;
	//nA - 1 operations needed to make A be an upper tri mat
	for (int i = 0; i < nA - 1; i++) {
		//find max col
		double *MaxRow = MaxAbs(A + lda*i + i, nA - i, lda);
		int maxrow = (MaxRow - (A + i)) / lda;//C style
		//swap row maxrow and i, not in memory but in order
		if (i != maxrow)
			Swap(Order + i, Order + maxrow, 1);
		//row operation from Order[i+1] with Order[i]
		for (int j = i + 1; j < nA; j++) {
			double lji = A[lda*Order[j] + i] / A[lda*Order[i] + i];
			aXpY(-lji, A + lda*Order[i] + i, A + lda*Order[j] + i, nA - i);
			aXpY(-lji, b + ldb*Order[i], b + ldb*Order[j], nb);
		}
	}
	//it seems that U got, now back and got I
	for (int i = nA - 1; i >= 0; i--) {
		//operate row Order[i]
		Scale(b + ldb*Order[i], 1 / A[lda*Order[i] + i], nb, 1);
		A[lda*Order[i] + i] = 1;
		for (int j = i - 1; j >= 0; j--) {
			aXpY(-A[lda*Order[j] + i], b + ldb*Order[i], b + ldb*Order[j], nb, 1, 1);
			A[lda*Order[j] + i] = 0.0;
		}
	}
	//Change something to make it right
	//Here Sacrifice space for time
	double *Saveb = new double[nA*nb];
	if (Saveb == NULL) {
		printf("Memory allocate error\n");
		return -1;
	}
	//Perhaps Matrix Copy should be added later
	for (int i = 0; i < nA; i++) {
		Copy(Saveb + i*nb, b + i*ldb, nb, 1, 1);
	}
	//find each element order in Order
	for (int i = 0; i < nA; i++) {
		//int index = 0;
		//while(Order[index] != i)
		//	index++;
		Copy(b + i*ldb, Saveb + nb*Order[i], nb, 1, 1);
	}
	//Memory free
	delete[] Order;
	delete[] Saveb;
	return 1;
}
//Gauss's method to solve linear equation
int Gauss(int nA, int nb, double *A, int lda, double *b, int ldb) {
#define A(i, j) A[(i - 1)*lda + j - 1]
#define b(i, j) b[(i - 1)*ldb + j - 1]
	//i is row to be multiply , perhaps some subroutines should be added to make code shorter
	for (int i = 1; i < nA; i++) {
		//j is the row to be add
		for (int j = i + 1; j <= nA; j++) {
			double Lij = A(j, i) / A(i, i);
			A(j, i) = 0.0;
			//k is the number (j to nA)
			for (int k = i + 1; k <= nA; k++)
				A(j, k) -= Lij*A(i, k);
			//change b
			for (int k = 1; k <= nb; k++)
				b(j, k) -= Lij*b(i, k);
		}
	}
	//U got , get x now, make A unit matrix and b should be the answer
	//from lowest to highest
	//i is the row to multiply
	for (int i = nA; i >= 1; i--) {
		//make A(i,i) to be one
		for (int k = 1; k <= nb; k++)
			b(i, k) /= A(i, i);
		A(i, i) = 1;
		//j is current row to make one
		for (int j = i - 1; j >= 1; j--) {
			//k is current number(nA to i - 1)
			for (int k = 1; k <= nb; k++)
				b(j, k) -= A(j, i)*b(i, k);
			A(j, i) = 0.0;
		}
	}
#undef A
#undef b
	return 1;
}

