#ifndef TIGERTOOLS
#define TIGERTOOLS
#include "stdlib.h"
#include "stdio.h"
#include <cmath>
#include <vector>
typedef int(*Fun)(const int n, const int m, const double *x0, double *y, const void *inPara, void *outPara);
int Gauss(int nA, int nb, double *A, int lda, double *b, int ldb);
int GaussRow(int nA, int nb, double *A, int lda, double *b, int ldb);
int ForwardDiff(Fun fun, const int n, const int m, const double *x0, const double step,
	double *Jac, const void *inPara, void *outPara);

//static std::default_random_engine generator;
//static std::mt19937 generator;
//static std::normal_distribution<double> distribution(0, 1);

//gen random between [-Amp Amp]
void inline RandomGen(double *x, int n, double Amp = 0.5) {
	for (int i = 0; i < n; i++) {
		x[i] = ((double)rand() / RAND_MAX - 0.5)*2.*Amp;
	}
}

//gen Laplace random number with b and mu
//since b and mu are used, all of them have the same b and mu
//X=mu-b*sgn(U)ln(1-2*abs(U))
void inline LaplaceRandGen(double *x, int n, double b, double mu){
	for (int i = 0; i < n; ++i)	{
		x[i] = ((double)rand() + 1) / RAND_MAX - 0.5;//Generate uniformly distributed within (-1/2, 1/2)
		x[i] = mu - b*(x[i]>0?1:-1)*log(1-2*fabs(x[i]));
	}
}

//gen Laplace random number with vector b and mu, meaning every variable has different b and mu
void inline LaplaceRandGen(double *x, int n, double *b, double *mu){
	for (int i = 0; i < n; ++i)	{
		x[i] = ((double)rand()) / RAND_MAX - 0.5;//Generate uniformly distributed within (-1/2, 1/2)
		x[i] = mu[i] - b[i]*(x[i]>0?1:-1)*log(1-2*fabs(x[i]));
	}
}

//void inline GaussianSeed(uint32_t seed){
//	generator.seed(seed);
//}

//gen random number with normal distribution
//For this interface, mu and sigma for every element is the same
/*
void inline GaussianGen(double *x, double mu = 0, double sigma = 1, int n = 1){
	for (int i=0; i<n; ++i) {
		if(sigma > 0)
			x[i] = sigma * distribution(generator) + mu;
		else
			x[i] = mu;
	}
}

//gen random number with normal distribution
//For this interface, mu and sigma for every element is the same
void inline GaussianGen(double *x, const double *mu, const double *sigma, int n = 1){
	for (int i=0; i<n; ++i) {
		if(sigma[i] > 0)
			x[i] = distribution(generator) * sigma[i] + mu[i];
		else
			x[i] = mu[i];
	}
}
*/
double inline rand_normal(double mean, double stddev)
{//Box muller method
    static double n2 = 0.0;
    static int n2_cached = 0;
    if (!n2_cached)
    {
        double x, y, r;
        do
        {
            x = 2.0*rand()/RAND_MAX - 1;
            y = 2.0*rand()/RAND_MAX - 1;

            r = x*x + y*y;
        }
        while (r == 0.0 || r > 1.0);
        {
            double d = sqrt(-2.0*log(r)/r);
            double n1 = x*d;
            n2 = y*d;
            double result = n1*stddev + mean;
            n2_cached = 1;
            return result;
        }
    }
    else
    {
        n2_cached = 0;
        return n2*stddev + mean;
    }
}

void inline GaussianGen(double *x, double mu = 0, double sigma = 1, int n = 1){
	for (int i=0; i<n; ++i) {
		if(sigma > 0)
			x[i] = rand_normal(mu, sigma);
		else
			x[i] = mu;
	}
}

//gen random number with normal distribution
//For this interface, mu and sigma for every element is the same
void inline GaussianGen(double *x, const double *mu, const double *sigma, int n = 1){
	for (int i=0; i<n; ++i) {
		if(sigma[i] > 0)
			x[i] = rand_normal(mu[i], sigma[i]);
		else
			x[i] = mu[i];
	}
}

template<class T>std::vector<T> toRowVec(T*a, size_t n){
	std::vector<T> vec;
	vec.reserve(n);
	for (int i = 0; i < n; ++i){
		vec.push_back(a[i]);
	}
	return vec;
}

template<class T>void fromRowVec(std::vector<T> &vec, T *a){
	for (int i = 0; i < vec.size(); ++i){
		a[i] = vec[i];
	}
}

template<class T>std::vector<std::vector<T>> toColVec(T*a, size_t n){
	std::vector<std::vector<T>> vec;
	vec.reserve(n);
	for (int i = 0; i < n; ++i){
		std::vector<T> tmp;
		tmp.push_back(a[i]);
		vec.push_back(tmp);
	}
	return vec;
}

template<class T>void fromColVec(std::vector<std::vector<T>> &vec, T *a){
	for (int i = 0; i < vec.size(); ++i){
		a[i] = vec[i][0];
	}
}
void inline Normalize(double *x, int n) {
	double norm = 0.0;
	for (int i = 0; i < n; i++) {
		norm += x[i] * x[i];
	}
	norm = sqrt(norm);
	for (int i = 0; i < n; i++) {
		x[i] /= norm;
	}
}
void inline Nullify(double *x, int n) {
	for (int i = 0; i < n; i++)
		x[i] = 0;
}
void inline Nullify(int *x, int n) {
	for (int i = 0; i < n; i++)
		x[i] = 0;
}

template<typename T> void WriteRow(FILE *fp, T *x, int n,const char sgl[] = "%lf\t", const char pre[] = "", char const post[] = "\n"){
	fprintf(fp, "%s", pre);
	for (int i = 0; i < n; ++i){
		fprintf(fp, sgl, x[i]);
	}
	fprintf(fp, "%s", post);
}

template<typename T> void PrintRow(T *x, int n, const char sgl[] = "%lf\t", const char pre[] = "", const char post[] = "\n"){
	printf("%s", pre);
	for (int i = 0; i < n; ++i){
		printf(sgl, x[i]);
	}
	printf("%s", post);
}

int inline ReadInt(FILE *fp, int *x, int n) {
	int flag = 0;
	for (int i = 0; i < n; i++) {
		flag = fscanf(fp, "%d", x + i);
	}
	return flag;
}
int inline ReadDbl(FILE *fp, double *x, int n) {
	int flag = 0;
	for (int i = 0; i < n; i++) {
		flag = fscanf(fp, "%lf", x + i);
	}
	return flag;
}
void inline WriteInt(FILE *fp, const int *x, int n) {
	for (int i = 0; i < n; i++) {
		fprintf(fp, "%d\n", x[i]);
	}
}
void inline WriteDbl(FILE *fp, const double *x, int n) {
	for (int i = 0; i < n; i++) {
		fprintf(fp, "%22.14e\n", x[i]);
	}
}
void inline WriteDbl(const char *fn, const double *x, int n) {
	FILE *fp = fopen(fn, "w");
	if (fp == NULL)
		return;
	for (int i = 0; i < n; i++) {
		fprintf(fp, "%22.14e\n", x[i]);
	}
	fclose(fp);
}
void inline NextLine(FILE *fp) {
	fprintf(fp, "\n");
}

void inline Gauss(int nA, double **A, double *b) {
#define A(i, j) A[(i - 1)][j - 1]
#define b(i, j) b[(i - 1)]
	//i is row to be multiply , perhaps some subroutines should be added to make code shorter
	for (int i = 1; i < nA; i++) {
		//j is the row to be add
		for (int j = i + 1; j <= nA; j++) {
			double Lij = A(j, i) / A(i, i);
			A(j, i) = 0.0;
			//k is the number (j to nA)
			for (int k = i + 1; k <= nA; k++)
				A(j, k) -= Lij*A(i, k);
			//change b
			for (int k = 1; k <= 1; k++)
				b(j, k) -= Lij*b(i, k);
		}
	}
	//U got , get x now, make A unit matrix and b should be the answer
	//from lowest to highest
	//i is the row to multiply
	for (int i = nA; i >= 1; i--) {
		//make A(i,i) to be one
		for (int k = 1; k <= 1; k++)
			b(i, k) /= A(i, i);
		A(i, i) = 1;
		//j is current row to make one
		for (int j = i - 1; j >= 1; j--) {
			//k is current number(nA to i - 1)
			for (int k = 1; k <= 1; k++)
				b(j, k) -= A(j, i)*b(i, k);
			A(j, i) = 0.0;
		}
	}
#undef A
#undef b
}
//For visualizing sparse matrix, we need to store them
template <class T, class V> void SparseStore(int n, T *r, T *c, V *v, const char *fn = NULL){
	FILE *fp = NULL;
	if(fn)
		fp = fopen(fn, "w");
	else
		fp = fopen("SparseTmp.txt", "w");
	if(fp == NULL){
		printf("File open error.\n");
		return;
	}
	for(int i = 0; i < n; i++){
		fprintf(fp, "%d\t%d\t%lf\n", r[i] + 1, c[i] + 1, v[i]);
	}
	fclose(fp);
	return;
}
#endif
