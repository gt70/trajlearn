/*	Learn from Casalino's linear approximation of control law
* 	Minpack is used to solve
*
*  For rendezvous case, estimate the cost, it shall be larger than required
* 	Algorithm:estimate rendezvous time according to \Delta \phi and average angular velocity
* 	Use planar and outer case solver to estimate cost, and compare with optimal solver
*/
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

#include "BasicApproximate.h"
#include "BasicDynFun.h"
#include "TigerTools.h"
#include "TigerMinpackSolver.h"
#include "Integrator.h"
#include <cstdio>
#include "../Tools/VecMat.h"
#include "stdlib.h"
#include "time.h"
#include "InitFile.h"
#include "../Tools/Constant.h"
#include "../Tools/OrbitFun.h"
#include "TOPTP2MPClass.h"
#include "StaticMembers.h"
#include "FastOptSolver.h"
using namespace std;
double mu = mhNU;
double T = 0.3 / (MUnit*AUnit);
double Ispg0 = 3000 * 9.80665 / VUnit;
double m0 = 2000 / MUnit;
TigerMinpackSolver::TigerMinpackSolver OuterSolver(2, 0);
TigerMinpackSolver::TigerMinpackSolver PlaneSolver(3, 0);
TigerMinpackSolver::TigerMinpackSolver ThetaSolver(1, 0);
TigerIntegrator::ODE45Integrator ODE45(12);
TigerMinpackSolver::TigerMinpackSolver MSolver(8, 0);
TigerConfig::Config cfg;

//Global parameters for fast iteration
double solx1[3], fvec1[3], solx2[2], fvec2[2];
//declaration of possible functions
int ThetaShoot(int n, const double *x, double *dx, int iflag, const double *para);
int TOPTShoot(int n, const double *x, double *fvec, int iflag, const double *para);
int main() {
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	srand(time(NULL));
	clock_t start, end;
	cfg.init("MyOwnTimeOptEsti.ini");
	//UpdateT
	double tin;
	cfg.getDoubleArray("T", &tin);
	T = tin / (MUnit*AUnit);
	double coe0[6], coef[6];//coe means classical orbital elements
	cfg.getDoubleArray("coe0", coe0);
	cfg.getDoubleArray("coef", coef);
	//set EstPara
	EstPara para;
	para.setWithCOE(coe0, coef);
	para.theta0 = fmod(para.theta0, D2PI);
	double guessdeltatheta;
	cfg.getDoubleArray("GuessDeltaTheta", &guessdeltatheta);
	para.thetaf = para.theta0 + guessdeltatheta;
	para.A = T / m0 / (mu / para.r / para.r);

	start = clock();
	//get matrix and initial lmd
	double lmdp[3] = { para.da, para.dex, para.dey }, lmdi[2] = { para.dhx, para.dhy }, M1[9], M2[4];
	MatGen(M1, M2, para.A, para.r, para.theta0, para.thetaf);
	Gauss(3, 1, M1, 3, lmdp, 1);
	Gauss(2, 1, M2, 2, lmdi, 1);

	double thetai0 = atan2(lmdi[1], lmdi[0]);
	//Solve one case to see if it's ok
	solx2[0] = thetai0;
	solx2[1] = para.A / 1.5;
	int flag = OuterSolver.Solve(OuterShootA, solx2, fvec2, (double*)(&para));
	printf("Initial Outer case flag = %d, norm %lf\n", flag, V_Norm2(fvec2, 2));

	double theta_ei = atan2(lmdp[2], lmdp[1]);
	para.dir = false;
	if (lmdp[0] < 0) {
		theta_ei = theta_ei + DPI;
		para.dir = true;
	}
	double Lambdai = 0.5;
	solx1[0] = Lambdai;
	solx1[1] = theta_ei;
	solx1[2] = para.A / 1.5;
	flag = PlaneSolver.Solve(PlaneShootA, solx1, fvec1, (double*)(&para));
	printf("Initial Plane case flag = %d, norm %lf\n", flag, V_Norm2(fvec1, 3));

	double mf = m0, m = (m0 + mf) / 2.;
	double thetaf, fvecf;
	double omega = sqrt(mu / pow(para.r, 3.0));
	thetaf = para.thetaf;
	//Iterate a few times to obatain converged mass
	for (int i = 0; i < 3; i++) {
		para.A = T / m / (mu / para.r / para.r);
		flag = ThetaSolver.Solve(ThetaShoot, &thetaf, &fvecf, (double*)(&para));
		if (fabs(fvecf) > 1e-6) {
			printf("May be wrong for thetashoot\n");
		}
		para.thetaf = thetaf;
		//disp time
		double time = (thetaf - para.theta0) / omega;
		//update m
		mf = m0 - time * T / Ispg0;
		m = (m0 + mf) / 2;
		double V = -Ispg0 * log(mf / m0);
		printf("time %lf thetaf %lf mf %lf V %lf\n", time, thetaf, mf, V);
	}
	end = clock();
	printf("Time cost:%lf\n", (double)(end - start) / CLOCKS_PER_SEC);
	//Optimal control
	printf("Try Optimal control\n");
	double sfpara[15] = { 0 };
	coe2ee(flag, sfpara, coe0, mu);
	sfpara[6] = m0;
	coe2ee(flag, sfpara + 7, coef, mu);
	double x[8], fvec[8];
	x[0] = para.da;
	x[1] = para.dex;
	x[2] = para.dey;
	x[3] = para.dhx;
	x[4] = para.dhy;
	MatGen(M1, M2, para.A, para.r, para.theta0, thetaf);
	Gauss(3, 1, M1, 3, x, 1);
	Gauss(2, 1, M2, 2, x + 3, 1);
	x[5] = 0;
	x[6] = 1;
	double norm = V_Norm2(x, 7);
	for (int i = 0; i < 7; i++)	x[i] /= norm;
	x[7] = (thetaf - para.theta0) / omega;
	V_Output(cout, x, 8);
	start = clock();
	MSolver.Solve(TOPTShoot, x, fvec, sfpara);
	end = clock();
	printf("Norm of fvec:%lf\n", V_Norm2(fvec, 8));
	V_Output(cout, x, 8);
	double massf = m0 - T / Ispg0*x[7];
	printf("Final time, mass, dV:%lf %lf %lf\n", x[7], massf, -Ispg0*log(massf / m0));
	
	printf("Time cost:%lf\n", (double)(end - start) / CLOCKS_PER_SEC);

	//test cost of random guess
	x[7] = guessdeltatheta / omega;
	for (int i = 0; i < 7; i++) {
		x[i] = (double)rand() / RAND_MAX - 0.5;
	}
	x[6] += 0.5;
	start = clock();
	MSolver.Solve(TOPTShoot, x, fvec, sfpara);
	end = clock();
	printf("Norm of fvec:%lf\n", V_Norm2(fvec, 8));
	printf("Final time, mass, dV:%lf %lf %lf\n", x[7], massf, -Ispg0*log(massf / m0));
	printf("Time cost:%lf\n", (double)(end - start) / CLOCKS_PER_SEC);
	sfpara[14] = 1;
	TOPTShoot(8, x, fvec, 1, sfpara);

	/***********************/
	//test fuel optimal solver
	start = clock();
	double trantime = x[7] * 1.3;
	coef[5] = f0dt2ft(flag, coef[5], trantime, coef[0], coef[1], mu);
	foptsolver::FOPTSolver solver(mu, T, Ispg0, 0.01);
	fastoptsolver::GuessGen(solver.x, coe0, coef, trantime, para);
	//solver.setNprint(0);
	flag = fastoptsolver::FOPTSolve(coe0, coef, m0, trantime, solver);
	end = clock();
	printf("FOPT flag = %d mass %lf using %lf\n", flag, m0, (double)(end - start)/CLOCKS_PER_SEC);
	/***********************/
	//test TOPTEstimate Class
	printf("Try using class\n");
	fastoptsolver::TOPTEstimate TOPTEST(mu, T, Ispg0);
	TOPTEST.estpara->setWithCOE(coe0, coef);
	m0 = 1.0;
	double time = TOPTEST.solve(m0, guessdeltatheta);
	printf("Transfer time %lf theta %lf\n", time, TOPTEST.estpara->thetaf - TOPTEST.estpara->theta0);
	system("pause");
	return 0;
}
//Shoot function for thetaf
//given thetaf, iterate to make norm([sinbeta, cosbeta]) == 1
int ThetaShoot(int n, const double *x, double *dx, int iflag, const double *para) {
	EstPara *estpara = (EstPara*)(para);
	//change thetaf
	estpara->thetaf = x[0];
	PlaneSolver.Solve(PlaneShootA, solx1, fvec1, (double*)estpara);
	OuterSolver.Solve(OuterShootA, solx2, fvec2, (double*)estpara);
	//check if success
	if (V_Norm2(fvec1, 3) > 1e-6 || solx1[0] > 1.001 || solx1[0] < -0.001) {
		printf("Planar case failed\n");
		return -1;
	}
	if (V_Norm2(fvec2, 2) > 1e-6) {
		printf("Outer case failed\n");
		return -1;
	}
	dx[0] = sqrt(solx2[1] * solx2[1] + solx1[2] * solx1[2]) - estpara->A;
	return 1;
}

//int DynFunMP(double t, const double* x, double* dx, const double* hpara);
//double HamP2MPMP(double t, const double *x, double lmd0, const double *hpara);
//Shoot function for TOPT transfer
//x stores lmd(6), lmd0, tf
//para stores init(7), final(7)
int TOPTShoot(int n, const double *x, double *fvec, int iflag, const double *para) {
	const double *lmd = x, lmd0 = x[6], tf = x[7];
	const double *start = para, *end = para + 7, m0 = para[6];
	double xx[12];
	double intpara[4] = { m0, T, Ispg0, mu };
	V_Copy(xx, start, 6);
	V_Copy(xx + 6, lmd, 6);
	double tin = 0, tout = tf;
	FILE *fp = NULL;
	if (para[14] == 1)
		fp = fopen("TOPTResult.txt", "w");
	int flag = ODE45.Integrate(basicdynfun::TOPTDynFun, xx, intpara, tin, tout, fp);
	if (fp)
		fclose(fp);
	if (flag != 1)
		return -1;
	double Hamf = basicdynfun::TOPTHamFun(tout, xx, lmd0, intpara);
	V_Minus(fvec, xx, end, 5);
	fvec[5] = xx[11];
	fvec[6] = Hamf;
	fvec[7] = V_Norm2(x, 7) - 1.0;
	return 1;
}
