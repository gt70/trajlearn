/*	Try to combine with estimation method
	Generate Time optimal or Fuel optimal results
	Turn into Class
*/
#include "BasicApproximate.h"
#include "TigerTools.h"
#include "FastOptSolver.h"
#include "BasicDynFun.h"
#include "TigerMinpackSolver.h"
#include "FOPTSolver.h"
#include "../Tools/OrbitFun.h"
#include <ctime>
#include <cstdlib>
//static const double D2PI = 6.283185307179586;
//static const double DPI = D2PI / 2.0;
namespace fastoptsolver {
	//Generate 
	int GuessGen(double *x, const double *coe0, const double *coef, const double trantime, EstPara &para) {
		double M1[9], M2[4];
		double thetaf = coef[3] + coef[4] + coef[5];
		x[0] = para.da;
		x[1] = para.dex;
		x[2] = para.dey;
		x[3] = para.dhx;
		x[4] = para.dhy;
		MatGen(M1, M2, para.A, para.r, para.theta0, para.thetaf);
		Gauss(3, 1, M1, 3, x, 1);
		Gauss(2, 1, M2, 2, x + 3, 1);
		x[5] = 0;//lmd_L
		x[6] = 0;//lmd_m
		x[7] = 1;//lmd0
		double norm = 0;
		for (int i = 0; i < 5; i++)	norm += x[i] * x[i];
		norm += 1.0;
		norm = sqrt(norm);
		for (int i = 0; i < 5; i++)	x[i] /= norm;
		x[7] /= norm;
		return 1;
	}
	int RandomGuessGen(double *x) {
		for (int i = 0; i < 6; i++) {
			x[i] = (double)rand() / RAND_MAX - .5;
		}
		x[6] = (double)rand() / RAND_MAX;
		x[7] = (double)rand() / RAND_MAX;
		double norm = 0;
		for (int i = 0; i < 8; i++)	norm += x[i] * x[i];
		norm = sqrt(norm);
		for (int i = 0; i < 8; i++)	x[i] /= norm;
		return 1;
	}
	int FOPTShoot(int n, const double *x, double *fvec, int iflag, const double *para);
	//Fopt solve, 
	int FOPTSolve(const double *coe0, const double *coef, double &m, const double trantime, foptsolver::FOPTSolver &solver) {
		//Transform from coe to ee, which is allocated inside solver
		int flag = 0;
		double *ee0 = solver.para, *eef = solver.para + 7;
		solver.para[14] = trantime;

		coe2ee(flag, ee0, coe0, solver.mu);
		coe2ee(flag, eef, coef, solver.mu);
		ee0[5] -= D2PI*floor(ee0[5] / D2PI);
		eef[5] -= D2PI*floor(eef[5] / D2PI);
		//if diff too big seems that have to add 2pi for the smaller one
		
		if (fabs(ee0[5] - eef[5]) > DPI) {
			if (ee0[5] < eef[5])
				ee0[5] += D2PI;
			else
				eef[5] += D2PI;
		}
		
		//set other para
		ee0[6] = m;
		flag = solver.MSolver->Solve(FOPTShoot, solver.x, solver.fvec, (double*)(&solver));
		if (solver.succeed()) {
			//solver.output = -1;
			//FOPTShoot(8, solver.x, solver.fvec, 1, (double*)(&solver));
			m = solver.mf;
			//solver.output = 0;
			return 1;
		}
		else
			return -1;
	}
	//Shoot function for TOPT transfer
	//x stores lmd(6), lmd0, tf
	//para stores init(7), final(7), trantime, eps, output, if output = -1, return m_f
	int FOPTShoot(int n, const double *x, double *fvec, int iflag, const double *para) {
		foptsolver::FOPTSolver *solver = (foptsolver::FOPTSolver *)para;
		const double *lmd = x, lmd0 = x[7];
		const double *start = solver->para, *end = solver->para + 7, eps = solver->epsilon;
		double tf = solver->para[14];
		int output = solver->output;
		double xx[14];
		double intpara[5] = { lmd0, eps, solver->Tmax, solver->Ispg0, solver->mu };
		V_Copy(xx, start, 7);
		V_Copy(xx + 7, lmd, 7);
		double tin = 0, tout = tf;
		FILE *fp = NULL;
		if (output == 1)
			fp = fopen(solver->filename.c_str(), "w");
		int flag = solver->ODE45->Integrate(basicdynfun::FOPTDynFun, xx, intpara, tin, tout, fp);
		if (fp)	fclose(fp);
		if (flag != 1)
			return -1;
		V_Minus(fvec, xx, end, 6);
		fvec[6] = xx[13];
		fvec[7] = V_Norm2(x, 8) - 1.0;
		//if (output == -1 || output == 1)
		solver->mf = xx[6];
		return 1;
	}
	//Fast evaluation, turn into subroutine to call later
	//return transfer time, if negative, means fail
	double TOPTEstimate::solve(double m0, double guessdeltatheta) {
		//get matrix and initial lmd
		double lmdp[3] = { estpara->da, estpara->dex, estpara->dey }, lmdi[2] = { estpara->dhx, estpara->dhy }, M1[9], M2[4];
		double thetaf = estpara->theta0 + guessdeltatheta;
		estpara->thetaf = thetaf;
		//set A
		estpara->setA(T, m0, mu);
		MatGen(M1, M2, estpara->A, estpara->r, estpara->theta0, thetaf);
		Gauss(3, 1, M1, 3, lmdp, 1);
		Gauss(2, 1, M2, 2, lmdi, 1);
		double thetai0 = atan2(lmdi[1], lmdi[0]);
		//Solve one case to see if it's ok
		solx2[0] = thetai0;
		solx2[1] = estpara->A / 1.5;
		int flag = OuterSolver.Solve(OuterShootA, solx2, fvec2, (double*)(estpara));
		//printf("Initial Outer case flag = %d, norm %lf\n", flag, V_Norm2(fvec2, 2));
		double theta_ei = atan2(lmdp[2], lmdp[1]);
		estpara->dir = false;
		if (lmdp[0] < 0) {
			theta_ei = theta_ei + DPI;
			estpara->dir = true;
		}
		double Lambdai = 0.5;
		solx1[0] = Lambdai;
		solx1[1] = theta_ei;
		solx1[2] = estpara->A / 1.5;
		flag = PlaneSolver.Solve(PlaneShootA, solx1, fvec1, (double*)(estpara));
		//printf("Initial Plane case flag = %d, norm %lf\n", flag, V_Norm2(fvec1, 3));

		double mf = m0, m = (m0 + mf) / 2.;
		double fvecf = 0, time = 0;
		double omega = sqrt(mu / pow(estpara->r, 3.0));
		thetaf = estpara->thetaf;
		//Iterate a few times to obatain converged mass
		for (int i = 0; i < 3; i++) {
			estpara->A = T / m / (mu / estpara->r / estpara->r);
			flag = ThetaSolver.Solve(ThetaShoot, &thetaf, &fvecf, (double*)(this));
			if (fabs(fvecf) > 1e-6 || flag == -1) {
				//printf("May be wrong for thetashoot\n");
				return -1;
			}
			estpara->thetaf = thetaf;
			//disp time
			time = (thetaf - estpara->theta0) / omega;
			//update m
			mf = m0 - time * T / Ispg0;
			m = (m0 + mf) / 2;
			double V = -Ispg0 * log(mf / m0);
			//printf("time %lf thetaf %lf mf %lf V %lf\n", time, thetaf, mf, V);
		}
		//printf("Time cost:%lf\n", (double)(end - start) / CLOCKS_PER_SEC);
		return time;
	}

	//Shoot function for thetaf
	//given thetaf, iterate to make norm([sinbeta, cosbeta]) == 1
	int ThetaShoot(int n, const double *x, double *dx, int iflag, const double *para) {
		TOPTEstimate *solver = (TOPTEstimate*)(para);
		//change thetaf
		solver->estpara->thetaf = x[0];
		solver->PlaneSolver.Solve(PlaneShootA, solver->solx1, solver->fvec1, (double*)solver->estpara);
		solver->OuterSolver.Solve(OuterShootA, solver->solx2, solver->fvec2, (double*)solver->estpara);
		//check if success
		double norm = 0;
		for (int i = 0; i < 3; i++)	norm += pow(solver->fvec1[i], 2.0);
		norm = sqrt(norm);
		if (norm > 1e-6 || solver->solx1[0] > 1.001 || solver->solx1[0] < -0.001) {
			//printf("Planar case failed\n");
			return -1;
		}
		norm = 0;
		for (int i = 0; i < 2; i++)	norm += pow(solver->fvec2[i], 2.0);
		norm = sqrt(norm);
		if (norm > 1e-6) {
			//printf("Outer case failed\n");
			return -1;
		}
		dx[0] = sqrt(solver->solx2[1] * solver->solx2[1] + solver->solx1[2] * solver->solx1[2]) - solver->estpara->A;
		return 1;
	}
}