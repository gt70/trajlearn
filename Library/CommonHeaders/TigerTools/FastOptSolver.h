/*	Try to combine with estimation method
Generate Time optimal or Fuel optimal results
Turn into Class
*/
#ifndef FASTOPTSOLVER_H
#define FASTOPTSOLVER_H
#include "BasicApproximate.h"
#include "FOPTSolver.h"
namespace fastoptsolver {
	int GuessGen(double *x, const double *coe0, const double *coef, const double trantime, EstPara &para);
	int RandomGuessGen(double *x);
	int FOPTSolve(const double *coe0, const double *coef, double &m, const double trantime, foptsolver::FOPTSolver &solver);
	int ThetaShoot(int n, const double *x, double *dx, int iflag, const double *para);


	typedef TigerMinpackSolver::TigerMinpackSolver TMS;
	//Define a class for fast evaluation of Casalino's approach
	class TOPTEstimate {
	public:
		TMS OuterSolver;
		TMS PlaneSolver;
		TMS ThetaSolver;
		EstPara *estpara;

		double solx1[3], solx2[2];
		double fvec1[3], fvec2[2];
		double T;
		double mu;
		double Ispg0;
		//methods
		TOPTEstimate(const double mu, const double T, const double Ispg0) :OuterSolver(2, 0), PlaneSolver(3, 0), ThetaSolver(1, 0) {
			estpara = new EstPara();
			this->T = T;
			this->mu = mu;
			this->Ispg0 = Ispg0;
		}
		double solve(double m0, double guessdeltatheta);
		~TOPTEstimate() {
			delete estpara;
		}
	};
}
#endif