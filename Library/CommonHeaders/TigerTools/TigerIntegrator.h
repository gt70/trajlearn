#ifndef TIGERINTEGRATOR_H
#define TIGERINTEGRATOR_H
namespace TigerIntegrator{
//a class which takes input of a functor which has definition of function int f(t, x, dx)
//That's all we need to do this, Probably I have to rewrite the ODE45 fun, but it should be fine
template<class T>
class Integrator{
public:
    int dim;
    double RelTol;
    double *AbsTol;
    double *work;
    int MaxIter;
    int NumPoint;
    int NormControl;
    double EventTol;
    double MaxStep;
    double InitialStep;
    T *Funtor;

    static const double A[7];
    static const double B[7][6];
    static const double E[7];

    Integrator(int n){
        dim = n;
        RelTol = 1e-10;
        EventTol = 1e-10;
        AbsTol = new double[n];
        work = new double[10*n];
        if(AbsTol == NULL || work == NULL){
            exit(0);
        }
        for(int i = 0;i < n;i++)    AbsTol[i] = 1e-10;
        MaxIter = 100000;
        MaxStep = -1;
        InitialStep = -1;
        NormControl = -1;
    };
    int Integrate(double*, double, double, FILE *);
    void onestep(double t0, double tf, double *x, double *xnew);
    void setDim(int n){
        if(dim < n){
            delete[] AbsTol;
            delete[] work;
            AbsTol = new double[n];
            work = new double[10*n];
            if(AbsTol == NULL || work == NULL){
                exit(0);
            }
            for(int i = 0;i < n;i++)	AbsTol[i] = 1e-10;
        }
        dim = n;
    }
    void setRelTol(double tol){
        RelTol = tol;
    }
    void setEventTol(double tol){
        EventTol = tol;
    }
    void setAbsTol(double *tol){
        for(int i = 0;i < dim;i++)	AbsTol[i] = tol[i];
    }
    void setAbsTol(double tol){
        for(int i = 0;i < dim;i++)	AbsTol[i] = tol;
    }
    void setMaxIter(int maxiter){
        MaxIter = maxiter;
    }
    void setMaxStep(double step){
        MaxStep = step;
    }
    void setInitialStep(double step){
        InitialStep = step;
    }
    ~Integrator(){
        delete[] AbsTol;
        delete[] work;
    }
};


template<class T>const double Integrator<T>::A[7] = {1.0/5.0, 3.0/10.0, 4.0/5.0, 8.0/9.0, 1.0, 1.0};
template<class T>const double Integrator<T>::B[7][6] = {{1.0/5.0,    3.0/40.0,    44.0/45.0,    19372.0/6561.0,        9017.0/3168.0,        35.0/384.0        },
    {0.0,        9.0/40.0,    -56.0/15.0,    -25360.0/2187.0,    -355.0/33.0,        0.0                },
    {0.0,        0.0,        32.0/9.0,    64448.0/6561.0,        46732.0/5247.0,        500.0/1113.0    },
    {0.0,        0.0,        0.0,        -212.0/729.0,        49.0/176.0,            125.0/192.0        },
    {0.0,        0.0,        0.0,        0.0,                -5103.0/18656.0,    -2187.0/6784.0    },
    {0.0,        0.0,        0.0,        0.0,                0.0,                11.0/84.0        },
    {0.0,        0.0,        0.0,        0.0,                0.0,                0.0                }};
template<class T>const double Integrator<T>::E[7]={71.0/57600.0, 0.0, -71.0/16695.0, 71.0/1920.0, -17253.0/339200.0, 22.0/525.0, -1.0/40.0};

static double eps(double t)
{
    if(t==0)
        return pow(2.0, -1074.0);
    else
        return pow(2.0, -52.0+floor(log(fabs(t))/log(2.0)));
}

template <class T> inline T MAX(T x, T y)
{
    return (x>y)?x:y;
}


//Çó×îÐ¡Öµ
template <class T> inline T MIN(T x, T y)
{
    return (x<y)?x:y;
}
//get 1, 2 and inf norm
static double norm(const double* vec, int n, int type)
{
    int i;
    double res=0.0;
    if(type==1)
    {
        for(i=0;i<n;i++)
        {
            if(vec[i]>=0)
                res+=vec[i];
            else
                res-=vec[i];
        }
    }
    else if(type==2)
    {
        for(i=0;i<n;i++)
            res+=vec[i]*vec[i];
        res=sqrt(res);
    }
    else
    {
        for(i=0;i<n;i++)
        {
            if(vec[i]>=0.0)
            {
                if(vec[i]>res)
                    res=vec[i];
            }
            else
            {
                if(-vec[i]>res)
                    res=-vec[i];
            }
        }
    }
    return res;
}
//Integrate one step further
template<class T>void Integrator<T>::onestep(double t0, double tf, double *x, double *xnew)
{
    double t = t0, h = tf - t0;
    int n = dim;//From Integrator
    double hB[7][6] = {0.0};
    for (int i = 0; i<7; i++)
        for (int j = i; j<6; j++)
            hB[i][j] = h*B[i][j];
    for (int k = 0; k<5; k++){
        for (int i = 0; i<n; i++){
            work[8 * n + i] = x[i];
            for (int j = 0; j <= k; j++) work[8 * n + i] += work[j*n + i] * hB[j][k];
        }
        int flag = Funtor->operator()(t + h*A[k], &work[8 * n], &work[n*(k + 1)]);
    }
    for (int i = 0; i<n; i++){
        xnew[i] = x[i];
        for (int j = 0; j <= 5; j++)
            xnew[i] += work[j*n + i] * hB[j][5];
    }
}


template<class T>int Integrator<T>::Integrate(double* x, double t0, double tf, FILE *fid){
    int flag=0, odeflag=0;
    int nsteps, nfailed, nfevals, tdir, nofailed;
    double rtol, normx, t, powth, hmax, hmin, absh, rh, h, tnew, normxnew, errwt, err, temp;
    int i, j, k;
    int n = dim;
    double* xnew=&work[9*n];
    double hB[7][6] = {0.0};
    flag=Funtor->operator()(t0, x, work);
    if(flag<1)
    { odeflag=0;goto Final;}
    nsteps  = 0;
    nfailed = 0;
    nfevals = 0;
    // Handle solver arguments
    tdir=(tf-t0) > 0 ? 1 : -1;
    rtol=RelTol;
    if(rtol<100.0*eps(1.0))
        rtol=100.0*eps(1.0);
    normx=0.0;
    work[7*n]=AbsTol[0]/rtol;
    if(NormControl)
        normx=norm(x,n,2);
    else
        for(i=1;i<n;i++)
            work[7*n+i]=AbsTol[i]/rtol;
    if(MaxStep>0.0)
        hmax=MIN(fabs(tf-t0),MaxStep);
    else
        hmax=MIN(fabs(tf-t0), fabs(0.1*(tf-t0)));
    nfevals=nfevals+1;
    t=t0;
    // Allocate memory if we're generating output.
    // alloc in chunks
    NumPoint = 1;
    if(fid)
    {
        fprintf(fid,"%22.14e",t);
        for(i=0;i<n;i++)
            fprintf(fid,"%24.14e",x[i]);
        fflush(fid);
    }

    // Initialize method parameters.
    powth=1.0/5.0;
    hmin=16.0*eps(t);
    if(InitialStep<=0.0)
    {
        // Compute an initial step size h using y'(t).
        absh=MIN(hmax, fabs(tf-t0));
        if(NormControl)
            rh=(norm(work,n,2)/MAX(normx,work[7*n]))/(0.8*pow(rtol, powth));
        else
        {
            for(i=0;i<n;i++)
                xnew[i]=work[i]/MAX(fabs(x[i]), work[7*n+i]);
            rh=norm(xnew, n, 3)/(0.8*pow(rtol, powth));
        }
            /*rh=norm(work,n,3)/max(fabs(x[i]),work[7*n+i])/(0.8*pow(rtol, powth));*/
        if(absh*rh>1.0)
            absh=1.0/rh;
        absh=MAX(absh, hmin);
    }
    else
        absh=MIN(hmax, MAX(hmin, InitialStep));

    // THE MAIN LOOP
    int done;
    done = 0;
    while(!done)
    {
        // By default, hmin is a small number such that t+hmin is only slightly
        // different than t.  It might be 0 if t is 0.
        hmin = 16.0*eps(t);
        absh = MIN(hmax, MAX(hmin, absh));    // couldn't limit absh until new hmin
        h = tdir * absh;

        // Stretch the step if within 10// of tf-t.
        if(1.1*absh >= fabs(tf - t))
        {
            h = tf - t;
            absh = fabs(h);
            done = 1;
        }

        // LOOP FOR ADVANCING ONE STEP.
        nofailed = 1;                      // no failed attempts
        while(1)
        {
            for(i=0;i<7;i++)
                for(j=i;j<6;j++)
                    hB[i][j]=h*B[i][j];
            for(k=0;k<5;k++)
            {
                for(i=0;i<n;i++)
                {
                    work[8*n+i]=x[i];
                    for(j=0;j<=k;j++) work[8*n+i]+=work[j*n+i]*hB[j][k];
                }
                flag=Funtor->operator()(t+h*A[k],&work[8*n],&work[n*(k+1)]);
                if(flag<1){ odeflag=0;goto Final;}
            }
            tnew=t+h*A[5];
            if(done)
                tnew=tf;   // Hit end point exactly.
            for(i=0;i<n;i++)
            {
                xnew[i]=x[i];
                for(j=0;j<=5;j++) xnew[i]+=work[j*n+i]*hB[j][5];
            }
            flag=Funtor->operator()(tnew,xnew,&work[6*n]);
            if(flag<1) { odeflag=0;goto Final;}

            nfevals = nfevals + 6;

            // Estimate the error.
            for(i=0;i<n;i++)
            {
                work[8*n+i]=0.0;
                for(j=0;j<7;j++) work[8*n+i]+=work[j*n+i]*E[j];
            }
            if(NormControl)
            {
                normxnew = norm(xnew, n, 2);
                errwt = MAX(MAX(normx,normxnew),work[7*n]);
                err = absh * (norm(&work[8*n], n, 2) / errwt);
            }
            else
            {
                for(i=0;i<n;i++)
                    work[8*n+i]/=MAX(MAX(fabs(x[i]),fabs(xnew[i])),work[7*n+i]);
                err = absh * norm(&work[8*n], n, 3);
            }
            if(err>rtol)                      // Failed step
            {
                nfailed = nfailed + 1;
                if(absh <= hmin)
                {
                    printf("Failure at t=%e.,...Unable to meet integration tolerances without reducing the step size below the smallest value allowed (%e) at time t.",t,hmin);
                    odeflag=0;
                    goto Final;
                }

                if(nofailed)
                {
                    nofailed = false;
                    absh = MAX(hmin, absh * MAX(0.1, 0.8*pow(rtol/err, powth)));
                }
                else
                    absh = MAX(hmin, 0.5 * absh);
                h = tdir * absh;
                done = false;
            }
            else        // Successful step
                break;
        }
        nsteps = nsteps + 1;
        if(nsteps>MaxIter)
        {odeflag=0;goto Final;}
        NumPoint = NumPoint + 1;
        if(fid)
        {
            fprintf(fid,"\n");
            fprintf(fid,"%22.14e",tnew);
            for(i=0;i<n;i++)
                fprintf(fid,"%24.14e",xnew[i]);
            fflush(fid);
        }
        if(done)
            break;
        // If there were no failures compute a new h.
        if(nofailed)
        {
            // Note that absh may shrink by 0.8, and that err may be 0.
            temp = 1.25*pow(err/rtol,powth);
            if(temp > 0.2)
                absh = absh / temp;
            else
                absh = 5.0*absh;
        }

        // Advance the integration one step.
        t = tnew;
        for(i=0;i<n;i++) x[i] = xnew[i];
        if(NormControl)
            normx = normxnew;
        for(i=0;i<n;i++) work[i]= work[6*n+i];  // Already have f(tnew,ynew)
    }
    for(i=0;i<n;i++) x[i] = xnew[i];
    odeflag=1;
Final:
    return odeflag;
}

//End of namespace
}
template<class T>
using Integrator = TigerIntegrator::Integrator<T>;
#endif
