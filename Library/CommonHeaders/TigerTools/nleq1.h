/* Simply write a user interface for such a stupid program
 */

#include "stdio.h"

typedef double real;
typedef void(*FUN)(int *N, real *x, real *f, int *ifail);
typedef void(*JACFUN)(int *N, int *ldjac, real *x, real *jac, int *ifail);
//Declare type of function
#ifdef __cplusplus
extern "C" {
#endif
void nleq1_(int *N, FUN fcn, JACFUN jfun, real *x, real *xscal, real *rtol, int *iopt, int *ierr, int *liwk, int *iwk, int *lrwk, real *rwk);

#ifdef __cplusplus
}
#endif

//Define a class to solve this problem?
class NLEQ1{
public:
    int N_;
    FUN fun_;
    JACFUN jfun_;
    real *x_, *xscal_, *f_;
    real rtol_;
    int iopt_[50];
    int ierr_, liwk_, lrwk_;
    int *iwk_;
    real *rwk_;
    NLEQ1(int N):N_(N){
        x_ = new real[3 * N_];
        f_ = x_ + N_;
        xscal_ = f_ + N_;
        rtol_ = 1e-10;
        liwk_ = N + 50;
        iwk_ = new int[liwk_];
        ierr_ = -1;
        int nbroy = N > 10 ? N : 10;
        lrwk_ = (N + nbroy + 13)*N + 61;
        rwk_ = new real[lrwk_];
        //set default parameters
        for(int i = 0; i < 50; i++) iopt_[i] = 0;
        for(int i = 0; i < liwk_; i++) iwk_[i] = 0;
        for(int i = 0; i < lrwk_; i++) rwk_[i] = 0;
        for(int i = 0; i < N_; i++) xscal_[i] = 0;
        setiopt(3, 1);//User defined Jac
        setiopt(4, 0);//Dense Jacobian
        setiopt(9, 1);//Turn off scaling?

    }
    void enableprint(){
        setiopt(11, 3);
        setiopt(12, 6);
        setiopt(13, 3);
        setiopt(14, 6);
        setiopt(15, 1);
        setiopt(16, 6);
        setiopt(19, 1);
        setiopt(20, 6);
    }
    void disableprint(){
        setiopt(11, 0);
        setiopt(13, 0);
        setiopt(15, 0);
        setiopt(19, 0);
    }
    void setx(double *x){
        for(int i = 0; i < N_; i++) x_[i] = x[i];
    }
    void getx(double *x){
        for(int i = 0; i < N_; i++) x[i] = x_[i];
    }
    void setxscale(double *xscale){
        for(int i = 0; i < N_; i++) xscal_[i] = xscale[i];
    }
    void setrtol(double rtol){rtol_ = rtol;}
    //watch out, fortran style setting is used here
    void setiopt(int index, int val){iopt_[index - 1] = val;}
    void solve(){
        nleq1_(&N_, fun_, jfun_, x_, xscal_, &rtol_, iopt_, &ierr_, &liwk_, iwk_, &lrwk_, rwk_);
    }
    void funeval(){
        fun_(&N_, x_, f_, &ierr_);
    }
    void funeval(double *x){
        fun_(&N_, x, f_, &ierr_);
    }
    void funeval(double *x, double *f){
        fun_(&N_, x, f, &ierr_);
    }
    bool solved(double tol = 1e-5){
        double norm2 = 0;
        fun_(&N_, x_, f_, &ierr_);
        for(int i = 0; i < N_; i++)
            norm2 += f_[i] * f_[i];
        if(sqrt(norm2) < tol)
            return true;
        else
            return false;
    }
    ~NLEQ1(){
        delete[] x_;
        delete[] iwk_;
        delete[] rwk_;
    }

};
/*
//Define the function and jac function

void fun(int *N, real *x, real *f, int *ifail){
    f[0] = x[0];
    f[1] = x[1];
}
void jfun(int *N, int *ldjac, real *x, real *jac, int *ifail){
    jac[0] = 1;
    jac[1] = 0;
    jac[2] = 0;
    jac[3] = 1;
}

//Write a function to call it
int main(){
    NLEQ1 nleq(2);
    nleq.fun_ = fun;
    nleq.jfun_ = jfun;
    double x0[2] = {1, 1};
    nleq.setx(x0);
    nleq.solve();
    //get x
    nleq.getx(x0);
    printf("Sol is %lf %lf\n", x0[0], x0[1]);
    return 1;
}
*/
