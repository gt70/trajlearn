#ifndef P2PFOPTCLASS_H
#define P2PFOPTCLASS_H
#include "Tools/orbit.h"
#include <string>
class P2PFOPTCLASS {
public:
	double state0[6];
	double statef[6];
	double x[8];
	double lmdf[7];
	double m0;
	double trant;
	double eps;
	double mf;
	double T;
	double IG;
	double mu;
	double outflag;
	int maxiter;
	int miniter;
	double ftol;
	double *tmpdata = NULL;
	std::string str = "tmp";

	P2PFOPTCLASS() {
		mf = -1;
		ftol = 1e-10;
		outflag = 0;
		maxiter = 100;
		miniter = 3;
	}
	P2PFOPTCLASS(P2PFOPTCLASS &PP):m0(PP.m0), trant(PP.trant), eps(PP.eps), T(PP.T), IG(PP.IG), mu(PP.mu){
		mf = -1;
		ftol = 1e-10;
		outflag = 0;
		maxiter = 100;
		miniter = 3;
	}
	void setState0(double *s0) { for (int i = 0; i < 6; i++) state0[i] = s0[i]; }
	void setStatef(double *sf) { for (int i = 0; i < 6; i++) statef[i] = sf[i]; }
	~P2PFOPTCLASS() { if (tmpdata) delete[] tmpdata; }
};
//Class for GA only
//including MOrbit, EstGAMJD, RMin, muGA
class GACLASS {
public:
	TigerOrbit::Morbit Morb;
	double EstGAMJD, RMin, mu;
	double vinfn[3], vinfp[3];
	double delta, RRad, tmpm;//GA data
};
//Class for a trajectory with multiple GA
//Store with Pointers to them and initialize class with GANum
class P2PFOPTGACLASS : public P2PFOPTCLASS {
public:
	int n;
	GACLASS *GA;
	//P2PFOPTCLASS *P2P;
	/*double state0[6], statef[6];
	double MJD0, m0, trant, eps, mf, T, IG, mu, outflag;
	int maxiter, miniter;
	double ftol;*/
	double MJD0;
	double TUnit, DAY;
	bool WarmStart;
	//set an array to store estgadate not mjd
	double *GATime;
	//an array to store solution
	double *x;
	P2PFOPTGACLASS(int n):n(n) {
		DAY = 86400;
		GA = new GACLASS[n];
		GATime = new double[n];
		if (n == 1)
			x = new double[8 + 7 * n];
		else
			x = new double[11 * n + 4];
	}
	P2PFOPTGACLASS(int n, P2PFOPTCLASS &PP):n(n), P2PFOPTCLASS(PP) {
		DAY = 86400;
		GA = new GACLASS[n];
		GATime = new double[n];
		if (n == 1)
			x = new double[8 + 7 * n];
		else
			x = new double[11 * n + 4];
	}
	~P2PFOPTGACLASS() { delete[] GA; delete[] GATime; delete[] x; }
};
#define UseHam_No
//Function to generate Equality Constraints for Free or Minimum flyby case
//Here 0 means before GA, f means after GA
//fvec include: VR(6), GACon(6), H, lmdm
//SecRCon means if assign R constraint at t_p, UseHr means use Hr to generate Fac
//Generate 15 or 11 constraints
template<class T>int FlybyConGen(double *Con, const double *s0, const double *sf, P2PFOPTGACLASS *RP, int GAInd, double MJD, double *opara, bool isFree, bool isMul, bool UseHr, bool SecRCon, T outflag = 0) {
	double stateIn[12], stateOut[12], RVGA[6], Nz[3], Northo[3], Nv[3], vinfn[3], vinfp[3], mIn, lmdmIn, lmdmOut, mOut;
	double *lmdrin = stateIn + 6, *lmdvin = stateIn + 9, *lmdrout = stateOut + 6, *lmdvout = stateOut + 9;
	double *fvec = Con;
	int flag = 0;
	//Transfer from EE to RV
	ee2rv(flag, stateIn, s0, RP->mu);
	basicdynfun::AdjointTranEEToRV(s0, s0 + 7, lmdrin, RP->mu);
	mIn = s0[6];
	lmdmIn = s0[13];
#ifndef UseHam
	double RhoIn = basicdynfun::RVFOPTRho(mIn, lmdmIn, lmdvin, opara);
	//double RhoIn = basicdynfun::rho(s0, s0 + 7, opara);
#else
	double Hn = basicdynfun::FOPTHamFun(0, s0, opara);
#endif
	//Transfer from EE to RV for s0
	ee2rv(flag, stateOut, sf, RP->mu);
	basicdynfun::AdjointTranEEToRV(sf, sf + 7, lmdrout, RP->mu);
	mOut = sf[6];
	lmdmOut = sf[13];
#ifndef UseHam
	double RhoOut = basicdynfun::RVFOPTRho(mOut, lmdmOut, lmdvout, opara);
	//double RhoOut = basicdynfun::rho(sf, sf + 7, opara);
#else
	double Hp = basicdynfun::FOPTHamFun(0, sf, opara);
#endif
	//Assign fvec or R
	RP->GA[GAInd].Morb.calcxyz(MJD, RVGA, RP->mu, RP->TUnit);
#ifdef UseHam
	double AMars[3], dlmd[6], *RVMars = RVGA;
	double RPow3 = pow(RVMars[0] * RVMars[0] + RVMars[1] * RVMars[1] + RVMars[2] * RVMars[2], 1.5);
	for (int i = 0; i < 3; i++)	AMars[i] = -RP->mu*RVMars[i] / RPow3;
	V_Minus(dlmd, lmdrout, lmdrin, 6);
	double dH = Hn - Hp + V_Dot(dlmd, RVMa*/rs + 3, 3) + V_Dot(dlmd + 3, AMars, 3);
#endif
	V_Minus(fvec, stateIn, RVGA, 3);
	if (SecRCon)
		V_Minus(fvec + 3, stateOut, RVGA, 3);
	else
		fvec -= 3;
	//Assign constraint of equal vinf
	V_Minus(vinfn, stateIn + 3, RVGA + 3, 3);
	V_Minus(vinfp, stateOut + 3, RVGA + 3, 3);
#ifndef UseHam
	double uIn = 2 * RP->eps / (RhoIn + 2 * RP->eps + sqrt(RhoIn*RhoIn + 4 * RP->eps*RP->eps));
	double uOut = 2 * RP->eps / (RhoOut + 2 * RP->eps + sqrt(RhoOut*RhoOut + 4 * RP->eps*RP->eps));
	double HrIn = opara[0] * RP->T / RP->IG*RhoIn*uIn - RP->eps*RP->T/RP->IG*log(uIn*(1-uIn)) + V_Dot(lmdrin, vinfn, 3); //We ignore if U might be zero
	double HrOut = opara[0] * RP->T / RP->IG*RhoOut*uOut - RP->eps*RP->T / RP->IG*log(uOut*(1 - uOut)) + V_Dot(lmdrout, vinfp, 3); //We ignore if U might be zero
#endif
	double Nvinfn = V_Norm2(vinfn, 3), Nvinfp = V_Norm2(vinfp, 3);
	double Nlmdvn = V_Norm2(lmdvin, 3), Nlmdvp = V_Norm2(lmdvout, 3);
	fvec[6] = Nvinfn - Nvinfp;//Equality of NVinf
	//Calc Three axis of lmdv
	V_Cross(Nz, vinfn, vinfp);
	Normalize(Nz, 3);
	V_Copy(Nv, vinfn, 3);
	Normalize(Nv, 3);
	V_Cross(Northo, Nz, Nv);
	double lmdvorthoIn = V_Dot(lmdvin, Northo, 3);
	double lmdvvIn = V_Dot(lmdvin, Nv, 3);

	V_Copy(Nv, vinfp, 3);
	Normalize(Nv, 3);
	V_Cross(Northo, Nz, Nv);
	double lmdvorthoOut = V_Dot(lmdvout, Northo, 3);
	double lmdvvOut = V_Dot(lmdvout, Nv, 3);
	fvec[7] = V_Dot(lmdvin, Nz, 3) / Nlmdvn;//perpendicular
	fvec[8] = V_Dot(lmdvout, Nz, 3) / Nlmdvp;//perpendicular
	double Fac = 1;
	if (isMul) {
		if (UseHr)
#ifndef UseHam
			Fac = HrOut / HrIn;
#else
			Fac = 0;
#endif
		else
			Fac = 1 - lmdmOut;
	}
	if (isFree) {
		//Null of ortho lmdv
		fvec[9] = lmdvorthoIn / Nlmdvn;
		fvec[10] = lmdvorthoOut / Nlmdvp;
		//Equality of lmdvv
		fvec[11] = lmdvvOut - Fac * lmdvvIn;
	}
	else {
		//Equality
		fvec[9] = Fac * lmdvorthoIn - lmdvorthoOut;
		double sinhalfdelta = 1 / (1 + RP->GA[GAInd].RMin*Nvinfn*Nvinfn / RP->GA[GAInd].mu);
		double cosdelta = 1 - 2 * sinhalfdelta*sinhalfdelta;
		double tanhalfdelta = sinhalfdelta / sqrt(1 - sinhalfdelta*sinhalfdelta);
		double A = -2 * tanhalfdelta / (1 + RP->GA[0].mu / RP->GA[0].RMin / Nvinfn / Nvinfn);
		fvec[10] = lmdvvOut - Fac * (lmdvvIn - 2 * A*lmdvorthoIn);
		fvec[11] = V_Dot(vinfn, vinfp, 3) - Nvinfn*Nvinfp*cosdelta;
	}
	//Concerning m amd lmdm
	if (SecRCon)
		fvec[12] = mIn - mOut;
	else
		fvec -= 1;
	if (isMul) {
		fvec[13] = lmdmIn;//lmdm be zero to satisfy
		if (UseHr)
			fvec[14] = 1 - lmdmOut - Fac;
		else
#ifndef UseHam
			fvec[14] = HrOut - Fac * HrIn;
#else
			fvec[14] = 0;
#endif
	}
	else {
		fvec[13] = lmdmIn - lmdmOut;
#ifndef UseHam
		fvec[14] = HrOut - HrIn;
#else
		fvec[14] = dH;
#endif
	}
	if (outflag != 0) {
		RP->GA[GAInd].delta = acos(V_Dot(vinfn, vinfp, 3) / Nvinfp / Nvinfn);
		RP->GA[GAInd].RRad = (1 / sin(RP->GA[0].delta / 2) - 1) * RP->GA[0].mu / Nvinfn / Nvinfp;
		RP->GA[GAInd].tmpm = mIn;
		V_Copy(RP->GA[GAInd].vinfn, vinfn, 3);
		V_Copy(RP->GA[GAInd].vinfp, vinfp, 3);
	}
	return 1;
}

//Function to generate Equality Constraints for Free or Minimum flyby case
//Here 0 means before GA, f means after GA
//fvec include: VR(6), GACon(6), H, lmdm
//SecRCon means if assign R constraint at t_p, UseHr means use Hr to generate Fac
//Generate 15 or 11 constraints
template<class T>int FlybyConGenBang(double *Con, const double *s0, const double *sf, P2PFOPTGACLASS *RP, int GAInd, double MJD, double *opara, bool isFree, bool isMul, bool SecRCon, T outflag = 0) {
	double stateIn[12], stateOut[12], RVGA[6], Nz[3], Northo[3], Nv[3], vinfn[3], vinfp[3], mIn, lmdmIn, lmdmOut, mOut;
	double *lmdrin = stateIn + 6, *lmdvin = stateIn + 9, *lmdrout = stateOut + 6, *lmdvout = stateOut + 9;
	double *fvec = Con;
	int flag = 0;
	//Transfer from EE to RV
	ee2rv(flag, stateIn, s0, RP->mu);
	basicdynfun::AdjointTranEEToRV(s0, s0 + 7, lmdrin, RP->mu);
	mIn = s0[6];
	lmdmIn = s0[13];
	double RhoIn = basicdynfun::RVFOPTRho(mIn, lmdmIn, lmdvin, opara);
	//Transfer from EE to RV for s0
	ee2rv(flag, stateOut, sf, RP->mu);
	basicdynfun::AdjointTranEEToRV(sf, sf + 7, lmdrout, RP->mu);
	mOut = sf[6];
	lmdmOut = sf[13];
	double RhoOut = basicdynfun::RVFOPTRho(mOut, lmdmOut, lmdvout, opara);
	//Assign fvec or R
	RP->GA[GAInd].Morb.calcxyz(MJD, RVGA, RP->mu, RP->TUnit);
	V_Minus(fvec, stateIn, RVGA, 3);
	if (SecRCon)
		V_Minus(fvec + 3, stateOut, RVGA, 3);
	else
		fvec -= 3;
	//Assign constraint of equal vinf
	V_Minus(vinfn, stateIn + 3, RVGA + 3, 3);
	V_Minus(vinfp, stateOut + 3, RVGA + 3, 3);
	double uIn = RhoIn < 0 ? 1 : 0;
	double uOut = RhoOut < 0 ? 1 : 0;
	double HrIn = opara[0] * RP->T / RP->IG*RhoIn*uIn + V_Dot(lmdrin, vinfn, 3);
	double HrOut = opara[0] * RP->T / RP->IG*RhoOut*uOut  + V_Dot(lmdrout, vinfp, 3);
	double Nvinfn = V_Norm2(vinfn, 3), Nvinfp = V_Norm2(vinfp, 3);
	double Nlmdvn = V_Norm2(lmdvin, 3), Nlmdvp = V_Norm2(lmdvout, 3);
	fvec[6] = Nvinfn - Nvinfp;//Equality of NVinf
							  //Calc Three axis of lmdv
	V_Cross(Nz, vinfn, vinfp);
	Normalize(Nz, 3);
	V_Copy(Nv, vinfn, 3);
	Normalize(Nv, 3);
	V_Cross(Northo, Nz, Nv);
	double lmdvorthoIn = V_Dot(lmdvin, Northo, 3);
	double lmdvvIn = V_Dot(lmdvin, Nv, 3);

	V_Copy(Nv, vinfp, 3);
	Normalize(Nv, 3);
	V_Cross(Northo, Nz, Nv);
	double lmdvorthoOut = V_Dot(lmdvout, Northo, 3);
	double lmdvvOut = V_Dot(lmdvout, Nv, 3);
	fvec[7] = V_Dot(lmdvin, Nz, 3) / Nlmdvn;//perpendicular
	fvec[8] = V_Dot(lmdvout, Nz, 3) / Nlmdvp;//perpendicular
	double Fac = 1;
	if (isMul) {
		Fac = 1 - lmdmOut;
	}
	if (isFree) {
		//Null of ortho lmdv
		fvec[9] = lmdvorthoIn / Nlmdvn;
		fvec[10] = lmdvorthoOut / Nlmdvp;
		//Equality of lmdvv
		fvec[11] = lmdvvOut - Fac * lmdvvIn;
	}
	else {
		//Equality
		fvec[9] = Fac * lmdvorthoIn - lmdvorthoOut;
		double sinhalfdelta = 1 / (1 + RP->GA[GAInd].RMin*Nvinfn*Nvinfn / RP->GA[GAInd].mu);
		double cosdelta = 1 - 2 * sinhalfdelta*sinhalfdelta;
		double tanhalfdelta = sinhalfdelta / sqrt(1 - sinhalfdelta*sinhalfdelta);
		double A = -2 * tanhalfdelta / (1 + RP->GA[0].mu / RP->GA[0].RMin / Nvinfn / Nvinfn);
		fvec[10] = lmdvvOut - Fac * (lmdvvIn - 2 * A*lmdvorthoIn);
		fvec[11] = V_Dot(vinfn, vinfp, 3) - Nvinfn*Nvinfp*cosdelta;
	}
	//Concerning m amd lmdm
	if (SecRCon)
		fvec[12] = mIn - mOut;
	else
		fvec -= 1;
	if (isMul) {
		fvec[13] = lmdmIn;//lmdm be zero to satisfy
		fvec[14] = HrOut - Fac * HrIn;
	}
	else {
		fvec[13] = lmdmIn - lmdmOut;
		fvec[14] = HrOut - HrIn;
	}
	if (outflag != 0) {
		RP->GA[GAInd].delta = acos(V_Dot(vinfn, vinfp, 3) / Nvinfp / Nvinfn);
		RP->GA[GAInd].RRad = (1 / sin(RP->GA[0].delta / 2) - 1) * RP->GA[0].mu / Nvinfn / Nvinfp;
		RP->GA[GAInd].tmpm = mIn;
		V_Copy(RP->GA[GAInd].vinfn, vinfn, 3);
		V_Copy(RP->GA[GAInd].vinfp, vinfp, 3);
	}
	return 1;
}
#endif