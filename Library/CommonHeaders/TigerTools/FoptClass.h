/*	Header file for two primary classes
* 	Ability for parallel calculation has not been tested yet
*/
#ifndef FOPTCLASS_H
#define FOPTCLASS_H
#include "Integrator.h"
#include "TigerMinpackSolver.h"
#include "../Tools/VecMat.h"
namespace optsolver {
	class FSolver {
	public:
		FSolver() {
			ODE45 = new TigerIntegrator::ODE45Integrator(14);
			MinpackSolver = new TigerMinpackSolver::TigerMinpackSolver(8, 1);
			output = 0;
			epsilon = 0.01;
		};
		~FSolver() {
			delete ODE45;
			delete MinpackSolver;
		}
		//int Solve(double *x0, const double *coe0, const double *coef, const double tf, const double m0);
		int Solve(double *x0, const double *coe0, const double *coef, const double tf, const double m0, const char* filename = NULL);
		int BangSolve(double *x0, const double *coe0, const double *coef, const double tf, const double m0, const char* filename = NULL);
		void GenRandom(double *x0);
		//Solve several times and return best result
		int SolveUntil(const int miniter, const int maxiter, double *bestx, const double *coe0, const double *coef, const double tf, const double m0);
		double GetMass(const double *x0, const double eps = 0, const char *filename = NULL);//Remember to change eps

		static int DynFunMP(double t, const double* x, double* dx, const double* dfpara);
		static int ThrustDyn(double t, const double* x, double* dx, const double* hpara);
		static int CoastDyn(double t, const double* x, double* dx, const double* hpara);
		static double Rho(const double* x, const double* hpara);
		static double dRhodt(const double* x, const double* hpara);
		static int SFun(double t, const double *x, const double *para, double &fval, double &fgrad, int calcflag);
		bool Succeed() { return V_Norm2(fvec, 8) < 1e-6; };

		double ee0[6];
		double eef[6];
		double m0;//Record initial mass
		double tf;//Record transfer length
		double ThrustEnd;//Record Last thrust
		double mf;//Record Last Mass
		double fvec[8];
		char filename[20];
		double epsilon;

		int output;
		TigerIntegrator::ODE45Integrator *ODE45;//One ODE solver, with 12 dimension allowed
		TigerMinpackSolver::TigerMinpackSolver *MinpackSolver;//Nonlinear solver, with lmd0

		static double Tmax;
		static double Ispg0;
		static double mu;

		//Methods, set static parameters
		static void setTmax(const double T) { Tmax = T; };
		static void setIspg0(const double IG) { Ispg0 = IG; };
		static void setMu(const double Mu) { mu = Mu; };
	};
}
#endif
