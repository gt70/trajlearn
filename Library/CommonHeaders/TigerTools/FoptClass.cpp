/*	Dev by tiger at 06.04.2015
* 	Class, including, all in one class
* 	analytical Jacobian is not considered here
* 	fueloptimal solver with ee and homotopic form
* 	fueloptimal solver with bangbang structure
*/
#include "Integrator.h"
#include "TigerMinpackSolver.h"
#include "../Tools/VecMat.h"
#include "../Tools/OrbitFun.h" 
#include <cmath>
#include "string.h"
#include "FoptClass.h"
double L0dt2Lt(double L0, double dt, const double* ee);
namespace optsolver {
	//hpara contains:lmd0, eps
	int FSolver::DynFunMP(double t, const double* x, double* dx, const double* hpara) {
		const double P = x[0], ex = x[1], ey = x[2], hx = x[3], hy = x[4], L = x[5], m = x[6];
		const double *lmd = x + 7, lmdm = x[13];
		const double lmd0 = hpara[0], epsilon = hpara[1];
		const double t1 = 0.1e1 / mu;
		const double t3 = sqrt(t1 * P);
		const double t5 = cos(L);
		const double t7 = sin(L);
		const double t9 = t5 * ex + t7 * ey + 0.1e1;
		const double t10 = 0.1e1 / t9;
		const double t11 = Tmax * t10;
		const double t13 = 0.1e1 / m;
		const double t14 = epsilon * t13;
		const double t15 = t3 * lmd[1];
		const double t17 = t3 * lmd[2];
		const double t19 = -t7 * t15 + t5 * t17;
		const double t20 = t19 * t19;
		const double t21 = t3 * lmd[0];
		const double t22 = t10 * P;
		const double t25 = ex + t5;
		const double t27 = t10 * t25 + t5;
		const double t29 = ey + t7;
		const double t31 = t10 * t29 + t7;
		const double t33 = -t27 * t15 - t31 * t17 - 0.2e1 * t22 * t21;
		const double t34 = t33 * t33;
		const double t35 = t10 * lmd[1];
		const double t38 = t7 * hx - t5 * hy;
		const double t39 = t38 * t3;
		const double t42 = t10 * lmd[2];
		const double t45 = t10 * lmd[3];
		const double t46 = hx * hx;
		const double t47 = hy * hy;
		const double t48 = t46 + t47 + 0.1e1;
		const double t49 = t48 * t3 / 0.2e1;
		const double t50 = t5 * t49;
		const double t52 = t10 * lmd[4];
		const double t53 = t7 * t49;
		const double t55 = t10 * lmd[5];
		const double t57 = -ex * t39 * t42 + ey * t39 * t35 - t39 * t55 - t50 * t45 - t53 * t52;
		const double t58 = t57 * t57;
		const double t60 = sqrt(t20 + t34 + t58);
		const double t62 = 0.1e1 / lmd0;
		const double t64 = t62 * t13 * t60 * Ispg0;
		const double t65 = t62 * lmdm;
		const double t68 = pow(0.1e1 - t64 - t65, 0.2e1);
		const double t69 = epsilon * epsilon;
		const double t72 = sqrt(t68 + 0.4e1 * t69);
		const double t74 = 0.1e1 / (0.1e1 - t64 - t65 + 0.2e1 * epsilon + t72);
		const double t75 = 0.1e1 / t60;
		const double t76 = t75 * t74;
		const double t78 = t33 * t76 * t14;
		const double t81 = t7 * t3;
		const double t82 = t13 * Tmax;
		const double t84 = t74 * epsilon;
		const double t85 = t19 * t75;
		const double t86 = t85 * t84;
		const double t87 = t86 * t82 * t81;
		const double t90 = t33 * t75;
		const double t91 = t90 * t84;
		const double t93 = t3 * t10;
		const double t95 = Tmax * ey * t38;
		const double t97 = t57 * t76;
		const double t98 = t97 * t14;
		const double t101 = t5 * t3;
		const double t103 = t86 * t82 * t101;
		const double t108 = Tmax * ex * t38;
		const double t113 = Tmax * t5 * t48 / 0.2e1;
		const double t118 = Tmax * t7 * t48 / 0.2e1;
		const double t122 = 0.1e1 / P;
		const double t124 = sqrt(t122 * mu);
		const double t125 = t9 * t9;
		const double t126 = t125 * t124;
		const double t128 = Tmax * t38;
		const double t130 = t98 * t128 * t93;
		const double t137 = 0.1e1 / t3;
		const double t141 = t74 * t14;
		const double t142 = t1 * t90;
		const double t152 = t1 * t85 * t84;
		const double t156 = t142 * t84;
		const double t158 = t137 * t10;
		const double t160 = t57 * t75;
		const double t161 = t1 * t160;
		const double t162 = t161 * t141;
		const double t184 = P * P;
		const double t192 = t13 * t128;
		const double t199 = 0.1e1 / t125;
		const double t202 = Tmax * t199 * P * t21;
		const double t207 = t199 * t25;
		const double t213 = t3 * t199;
		const double t214 = t95 * t213;
		const double t215 = t5 * t160;
		const double t216 = t215 * t141;
		const double t225 = t108 * t213;
		const double t229 = t199 * lmd[3];
		const double t230 = t5 * t5;
		const double t234 = t97 * epsilon * t82;
		const double t237 = t199 * lmd[4];
		const double t239 = t118 * t3 * t237;
		const double t242 = t9 * t124;
		const double t245 = t192 * t213;
		const double t260 = t7 * t160;
		const double t261 = t260 * t141;
		const double t265 = t199 * t29;
		const double t275 = t113 * t3 * t229;
		const double t278 = t7 * t7;
		const double t296 = hx * t3;
		const double t313 = hy * t3;
		const double t326 = -t7 * ex + t5 * ey;
		const double t337 = t326 * t160;
		const double t338 = t337 * t141;
		const double t342 = t5 * hx + t7 * hy;
		const double t383 = m * m;
		dx[0] = 0.4e1 * t78 * t11 * P * t3;
		dx[1] = 0.2e1 * t91 * t82 * t27 * t3 - 0.2e1 * t98 * t95 * t93 + 0.2e1 * t87;
		dx[2] = 0.2e1 * t91 * t82 * t31 * t3 + 0.2e1 * t98 * t108 * t93 - 0.2e1 * t103;
		dx[3] = 0.2e1 * t98 * t113 * t93;
		dx[4] = 0.2e1 * t98 * t118 * t93;
		dx[5] = t122 * t126 + 0.2e1 * t130;
		dx[6] = -0.2e1 * Tmax / Ispg0 * t84;
		dx[7] = -0.2e1 * t142 * t141 * Tmax * t22 * t137 * lmd[0] - 0.4e1 * t78 * t11 * t21 - (t152 * t82 * t7 * t137 + t156 * t82 * t27 * t137 - t162 * t95 * t158) * lmd[1] - (-t152 * t82 * t5 * t137 + t156 * t82 * t31 * t137 + t162 * t108 * t158) * lmd[2] - t162 * t113 * t137 * t45 - t162 * t118 * t137 * t52 - (-mu / t184 / P * t125 / t124 / 0.2e1 - 0.1e1 / t184 * t126 + t161 * t84 * t192 * t158) * lmd[5];
		dx[8] = 0.4e1 * t5 * t90 * t141 * t202 - 0.2e1 * (t91 * t82 * (-t5 * t207 + t10) * t3 + t216 * t214) * lmd[1] - 0.2e1 * (-t78 * Tmax * t5 * t199 * t29 * t3 - t216 * t225 + t130) * lmd[2] + 0.2e1 * t234 * t230 * t49 * t229 + 0.2e1 * t216 * t239 - 0.2e1 * (t5 * t122 * t242 - t215 * t84 * t245) * lmd[5];
		dx[9] = 0.4e1 * t7 * t90 * t141 * t202 - 0.2e1 * (-t78 * Tmax * t7 * t199 * t25 * t3 + t261 * t214 - t130) * lmd[1] - 0.2e1 * (t91 * t82 * (-t7 * t265 + t10) * t3 - t261 * t225) * lmd[2] + 0.2e1 * t261 * t275 + 0.2e1 * t234 * t278 * t49 * t237 - 0.2e1 * (t7 * t122 * t242 - t260 * t84 * t245) * lmd[5];
		dx[10] = -0.2e1 * t98 * Tmax * t81 * t55 - 0.2e1 * t234 * ex * t81 * t42 + 0.2e1 * t234 * ey * t81 * t35 - 0.2e1 * t234 * t5 * t296 * t45 - 0.2e1 * t234 * t7 * t296 * t52;
		dx[11] = 0.2e1 * t98 * Tmax * t101 * t55 + 0.2e1 * t234 * ex * t101 * t42 - 0.2e1 * t234 * ey * t101 * t35 - 0.2e1 * t234 * t5 * t313 * t45 - 0.2e1 * t234 * t7 * t313 * t52;
		dx[12] = 0.4e1 * t326 * t90 * t141 * t202 - 0.2e1 * (t103 + t91 * t82 * (-t7 * t10 - t326 * t207 - t7) * t3 + t338 * t214 - t98 * Tmax * ey * t342 * t93) * lmd[1] - 0.2e1 * (t87 + t91 * t82 * (t5 * t10 - t326 * t265 + t5) * t3 - t338 * t225 + t98 * Tmax * ex * t342 * t93) * lmd[2] + 0.2e1 * t338 * t275 + 0.2e1 * t234 * t53 * t45 + 0.2e1 * t338 * t239 - 0.2e1 * t234 * t50 * t52 - 0.2e1 * (t98 * Tmax * t342 * t93 + t326 * t122 * t242 - t337 * t84 * t245) * lmd[5];
		dx[13] = -0.2e1 / t383 * t60 * Tmax * t84;
		return 1;
	}
	//hpara contains:
	int FSolver::ThrustDyn(double t, const double* x, double* dx, const double* hpara) {
		const double P = x[0], ex = x[1], ey = x[2], hx = x[3], hy = x[4], L = x[5], m = x[6];
		const double *lmd = x + 7, lmdm = x[13];
		const double t1 = 0.1e1 / mu;
		const double t3 = sqrt(P * t1);
		const double t5 = cos(L);
		const double t7 = sin(L);
		const double t9 = t5 * ex + t7 * ey + 0.1e1;
		const double t10 = 0.1e1 / t9;
		const double t12 = 0.1e1 / m;
		const double t13 = t12 * Tmax;
		const double t14 = t3 * lmd[1];
		const double t16 = t3 * lmd[2];
		const double t18 = -t7 * t14 + t5 * t16;
		const double t19 = t18 * t18;
		const double t20 = t3 * lmd[0];
		const double t21 = t10 * P;
		const double t24 = ex + t5;
		const double t26 = t10 * t24 + t5;
		const double t28 = ey + t7;
		const double t30 = t10 * t28 + t7;
		const double t32 = -t26 * t14 - t30 * t16 - 0.2e1 * t21 * t20;
		const double t33 = t32 * t32;
		const double t34 = t10 * lmd[1];
		const double t37 = t7 * hx - t5 * hy;
		const double t38 = t37 * t3;
		const double t41 = t10 * lmd[2];
		const double t44 = t10 * lmd[3];
		const double t45 = hx * hx;
		const double t46 = hy * hy;
		const double t47 = t45 + t46 + 0.1e1;
		const double t48 = t47 * t3 / 0.2e1;
		const double t49 = t5 * t48;
		const double t51 = t10 * lmd[4];
		const double t52 = t7 * t48;
		const double t54 = t10 * lmd[5];
		const double t56 = -ex * t38 * t41 + ey * t38 * t34 - t38 * t54 - t49 * t44 - t52 * t51;
		const double t57 = t56 * t56;
		const double t59 = sqrt(t19 + t33 + t57);
		const double t60 = 0.1e1 / t59;
		const double t61 = t32 * t60;
		const double t62 = t61 * t13;
		const double t65 = t7 * t3;
		const double t67 = t60 * t12;
		const double t68 = t18 * t67;
		const double t69 = t68 * Tmax * t65;
		const double t72 = t32 * t67;
		const double t74 = t3 * t10;
		const double t75 = ey * t37;
		const double t77 = t56 * t60;
		const double t78 = t77 * t13;
		const double t81 = t5 * t3;
		const double t83 = t68 * Tmax * t81;
		const double t87 = ex * t37;
		const double t97 = 0.1e1 / P;
		const double t99 = sqrt(t97 * mu);
		const double t100 = t9 * t9;
		const double t101 = t100 * t99;
		const double t104 = t78 * t37 * t74;
		const double t108 = 0.1e1 / t3;
		const double t120 = t1 * t18 * t67;
		const double t125 = t1 * t32 * t67;
		const double t127 = t108 * t10;
		const double t130 = t1 * t77 * t13;
		const double t144 = t47 * t108 / 0.2e1;
		const double t155 = P * P;
		const double t163 = Tmax * t37;
		const double t172 = 0.1e1 / t100;
		const double t174 = t172 * P * t20;
		const double t179 = t172 * t24;
		const double t185 = t3 * t172;
		const double t186 = t75 * t185;
		const double t188 = t5 * t77 * t13;
		const double t196 = t87 * t185;
		const double t200 = t172 * lmd[3];
		const double t202 = t5 * t5;
		const double t204 = t56 * t67;
		const double t207 = t172 * lmd[4];
		const double t208 = t52 * t207;
		const double t210 = t9 * t99;
		const double t214 = t163 * t185;
		const double t230 = t7 * t77 * t13;
		const double t234 = t172 * t28;
		const double t243 = t49 * t200;
		const double t246 = t7 * t7;
		const double t261 = t204 * ey * Tmax;
		const double t265 = t204 * ex * Tmax;
		const double t267 = hx * t3;
		const double t279 = hy * t3;
		const double t289 = -t7 * ex + t5 * ey;
		const double t301 = t289 * t77 * t13;
		const double t305 = t5 * hx + t7 * hy;
		const double t341 = m * m;
		dx[0] = 0.2e1 * t62 * t10 * t3 * P;
		dx[1] = t72 * Tmax * t26 * t3 - t78 * t75 * t74 + t69;
		dx[2] = t72 * Tmax * t30 * t3 + t78 * t87 * t74 - t83;
		dx[3] = t78 * t5 * t47 * t74 / 0.2e1;
		dx[4] = t78 * t7 * t47 * t74 / 0.2e1;
		dx[5] = t97 * t101 + t104;
		dx[6] = -Tmax / Ispg0;
		dx[7] = -t1 * t61 * t13 * t21 * t108 * lmd[0] - 0.2e1 * t62 * t10 * t20 - (t120 * Tmax * t7 * t108 + t125 * Tmax * t26 * t108 - t130 * t75 * t127) * lmd[1] / 0.2e1 - (-t120 * Tmax * t5 * t108 + t125 * Tmax * t30 * t108 + t130 * t87 * t127) * lmd[2] / 0.2e1 - t130 * t5 * t144 * t44 / 0.2e1 - t130 * t7 * t144 * t51 / 0.2e1 - (-mu / t155 / P * t100 / t99 / 0.2e1 - 0.1e1 / t155 * t101 + t1 * t56 * t67 * t163 * t127 / 0.2e1) * lmd[5];
		dx[8] = 0.2e1 * t5 * t61 * t13 * t174 - (t72 * Tmax * (-t5 * t179 + t10) * t3 + t188 * t186) * lmd[1] - (-t62 * t5 * t172 * t28 * t3 - t188 * t196 + t104) * lmd[2] + t204 * Tmax * t202 * t48 * t200 + t188 * t208 - (-t5 * t56 * t67 * t214 + 0.2e1 * t5 * t97 * t210) * lmd[5];
		dx[9] = 0.2e1 * t7 * t61 * t13 * t174 - (-t62 * t7 * t172 * t24 * t3 + t230 * t186 - t104) * lmd[1] - (t72 * Tmax * (-t7 * t234 + t10) * t3 - t230 * t196) * lmd[2] + t230 * t243 + t204 * Tmax * t246 * t48 * t207 - (-t7 * t56 * t67 * t214 + 0.2e1 * t7 * t97 * t210) * lmd[5];
		dx[10] = -t188 * t267 * t44 - t230 * t267 * t51 + t261 * t65 * t34 - t265 * t65 * t41 - t78 * t65 * t54;
		dx[11] = -t188 * t279 * t44 - t230 * t279 * t51 - t261 * t81 * t34 + t265 * t81 * t41 + t78 * t81 * t54;
		dx[12] = 0.2e1 * t289 * t61 * t13 * t174 - (t83 + t72 * Tmax * (-t7 * t10 - t289 * t179 - t7) * t3 + t301 * t186 - t78 * ey * t305 * t74) * lmd[1] - (t69 + t72 * Tmax * (t5 * t10 - t289 * t234 + t5) * t3 - t301 * t196 + t78 * ex * t305 * t74) * lmd[2] + t301 * t243 + t230 * t48 * t44 + t301 * t208 - t188 * t48 * t51 - (-t289 * t56 * t67 * t214 + 0.2e1 * t289 * t97 * t210 + t78 * t305 * t74) * lmd[5];
		dx[13] = -0.1e1 / t341 * Tmax * t59;
		return 1;
	}
	//hpara can be NULL
	int FSolver::CoastDyn(double t, const double* x, double* dx, const double* hpara) {
		const double P = x[0], ex = x[1], ey = x[2], hx = x[3], hy = x[4], L = x[5], m = x[6];
		const double *lmd = x + 7, lmdm = x[13];
		const double t1 = 0.1e1 / P;
		const double t3 = sqrt(t1 * mu);
		const double t4 = cos(L);
		const double t6 = sin(L);
		const double t8 = t4 * ex + t6 * ey + 0.1e1;
		const double t9 = t8 * t8;
		const double t10 = t9 * t3;
		const double t14 = P * P;
		const double t24 = t3 * lmd[5];
		const double t25 = t1 * t8;
		dx[0] = 0;
		dx[1] = 0;
		dx[2] = 0;
		dx[3] = 0;
		dx[4] = 0;
		dx[5] = (t1 * t10);
		dx[6] = 0;
		dx[7] = -((-mu / t14 / P * t9 / t3 / 0.2e1 - 0.1e1 / t14 * t10) * lmd[5]);
		dx[8] = -(0.2e1 * t4 * t25 * t24);
		dx[9] = -(0.2e1 * t6 * t25 * t24);
		dx[10] = 0;
		dx[11] = 0;
		dx[12] = -(0.2e1 * (-t6 * ex + t4 * ey) * t25 * t24);
		dx[13] = 0;
		return 1;
	}
	//hpara:lmd0
	double FSolver::Rho(const double* x, const double* hpara) {
		const double P = x[0], ex = x[1], ey = x[2], hx = x[3], hy = x[4], L = x[5], m = x[6];
		const double *lmd = x + 7, lmdm = x[13];
		const double lmd0 = hpara[0];
		const double t3 = sqrt(P / mu);
		const double t4 = t3 * lmd[1];
		const double t5 = sin(L);
		const double t7 = t3 * lmd[2];
		const double t8 = cos(L);
		const double t11 = pow(-t5 * t4 + t8 * t7, 0.2e1);
		const double t16 = 0.1e1 / (t8 * ex + t5 * ey + 0.1e1);
		const double t29 = pow(-0.2e1 * t16 * P * t3 * lmd[0] - (t8 + t16 * (ex + t8)) * t4 - (t5 + t16 * (ey + t5)) * t7, 0.2e1);
		const double t34 = (t5 * hx - t8 * hy) * t3;
		const double t41 = hx * hx;
		const double t42 = hy * hy;
		const double t44 = (t41 + t42 + 0.1e1) * t3 / 0.2e1;
		const double t53 = pow(-ex * t34 * t16 * lmd[2] + ey * t34 * t16 * lmd[1] - t5 * t44 * t16 * lmd[4] - t8 * t44 * t16 * lmd[3] - t34 * t16 * lmd[5], 0.2e1);
		const double t55 = sqrt(t11 + t29 + t53);
		const double t58 = 0.1e1 / lmd0;
		const double t62 = 0.1e1 - t58 / m * t55 * Ispg0 - t58 * lmdm;
		return t62;
	}
	double FSolver::dRhodt(const double* x, const double* hpara) {
		const double P = x[0], ex = x[1], ey = x[2], hx = x[3], hy = x[4], L = x[5], m = x[6];
		const double *lmd = x + 7, lmdm = x[13];
		const double lmd0 = hpara[0];
		const double t3 = sqrt(P / mu);
		const double t4 = t3 * lmd[1];
		const double t5 = sin(L);
		const double t7 = t3 * lmd[2];
		const double t8 = cos(L);
		const double t10 = -t5 * t4 + t8 * t7;
		const double t11 = t10 * t10;
		const double t12 = t3 * lmd[0];
		const double t15 = t8 * ex + t5 * ey + 0.1e1;
		const double t16 = 0.1e1 / t15;
		const double t20 = ex + t8;
		const double t22 = t16 * t20 + t8;
		const double t24 = ey + t5;
		const double t26 = t16 * t24 + t5;
		const double t28 = -0.2e1 * t16 * P * t12 - t22 * t4 - t26 * t7;
		const double t29 = t28 * t28;
		const double t30 = t16 * lmd[1];
		const double t33 = t5 * hx - t8 * hy;
		const double t34 = t33 * t3;
		const double t35 = ey * t34;
		const double t37 = t16 * lmd[2];
		const double t38 = ex * t34;
		const double t40 = t16 * lmd[3];
		const double t41 = hx * hx;
		const double t42 = hy * hy;
		const double t43 = t41 + t42 + 0.1e1;
		const double t44 = t43 * t3 / 0.2e1;
		const double t45 = t8 * t44;
		const double t47 = t16 * lmd[4];
		const double t48 = t5 * t44;
		const double t50 = t16 * lmd[5];
		const double t52 = t35 * t30 - t34 * t50 - t38 * t37 - t45 * t40 - t48 * t47;
		const double t53 = t52 * t52;
		const double t55 = sqrt(t11 + t29 + t53);
		const double t57 = 0.1e1 / t55 * Ispg0;
		const double t60 = 0.1e1 / lmd0 / m;
		const double t66 = t15 * t15;
		const double t67 = 0.1e1 / t66;
		const double t71 = -t5 * ex + t8 * ey;
		const double t95 = (t8 * hx + t5 * hy) * t3;
		const double t124 = 0.1e1 / P;
		const double t126 = sqrt(t124 * mu);
		const double t137 = P * P;
		const double t151 = t3 * t10;
		const double t153 = t3 * t28;
		const double t155 = t16 * t52;
		const double t160 = t126 * lmd[5];
		const double t161 = t124 * t15;
		const double t182 = -t124 * t66 * t126 * ((-t8 * t4 - t5 * t7) * t10 + (0.2e1 * t71 * t67 * P * t12 - (-t71 * t67 * t20 - t5 * t16 - t5) * t4 - (-t71 * t67 * t24 + t8 * t16 + t8) * t7) * t28 + (-t71 * ey * t33 * t3 * t67 * lmd[1] + ey * t95 * t30 + t71 * ex * t33 * t3 * t67 * lmd[2] - ex * t95 * t37 + t71 * t8 * t43 * t3 * t67 * lmd[3] / 0.2e1 + t48 * t40 + t71 * t5 * t43 * t3 * t67 * lmd[4] / 0.2e1 - t45 * t47 + t71 * t34 * t67 * lmd[5] - t95 * t50) * t52) * t60 * t57 - 0.2e1 * (-mu / t137 / P * t66 / t126 / 0.2e1 - 0.1e1 / t137 * t66 * t126) * t50 * P * t3 * t28 * t60 * t57 + 0.2e1 * t8 * t161 * t160 * (-t5 * t151 - t22 * t153 + t35 * t155) * t60 * t57 + 0.2e1 * t5 * t161 * t160 * (t8 * t151 - t26 * t153 - t38 * t155) * t60 * t57 - 0.2e1 * t71 * t124 * t126 * lmd[5] * t34 * t52 * t60 * t57;
		return t182;
	}
	int FSolver::SFun(double t, const double *x, const double *para, double &fval, double &fgrad, int calcflag) {
		if (calcflag == 0) {	//Only rho needed
			fval = FSolver::Rho(x, para);
		}
		else {
			fval = FSolver::Rho(x, para);
			fgrad = FSolver::dRhodt(x, para);
		}
		return 1;
	}

	//x:lam0,lam1-6,dt
	//para:this pointer
	int ShootFunFoptEE(int n, const double *x, double *fvec, int iflag, const void* para) {
		const FSolver *that = (FSolver *)para;
		double x0[14] = { 0.0 };
		double dfpara[2] = { x[7], that->epsilon };
		double m0 = that->m0;
		int outflag = that->output;
		FILE *fp = NULL;
		if (outflag > 0) {
			fp = fopen(that->filename, "w");
		}
		V_Copy(x0, that->ee0, 6);
		x0[6] = m0;
		V_Copy(x0 + 7, x, 7);
		//Integrate
		int flag = that->ODE45->Integrate(FSolver::DynFunMP, x0, dfpara, 0.0, that->tf, fp);
		if (fp)	fclose(fp);
		if (flag<1)
			return -1;
		V_Minus(fvec, x0, that->eef, 6);
		fvec[6] = x0[13];
		fvec[7] = V_Norm2(x, 8) - 1.0;
		return 0;
	}
	//x:lam0,lam1-6,dt
	//para:this pointer
	int BangShootFoptEE(int n, const double *x, double *fvec, int iflag, const void* para) {
		FSolver *that = (FSolver*)para;
		double x0[14] = { 0.0 };
		double dfpara[2] = { x[7], that->epsilon };
		double m0 = that->m0;
		int output = that->output;
		FILE *fp = NULL;
		if (output > 0)	fp = fopen(that->filename, "w");

		V_Copy(x0, that->ee0, 6);
		x0[6] = m0;
		V_Copy(x0 + 7, x, 7);
		//Integrate
		//Use Rho to judge starting
		double rho = that->Rho(x0, dfpara);
		bool ThrustLeg = (rho < 0);
		int eventflag = 0;
		int eventdir = ThrustLeg ? 1 : -1;
		int flag = 0;
		double tf = that->tf;
		double tin = 0, tout = tf;
		double thrustend = 0;
		while (1) {
			if (ThrustLeg) {
				flag = that->ODE45->Integrate(FSolver::ThrustDyn, x0, dfpara, FSolver::SFun, dfpara, eventflag, eventdir, tin, tout, fp);
				thrustend = tout;
				//SFun(tout, xx, para, fval, fgrad, 1);
				//if(output > 0){
				//	cout << "Exit Thrust Mode\n";
				//	printf("At %22.14e rho = %22.14e drho = %22.14e\n", tout, fval, fgrad);
				//}
			}
			else {
				flag = that->ODE45->Integrate(FSolver::CoastDyn, x0, dfpara, FSolver::SFun, dfpara, eventflag, eventdir, tin, tout, fp);
				//SFun(tout, xx, para, fval, fgrad, 1);
				//if(output > 0){
				//	cout << "Exit Coast Mode\n";
				//	printf("At %22.14e rho = %22.14e drho = %22.14e\n", tout, fval, fgrad);
				//}
			}
			if (fp)	fprintf(fp, "\n");
			//Disp current 
			//flag = ODE45Huge.Integrate(SwitchDyn, xx, para, SFun, para, eventflag, eventdir, tin, tout, fp);
			if (fabs(tout - tf) < 1e-10)	break;
			ThrustLeg = !ThrustLeg;
			eventdir = -eventdir;
			tin = tout;
			tout = tf;
		}

		V_Minus(fvec, x0, that->eef, 6);
		fvec[6] = x0[13];
		fvec[7] = V_Norm2(x, 8) - 1.0;
		that->ThrustEnd = thrustend;
		return 0;
	}

	void FSolver::GenRandom(double *x0) {
		for (int i = 0; i < 8; i++)
			x0[i] = (double)rand() / RAND_MAX - 0.5;
		x0[6] += 0.5;
		x0[7] += 0.5;
	}

	double FSolver::GetMass(const double *x, const double eps, const char *filename) {
		double x0[14] = { 0.0 };
		double dfpara[2] = { x[7], eps };
		//double m0 = that -> m0;	//Not necessary
		FILE *fp = NULL;
		if (filename != NULL) {
			fp = fopen(filename, "w");
		}
		V_Copy(x0, ee0, 6);
		x0[6] = m0;
		V_Copy(x0 + 7, x, 7);
		//Integrate
		if (eps != 0)
			int flag = ODE45->Integrate(FSolver::DynFunMP, x0, dfpara, 0.0, tf, fp);
		else {
			double rho = Rho(x0, dfpara);
			bool ThrustLeg = (rho < 0);
			int eventflag = 0;
			int eventdir = ThrustLeg ? 1 : -1;
			int flag = 0;
			double tin = 0, tout = tf;
			double thrustend = 0;
			while (1) {
				if (ThrustLeg) {
					flag = ODE45->Integrate(FSolver::ThrustDyn, x0, dfpara, FSolver::SFun, dfpara, eventflag, eventdir, tin, tout, fp);
					ThrustEnd = tout;
				}
				else {
					flag = ODE45->Integrate(FSolver::CoastDyn, x0, dfpara, FSolver::SFun, dfpara, eventflag, eventdir, tin, tout, fp);
				}
				if (fp)	fprintf(fp, "\n");
				if (fabs(tout - tf) < 1e-10)	break;
				ThrustLeg = !ThrustLeg;
				eventdir = -eventdir;
				tin = tout;
				tout = tf;
			}
		}
		if (fp)	fclose(fp);
		return x0[6];
	}

	int FSolver::Solve(double *x0, const double *coe0, const double *coef, const double tf, const double m0, const char* filename) {
		//coetrans(tempv, coe0);
		int info = 0;
		coe2ee(info, ee0, coe0, mu);
		ee0[5] -= D2PI*floor(ee0[5] / D2PI);
		//coetrans(tempv, coe1);
		coe2ee(info, eef, coef, mu);
		eef[5] -= D2PI*floor(eef[5] / D2PI);
		//if diff too big seems that have to add 2pi for the smaller one
		if (fabs(ee0[5] - eef[5]) > DPI) {
			if (ee0[5] < eef[5])
				ee0[5] += D2PI;
			else
				eef[5] += D2PI;
		}
		eef[5] = L0dt2Lt(eef[5], tf, eef);

		this->m0 = m0;
		this->tf = tf;
		if (filename != NULL) {
			this->output = 1;
			strcpy(this->filename, filename);
			ShootFunFoptEE(8, x0, this->fvec, 1, this);
			this->output = 0;
			return 1;
		}
		info = this->MinpackSolver->Solve(ShootFunFoptEE, x0, this->fvec, (double*)this);
		return info;
	}

	//Solve several times and return best result
	int FSolver::SolveUntil(const int miniter, const int maxiter, double *bestx, const double *coe0, const double *coef, const double tf, const double m0) {
		double mostmass = -10000, currentmass = 0;
		double xguess[8];
		int okiter = 0;
		bool succeed = false;
		for (int i = 0; i < maxiter; i++) {
			GenRandom(xguess);
			Solve(xguess, coe0, coef, tf, m0, NULL);
			if (Succeed()) {
				currentmass = GetMass(xguess, epsilon, NULL);
				if (currentmass > mostmass) {
					V_Copy(bestx, xguess, 8);
					succeed = true;
					okiter++;
					if (okiter >= miniter) {
						break;
					}
				}
			}
		}
		if (succeed)
			return 1;
		else
			return -1;
	}


	int FSolver::BangSolve(double *x0, const double *coe0, const double *coef, const double tf, const double m0, const char* filename) {
		//coetrans(tempv, coe0);
		int info = 0;
		coe2ee(info, ee0, coe0, mu);
		ee0[5] -= D2PI*floor(ee0[5] / D2PI);
		//coetrans(tempv, coe1);
		coe2ee(info, eef, coef, mu);
		eef[5] -= D2PI*floor(eef[5] / D2PI);
		//if diff too big seems that have to add 2pi for the smaller one
		if (fabs(ee0[5] - eef[5]) > DPI) {
			if (ee0[5] < eef[5])
				ee0[5] += D2PI;
			else
				eef[5] += D2PI;
		}
		eef[5] = L0dt2Lt(eef[5], tf, eef);

		this->m0 = m0;
		this->tf = tf;
		if (filename != NULL) {
			this->output = 1;
			strcpy(this->filename, filename);
			BangShootFoptEE(8, x0, this->fvec, 1, this);
			this->output = 0;
			return 1;
		}
		info = this->MinpackSolver->Solve(BangShootFoptEE, x0, this->fvec, (double*)this);
		return info;
	}
	//End of namespace optsolver
}
/*
double FSolver::Tmax = TmaxNU;
double FSolver::Ispg0 = IspPg0NU;
double FSolver::mu = mhNU;
int main(){
srand(time(NULL));
double coe0[6] = {3.0, 0, 0, 0, 0, 0};
double coef[6] = {3.1, 0, 0, 0, 0, 0.05};
//allocate
TigerIntegrator::ODE45Integrator ODE45(14);
ODE45.setRelTol(1e-10);
ODE45.setAbsTol(1e-10);
TigerMinpackSolver::TigerMinpackSolver Min(8, 0);
Min.setXtol(1e-7);
FSolver Solver(&ODE45, &Min);

double x0[8];
clock_t start, end;
start = clock();
int maxiter = 20, succeed = 0;
double result, m0 = 0.8, tf = 5;
for(int i = 0;i < maxiter;i++){
for(int j = 0;j < 8;j++)	x0[j] = (double)rand()/RAND_MAX - 0.5;
x0[6] += 0.5;
x0[7] += 0.5;
double norm = V_Norm2(x0, 8);
for(int j = 0;j < 8;j++)	x0[j] /= norm;
int flag = Solver.Solve(x0, coe0, coef, tf, m0);
if(Solver.Succeed()){
//Solver.Solve(x0, coe0, coef, tf, m0, "HomoResult.txt");
//exit(0);
Solver.BangSolve(x0, coe0, coef, tf, m0);
if(Solver.Succeed()){
succeed++;
Solver.BangSolve(x0, coe0, coef, tf, m0, "BangResult.txt");
break;
}
}
}
end = clock();
cout << "Time cost : " << (double)(end - start)/CLOCKS_PER_SEC << endl;
cout << "Succeed " << succeed << " times" << endl;
cout << "Endtime = " << Solver.ThrustEnd << endl;
//cout << flag << endl;
//cout << "Time = " << x0[7] << endl;
//cout << "mf = " << m0 - x0[7]*TmaxNU/IspPg0NU << endl;
return 0;
}
*/
//
