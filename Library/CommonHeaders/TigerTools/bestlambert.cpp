//#include "BestLambert.h"
#include "../Tools/OrbitFun.h"
#include "../Tools/VecMat.h"
#include <algorithm>
//Find best lambert transfer
//v1 and v2 are initial velocity for objective calculation
//dv1 and dv2 are impulse
void BestLambert(int &flag, double *r1, double *r2, double t, double *v1, double *v2, int mincircle, int maxcircle, double *dv1, double *dv2, double &a, double &e, double mu, bool output) {
	int trynum = 2 * (maxcircle - mincircle + 1);
	double *Vdv1 = new double[3 * trynum];
	double *Vdv2 = new double[3 * trynum];
	double *Normv = new double[trynum];
	//double *Normv2 = new double[trynum];
	double *Va = new double[trynum];
	double *Ve = new double[trynum];
	double tmpv1[3], tmpv2[3];
	int realcalcnum = 0;
	int flag1, flag2;
	for (int i = 0; i <= maxcircle - mincircle; i++) {
		lambert(tmpv1, tmpv2, Va[2 * i], Ve[2 * i], r1, r2, t, 0, flag1, mu, 0, i, 0);
		realcalcnum++;
		if (flag1 == 1) {
			V_Minus(Vdv1 + 3 * (2 * i), tmpv1, v1, 3);
			V_Minus(Vdv2 + 3 * (2 * i), v2, tmpv2, 3);
			Normv[2 * i] = V_Norm2(Vdv1 + 3 * (2 * i), 3) + V_Norm2(Vdv2 + 3 * (2 * i), 3);
		}
		else {
			Normv[2 * i] = 100000;
		}
		lambert(tmpv1, tmpv2, Va[2 * i + 1], Ve[2 * i + 1], r1, r2, t, 0, flag2, mu, 0, i, 1);
		realcalcnum++;
		if (flag2 == 1) {
			V_Minus(Vdv1 + 3 * (2 * i + 1), tmpv1, v1, 3);
			V_Minus(Vdv2 + 3 * (2 * i + 1), v2, tmpv2, 3);
			Normv[2 * i + 1] = V_Norm2(Vdv1 + 3 * (2 * i + 1), 3) + V_Norm2(Vdv2 + 3 * (2 * i + 1), 3);
		}
		else {
			Normv[2 * i + 1] = 100000;
		}
		//if  both flag = 0, then jumpout
		if ((flag1 != 1) && (flag2 != 1)) {
			if (i == 0)
				flag = -1;
			break;
		}
	}
	//Compare Normv to get best normv and others
	double *mindv = min_element(Normv, Normv + realcalcnum);
	int rank = mindv - Normv;
	//get best dv1 and dv2 and a and e
	V_Copy(dv1, Vdv1 + 3 * rank, 3);
	V_Copy(dv2, Vdv2 + 3 * rank, 3);
	a = Va[rank];
	e = Ve[rank];
	delete[] Vdv1;
	delete[] Vdv2;
	delete[] Normv;
	delete[] Va;
	delete[] Ve;
}
