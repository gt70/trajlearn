/*	Header file for a list of functions for approximation
*/
#ifndef BASICAPPROXIMATE_H
#define BASICAPPROXIMATE_H
#include <cmath>
static const double DTPI = 6.283185307179586;
//static const double DPI = 3.141592653589793;
class EstPara {
public:
	double theta0;
	double thetaf;
	double A;
	double r;

	//Remember, hx and hy with a are used, why not P?
	double da;
	double dex;
	double dey;
	double dhx;
	double dhy;
	bool dir;
	EstPara() {};
	void setWithEE(const double *ee0, const double *eef) {
		double a1 = ee0[0] * (1 - ee0[1] * ee0[1] - ee0[2] * ee0[2]);
		double a2 = eef[0] * (1 - eef[1] * eef[1] - eef[2] * eef[2]);
		da = a1 - a1;
		dex = eef[1] - ee0[1];
		dey = eef[2] - ee0[2];
		dhx = eef[3] - ee0[3];
		dhy = eef[4] - ee0[4];
		r = (a1 + a2) / 2.;
		theta0 = ee0[5];
		thetaf = eef[5];
	}
	void setWithCOE(const double *coe0, const double *coef) {
		da = coef[0] - coe0[0];
		double ex0 = coe0[1] * cos(coe0[3] + coe0[4]);
		double ey0 = coe0[1] * sin(coe0[3] + coe0[4]);
		double hx0 = tan(coe0[2] / 2.)*cos(coe0[3]);
		double hy0 = tan(coe0[2] / 2.)*sin(coe0[3]);
		double exf = coef[1] * cos(coef[3] + coef[4]);
		double eyf = coef[1] * sin(coef[3] + coef[4]);
		double hxf = tan(coef[2] / 2.)*cos(coef[3]);
		double hyf = tan(coef[2] / 2.)*sin(coef[3]);
		dex = exf - ex0;
		dey = eyf - ey0;
		dhx = hxf - hx0;
		dhy = hyf - hy0;
		r = (coe0[0] + coef[0]) / 2.0;
		theta0 = coe0[3] + coe0[4] + coe0[5];
		theta0 = fmod(theta0, DTPI);
		thetaf = coef[3] + coef[4] + coef[5];
		thetaf = fmod(thetaf, DTPI);
	}
	void setWithECOE(const double *Ecoe0, const double *Ecoef) {
		da = Ecoef[0] - Ecoe0[0];
		r = (Ecoef[0] - Ecoe0[0]) / 2.0;
		dex = Ecoef[1] - Ecoe0[1];
		dey = Ecoef[2] - Ecoe0[2];
		dhx = Ecoef[3] - Ecoe0[3];
		dhy = Ecoef[4] - Ecoe0[4];
		theta0 = Ecoe0[5];
		thetaf = Ecoef[5];
	}
	void setA(const double T, const double m, const double mu) {
		A = T / m / (mu / r / r);
	}
};
int OuterShootTheta(int n, const double *x, double *dx, int iflag, const double *para);
int OuterShootA(int n, const double *x, double *dx, int iflag, const double *para);
int PlaneShootTheta(int n, const double *x, double *dx, int iflag, const double *para);
int PlaneShootA(int n, const double *x, double *dx, int iflag, const double *para);
void MatGen(double *M1, double *M2, const double A, const double P, const double L0, const double Lf);
#endif
