#ifndef INCLUDELIBS_H
#define INCLUDELIBS_H

#if (_MSC_VER == 1900)
	//For VS2015
	#ifndef _DEBUG
	#pragma comment(lib, "MinpackVS2015.lib")
	#pragma comment(lib, "TigerToolsVS2015.lib")
	#pragma comment(lib, "ToolsVS2015.lib")
	#else
	#pragma comment(lib, "MinpackVS2015Debug.lib")
	#pragma comment(lib, "TigerToolsVS2015Debug.lib")
	#pragma comment(lib, "ToolsVS2015Debug.lib")
	#endif
	//For VS2013
#else
	#ifndef _DEBUG
	#pragma comment(lib, "Minpack.lib")
	#pragma comment(lib, "TigerTools.lib")
	#pragma comment(lib, "Tools.lib")
	#else
	#pragma comment(lib, "MinpackDebug.lib")
	#pragma comment(lib, "TigerToolsDebug.lib")
	#pragma comment(lib, "ToolsDebug.lib")
	#endif
#endif

#endif