#include <iostream>
#include <iomanip>
#include "stdio.h"
#include "VecMat.h"
#include "OrbitFun.h"
#include "orbit.h"
#include "MathConstants.h"
#define day (86400.0)
namespace TigerOrbit {
	Morbit::Morbit(cd *coe) {
		if (Mcoe == NULL)
			Mcoe = new double[7];
		V_Copy(Mcoe, coe, 7);
	}
	Morbit::Morbit(FILE *fp) {
		if (Mcoe == NULL)
			Mcoe = new double[7];
		fread(Mcoe, sizeof(double), 7, fp);
	}
	void Morbit::output(ostream &os) {
		V_Output(os, Mcoe, 7);
	}
	void Morbit::init(cd *coe) {
		if (Mcoe == NULL)
			Mcoe = new double[7];
		V_Copy(Mcoe, coe, 7);
	}
	void Morbit::calcxyz(cd tf, double *rv, cd mu, cd tunit) {
		int flag = 0;
		double M0 = Mcoe[6];
		double Mt = Mcoe[6] + (tf - Mcoe[0])*day / tunit*sqrt(mu / pow(Mcoe[1], 3.0));
		Mt = fmod(Mt, D2PI);
		double ft = E2f(flag, M2E(flag, Mt, Mcoe[2]), Mcoe[2]);
		Mcoe[6] = ft;
		coe2rv(flag, rv, Mcoe + 1, mu);
		Mcoe[6] = M0;
	}
	void Morbit::movexyz(cd tf, double *rv, cd mu, cd tunit) {
		int flag = 0;
		double Mtmp = Mcoe[6] + (tf - Mcoe[0])*day / tunit*sqrt(mu / pow(Mcoe[1], 3.0));
		double Mt = fmod(Mtmp, D2PI);
		Mt = fmod(Mt, D2PI);
		double ft = E2f(flag, M2E(flag, Mt, Mcoe[2]), Mcoe[2]);
		Mcoe[6] = ft;
		coe2rv(flag, rv, Mcoe + 1, mu);
		Mcoe[6] = Mtmp;
		Mcoe[0] = tf;
	}
	Morbit& Morbit::move_day(cd tf, cd mu, cd tunit) {
		Mcoe[6] += (tf - Mcoe[0])*day / tunit*sqrt(mu / pow(Mcoe[1], 3.0));
		Mcoe[6] = fmod(Mcoe[6], D2PI);
		Mcoe[0] = tf;
		return *this;
	}
	Morbit& Morbit::move_to(cd phif, cd mu, cd tunit) {
		Mcoe[0] += (phif - Mcoe[6]) / sqrt(mu / pow(Mcoe[1], 3.0)) * tunit / day;
		Mcoe[6] = phif;
		return *this;
	}


	//Get of eeorbit
	EEorbit::EEorbit(cd t, cd *ee_) {
		time = t;
		V_Copy(ee, ee_, 6);
	}
	void EEorbit::init(cd t, cd *ee_) {
		time = t;
		V_Copy(ee, ee_, 6);
	}
	void EEorbit::calcxyz(cd tf, double *rv, cd mu) {
		//change ee to coe
		double coe[6];
		int flag = 0;
		ee2coe(flag, coe, ee, mu);
		//change to M
		coe[5] = E2M(flag, f2E(flag, coe[5], coe[1]), coe[1]);
		coe[5] += (tf - time)*sqrt(mu / pow(coe[0], 3.0));
		coe[5] = E2f(flag, M2E(flag, coe[5], coe[1]), coe[1]);
		//Convert coe 2 rv
		coe2rv(flag, rv, coe, mu);
	}
	EEorbit& EEorbit::moveto(cd tf, cd mu) {
		//change ee to coe
		double coe[6];
		int flag = 0;
		ee2coe(flag, coe, ee, mu);
		//change to M
		coe[5] = E2M(flag, f2E(flag, coe[5], coe[1]), coe[1]);
		coe[5] += (tf - time)*sqrt(mu / pow(coe[0], 3.0));
		coe[5] = E2f(flag, M2E(flag, coe[5], coe[1]), coe[1]);
		time = tf;
		coe2ee(flag, ee, coe, mu);
		return *this;
	}
	double EEorbit::gettime() {
		return time;
	}
	double *EEorbit::getee() {
		return ee;
	}
	EEorbit::~EEorbit() {
		delete[] ee;
	}
	//void EECoastTime(double *ee, cd t0, cd tf, cd mu) {
	//	double coe[6];
	//	int flag = 0;
	//	ee2coe(flag, coe, ee, mu);
	//	coe[5] = f2E(flag, M2E(flag, coe[5], coe[1]), coe[1]);
	//	coe[5] += sqrt(mu / pow(coe[0], 3))*(tf - t0);
	//	coe[5] = E2f(flag, M2E(flag, coe[5], coe[1]), coe[1]);
	//	coe2ee(flag, ee, coe, mu);
	//}
	//void RVCoastTime(double *rv, double t0, double tf, double mu) {
	//	double coe[6];
	//	int flag = 0;
	//	rv2coe(flag, coe, rv, mu);
	//	coe[5] = f2E(flag, M2E(flag, coe[5], coe[1]), coe[1]);
	//	coe[5] += sqrt(mu / pow(coe[0], 3))*(tf - t0);
	//	coe[5] = E2f(flag, M2E(flag, coe[5], coe[1]), coe[1]);
	//	coe2rv(flag, rv, coe, mu);
	//}
	//void fCOECoastTime(double *coe, double t0, double tf, double mu) {
	//	int flag = 0;
	//	//Determine period
	//	coe[5] = f2E(flag, M2E(flag, coe[5], coe[1]), coe[1]);
	//	coe[5] += sqrt(mu / pow(coe[0], 3))*(tf - t0);
	//	coe[5] = E2f(flag, M2E(flag, coe[5], coe[1]), coe[1]);
	//}
	//void MCOECoastTime(double *coe, double t0, double tf, double mu) {
	//	int flag = 0;
	//	//Determine period
	//	coe[5] += sqrt(mu / pow(coe[0], 3))*(tf - t0);
	//}
}