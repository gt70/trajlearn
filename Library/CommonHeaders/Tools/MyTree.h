/*
Definition of my tree, whose child can be many while parent only one
template is used
*/
#ifndef MYTREE_H
#define MYTREE_H
#include <vector>
template<class T>class MyTreeNode
{
public:
	int Level;
	T *Self;
	MyTreeNode *Parent;
	vector<MyTreeNode*> Child;
	static unsigned MAXCHILD;
	//Usually initialization with level, pointer to self Node, and point to parent
	MyTreeNode(const int l, T *TP, MyTreeNode* Tpar):Level(l), Self(TP), Parent(Tpar)
	{
		Child.reserve(MAXCHILD);
	}
	MyTreeNode(const MyTreeNode &treenode)
	{
		Level = treenode.Level;
		Self = treenode.Self;
		Parent = treenode.Parent;
		Child.reserve(MAXCHILD);
		for (auto iter = treenode.Child.begin(); iter != treenode.Child.end(); iter++)
		{
			Child.push_back(*iter);
		}
		
	}
	//add child
	void AddChild(const T *child)
	{
		Child.push_back(child);
	}
	bool Addable() const
	{
		return Child.size() < MAXCHILD;
	}
	~MyTreeNode(){}
};
#endif