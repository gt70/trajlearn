#ifndef NODE_H
#define NODE_H
#include <vector>
#include <iostream>
class NODE
{
public:
	double start;
	double target;
	double tstart;//Day
	double ttrans;//Day
	double m0;//mass at start
	double mt;//mass when finish
	//Next sentence seems useless
	NODE() :start(0), target(0), tstart(0), ttrans(0), m0(0), mt(0){}
	NODE(double s, double t, double ts, double tt, double M0, double Mt) :start(s), target(t), tstart(ts), ttrans(tt), m0(M0), mt(Mt){}
	void init(double s, double t, double ts, double tt, double M0, double Mt)
	{
		start = s;
		target = t;
		tstart = ts;
		ttrans = tt;
		m0 = M0;
		mt = Mt;
	}
	void output()
	{
		std::cout << "  " << start << " to " << target << std::endl;
		//cout << "  " << orbittable[7*start] << " to " << orbittable[7*target] << endl;
		std::cout << tstart << " to " << ttrans + tstart << std::endl;
		std::cout << m0 << " to " << mt << std::endl;
	}
	void tofile(FILE *fp)
	{
		//fprintf(fp, "\t\t%5.0lf\t\t%5.0lf\n", start, target);
		//fprintf(fp, "\t\t%5.6lf\t%5.6lf\n", tstart, ttrans);
		//fprintf(fp, "\t\t%1.5lf\t\t%1.5lf\n", m0, mt);
		fprintf(fp, "%22.14e\n%22.14e\n%22.14e\n%22.14e\n%22.14e\n%22.14e\n",
			start, target, tstart, ttrans, m0, mt);
	}
};
//A function to insert a node to vector<NODE>
void Insert(std::vector<NODE> &seq, double s, double t, double ts, double tt, double M0, double Mt)
{
	if (seq.size() == seq.capacity())
	{
		seq.reserve(2 * seq.size());
	}
	NODE tpnode(s, t, ts, tt, M0, Mt);
	seq.push_back(tpnode);
}
#endif