#ifndef ORBIT_H
#define ORBIT_H
#include <iostream>
#include "VecMat.h"
#include "OrbitFun.h"
#include "Constant.h"
using namespace std;
class orbit
{
public:
    double *coe;  
    orbit(){coe = new double[7];}
    orbit(double *coe2):coe(coe2){};
    orbit(const orbit &orb2);
    void output();
    //Moving on
    orbit& move_day(double tf);
    bool satisfied(double,double,double,double,double,double);
    double phi();
};
orbit::orbit(const orbit &orb2)
{
    coe = new double[7];
    V_Copy(coe,orb2.coe,7);
}
//double f0dt2ft(int& flag, double dt, double ft, double a, double e, double mu=3.98600441800e+14);}
orbit& orbit::move_day(double tf)
{
    int flag = 0;
    coe[6] = f0dt2ft(flag,coe[6],(tf - coe[0])*day/TUnit,coe[1],coe[2],mhNU);
    coe[6] = fmod(coe[6],D2PI);
    coe[0] = tf;
    return *this;
}
double orbit::phi()
{
    double ph = coe[4] + coe[5] + coe[6];
    ph = fmod(ph,D2PI);
    return ph;
}
void orbit::output()
{
    cout << "t = " << coe[0]  << endl;
    cout << "a = " << coe[1] << endl;
    cout << "e = " << coe[2] << endl;
    cout << "i = " << coe[3] << endl;
    cout << "W = " << coe[4] << endl;
    cout << "w = " << coe[5] << endl;
    cout << "f = " << coe[6] << endl;
}
bool orbit::satisfied(double mina,double maxa,double mine,double maxe,double mini,double maxi)
{
    return(((coe[1] >= mina && coe[1] <= maxa) && coe[2] >= mine) && ((coe[2] <= maxe && coe[3] >= mini) && coe[3] <= maxi));
}

class numorbit
{
public:
    double num;
    orbit orb;
//default 
    numorbit()
    {
        num = 0.0;    
    };
//init with num and pointer
    numorbit(double n,double *pt):num(n),orb(pt){};
//init from file
    numorbit(FILE *fp)
    {
        orb.coe = new double[7];
        fread(&num,sizeof(double),1,fp);
        fread(orb.coe,sizeof(double),7,fp);
    };
    //init fromnumorbit
    //reinit from file
    void init(FILE *fp)
    {
        orb.coe = new double[7];
        fread(&num,sizeof(double),1,fp);
        fread(orb.coe,sizeof(double),7,fp);
    };
    // return true if satisfied
    bool satisfied(double,double,double,double,double,double);
    void output()
    {
        cout << "Num = " << num << endl;
        orb.output();
    }
    //move to a time
    numorbit& move_day(double tf)
    {
        orb.move_day(tf);
        return *this;
    }
    //return PHI
    double phi(){return orb.phi();}
	//return phi at time
	double phiat(double t)
	{
		this -> move_day(t);
		return this -> phi();
	}
	//return f
	double f(){return orb.coe[6];}
	//return f given ohi
	double f(double phi)
	{
		double tmpf = phi - orb.coe[4] - orb.coe[5];
		tmpf -= D2PI*floor(tmpf/D2PI);
		return tmpf;
	}
	//return time at phi
	double timeatphi(double phi,double reftime)
	{
		this -> move_day(reftime);
		double f0 = this -> f();
		double ft = this -> f(phi);
		ft = ft < f0 ? ft + D2PI : ft;
		int flag = 0;
		double dt = f0ft2dt(flag, f0, ft, orb.coe[1], orb.coe[2], mhNU);
		return dt*TUnit/day + reftime;
	}
	//return averageomega

};
bool numorbit::satisfied(double mina,double maxa,double mine,double maxe,double mini,double maxi)
{
    return(((orb.coe[1] >= mina && orb.coe[1] <= maxa) && orb.coe[2] >= mine) && ((orb.coe[2] <= maxe && orb.coe[3] >= mini) && orb.coe[3] <= maxi));
}
#endif
