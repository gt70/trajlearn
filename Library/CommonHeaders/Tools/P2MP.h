#ifndef P2MP_H
#define P2MP_H
//参数为：两个轨道根数（待转移时刻）的，质量m（出来时为最终质量），待求转移时间t print = 1 means print something
//return flag = 1 is best
int P2MP(const double *coe0, const double *coe1, double &m, double &t, bool print = 1);

#endif