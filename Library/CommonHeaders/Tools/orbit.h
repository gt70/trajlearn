#ifndef ORBIT_H
#define ORBIT_H
#include "stdio.h"
#include <iostream>
using namespace std;
typedef const double cd;
namespace TigerOrbit {
	class Morbit {
	public:
		double *Mcoe = NULL;
		Morbit() { Mcoe = new double[7]; };
		Morbit(const Morbit & orb) { 
			if(Mcoe == NULL) 
				Mcoe = new double[7]; 
			for (int i = 0; i < 7; i++) Mcoe[i] = orb.Mcoe[i]; 
		}
		Morbit & operator = (const Morbit &orb) {
			if (Mcoe == NULL)
				Mcoe = new double[7];
			for (int i = 0; i < 7; i++) Mcoe[i] = orb.Mcoe[i];
			return *this;
		}
		Morbit(const double *coe);
		Morbit(FILE *fp);
		void init(const double *coe);
		void calcxyz(const double tf, double *rv, const double mu, const double tunit);
		void movexyz(const double tf, double *rv, const double mu, const double tunit);
		void output(ostream &os);
		//move to a day or to a phi
		Morbit& move_day(cd tf, cd mu, cd tunit);
		Morbit& move_to(cd phif, cd mu, cd tunit);
		double epoch() { return Mcoe[0]; }
		double &a() { return Mcoe[1]; }
		double &e() { return Mcoe[2]; }
		double &i() { return Mcoe[3]; }
		double &Omega() { return Mcoe[4]; }
		double &omega() { return Mcoe[5]; }
		double M() { return Mcoe[6]; }
		double phi() { return Mcoe[4] + Mcoe[5] + Mcoe[6]; };
		~Morbit() { if(Mcoe!=NULL) delete[] Mcoe; };
	};
	class EEorbit {
	public:
		EEorbit() { ee = new double[6]; time = 0; };
		EEorbit(cd, cd *);
		void init(cd, cd *ee);
		void calcxyz(cd tf, double *rv, cd mu);
		EEorbit& moveto(cd tf, cd mu);
		double *getee();
		double gettime();
		~EEorbit();
	private:
		double time;
		double *ee;
	};
}
#endif
