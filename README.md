# Optimal trajectory learning project

Author: Gao Tang (gao.tang@duke.edu)

In this project, we develop a systematic approach to learn optimal trajectories.
This can potentially solve nonlinear optimal control problems in real time.

The idea is simple: formulate the problem as parametric optimization problem so we have input and output for a function.
The input, of course, is problem parameters.
The optimal trajectories are outputs.
Math shows solution to parametric optimization problems is a function of problem parameters.
Since it is a function, we can use data-driven approach to approximate it.

The tricky thing is, the optimal trajectories are not easy to obtain.
We rely on nonlinear optimization technique to solve them together with nearest-neighbor optimal control to obtain them.

After data collection, we train a neural network to approximate this function.
We note that this function could have discontinuity so we employ a mixture of expert model.
We encourage you to check our paper <https://arxiv.org/abs/1803.02493> for more details.
Technical details on database generation can be found in <https://ieeexplore.ieee.org/document/8206362>.

# Who might be interested

You might need this project if you
1. Have a system and want to calculate optimal trajectories of the system to achieve tasks. A trajectory optimizer is included.
2. Want to solve optimal trajectories for a bunch of problems, for example, a drone to difference targets. A scheme to both
generate data and possibly learn a function that approximately solves the problems.
3. Have a bunch of optimal trajectories and want to use a neural network to fit them. Due to inherent discontinuity between
trajectories, you might want to use Mixture of Experts (MoE) to do it. A pipeline to fit MoE is included.
4. Curious about how to use machine learning methods to solve parametric optimal control problems. This
   project includes all necessary contents.

# Structure of this project

## Headers

The header files to build the C++ library to solve a single optimization problem.

## Src

The C++ source files. Users can modify Src/quadDyn.cpp and related header file to solve another problem.

## resource

Several configuration files for drone problem specification.

## Library

External libraries. We include some snopt dynamic libraries but you should compile them yourself. 
In folder snopt, you put header files in include and library files in lib.
If you have snopt installed, you can pass them to cmake.

## Script

Contains Python source files to perform massive data generation and learning. Read the README file inside this folder for more details.
*All code for generating data, training amd validating model are located in this folder.*


# Dependencies (If you intend to build it)

On C++ side, you need snopt, Eigen3, pybind11.

If you have snopt installed, specify it by

export SNOPT_DIR=/home/your_name/Code/Snopt

so that CMAKE can find it.

*Caution*, current SNOPT wrapper is designed for version >= 7.6. You might have trouble linking it with previous versions.
If you are installing SNOPT from source, remember to use ./configure --with-cpp=yes to generate cpp library.

pybind11 can be installed by following instruction on https://github.com/pybind/pybind11

jsoncpp is included in Library/jsoncpp and should be installed first if you do not have one.


# Building process (Not necessary)

*Note*: we have provided a dataset for you. If you do not want to generate them yourself, you can skip this solver building part.
The dataset can be downloaded at [google drive](https://drive.google.com/open?id=17eMoC-9LP45Z6nP_VihF9QjXY5hOyr96).
Unzip the file and place data folder inside folder *Script*.

1. Navigate to this folder in command line
2. Install jsoncpp package, it is located in Library/jsoncpp, see instructions there.
3. If you haven't specified SNOPT directory, do it by "export SNOPT_DIR=/path/to/your/snopt/installation"
4. In command line, use "mkdir Build && cd Build" to create folder and go inside.
5. Type "cmake ..", please check for python version. You can specify it by "cmake -DPYBIND11_PYTHON_VERSION=2.7 .." to use 2.7. Similarly, you can specify 3.6
6. Type "make", this should build the required solver. It will be placed in ../Script folder.


# How to specify your own problem (Advanced users only)

Currently the source files demonstrate a quadcopter problem and you can build your own system.
This requires coding your own system dynamics and constraints.
If you only want to taste what trajectory learning is doing, you can skip this part and directly use the
model I've built up for you.

First, you have to write your system dynamics, as in Header/utilityClass.h a Rotor system is built. 
This class has to subclass tigerSys.
The instructor has to specify number of states and controls.
and define the virtual function void dyn(const double t, cRefV x, cRefV u, RefV f, RefM df)
where t is current time, cRefV is an alias for Eigen::Ref<const VectorXd> object, RefV is Eigen::Ref<VectorXd>, RefM is Eigen::Ref<MatrixXd>.
Function argument x is current state of the system, u is the control.
Argument f should be written by the derivative of state, i.e. $\dot{x}$.
Argument df should be written by Jacobian of $\dot{x}$, the size if nx by (1 + nx + nu) where nx and nu are dimensions of states and controls.
In other words, Jacobian is calculated w.r.t. $[t, x, u]$.

Path constraints can also be defined by subclassing TrajOpt::constraintFunctor.
The user has to implement void eval(const double t, cRefV x, cRefV u, RefV f, SpMX &fx).
The constraints are written to f, and fx is a sparse Jacobian.

After defining those classes, you have to modify line 48 in Src/solveServer.cpp to specify your system; if you have constraints,
you also have to modify line 168 of Headers/solveServer.h and change to desired constraint.
After those changes, you should be able to solve trajectory optimization problem for your own system.
Currently it supports LQR type cost function and is configurable in resource/droneConfig.json.
Customized cost function is available soon.
Or you can try our new project trajOptLib <https://gitlab.oit.duke.edu/gt70/trajOptLib> which has more functions for solving trajectory optimization
problems. However, it is written in Python and may be slower.

# Pipeline
Please navigate to Script folder for more details.
