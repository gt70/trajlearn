#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2018 Gao Tang <gt70@duke.edu>
#
# Distributed under terms of the MIT license.

"""
names.py

Just as a global container for file name.
"""
import sys, os, time


# about project and config file
PRJ_DIR = os.path.dirname(os.path.abspath(__file__))
CFGFILE = os.path.join(os.path.dirname(PRJ_DIR), 'resource/droneConfig.json')
# about database and clustering by kmeans
TRAJ_DATA_V0 = 'data/trajdata.npz'
TRAJ_DATA_V1 = 'data/trajdata.npz'
KMEAN_CLUSTER_FILE = 'data/kmeans/kmean_cluster_file.npz'
MANUAL_CLUSTER_FILE = 'data/manual_label.npy'
# about validation set, data and result
VALID_DATA_V0 = 'data/validation_trajdata.npz'
VALID_KMEANS_PREDICT = 'data/valid_kmeans_predict.npz'
# about manual features
MANUAL_FEATURE_V0 = 'data/manual_feature.npy'


THE_CONFIG = 'resource/droneConfig.json'
THE_MODEL = 'models/sol_12_50_317.pt'


if __name__ == '__main__':
    print(PRJ_DIR)
    print(TRAJ_DATA_V0)
