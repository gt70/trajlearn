#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2018 Gao Tang <gt70@duke.edu>
#
# Distributed under terms of the MIT license.

"""
utility.py

Utility functions
"""
from __future__ import print_function, division
import sys, os, time
import numpy as np
import matplotlib.pyplot as plt

from gaolib.io import ddctParse, sharedmemory as sm, getJson
import gaolib.plot as pld

import names


glbVar = getJson(names.CFGFILE)
N, dimx, dimu = glbVar['N'], glbVar['dimx'], glbVar['dimu']


def load_traj_data(load=False):
    """Load the database of trajectories.

    By default it tries to load from shared memory but if load is set True
    """
    uid = 'droneone'  # this is stolen 
    if not load and sm.has(uid):
        return sm.get(uid)
    else:
        data = ddctParse(names.TRAJ_DATA_V1)
        if 'flag' in data:
            flag = data['flag'] == 1
            x, y, mu = data['obs'], data['tjf'], data['mu']
            flt_data = {'obs': x[flag], 'tjf': y[flag], 'mu': mu[flag]}
        else:
            flt_data = data
        sm.add(flt_data, uid)
        return flt_data


def load_valid_data(load=False):
    """Load database of validation set of trajectories."""
    data = ddctParse(names.VALID_DATA_V0)
    if 'flag' in data.keys():
        flag = data['flag'] == 1
        x, y, mu = data['obs'], data['tjf'], data['mu']
        flt_data = {'obs': x[flag], 'tjf': y[flag], 'mu': mu[flag]}
        return flt_data
    else:
        return data


def draw_one_obs_traj_nn(ax, obs, tjf, traj_kw={}, ball_kw={}, show_ball=True, show=False):
    """Plot one guy.

    :param ax: the axis, preferably in 3D
    :param obs: ndarray, (7,) x0 + obs1
    :param tjf: ndarray, (317,)
    :param traj_kw: dict, keyword arguments for drawing trajectory
    :param ball_kw: dict, keyword arguments for drwaing sphere
    :param show_ball: bool, set this to True to draw spheres
    """
    if ax is None:
        fig, ax = pld.get3dAxis()
    tX, _, _ = parseX(tjf)
    ln, = ax.plot(tX[:, 0], tX[:, 1], tX[:, 2], **traj_kw)
    if show_ball:
        pld.addSphere(ax, *obs[3:7], **ball_kw)
    else:
        ln_c = ln.get_color()
        dist = np.linalg.norm(tX[:, :3] - obs[3:6], axis=1)
        ind0 = np.argmin(dist)
        if np.abs(dist[ind0] - obs[6]) < 1e-4:
            ax.quiver(obs[3], obs[4], obs[5], tX[ind0, 0] - obs[3], tX[ind0, 1] - obs[4], tX[ind0, 2] - obs[5], color=ln_c)
    if show:
        plt.show()
    return ln


def add3dTraj(ax, y, **kw):
    """Draw a 3d trajectory on axis"""
    tX, _, _ = parseX(y)
    ax.plot(tX[:, 0], tX[:, 1], tX[:, 2], **kw)


def parseX(X, N=N, dimx=dimx, dimu=dimu):
    """Parse a solution into traj, ctrl, tf"""
    if not isinstance(X, np.ndarray):
        X = np.array(X)
    traj = np.reshape(X[0:N*dimx], (N, dimx))
    ctrl = np.reshape(X[N*dimx:-1], (N - 1, dimu))
    tf = X[-1]
    return traj, ctrl, tf


def getConfig(path):
    """Parse a configuration file given in json format"""
    with open(path, 'r') as f:
        cfg = json.load(f)
        return cfg


if __name__ == '__main__':
    main()
