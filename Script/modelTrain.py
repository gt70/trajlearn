#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2018 Gao Tang <gt70@duke.edu>
#
# Distributed under terms of the MIT license.

"""
modelTrain.py

Perform cross-validation like pipeline so we test what happens if we are allowed to change # clusters, clustering approach, etc.
So I will generate data separately and train them afterwards.
When generating data, I should be able to specify downsample rate, cluster numbers.
"""
from __future__ import print_function, division
import torch
import scipy.linalg
import sys, os, time
import numpy as np
import matplotlib.pyplot as plt
import glob
import re
from sklearn.cluster import MiniBatchKMeans

from gaolib.io import getArgs, sharedmemory as sm, joinNumber, ddctParse
from gaolib.train import trainOne, genTrainConfig, modelLoader, MoMNet
from gaolib.math import getStandardData, Query, getPCA, getAffinityXY, getNNIndex
import gaolib.parallel as pp
import gaolib.plot as pld

from utility import glbVar, parseX, load_traj_data
import names
import settings
from settings import KMEANK, XNAME, YNAME


def main():
    # train train model for new clustered files
    # gen generate the cluster files
    # snn enables the training of a universal model on all available data
    # clas means play with classification network
    args = getArgs('train', 'gen', 'snn', 'clas')
    if args.gen:
        generateClusterData(args)
    if args.train:
        if args.clas:
            runClassifierTrain(args.check, args)
        elif args.snn:
            runUnivModelTrain(args.check, args)
        else:
            runClusterTrain(args.check, args)


def runUnivModelTrain(test, args):
    """Train a universal model using the same architecture as before.
    """
    xydata = load_traj_data()
    x = xydata[XNAME]
    y = xydata[YNAME]
    net = [x.shape[1]] + settings.UNIV_MODEL_HIDDEN + [y.shape[1]]
    config = genTrainConfig(network=net, outdir='models/kmeans', outname='snn_model_of_%s.pt' % joinNumber(net))
    if not test:
        trainOne(config, {'x': x, 'y': y})
    else:
        model_name = os.path.join(config['outdir'], config['outname'])
        mdlfun = modelLoader(model_name, cuda=True)
        predy = mdlfun(x)
        return predy


def runClusterTrain(test, args):
    """Training for some data"""
    manual = False
    xydata = load_traj_data()
    cluster_data = np.load(names.KMEAN_CLUSTER_FILE)
    n_config = len(cluster_data.keys())
    y = xydata[YNAME]
    x = xydata[XNAME]
    # calculate # parameters for unknowns
    nx = x.shape[1]
    ny = y.shape[1]
    hidden = settings.UNIV_MODEL_HIDDEN
    total_param = nx * hidden[0] + ny * hidden[-1] + np.prod(hidden)
    total_data = x.shape[0]
    start_config = args.startk
    for i in range(start_config, n_config):
        datai = cluster_data['%d' % i].item()
        label = datai['label']
        nclus = datai['nclus']
        nProcess = settings.TRAIN_PROCESS
        if nProcess > nclus:
            nProcess = nclus
        tasks = pp.getTaskSplit(nclus, nProcess)
        x_arr, y_arr, label_arr = pp.getSharedNumpy(x, y, label)
        mp = pp.MulProcess(_single_train, np.arange(nProcess), nProcess, tasks, test, manual, nclus, nx, ny, total_param, total_data, x_arr, y_arr, label_arr)
        rst = mp.run(wait=0.05)


def _single_train(idx, tasks, test, manual, nclus, nx, ny, total_param, total_data, x_arr, y_arr, label_arr):
    """A process to train some models"""
    task = tasks[idx]
    x, y, label = pp.getNumpy(x_arr, y_arr, label_arr)
    for i in range(task[0], task[1]):
        mask = label == i
        sub_x = x[mask]
        sub_y = y[mask]
        sub_ndata = sub_x.shape[0]  # I will try my best to avoid len since it is not numba compatible
        U = np.roots([1, nx + ny, -sub_ndata * total_param / total_data])
        # U = np.roots([1, nx + ny, -sub_ndata])
        if U[0] > U[1]:
            U = int(U[0])
        else:
            U = int(U[1])
        if U < 30:  # this might be some key?
            U = 30
        net = [nx, U, U, ny]
        print('cluster %d of core %d ' % (i, idx), net)
        sub_data = {'x': sub_x, 'y': sub_y}
        if not manual:
            config = genTrainConfig(network=net, outdir='models/kmeans', outname='kmean_cluster_%d_model_%d_of_%s.pt' % (nclus, i, joinNumber(net)))
        else:
            config = genTrainConfig(network=net, outdir='models', outname='manual_cluster_%d_model_%d_of_%s.pt' % (nclus, i, joinNumber(net)))
        if not test:
            trainOne(config, sub_data)
        else:
            model_name = os.path.join(config['outdir'], config['outname'])
            mdlfun = modelLoader(model_name, cuda=True)
            predy = mdlfun(sub_x)
            return predy


def runClassifierTrain(test, args):
    """Train a classifier to distinguish between choice of clusters."""
    xydata = load_traj_data()
    cluster_data = np.load(names.KMEAN_CLUSTER_FILE)
    n_config = len(cluster_data.keys())
    start_config = args.startk

    x = xydata[XNAME]  # this is what I need
    # calculate # parameters for unknowns
    nx = x.shape[1]
    if test:
        outy = {}
    for i in range(start_config, n_config):
        datai = cluster_data['%d' % i].item()
        label = datai['label']
        nclus = datai['nclus']
        # assemble a thing
        label_data = {'x%d' % i: x[label == i] for i in range(nclus)}
        net = [nx] + settings.CLASSIFIER_HIDDEN + [nclus]
        print('config %d ' % i, net)
        config = genTrainConfig(network=net, outdir='models/kmeans', outname='kmean_config_%d_classifier_of_%s.pt' % (i, joinNumber(net)))
        if not test:
            trainOne(config, label_data, is_reg_task=False)
        else:
            model_name = os.path.join(config['outdir'], config['outname'])
            mdlfun = modelLoader(model_name, cuda=True)
            predy = mdlfun(x)
            outy['%d' % i] = predy
    if test:
        return outy


def generateClusterData(args):
    """Generate several choices of data clustering."""
    data = load_traj_data()
    kmean_only_y_result = {}
    ds = 1  # means no down sampling
    num_cluster = settings.KMEANK
    kw = {'ds': ds, 'onlyy': True, 'scaley': True}
    for i, n_clus in enumerate(num_cluster):
        kw['nclus'] = n_clus
        labels = generate_cluster_data(data, **kw)
        kmean_only_y_result['%d' % i] = {'ds': ds, 'nclus': n_clus, 'label': labels}
    np.savez(names.KMEAN_CLUSTER_FILE, **kmean_only_y_result)


def generate_cluster_data(data, **kw):
    """By some metric and use some approach, generate cluster data.

    Parameters
    ----------
    data: a dict of data loaded elsewhere
    kw can include:
    ds: int, down-sample rate. We just pick data every ds times
    nclus: int, number of clusters
    onlyy: bool, perform kmeans only on y instead of concatenating x and y
    scale: getStandardData compatible data type
    """
    downsample = kw.get('ds', 1)
    if downsample > 1:
        data = {key: data[key][::downsample] for key in data.keys()}
    # perform kmeans on y only
    nclus = kw.get('nclus', 3)
    onlyy = kw.get('onlyy', True)
    if onlyy:
        y = data['tjf']
    else:
        y = np.concatenate((data[XNAME], data[YNAME]), axis=1)
    print('data put into kmeans ', y.shape)
    scaley = kw.get('scaley', True)
    std_y = getStandardData(y, cols=scaley)
    kmean = MiniBatchKMeans(nclus, batch_size=5000)
    kmean.fit(std_y)
    labels = kmean.labels_
    return labels


def _find_kmean_classifier_regressor(i):
    """For the i-th kmean cluster config, return classifier and regressors name."""
    pattern = re.compile(r'model_(\d+)_of')
    model_dir = 'models/kmeans'
    k = KMEANK[i]
    cls_name = glob.glob('%s/kmean_config_%d_*' % (model_dir, i))[0]
    reg_names = glob.glob('%s/kmean_cluster_%d_*' % (model_dir, k))
    name_number = [int(pattern.findall(reg_name)[0]) for reg_name in reg_names]
    order = np.argsort(name_number).tolist()
    reg_names = [reg_names[i] for i in order]
    return cls_name, reg_names


def _find_snn_model():
    """Return the name of the snn model ready to be loaded."""
    model_dir = 'models/snntune'
    snn_name = glob.glob('%s/snn_model_of' % model_dir)[0]
    return snn_name
    # return os.path.join(model_dir, 'snn_model_of_7_1000_1000_317.pt')


def _find_manual_classifier_regressor():
    """Return names of classifier and regressors for manual one."""
    cls_name = glob.glob('models/manual_classifier_*')[0]
    reg_names = glob.glob('models/manual_cluster_*')
    reg_names.sort()
    return cls_name, reg_names


if __name__ == '__main__':
    main()
