#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2018 Gao Tang <gt70@duke.edu>
#
# Distributed under terms of the MIT license.

"""
rolloutValidate.py

Perform rollout experiments on the validation set.
I will just use simulation one since dyn error is quite small so these two are quite similar.
"""
import torch
import scipy.linalg
import sys, os, time
import numpy as np
import matplotlib.pyplot as plt
import itertools
import cPickle as pkl
import numba

from gaolib.io import getArgs
from gaolib.math import l1loss
from gaolib.train import MoMNet, modelLoader
from gaolib.dynsys import QuadCopter
from gaolib.controller import InterpTrajectory
from gaolib.controller import TrackLQR, StabilizeLQR
from gaolib.simulator import Simulator
from gaolib.parallel import getSharedNumpy, getNumpy, getTaskSplit, MulProcess

from utility import glbVar, load_valid_data, draw_one_obs_traj_nn as draw
from modelTrain import _find_kmean_classifier_regressor, _find_snn_model, _find_manual_classifier_regressor, KMEANK
import names


N, dimx = glbVar['N'], glbVar['dimx']


def main():
    # eval just loads the models, makes predictions, and save into a file
    # sim loads the saved file and perform a rollout
    # evalsim also does simulation, but in the form of constraint evaluation.
    # test means we run sim in test mode
    # show compares prediction results for each model, it seems to be pretty bad
    # multipred test what happens if we are allowed to make multiple predictions and select from them
    args = getArgs('eval', 'evalsim', 'test', 'show', 'multipred')
    if args.eval:
        evaluateModels()
    if args.evalsim:
        evaluateSimulation()
    if args.multipred:
        testKMeansMultiPred()


def testKMeansMultiPred():
    """For models with kmeans clusters, we want to see if I can make several predictions. I select from 3"""
    validData = load_valid_data()
    obs, tjf = validData['obs'], validData['tjf']
    n_valid = obs.shape[0]
    print(obs.shape)
    lst_vio = []
    mdl_trial = 3
    tmp_pred = np.zeros((mdl_trial, tjf.shape[1]))

    def get_one(cls, reg):
        vio = np.zeros(n_valid)
        kMoM = MoMNet(cls, reg)
        n_clus = len(reg)
        vProb = kMoM.getClusY(obs)  # get probability prediction
        probOrder = np.argsort(vProb, axis=1)
        for j in range(n_valid):
            tmp_vio = np.zeros(mdl_trial)
            for k in range(mdl_trial):
                tmp_pred[k] = kMoM.getPredYi(obs[j], probOrder[j, -1 - k])
            vioj = eval_pred_vio(np.tile(obs[j], (mdl_trial, 1)), tmp_pred)
            vio[j] = np.amax(vioj)
        return vio

    # first manual one
    mcls, mreg = _find_manual_classifier_regressor()
    manvio = get_one(mcls, mreg)
    print(np.mean(manvio))

    raise SystemExit

    print([np.mean(vio) for vio in lst_vio])
    for i in range(len(KMEANK)):
        print('k %d' % i)
        kcls, kreg = _find_kmean_classifier_regressor(i)
        vio = get_one(kcls, kreg)
        lst_vio.append(vio)

    print([np.mean(vio) for vio in lst_vio])


def evaluateModels():
    """We intend to load those models and make predictions on this validation set.

    I will compare the SNN model and kmeans model
    """
    validData = load_valid_data()
    obs, tjf = validData['obs'], validData['tjf']
    if not os.path.exists(names.VALID_KMEANS_PREDICT):
        output = {}
        # load snn
        snn_model = _find_snn_model()
        snn_fun = modelLoader(snn_model)
        sPredy = snn_fun(obs)
        output['snn'] = sPredy
        # load kmeans
        for i in range(len(KMEANK)):
            kcls, kreg = _find_kmean_classifier_regressor(i)
            kMoM = MoMNet(kcls, kreg)
            output['%d' % i] = kMoM.getPredY(obs)
        # load manual one
        mcls, mreg = _find_manual_classifier_regressor()
        mMoM = MoMNet(mcls, mreg)
        mPredy = mMoM.getPredY(obs)
        output['manual'] = mPredy
        # save file
        np.savez(names.VALID_KMEANS_PREDICT, **output)
    else:
        output = np.load(names.VALID_KMEANS_PREDICT)
        sPredy = output['snn']
        mPredy = output['manual']
    # maybe show error information here using the l1loss
    sL1 = np.mean(l1loss(tjf, sPredy), axis=1)
    mL1 = np.mean(l1loss(tjf, mPredy), axis=1)
    kL1 = [np.mean(l1loss(tjf, output['%d' % i]), axis=1) for i in range(len(KMEANK))]
    all_error = [sL1]
    all_error.append(mL1)
    all_error.extend(kL1)
    print([np.mean(error) for error in all_error])
    fig, ax = plt.subplots()
    labels = ['SNN', 'Manual']
    labels.extend(['k=%d' % k for k in KMEANK])
    ax.hist(all_error, bins=20, label=labels)
    ax.legend()
    fig.savefig('gallery/kmeans/hist_mom_pred_error_mean_l1.pdf')
    plt.show()


def evaluateSimulation():
    """Simulation of trajectory tracking by only looking at the trajectories, this should be sufficient for many."""
    # load validation dataset
    validData = load_valid_data()
    obs, tjf = validData['obs'], validData['tjf']
    print(obs.shape)
    nSim = obs.shape[0]
    # load in the predictions
    validPred = np.load(names.VALID_KMEANS_PREDICT)
    n_kmean = len(validPred.keys()) - 2
    assert n_kmean == len(KMEANK)
    snnPred = validPred['snn']
    snn_vio = eval_pred_vio(obs, snnPred)
    snn_vio_mean = np.mean(snn_vio)
    print('snn avg vio', np.sum(snn_vio[snn_vio < 0]) / snn_vio.shape[0])
    manPred = validPred['manual']
    man_vio = eval_pred_vio(obs, manPred)
    man_vio_mean = np.mean(man_vio)
    print('manual avg vio', np.sum(man_vio[man_vio < 0]) / man_vio.shape[0])

    kmean_vio = [eval_pred_vio(obs, validPred['%d' % i]) for i in range(n_kmean)]
    kmean_vio_mean = [np.sum(tmp[tmp < 0]) / tmp.shape[0] for tmp in kmean_vio]
    print(kmean_vio_mean)
    # for worst
    print('manual worst vio ', np.amin(snn_vio))
    print([np.amin(tmp) for tmp in kmean_vio])
    fig, ax = plt.subplots()
    ax.plot(kmean_vio_mean)
    ax.axhline(snn_vio_mean, ls='--', label='SNN', color='r')
    ax.axhline(man_vio_mean, ls='--', label='Manual', color='g')
    ax.set_xticks(range(n_kmean))
    ax.set_xticklabels(map(str, KMEANK))
    ax.set_xlabel(r'# Clusters')
    ax.set_ylabel(r'Avg. Constr. Vio')
    ax.legend()
    fig.savefig('gallery/kmeans/sweatk.pdf')
    plt.show()


@numba.njit(fastmath=True)
def eval_pred_vio(obs, pred):
    """Evaluate amount of constraint violation for a trajectory prediction."""
    nSim = obs.shape[0]
    vec_vio = np.zeros(nSim)
    for i in range(nSim):
        pathi = pred[i]
        obsi0 = obs[i, 3:7]
        obsi1 = obs[i, 7:]
        worst_vio = 0
        tX = pathi[0:N*dimx].reshape((N, dimx))
        for j in range(N):
            constrvio = np.linalg.norm(tX[j, :3] - obsi0[:3]) - obsi0[3]
            if constrvio < worst_vio:
                worst_vio = constrvio
        vec_vio[i] = worst_vio
    return vec_vio


if __name__ == '__main__':
    main()
