# A project for optimal trajectory learning
Author: Gao Tang (gao.tang@duke.edu)

# Overview
## Background
The problem of solving for optimal trajectories to a series of related scenarios can be treated as parametric optimal control problem (POCP).
The solution to POCP is a mapping from problem parameters to problem solutions.
In general POCP with non-convex cost or constraints (from nonlinear system dynamics or non-convex constraints) has no closed-form solution and is numerically challenging to solve.
This project is to demonstrate the solutions to POCP can be efficiently approximated by data.

*Who might be interested*
You might need this project if you want to use precomputed data to approximately solve parametric optimal control problems, especially, to solve them in real time.
This project assumes you have a miracle that can solve a trajectory optimization problems with an initial guess. (In fact, this miracle can be built using C++ code in the upper folder.)
All you need to do is to first generate massive data and then learn from generated data.

## Problem description
Support an optimization problem is to be solved in the form of 
min f(x, p) 
s.t. h(x, p) \le 0
where $x \in \mathbb{R}^n, p \in \mathbb{R}^m$.
We note that both $x$ and $p$ are of fixed length. 
The solution to this optimization problem is denoted as z=z(p)=argmin f(x,p) s.t. h(x,p) \le 0. 
In other words, given $p$, $z$ is the solution to the optimization problem with $p$ fixed and is thus a function of $p$.
Generally, it is non-trivial to obtain a closed form solution of $z(p)$. 
For challenging problems, numerically solving $z(p)$ for given $p$ is also quite challenging.
However, even if challenging, offline computation can still provides us a $z$ for given $p$.
The idea is to use machine learning methods to approximate $z(p)$ given samples of $p$.

To achieve this task, two prerequirements are required: data and learning.
For the data part the task is to collect some data of $p$ and $z$, this is done by sampling $p$ and calculating corresponding $z(p)$.
How to sample $p$ depends on specific problems.
After sampling $p$, $z(p)$ can be calculated using the trajectory optimization subroutine.
We call this process data collection.
For the learning part the goal is to fit a model based on collected data.
We choose to use neural network as the model for its ability to approximate complex functions, given enough data.
We call this process model fitting.

## Data collection
One issue immediately arises in data collection part is the computational complexity.
For every $p$, $z(p)$ is obtained by solving non-convex optimization problems and typically we rely on random restart on lots of initial guess
to get the optimum. Even by doing so, there is chance that the optimum is locally optimal and even worse, no solution is obtained.
How to solve a non-convex optimization problem is challenging and we do not intend to address it here.
Instead, we assume that for the trajectory optimization problem we cared about random restart gives global optimum with high probability.
Another assumption is in most cases, two problems $p_1$ and $p_2$ have similar solutions $z_1$ and $z_2$ is they are close, i.e. $\|p_1-p_2\|$ is small.
We note that while this holds for most cases, there is still chance that $z(p)$ is discontinuous in $p$ in some regions of $p$.
Under those assumptions, we apply the Nearest Neighbor Optimal Control (NNOC) approach to efficiently collect data. 
The approach is to first compute a small dataset of optimal trajectories and solve novel problems using existing data as initial guesses.
Which existing data to use is determined by a nearest neighbor query for novel problems.
Since the nearest neighbor gives a reasonable initial guess, the optimization is efficient and achieves global optimum at high probability.
Moreover, this database can be built incrementally to obtain data faster.

Alternatively, in cases where an initial guess can be provided quite accurately, NNOC is not required.
If that happens to be the case, we just sample $p$ and solve $z(p)$ using those guesses.

In data_generation.py a function is written to assist in data generation.

See [Case study](#case-study) to see how we use the second approach to generate a dataset.

## Model fitting
After data collection, a model can be fit to approximate the function we cared about.
We note that the mapping from $p$ to $z$ usually have discontinuity and special care must be taken to handle them.
We provide subroutines to fit a universal model which is a single neural network and MoE where multiple neural networks are trained.
To fit MoE, the dataset has to be partitioned first using clustering methods. This is done by KMeans algorithm.
See [Case study](#case-study) section for how these functions are used.

## Model validation
After fitting the model, we have to evaluate it on a validation set.
This evaluation process provides a metric on how the learned models performs.
The user can provides a set of problems and a function that evaluates the predicted trajectory.
Then the metric scores are returned.
The metrics include but are not limited to prediction error.
In [Case study](#case-study), a metric is chosen as the amount of constraint violation.

# Case study
In this section, we use a quadcopter problem to study how those subroutines can be combined.
Using provided C++ code, we are able to obtain a solver that solves $z$ given $p$.
The solver can be imported by "import libsolver" after building it using CMake.
However, this is not necessary if you do not plan to generate data yourself.

## Background
The problem we are studying is a quadcopter obstacle avoidance problem.
The quadcopter starts from some position $x_0 \in \mathbb{R}^3$ and has to reach the target at the origin of a selected coordinate frame.
It has to avoid a spherical obstacle described by its center position $c \in \mathbb{R}^3$ and radius $r\mathbb{R}$.

This problem is formulated as POCP where the parameters are $x_0, c, r$, a concatenation of non-constant parameters across different problems.
The dimension of input is 7.
The solution to each PCOP parameter is the optimal path from $p$ to target while avoiding obstacle.
This path is parameterized by the states of the quadcopter at a uniform time grid.
Specifically, in our problem, the output dimension is 317.

## Data generation

In order to approximate $z(p)$, the first step is to generate massive samples of $z(p)$ with $p$ sampled from a distribution that
we are interested in, i.e. the distribution the quadcopter might encounter.
Since in previous research, we have trained a neural network to predict optimal trajectories where there is no obstacle.
It is directly used here to provide an initial guess.

It takes **long** time to generate data yourself so a [downloadable link](h ttps://drive.google.com/open?id=17eMoC-9LP45Z6nP_VihF9QjXY5hOyr96)  gives you access to the data. 
After unzipping, put the data folder in this folder. It has file with name 'trajdata.npz'. You can access this data using numpy.load().
It has three fields: "mu" stores some Lagrangian multipliers which will not be used for now; "obs" is indeed $p$ and "tjf" is indeed $z$.
Each field is a numpy.ndarray and have 189990 rows.

If you intend to generate data yourself, you can run "python massiveSimulate.py -massrun" to generate those data and "python massiveSimulate.py -massrun -valid" to generate validation dataset.
In settings.py the desired data size and cores being used can be set.

## Learning

Since we have obtained so many data, we can use machine learning algorithms to build a model that approximates $z(p)$.
We choose neural network as our model since it can approximate complex functions and there are many deep learning libraries to use.
Since our input features does not have any particular structures to explore, we will just use a multi-layer perceptron as our model.
It turns out we do not need a very deep neural network to learn $z(p)$ pretty well.
To learn $z(p)$ from data, you do not have to be familiar with TensorFlow or PyTorch since gaolib has a few useful functions for you.
The functions in gaolib is somewhat limited but sufficient for this task. It requires PyTorch being installed. 

The functions for model training are in "modelTrain.py", you can call them by "python modelTrain.py COMMANDS" where several commands can be used

1. -gen: it use kmeans to split the data into several clusters and save the corresponding cluster labels. A file data/kmeans/kmean_cluster_file.npz will be generated
2. -train -snn: Train the standard neural network (i.e. single mlp model.) A model will be saved in models/snn_model_of_YOUR_CHOIDE.pt. You can manually modify YOUR_CHOIDE on line 84
3. -train -clas: Train the classifier for MoE models, files will be put in models/kmeans/kmean_config\**_classifier_of\*****.pt
4. -train: Train experts. Many files will be generated in models/kmeans/kmean_cluster_*_model_*_of_*.pt

By default we use k=5, 10, 20, 40, 60, 80, 100 and it takes long time to train them all.
The cluster numbers and hidden layer size of the universal model can be tuned in settings.py

## Validation

After learning a model to approximate $z(p)$, we need validation of the model to check if it meets our requirement. 
If not, we might have to go back and rebuild the model.
For this task, we are curious about if the predicted trajectory indeed starts from $x_0$ and reaches the origin.
If it does, we also want to check if obstacle avoidance constraints are violated.
We can also check if the predicted trajectory obeys system dynamics.

User can write a quality function and I calculate those. Give instruction on how to write them?
Sections on customizing your own problem and what parameters to.

In order to do so, we need a function that loads the trained model which is saved on disk and use it to make predictions for problems not in the training set.
Luckily gaolib has such a function written for you.
We also include a file named "validation_trajdata.npz" in "data" folder which contains a validation set.
This file has similar structure to "trajdata.npz" so you can also check how far the predictions are from the ground truth.

So far, we have a function that makes predictions for new problems and a set of new problems.
We directly predict trajectories for the validation set and check if there is any constraint violation.

How about dynamics? Our prediction will unavoidably have some errors in dynamics. But can this prediction still be used?
We have to examine this by the experiments of using a feedback controller to track the predicted trajectory and checking if the target can be reached.
This sounds lots of work but gaolib has already written those for you.
You can design a LQR controller to track the trajectory and check if the quadcopter can indeed reach the goal.

The functions for model validation is in rolloutValidate.py, you call them by "python rolloutValidate.py COMMANDS" where commands can be

1. -eval compares prediction errors.
2. -evalsim compare constraint violation for each model


## Description of each file

### modelTrain.py

It trains several functions to train some models.
First, it can train SNN just execute this script with -train -snn
It also have functions to cluster the dataset and train a MoE on those.

### massiveSimulate.py

This file is used to generate the dataset we are using. It basically use parallel computation to do the task.
It relies on a binary library named libserver.so which relies on external commercial libraries and not included here.
It provides good examples on how gaolib can be used in parallel computing.

### play_with_nn_size.py

It contains code when I am looping over several choices of SNN architectures to check which one is the best.
It just changes number of hidden layers and number of neurons in those layers.

### rollout_on_validation_prediction.py

Perform feedback control validation. It relies on two files generated by SNN and MoE model.

### rolloutValidate.py

It evaluate model on validation set and only checks for obstacle avoidance constraint violation.

### show_bad_prediction.py

It shows some bad predictions by SNN.

### utility.py

Some utility functions.

### names.py
Basically it stores some filenames used across the project.

## Dependencies

In current folder, use "pip install -r requirements.txt" to install dependencies. There might be other missing dependencies, but they should also be installed using pip.
