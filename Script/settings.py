#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2019 motion <motion@motion-MS-7971>
#
# Distributed under terms of the MIT license.

"""
This file contains some settings.
"""

# use this configuration to generate dataset for learning
DATA_CONFIG = {
    'nProcess': 4,
    'nData': 100000
}

# use this configuration to generate dataset for validation
VALID_DATA_CONFIG = {
    'nProcess': 4,
    'nData': 1000
}

# use this configuration to generate dataset for function test
TEST_DATA_CONFIG = {
    'nProcess': 2,
    'nData': 100
}

# how to split dataset into clusters
KMEANK = [5, 10, 20, 40, 60, 80, 100]

# size of hidden layers for the universal model
UNIV_MODEL_HIDDEN = [1000, 1000]
CLASSIFIER_HIDDEN = [1000]

# number of processes for model training
TRAIN_PROCESS = 4

XNAME = 'obs'
YNAME = 'tjf'
