#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2018 Gao Tang <gt70@duke.edu>
#
# Distributed under terms of the MIT license.

"""
play_with_nn_size.py

I know previous architecture might not be good.
Now I try to explore a serious question, what is the optimal hyperparameters?
"""
from __future__ import print_function, division
import sys, os, time
import numpy as np
import itertools

from gaolib.io import getArgs, joinNumber
from gaolib.train import genTrainConfig, trainOne
import gaolib.parallel as pp

from utility import glbVar, parseX, load_traj_data
import names


def main():
    args = getArgs('haha', 'test')
    loop_over_hyper(args.test, args)


def loop_over_hyper(test, args):
    """Loop over hyperparameters such as size of neural network

    I shall try one, two, and three hidden layers of various sizes
    """
    xydata = load_traj_data()
    obs, tjf = xydata['obs'], xydata['tjf']
    nx = obs.shape[1]
    ny = tjf.shape[1]
    choices = [
            [nx, 1000, ny],
            [nx, 2000, ny],
            [nx, 500, 500, ny],
            [nx, 1000, 1000, ny],
            # [nx, 500, 500, 500, ny],
            # [nx, 1000, 1000, 1000, ny]
            ]
    # choices = [[nx, 1000, 1000, ny]]
    # choices = [[nx, 800, 800, 800, ny]]
    obs_arr, tjf_arr = pp.getSharedNumpy(obs, tjf)
    n_choice = len(choices)
    print(n_choice, ' choices')
    nProcess = 8
    if nProcess > n_choice:
        nProcess = n_choice
    lst_task = pp.getTaskSplit(n_choice, nProcess)
    data = {'x': obs, 'y': tjf}
    mp = pp.MulProcess(single_train, lst_task, nProcess, choices, test, obs_arr, tjf_arr)
    rst = mp.run(wait=0.1)
    if test:
        output = list(itertools.chain.from_iterable(rst))
        return output


def single_train(task, choices, test, obs_arr, tjf_arr):
    """A single process function that trains a series of neural networks.

    It has to be used with loop_over_hyper which use MulProcess to call this function.
    """
    obs, tjf = pp.getNumpy(obs_arr, tjf_arr)
    data = {'x': obs, 'y': tjf}
    output = []
    for _choice_ in choices[task[0]: task[1]]:
        print(_choice_)
        config = genTrainConfig(network=_choice_, outdir='models/snntune', outname='snn_model_of_%s.pt' % joinNumber(_choice_),
                batch_size=256, test_batch_size=2048)
        if not test:
            trainOne(config, data)
        else:
            model_name = os.path.join(config['outdir'], config['outname'])
            mdlfun = modelLoader(model_name, cuda=True)
            predy = mdlfun(x)
            output.append(predy)
    return output


if __name__ == '__main__':
    main()
