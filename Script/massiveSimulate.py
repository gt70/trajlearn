#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2018 Gao Tang <gt70@duke.edu>
#
# Distributed under terms of the MIT license.

"""
massiveSimulate.py

Build a database that contains many examples where a drone flies to a target in the presence of two spherical obstacles.

To make it general, I will sample obstacles along the straight line connecting start and goal.
"""
from __future__ import print_function, division
import sys, os, time
import numpy as np
import matplotlib.pyplot as plt
import numba

import gaolib.plot as pld
from gaolib.io import getArgs, sharedmemory as sm
from gaolib.train import modelLoader
from gaolib.parallel import getNumpy, getSharedNumpy, MulProcess, getTaskSplit

import libserver

from utility import parseX, glbVar, getConfig, add3dTraj
import settings
from settings import DATA_CONFIG, VALID_DATA_CONFIG, TEST_DATA_CONFIG
from data_generation import data_gen_with_guess, DataGenSolver


cfg = getConfig(glbVar.CFGFILE)
N, dimx, dimu = cfg['N'], cfg['dimx'], cfg['dimu']
THE_CONFIG = 'resource/droneConfig.json'
THE_MODEL = 'models/sol_12_50_317.pt'  # we pre-trained model


def main():
    # one just writes a subroutine that does this a few times
    # checkobs validates the obstacle generation subroutine to ensure it is correct
    # massrun massively generate data
    # valid generates a validation set of some small number
    # test run in test mode
    # notclose means we do not impose sampling of obstacles that are closer to straight line
    args = getArgs('one', 'checkobs', 'massrun', 'valid', 'test', 'notclose')  # move #process and #data here, have configuration file
    if args.massrun:
        new_style_massive_run(args)
        # massiveRun(args)


def new_style_massive_run(args):
    """Use new style"""
    not_close = args.notclose
    nProcess = DATA_CONFIG['nProcess']
    nData = DATA_CONFIG['nData']  # let's say I get this number of data
    if args.valid:
        nProcess = VALID_DATA_CONFIG['nProcess']
        nData = VALID_DATA_CONFIG['nData']
    if args.test:
        nProcess = TEST_DATA_CONFIG['nProcess']
        nData = TEST_DATA_CONFIG['nData']

    # define functions that return functions for p, z, solver
    def _pfun():
        def _gen_p_fun():
            target = np.zeros(3)
            while True:
                x0 = generate_x0_with_lower_bound(10, 2)  # generate random position within some range but with constraints
                if not_close:
                    flag, obs1 = find_one_obstacle(x0, target, 0.5, 1, 4)
                else:
                    flag, obs1 = find_one_obstacle_with_min_violation(x0, target, 0.5, 0.5, 1, 4)
                if flag == 1:
                    break
            return np.concatenate((x0, obs1))
        return _gen_p_fun

    def _zfun():
        mdlfun = modelLoader(THE_MODEL)
        def _gen_z_fun(p):
            return mdlfun(p)
        return _gen_z_fun

    def _class_gen():
        class solver(DataGenSolver):
            def __init__(self):
                solver = libserver.pysolver()
                solver.initfnm(THE_CONFIG)
                self.solver = solver
                self.x0 = np.zeros(12)
                self.xf = np.zeros(12)

            def update_problem(self, p):
                self.x0[:3] = p[:3]
                self.solver.updateX0Xf(self.x0, self.xf)
                self.solver.updateObstacle(p[3:], 0)

            def solve_guess(self, guess):
                flag = self.solver.solveWithGuess(guess.astype(np.float64))
                return self.solver.rst
        return solver()

    if args.valid:
        fnm = 'data/validation_trajdata.npz'
    else:
        fnm = 'data/trajdata.npz'
        data_gen_with_guess(_pfun, _zfun, _class_gen, nProcess, nData, fnm,
                          settings.XNAME, settings.YNAME, 'flag')


def massiveRun(args):
    """Massively run the optimization problem to generate some data"""
    not_close = args.notclose
    nProcess = DATA_CONFIG['nProcess']
    nData = DATA_CONFIG['nData']  # let's say I get this number of data
    if args.valid:
        nProcess = VALID_DATA_CONFIG['nProcess']
        nData = VALID_DATA_CONFIG['nData']
    if args.test:
        nProcess = TEST_DATA_CONFIG['nProcess']
        nData = TEST_DATA_CONFIG['nData']
    lstTask = getTaskSplit(nData, nProcess)
    # create shared array
    obs = np.zeros((nData, 7), dtype=np.float32)
    len_sol = N * dimx + (N - 1) * dimu + 1
    tjf = np.zeros((nData, len_sol), dtype=np.float32)
    mu = np.zeros((nData, N - 1), dtype=np.float32)
    solve_flag = np.zeros(nData, dtype=np.int8)
    obs_arr, tjf_arr, mu_arr, flag_arr = getSharedNumpy(obs, tjf, mu, solve_flag)
    mp = MulProcess(singleRun, np.arange(nProcess), nProcess, not_close, lstTask, obs_arr, tjf_arr, mu_arr, flag_arr)
    rst = mp.run(wait=0.1)
    # rst = mp.debug()
    print(rst)
    obs, tjf, mu, solve_flag = getNumpy(obs_arr, tjf_arr, mu_arr, flag_arr)
    if args.valid:
        np.savez('data/validation_trajdata.npz', obs=obs, tjf=tjf, mu=mu, flag=solve_flag)
    else:
        np.savez('data/trajdata.npz', obs=obs, tjf=tjf, mu=mu, flag=solve_flag)


# @numba.jit
def singleRun(idx, not_close, lstTask, task_x_arr, task_y_arr, task_mu_arr, task_flag_arr):
    """Single process ready to run"""
    task = lstTask[idx]
    np.random.seed(10000 + np.random.randint(task[1]))
    num_task = task[1] - task[0]
    task_x, task_y, task_mu, task_flag = getNumpy(task_x_arr, task_y_arr, task_mu_arr, task_flag_arr)
    # setup solver, mdlfun
    solver = libserver.pysolver()
    solver.initfnm(THE_CONFIG)
    mdlfun = modelLoader(THE_MODEL)  # this is useful for initial guess
    s0 = np.zeros(dimx)
    sf = np.zeros(dimx)
    num_ok = 0
    ten_percent = int(0.1 * num_task)
    for i in range(task[0], task[1]):
        if (i - task[0]) % ten_percent == ten_percent - 1:
            tmp_dict = {'x': task_x[task[0]: task[1]], 'y': task_y[task[0]: task[1]],
                        'mu': task_mu[task[0]: task[1]], 'flag': task_flag[task[0]: task[1]]}
            sm.add(tmp_dict, 'sub_%d' % idx)
        while True:
            x0 = generate_x0_with_lower_bound(10, 2)  # generate random position within some range but with constraints
            if not_close:
                flag, obs1 = find_one_obstacle(x0, sf[:3], 0.5, 1, 4)
            else:
                flag, obs1 = find_one_obstacle_with_min_violation(x0, sf[:3], 0.5, 0.5, 1, 4)
            if flag == 1:
                break
        s0[:3] = x0
        predy = mdlfun(s0)
        solver.updateX0Xf(s0, sf)
        solver.updateObstacle(obs1, 0)
        task_x[i] = np.concatenate((x0, obs1))
        # solve it
        flag = solver.solveWithGuess(predy.astype(np.float64))
        if flag == 1:
            task_y[i] = solver.rst.x
            task_mu[i] = solver.rst.Mu
            task_flag[i] = 1
            num_ok += 1
    return num_ok


@numba.njit
def generate_x0_with_lower_bound(xbd, min_mag):
    """Generate a x0 randomly but with rejection of magnitude"""
    while True:
        x0 = (2 * np.random.random(3) - 1) * xbd
        if np.linalg.norm(x0) > min_mag:
            return x0


@numba.njit
def dist_pnt_to_line(obs, x0, xf):
    """Find distance to a line"""
    D = np.linalg.norm(x0[:3] - xf[:3])
    l1 = np.linalg.norm(obs[:3] - x0[:3])
    l2 = np.linalg.norm(obs[:3] - xf[:3])
    s = (l1 + l2 + D) / 2
    area = np.sqrt(s * (s - D) * (s - l1) * (s - l2))
    dist = 2 * area / D
    return dist


@numba.njit
def find_one_obstacle(x0, xf, mindist, minradius, maxradius):
    """Given x0 and xf, return one obstacles that intersects with the line from x0 to xf.

    Instead of calculating them, I will use random sampling with rejection.
    In several cases, there might be no possible solution, I wish to detect them first.
    In theory, you can only sample within certain range along the line, within l=sqrt((r+d)^2-r^2), [l, D-l]

    :param x0: ndarray, (3,) start position
    :param xf: ndarray, (3,) goal position
    :param mindist: float, the minimum distance the obstacle is to start / goal
    :param minradius: float, minimum radius of sphere
    :param maxradius: float, maximum radius of sphere
    """
    flag = 0  # indicates success
    D = np.linalg.norm(xf[:3] - x0[:3])
    r2 = np.random.random() * (maxradius - minradius) + minradius
    flag1, out1 = sample_one(x0[:3], xf[:3], mindist, minradius, maxradius)
    if flag1 > 0:
        return 1, out1
    else:
        return 0, out1


@numba.njit
def find_one_obstacle_with_min_violation(x0, xf, mindist, minvio, minradius, maxradius):
    """Given x0 and xf, return one obstacles that intersects with the line from x0 to xf.

    Instead of calculating them, I will use random sampling with rejection.
    In several cases, there might be no possible solution, I wish to detect them first.
    In theory, you can only sample within certain range along the line, within l=sqrt((r+d)^2-r^2), [l, D-l]

    :param x0: ndarray, (3,) start position
    :param xf: ndarray, (3,) goal position
    :param mindist: float, the minimum distance the obstacle is to start / goal
    :param minvio: float, minimum violation on straight line
    :param minradius: float, minimum radius of sphere
    :param maxradius: float, maximum radius of sphere
    """
    flag = 0  # indicates success
    D = np.linalg.norm(xf[:3] - x0[:3])
    r2 = np.random.random() * (maxradius - minradius) + minradius
    flag1, out1 = sample_one_with_min_violation(x0[:3], xf[:3], mindist, minvio, minradius, maxradius)
    if flag1 > 0:
        return 1, out1
    else:
        return 0, out1


@numba.njit
def sample_one(x0, xf, mindist, minradius, maxradius):
    flag = 0
    vec = xf - x0
    D = np.linalg.norm(vec)
    out = np.zeros(4)
    for i in range(100):  # maximum sample times
        r = np.random.random() * (maxradius - minradius) + minradius
        l = np.sqrt((r + mindist) ** 2 - r ** 2)
        if l > D / 2:
            continue
        # randomly sample on x axis
        t = np.random.random() * (D - 2 * l) + l
        if t > D/2:
            if D - t < r + mindist:
                min_r = np.sqrt((r + mindist) ** 2 - (D - t) ** 2)
            else:
                min_r = 0
        else:
            if t < r + mindist:
                min_r = np.sqrt((r + mindist) ** 2 - t ** 2)
            else:
                min_r = 0
        # randomly sample a point along the arc
        ratio = min_r / r
        randr = np.sqrt(np.random.random() * (1 - ratio ** 2) + ratio ** 2) * r
        rand_angle = np.random.random() * 2 * np.pi
        # create a rotation matrix in order to locate
        rot_mat = np.zeros((3, 3))
        rot_mat[:, 0] = vec / D
        # find one with minimum absolute number to locate second axis
        min_idx = np.argmin(np.abs(vec))
        if min_idx == 0:
            id2 = 1
            id3 = 2
        elif min_idx == 1:
            id2 = 0
            id3 = 2
        else:
            id2 = 0
            id3 = 1
        val = np.sqrt(rot_mat[id2, 0] ** 2 + rot_mat[id3, 0] ** 2)
        rot_mat[min_idx, 1] = 0
        rot_mat[id2, 1] = rot_mat[id3, 0] / val
        rot_mat[id3, 1] = -rot_mat[id2, 0] / val
        rot_mat[0, 2] = rot_mat[1, 0] * rot_mat[2, 1] - rot_mat[2, 0] * rot_mat[1, 1]
        rot_mat[1, 2] = -rot_mat[0, 0] * rot_mat[2, 1] + rot_mat[0, 1] * rot_mat[2, 0]
        rot_mat[2, 2] = rot_mat[0, 0] * rot_mat[1, 1] - rot_mat[0, 1] * rot_mat[1, 0]
        # get local coordinate
        r_local = np.array([t, randr * np.cos(rand_angle), randr * np.sin(rand_angle)])
        r_world = x0 + np.dot(rot_mat, r_local)
        out[:3] = r_world
        out[3] = r
        flag = 1
        break
    return flag, out


@numba.njit
def sample_one_with_min_violation(x0, xf, mindist, minvio, minradius, maxradius):
    """Sample an obstacle along a straight line connecting initial and final states.

    We have to make sure the line has at least $minvio violation

    In fact, mindist is the minimum distance from an obstacle to one point
    minvio is the minimum violation
    minradius is the minimum radius of the obstacle
    maxradius is the maximum radius of the obstacle
    """
    flag = 0
    vec = xf - x0
    D = np.linalg.norm(vec)
    out = np.zeros(4)
    for i in range(100):  # maximum sample times
        r = np.random.random() * (maxradius - minradius) + minradius
        tuber = r - minvio
        rplusd = r + mindist
        l = np.sqrt(rplusd ** 2 - tuber ** 2)
        if l > D / 2:  # no way to sample it
            continue
        # randomly sample on x axis
        t = np.random.random() * (D - 2 * l) + l
        if t > D/2:
            if D - t < rplusd:
                min_r = np.sqrt(rplusd ** 2 - (D - t) ** 2)
            else:
                min_r = 0
        else:
            if t < rplusd:
                min_r = np.sqrt(rplusd ** 2 - t ** 2)
            else:
                min_r = 0
        # randomly sample a point along the arc
        ratio = min_r / tuber
        randr = np.sqrt(np.random.random() * (1 - ratio ** 2) + ratio ** 2) * tuber
        rand_angle = np.random.random() * 2 * np.pi
        # create a rotation matrix in order to locate
        rot_mat = np.zeros((3, 3))
        rot_mat[:, 0] = vec / D
        # find one with minimum absolute number to locate second axis
        min_idx = np.argmin(np.abs(vec))
        if min_idx == 0:
            id2 = 1
            id3 = 2
        elif min_idx == 1:
            id2 = 0
            id3 = 2
        else:
            id2 = 0
            id3 = 1
        val = np.sqrt(rot_mat[id2, 0] ** 2 + rot_mat[id3, 0] ** 2)
        rot_mat[min_idx, 1] = 0
        rot_mat[id2, 1] = rot_mat[id3, 0] / val
        rot_mat[id3, 1] = -rot_mat[id2, 0] / val
        rot_mat[0, 2] = rot_mat[1, 0] * rot_mat[2, 1] - rot_mat[2, 0] * rot_mat[1, 1]
        rot_mat[1, 2] = -rot_mat[0, 0] * rot_mat[2, 1] + rot_mat[0, 1] * rot_mat[2, 0]
        rot_mat[2, 2] = rot_mat[0, 0] * rot_mat[1, 1] - rot_mat[0, 1] * rot_mat[1, 0]
        # get local coordinate
        r_local = np.array([t, randr * np.cos(rand_angle), randr * np.sin(rand_angle)])
        r_world = x0 + np.dot(rot_mat, r_local)
        out[:3] = r_world
        out[3] = r
        flag = 1
        break
    return flag, out


if __name__ == '__main__':
    main()
