#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2018 Gao Tang <gt70@duke.edu>
#
# Distributed under terms of the MIT license.

"""
show_bad_prediction.py

Validate on those models, see why we are doing such a bad job.
"""
from __future__ import print_function, division
import torch
import scipy.linalg
import sys, os, time
import numpy as np
import numba
import matplotlib.pyplot as plt

from gaolib.io import getArgs
from gaolib.train import MoMNet, modelLoader
from gaolib.math import Query
import gaolib.plot as pld

import names
from utility import glbVar, load_traj_data, load_valid_data, draw_one_obs_traj_nn as draw, add3dTraj, parseX
from cvPipeline import _find_kmean_classifier_regressor, _find_snn_model, _find_manual_classifier_regressor
from rolloutValidate import eval_pred_vio


def main():
    # snn checks snn model
    # manual checks manual clustering
    # kmean checks clustering with kmeans
    # kmeanid is a integer of index of kmeans strategy
    # check_n means how many data we check
    # nn means number of neighbors for each validation set
    # checkdatavio checks a serious problem previously
    args = getArgs('snn', 'manual', 'kmean', 'kmeanid0', 'checkn20', 'nn5', 'checkdatavio')
    if args.snn or args.manual or args.kmean:
        checkBad(args)
    if args.checkdatavio:
        checkDataViolation()


@numba.jit
def checkDataViolation():
    """See which violates which."""
    tj_data = load_traj_data()
    obs, tjf = tj_data['obs'], tj_data['tjf']
    ndata = obs.shape[0]
    print(ndata)
    vio0 = 0  # just number
    vio1 = 0
    for i in range(ndata):
        obsi = obs[i]
        tjfi = tjf[i]
        tX, _, _ = parseX(tjfi)
        obs1 = obsi[3:7]
        obs2 = obsi[7:]
        dist1 = np.linalg.norm(tX[:, :3] - obs1[:3], axis=1) - obs1[3]
        dist2 = np.linalg.norm(tX[:, :3] - obs2[:3], axis=1) - obs2[3]
        vio0 += np.sum(dist1 > -1e-4) < 20
        vio1 += np.sum(dist2 > -1e-4) < 20
    print(vio0, vio1)


def checkBad(args):
    """Check why snn performs bad on selected dataset."""
    tj_data = load_traj_data()
    obs, tjf = tj_data['obs'], tj_data['tjf']
    vd_data = load_valid_data()
    vobs, vtjf = vd_data['obs'], vd_data['tjf']
    if args.snn:
        # load snn model
        snn_mdl_name = _find_snn_model()
        the_fun = modelLoader(snn_mdl_name)
        _check_a_model(vobs, vtjf, obs, tjf, the_fun, args.checkn, args.nn, True)
    elif args.manual:
        mcls, mregs = _find_manual_classifier_regressor()
        man_mdl = MoMNet(mcls, mregs)
        man_label = np.load(names.MANUAL_CLUSTER_FILE)
        man_ft = np.load(names.MANUAL_FEATURE_V0)
        sVec = np.zeros((man_ft.shape[0], 3))
        for i in range(3):
            sVec[:, i] = np.sum(man_ft[:, i::3], axis=1)
        _check_a_mom_model(vobs, vtjf, obs, tjf, man_label, man_mdl, args.checkn, args.nn, True, sVec)
    elif args.kmean:
        kid = args.kmeanid
        cluster_data = np.load(names.KMEAN_CLUSTER_FILE)
        datai = cluster_data['%d' % kid].item()
        k_label = datai['label']
        kcls, kregs = _find_kmean_classifier_regressor(args.kmeanid)
        knn_mdl = MoMNet(kcls, kregs)
        _check_a_mom_model(vobs, vtjf, obs, tjf, k_label, knn_mdl, args.checkn, args.nn, True)


def _check_a_mom_model(vobs, vtjf, obs, tjf, label, mfun, check_n, n_neighbor, show_f_nn=False, man_ft=None):
    """Check a model which is a mixture of experts. This is fundamentally different from others.

    if man_ft which means manual feature is not None, I will print those values
    """
    predy = mfun.getPredY(vobs)
    vio = eval_pred_vio(vobs, predy)
    # sort by value
    vio_order = np.argsort(vio)
    # create list of query instance
    n_label = np.amax(label) + 1
    lst_query = []
    lst_index_map = []
    for i in range(n_label):
        mask = label == i
        nn = Query(obs[mask], tjf[mask], n_neighbor, scale=True)
        lst_query.append(nn)
        lst_index_map.append(np.where(mask)[0])

    def show_one(ax, idx):
        print('violation is %f' % vio[idx])
        print('pos ', vobs[idx, :3], ' obs0 ', vobs[idx, 3:7], ' obs1 ', vobs[idx, 7:])
        clas_y = mfun.getClusY(vobs[idx])
        clas_label = np.argmax(clas_y)
        nn = lst_query[clas_label]
        rst = nn.query(vobs[idx])
        index, dist = rst['index'], rst['dist']
        print('(scaled) dist is ', dist)
        print('(unscaled) dist is ', np.linalg.norm(nn.A_us[index] - vobs[idx], axis=1))
        draw(ax, vobs[idx], vtjf[idx], {'color': 'b', 'lw': 2}, {'color': 'b'}, False)
        add3dTraj(ax, predy[idx], ls='--', color='b')
        if man_ft is not None:
            print(man_ft[lst_index_map[clas_label][index]])
        for ind in index:
            print('pos ', nn.A_us[ind, :3], ' obs0 ', nn.A_us[ind, 3:7], ' obs1 ', nn.A_us[ind, 7:])
            # add3dTraj(ax, tjf[ind])
            ln = draw(ax, nn.A_us[ind], nn.B[ind], {}, {}, False)
            if show_f_nn:
                pred = mfun.getPredYi(nn.A_us[ind], clas_label)
                add3dTraj(ax, pred, ls='--', color=ln.get_color())
        pld.set_axes_equal(ax)

    for i in range(check_n):
        gd_id = vio_order[i]
        fig, ax = pld.get3dAxis()
        show_one(ax, gd_id)
        bd_id = vio_order[-1 - i]
        fig, ax = pld.get3dAxis()
        show_one(ax, bd_id)
        plt.show()


def _check_a_model(vobs, vtjf, obs, tjf, fun, check_n, n_neighbor, show_f_nn=False):
    """A subroutine to check a model.

    Parameters
    ----------
    vobs: ndarray, (n_valid, dim_obs) the validation set of obstacles
    vtjf: ndarray, (n_valid, dim_tjf) the validation set of trajectories
    obs: ndarray, (n_data, dim_obs) the training set of obstacle
    tjf: ndarray, (n_data, dim_tjf) the training set of trajectories
    fun: callable, the function
    check_n: int, number of bad and good examples to check
    n_neighbor: int, number of neighbors to query in training set
    show_f_nn: bool, if we also show prediction on neighbors
    """
    # first call to get prediction on validation set
    predy = fun(vobs)
    vio = eval_pred_vio(vobs, predy)
    # sort by value
    vio_order = np.argsort(vio)
    # create knn object
    nn = Query(obs, None, n_neighbor, scale=True)  # yes, we need scaling of this

    def show_one(ax, idx):
        print('violation is %f' % vio[idx])
        print('pos ', vobs[idx, :3], ' obs0 ', vobs[idx, 3:7], ' obs1 ', vobs[idx, 7:])
        rst = nn.query(vobs[idx])
        index, dist = rst['index'], rst['dist']
        print('(scaled) dist is ', dist)
        print('(unscaled) dist is ', np.linalg.norm(obs[index] - vobs[idx], axis=1))
        draw(ax, vobs[idx], vtjf[idx], {'color': 'b', 'lw': 2}, {'color': 'b'}, False)
        add3dTraj(ax, predy[idx], ls='--', color='b')
        for ind in index:
            print('pos ', obs[ind, :3], ' obs0 ', obs[ind, 3:7], ' obs1 ', obs[ind, 7:])
            # add3dTraj(ax, tjf[ind])
            ln = draw(ax, obs[ind], tjf[ind], {}, {}, False)
            if show_f_nn:
                pred = fun(obs[ind])
                add3dTraj(ax, pred, ls='--', color=ln.get_color())
        pld.set_axes_equal(ax)

    for i in range(check_n):
        gd_id = vio_order[i]
        fig, ax = pld.get3dAxis()
        show_one(ax, gd_id)
        bd_id = vio_order[-1 - i]
        fig, ax = pld.get3dAxis()
        show_one(ax, bd_id)
        plt.show()


if __name__ == '__main__':
    main()
