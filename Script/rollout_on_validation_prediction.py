#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2018 Gao Tang <gt70@duke.edu>
#
# Distributed under terms of the MIT license.

"""
rollout_on_validation_prediction.py

I have no way but to move code for new experiments to this old repository.
By filename the user is able to tell the difference.
"""
from __future__ import print_function, division
import sys, os, time
import numpy as np
import matplotlib.pyplot as plt
import cPickle as pkl
import itertools

from gaolib.io import getArgs, getJson
from gaolib.dynsys import QuadCopter
from gaolib.controller import InterpTrajectory, TrackLQR, StabilizeLQR
from gaolib.simulator import Simulator
import gaolib.parallel as pp

import names
from utility import glbVar, parseX


home = os.path.expanduser('~')
THE_DIRECTORY = './'
VALIDATIONFILE = 'data/validation_trajdata.npz'
cfg = getJson(names.CFGFILE)
N, dimx, dimu = cfg['N'], cfg['dimx'], cfg['dimu']
ubase = np.array(cfg['ubase'])
Q, R, F = np.array(cfg['Q']), np.array(cfg['R']), np.array(cfg['F'])


def main():
    args = getArgs('pcakmean', 'snn')

    config = {
                'Q': 10*np.eye(dimx),
                'R': np.eye(dimu),
                'F': 100*np.eye(dimx),
                'sys': QuadCopter(),
                'simdt': 0.02,
                'Ubd': (np.array([-1]), np.array([1])),
                'tfadd': 5.0  # give sufficient time
            }

    # load validation set
    v_data = np.load(VALIDATIONFILE)
    x0 = v_data['obs']

    if args.pcakmean:
        datapath = os.path.join(THE_DIRECTORY, 'data/pca_kmean_label_validation_predict.npz')
        lbl_name = 'pca_kmean'
    if args.snn:
        datapath = os.path.join(THE_DIRECTORY, 'data/snn_validation_predict.npy')
        lbl_name = 'snn'
        y = np.load(datapath)
        error, vio = solTrackTest(x0, y)
        print('err mean ', np.mean(error), ' err max ', np.amax(error))
        print('vio mean ', np.mean(vio), ' vio max ', np.amax(vio))
        pkl_path = os.path.join(THE_DIRECTORY, 'data/%s_rollout_result.pkl' % lbl_name)
        with open(pkl_path, 'wb') as f:
            pkl.dump({'error': error, 'vio': vio}, f)  # save this for image drawing
        return

    data = np.load(datapath)
    keys = data.keys()
    keys.sort(key=int)  # sort keys by value

    verror = []
    vvio = []

    for key in keys:
        print('study key ', key)
        y = data[key]
        error, vio = solTrackTest(x0, y, config)
        verror.append(error)
        vvio.append(vio)
    print('err mean ', [np.mean(error) for error in verror])
    print('err max ', [np.amin(error) for error in verror])
    print('vio mean ', [np.mean(vio) for vio in vvio])
    print('vio max ', [np.amin(vio) for vio in vvio])
    pkl_path = os.path.join(THE_DIRECTORY, 'data/droneone/%s_rollout_result.pkl' % lbl_name)
    with open(pkl_path, 'wb') as f:
        pkl.dump({'error': verror, 'vio': vvio}, f)  # save this for image drawing
    fig, ax = plt.subplots()
    ax.hist(verror, bins=20, label=keys)
    ax.set_xlabel('Final State Error')
    ax.set_ylabel('Count')
    ax.set_yscale('log', nonposy='clip')
    ax.legend()
    fig.savefig(os.path.join(THE_DIRECTORY, 'gallery/droneone/%s_validation_rollout_error.pdf' % lbl_name))
    fig, ax = plt.subplots()
    ax.hist(vvio, bins=20, label=keys)
    ax.set_xlabel('Constraint Violation')
    ax.set_ylabel('Count')
    ax.set_yscale('log', nonposy='clip')
    ax.legend()
    fig.savefig(os.path.join(THE_DIRECTORY, 'gallery/droneone/%s_validation_rollout_violation.pdf' % lbl_name))
    plt.show()


def solTrackTest(x, y, config=None):
    """Since the final state is not fixed, I do not expect it could perform well.
    solFun takes in x0 and output a solution

    :param x: ndarray, (n_data, dim_x), the problem setting
    :param y: ndarray, (n_data, dim_sol), the prediction from your network
    :param config: dict, configuration for trajectory tracking controller:w
    :return error: ndarray, (n_data,) the final state error
    :return vSim: list, the results from each simulation
    """
    # prepare for rollout
    simN = y.shape[0]
    verror = np.zeros(simN)
    vio = np.zeros(simN)
    # we will run those in parallel
    nProcess = 8
    lst_task = pp.getTaskSplit(simN, nProcess)
    x_arr, y_arr, err_arr, vio_arr = pp.getSharedNumpy(x, y, verror, vio)
    mp = pp.MulProcess(single_track_result, lst_task, nProcess, x_arr, y_arr, err_arr, vio_arr)
    # mp.debug()
    mp.run(wait=0.1)
    error, vio = pp.getNumpy(err_arr, vio_arr)
    return error, vio


def single_track_result(task, x_arr, y_arr, error_arr, vio_arr, **kwargs):
    """Do a simple simulation"""
    x, y, error, vio = pp.getNumpy(x_arr, y_arr, error_arr, vio_arr)
    # initialize the controller
    sysm = QuadCopter()
    Q = kwargs.get('Q', 10*np.eye(dimx))
    R = kwargs.get('R', 0.01*np.eye(dimu))
    F = kwargs.get('F', 10*np.eye(dimx))
    additTime = kwargs.get('addTf', 3)
    dt = kwargs.get('dt', 0.02)
    x0 = np.zeros(dimx)

    for i in range(task[0], task[1]):
        print('entering %d' % i)
        sol = y[i]
        task = x[i]
        x0[:3] = task[:3]
        tjX, tjU, tf = parseX(sol, N, dimx, dimu)
        if tf <= 0:
            xf = x0
            error[i] = np.linalg.norm(xf)
            vio[i] = 0
        else:
            traj = InterpTrajectory(tjX, tjU, tf)  # construct the trajectory
            targetxf = np.zeros(dimx)
            targetuf = np.array(cfg['ubase'])
            traj.tfFun = lambda _: (targetxf, targetuf)
            ctrller = TrackLQR(sysm, traj, Q, R, F)
            stbCtrller = StabilizeLQR(sysm, Q, R, targetxf, targetuf, method='d', dt=dt)
            ctrller.scheduleK.setObjf(stbCtrller.K)
            ulb, uub = np.array(cfg['ulb']), np.array(cfg['uub'])
            ulb += targetuf
            uub += targetuf
            ctrller.setUbd(ulb, uub)
            stbCtrller.setUbd(ulb, uub)
            # setup the simulation environment
            sim = Simulator(sysm, method='c', ctrl=ctrller, dt=dt)
            sim.statefun = statefun  # set statefun
            # set initial state
            sim.setX0(x0)
            tspan = [0, tf]
            rst = sim.simulate(tspan)
            vt, vX = rst['vt'], rst['vX']
            # use another simulator only stabilizing the system
            sim2 = Simulator(sysm, method='d', ctrl=stbCtrller, dt=dt)
            sim2.setX0(vX[-1])
            sim2.statefun = statefun
            tspan = [vt[-1], vt[-1] + additTime]
            rst2 = sim2.simulate(tspan)
            # get final state
            xf = rst2['vX'][-1]
            error[i] = np.linalg.norm(xf)
            vio0 = np.amin(np.linalg.norm(rst['vX'][:, :3] - task[3:6], axis=1) - task[6])
            vio1 = np.amin(np.linalg.norm(rst2['vX'][:, :3] - task[3:6], axis=1) - task[6])
            vio[i] = min(vio0, vio1)


def statefun(t, x):
    """The function that checks if the system is still under control."""
    normx = np.linalg.norm(x)
    if normx < 0.1:
        return 1
    elif normx > 20.0:
        return -1
    else:
        return 0


if __name__ == '__main__':
    main()
