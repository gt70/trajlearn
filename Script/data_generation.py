#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2019 motion <motion@motion-MS-7971>
#
# Distributed under terms of the MIT license.

"""
Subroutines for data generation.
"""
import numpy as np
from gaolib.parallel import getNumpy, getSharedNumpy, MulProcess, getTaskSplit


class DataGenSolver(object):
    """An abstract solver"""
    def update_problem(self, p):
        """A function that has to be overriden, given specific problem parameter,
        it updates its internal states.

        This design is preferred since the same problem might be solved using many guesses.
        """
        raise NotImplementedError

    def solve_guess(self, guess):
        """A function that has to be overriden, given initial guess,
        it returns the results from SNOPT"""
        raise NotImplementedError


def data_gen_with_guess(pfun, zfun, solver_fun, n_process, n_data, fnm,
                        xname='x', yname='y', flagname='flag'):
    """A subroutine for data generation with initial guess.
    Parameters
    ----------
    pfun: callable, pfun() returns a function that calling it returns problem parameter
    zfun: callable, zfun() returns a function that calling it using p returns an initial guess
    solver: callable, solver_fun() returns a Solver object
    n_process: int, number of cores for simulation
    n_data: int, data size being generated
    fnm: str, the filename to store data
    xname: str, the name for saving p
    yname: str, the name for saving z
    flagname: str, the name for saving solving flags
    Returns
    -------
    n_success: int, return total number of successful solving.
    """
    # first step is to detect problem size
    p_tmp = pfun()()
    dimp = p_tmp.shape[0]
    z_tmp = zfun()(p_tmp)
    dimz = z_tmp.shape[0]
    # allocate space
    param = np.zeros((n_data, dimp), dtype=np.float32)
    sol = np.zeros((n_data, dimz), dtype=np.float32)
    solve_flag = np.zeros(n_data, dtype=np.int8)
    param_arr, sol_arr, flag_arr = getSharedNumpy(param, sol, solve_flag)
    lstTask = getTaskSplit(n_data, n_process)
    mp = MulProcess(_single_run, np.arange(n_process), n_process, lstTask,
                    pfun, zfun, solver_fun, param_arr, sol_arr, flag_arr)
    rst = mp.run(wait=0.05)
    param, sol, solve_flag = getNumpy(param_arr, sol_arr, flag_arr)
    save_dict = {xname: param, yname: sol, flagname: solve_flag}
    np.savez(fnm, **save_dict)
    return np.sum(rst)


def _single_run(idx, lstTask, pfun, zfun, solver_fun, p_arr, z_arr, f_arr):
    """The single process being run"""
    task = lstTask[idx]
    np.random.seed(10000 + np.random.randint(task[1]))
    num_task = task[1] - task[0]
    p, z, flag = getNumpy(p_arr, z_arr, f_arr)
    solver = solver_fun()
    p_fun = pfun()
    z_fun = zfun()
    # start looping
    num_ok = 0
    for i in range(task[0], task[1]):
        pi = p_fun()
        zi = z_fun(pi)  # the guess
        p[i] = pi
        solver.update_problem(pi)
        rst = solver.solve_guess(zi.astype(np.float64))
        z[i] = rst.sol
        flag[i] = rst.flag
        if rst.flag == 1:
            num_ok += 1
    return num_ok
