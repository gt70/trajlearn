cmake_minimum_required(VERSION 2.6)
project(QuadCopter)

# set up with jsoncpp library
find_package(jsoncpp REQUIRED)

set(JSONINCLUDE /usr/local/include)
set(JSONLINK "jsoncpp")
message(STATUS "Jsoncpp is in ${JSONINCLUDE}")
message(STATUS "Jsonlink is in ${JSONLINK}")

# set up with snopt library
set(SNOPTINCLUDE "${CMAKE_SOURCE_DIR}/Library/snopt/include")
set(SYS_SNOPT $ENV{SNOPT_DIR})
message(STATUS "SNOPT is ${SYS_SNOPT}")
if(${SYS_SNOPT} STREQUAL "")
set(SNOPTLIBRARY "${CMAKE_SOURCE_DIR}/Library/snopt/lib")
else()
set(SNOPTLIBRARY ${SYS_SNOPT})
endif()
message(STATUS "Use snopt library path ${SNOPTLIBRARY}")
set(SNOPTLINK snopt7_cpp snopt7)

# find external libraries
find_package (Eigen3 REQUIRED)
set(EIGEN_DIR ${EIGEN3_INCLUDE_DIR})
find_package(pybind11)

set(INCLUDEDIR "Library/CommonHeaders")

# setup C++11 stuff
set(CMAKE_CXX_FLAGS "-std=c++11")
set(CMAKE_CXX_STANDARD 11)

# setup build, output, etc
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${PROJECT_SOURCE_DIR}/bin)
set(CMAKE_BUILD_DIRECTORY ${PROJECT_SOURCE_DIR}/Build)

# include directories
include_directories(${INCLUDEDIR})
include_directories(${JSONINCLUDE})
include_directories(${SNOPTINCLUDE})
include_directories(${PROJECT_SOURCE_DIR}/Headers)

# link directories
message("Link ${SNOPTLIBRARY}")
link_directories(${SNOPTLIBRARY}/lib)
link_directories(${SNOPTLIBRARY})


# set up executable tqt
set(tqt_FILES
    Src/testQuadSolve.cpp Src/toyfunction.cpp Src/TrajOpt.cpp Src/Utility.cpp
        Headers/SNPSolve.h Headers/toyfunction.hh Headers/TrajOpt.h Headers/Utility.h)
add_executable(tqt ${tqt_FILES})
add_dependencies(tqt jsoncpp_lib)
target_link_libraries(tqt
    ${JSONLINK}  ${SNOPTLINK})

# generate the library
pybind11_add_module(
    server MODULE Src/solverwrapper.cpp Src/toyfunction.cpp Src/TrajOpt.cpp Src/Utility.cpp Src/solveServer.cpp
    Headers/SNPSolve.h Headers/toyfunction.hh Headers/TrajOpt.h Headers/Utility.h Headers/utilityClass.h
    )
target_link_libraries(server PRIVATE
    ${JSONLINK} ${SNOPTLINK}
    )
add_dependencies(server jsoncpp_lib)
target_include_directories(server PUBLIC ${EIGEN_DIR})

set_target_properties(server PROPERTIES
    LIBRARY_OUTPUT_DIRECTORY  "${PROJECT_SOURCE_DIR}/Script"
       PREFIX "lib")
