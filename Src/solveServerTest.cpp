/*
 * solveServerTest.cpp
 * Copyright (C) 2017 Gao <gao.tang@duke.edu>
 *
 * Distributed under terms of the  license.
 */

#include "solveServer.h"
#include "utilityClass.h"


dirTranODE *dT = NULL;
tigerSys *sys = NULL;


//define the function to be integrated here
void funwrapper(const double t, cRefV y, cRefV p, RefV dy, RefM J){
    dT->RKDynFun(t, y, p, dy, J);
}


int main(int argc, char *argv[]){
    if(argc != 3){
        printf("Need config in and out file!\n");
        exit(0);
    }
    srand(time(NULL));
    //Initialize dirTranODE instance
    sys = new Rotor();

    //create a server using the config file
    solveServer server(argv[1]);

    //directly call massSim
    int flag = server.massSim();

    /* Write the result file into a json and store it */
    server.dump(argv[2], NULL);

    delete sys;
    return 1;
}


