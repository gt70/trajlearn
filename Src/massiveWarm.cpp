//
// Created by Gao Tang on Sun 17 Dec 2017 09:39:06 AM EST
//

/* do massive simulation with changing initial state and obstacle */

#include "SNPSolve.h"
#include "TrajOpt.h"
#include "stdio.h"
#include "math.h"
#include "json/json.h"
#include "utilityClass.h"
#include <vector>
#include <fstream>
#include "modelLoader.h"
#include "QueryClass.h"
#include "cnpy.h"
#include <vector>

//Declare global variables
dirTranODE *dT;

//define the function to be integrated here
void funwrapper(const double t, cRefV y, cRefV p, RefV dy, RefM J){
    dT->RKDynFun(t, y, p, dy, J);
}

int main(int argc, char *argv[]){
    srand((unsigned int) time(0));
    //Initialize dirTranODE instance
    Rotor sys;
    //The obstacle avoidance constraint
    avoidConstraint obsCon;
    //Optionally read the json file that user provided
    std::ifstream fin("resource/droneConfig.json", std::ios::in);
    if(!fin.is_open()){
        printf("Fail to open config file\n");
        exit(0);
    }
    Json::Value jsin;
    fin >> jsin;
    dT = new dirTranODE(jsin);
    dT->system = &sys;
    double tfmin = dT->tfmin;
    int N = dT->N;
    /* Setting of obstacles, not necessary here, but is required for constructing problem*/
    obsCon.init(jsin);
    dT->conFun = &obsCon;
    fin.close();
    /* Read the database file for warm-start */
    std::string datafnm("data/ObsData.npz");
    cnpy::npz_t my_npz = cnpy::npz_load(datafnm);
    cnpy::NpyArray arr_tjf = my_npz["tjf"];
    cnpy::NpyArray arr_tj0 = my_npz["tj0"];
    cnpy::NpyArray arr_obs = my_npz["obs"];
    cnpy::NpyArray arr_index = my_npz["index"];
    int nData = arr_tjf.shape[0], lenSol = arr_tjf.shape[1];
    std::cout << "nData is " << nData << " lenSol is " << lenSol << std::endl;
    double *tjf = arr_tjf.data<double>();
    double *tj0 = arr_tj0.data<double>();
    double *obs = arr_obs.data<double>();
    int *index = arr_index.data<int>();
    double *x = new double[nData * 7];  // pos and obs
    for(int i = 0; i < nData; i++){
        V_Copy(x + 7*i, tj0+(index[i]*lenSol), 3);  //copy pos
        V_Copy(x + 7*i + 3, obs + 4*i, 4);  //copy obs
    }
    int querynum = 1, nn_ = 5;
    queryClass Query(7, lenSol, nData, querynum, nn_, x, tjf);  // from x to tjf
    Matrix<int> indMat(new int[nn_], querynum, nn_);
    /* read mssConfig file for running setup */
    fin.open("resource/mssConfig.json", std::ios::in);
    if(!fin.is_open()){
        printf("Fail to open config file\n");
        exit(0);
    }
    jsin.clear();
    fin >> jsin;
    int simN = jsin["Nsample"].asInt();
    double minRadius = jsin["minRadius"].asDouble();
    int maxTrial = jsin["maxTrial"].asInt();
    int minTrial = jsin["mintrial"].asInt();
    int dimx = sys.getdimx(), dimu = sys.getdimu();
    VX xlb(dimx), xub(dimx), dx(dimx);
    for (int i = 0; i < dimx; i++) {
        xlb(i) = jsin["xlb"][i].asDouble();
        xub(i) = jsin["xub"][i].asDouble();
    }
    dx = (xub - xlb);
    // read obstacle information
    VX obslb(4), obsub(4);
    for(int i = 0; i < 4; i++){
        obslb(i) = jsin["obslb"][i].asDouble();
        obsub(i) = jsin["obsub"][i].asDouble();
    }
    fin.close();
    /* Construct SNOPT problem */
    SNPSetting SNP(dT);
    traj tj = dT->ranGentj();
    SNP.SNPinit(tj);
    /* Prepare for files to record everything, basically, we need what states we tried and what results we got */
    std::string fnm, append;
    if(argc > 1){
        append = std::string(argv[1]);
    }
    fnm = std::string("Result/failStatesWarm") + append + std::string(".dat");
    FILE *fpfail = fopen(fnm.c_str(), "wb");
    fnm = std::string("Result/detailResultsWarm") + append + std::string(".dat");
    FILE *fpdtl = fopen(fnm.c_str(), "wb");
    if(!fpfail || !fpdtl){
        exit(0);
    }
    /* Loop over random states */
    int numFail = 0, numActive = 0, numOK = 0;
    double *bestx = new double[dT->numVar];
    double *sol = new double[dT->numVar];
    double *tmpx = new double[dT->numVar]; //store the result from model evaluation
    for (int simn = 0; simn < simN; simn++) {
        //Randomly generate xf since we fix x0
        VX randx0(dimx);
        for (int i = 0; i < dimx; i++) {
            randx0(i) = (double)rand()/RAND_MAX * dx(i) + xlb(i);
        }
        double dis0 = randx0.norm();
        if(dis0 < minRadius){
            simn--;
            continue;
        }
        dT->xf.setZero();
        dT->x0 = randx0;  //so we have fixed target and varying initial
        //Randomly generate obstacle
        double obs[4];
        while(1){
            for (int i = 0; i < 4; i++){
                obs[i] = (double)rand()/RAND_MAX*(obsub(i) - obslb(i)) + obslb(i);
            }
            // jump out if obs is not in collision with initial and final points
            obsCon.update(obs);  //Change information of obstacle
            if(obsCon.violate(dT->x0.data()) || obsCon.violate(dT->xf.data()))
                continue;
            else
                break;
        }
        // do the query
        double x0[7];
        V_Copy(x0, randx0.data(), 3);
        V_Copy(x0 + 3, obs, 4);
        Query.query(x0, indMat);
        // loop over neighbors only
        int flag = 0;
        int succeed = 0;
        double bestJ = 1e10;
        for(int tryind = 0; tryind < nn_; tryind++){
            V_Copy(sol, tjf + indMat[0][tryind]*lenSol, lenSol);  //copy neighbor results
            flag = SNP.SNPsolve(sol);
            if (flag == 1) {
                succeed++;
                if(SNP.getObj() < bestJ){
                    bestJ = SNP.getObj();
                    V_Copy(bestx, SNP.getX(), dT->numVar);
                }
                if(succeed >= minTrial)
                    break;
            }
        }
        // store the data if a solution is obtained.
        if(succeed > 0){
            V_Copy(SNP.getX(), bestx, dT->numVar);
            numOK++;
            SNP.basicSaveAll(fpdtl, true);  //save x0, xf, J, sol
            fwrite(obs, sizeof(double), 4, fpdtl); //save obstacle location information, too
        }
        else{
            numFail++;
            fwrite(dT->x0.data(), sizeof(double), dimx, fpfail);
            fwrite(dT->xf.data(), sizeof(double), dimx, fpfail);
            fwrite(obs, sizeof(double), 4, fpfail);
        }
    }
    printf("Succeed : %d, Active %d, Fail : %d\n", numOK, numActive, numFail);
    delete dT;
    delete[] x;
    delete[] bestx;
    delete[] tmpx;
    delete[] sol;
    fclose(fpfail);
    fclose(fpdtl);
    return 1;
}
