//
// Created by Gao Tang on Thu 21 Dec 2017 07:54:44 PM EST
//

/* Do this, use fixed initial state, only change obstacle, see if we can learn this */

#include "SNPSolve.h"
#include "TrajOpt.h"
#include "stdio.h"
#include "math.h"
#include "json/json.h"
#include "utilityClass.h"
#include "utilityFun.h"
#include <vector>
#include <fstream>
#include "modelLoader.h"
#include "cnpy.h"
#include <vector>

//Declare global variables
dirTranODE *dT;

//Declare several functions
void genTraj(traj &tj, FILE *fp, int numTraj, int solLen, int N, int dimx, int dimu);
void genTraj(traj &tj, double *sol, int numTraj, int solLen);

//define the function to be integrated here
void funwrapper(const double t, cRefV y, cRefV p, RefV dy, RefM J){
    dT->RKDynFun(t, y, p, dy, J);
}

int main(int argc, char *argv[]){
    srand((unsigned int) time(0));
    //Initialize dirTranODE instance
    Rotor sys;
    //The obstacle avoidance constraint
    avoidConstraint obsCon;
    //Optionally read the json file that user provided
    std::ifstream fin("resource/droneConfig.json", std::ios::in);
    if(!fin.is_open()){
        printf("Fail to open config file\n");
        exit(0);
    }
    Json::Value jsin;
    fin >> jsin;
    dT = new dirTranODE(jsin);
    dT->system = &sys;
    double tfmin = dT->tfmin;
    int N = dT->N, dimx = dT->dimx, dimu = dT->dimu;
    /* Setting of obstacles, not necessary here, but is required for constructing problem*/
    obsCon.init(jsin);
    dT->conFun = &obsCon;
    fin.close();
    /* read mssConfig file for running setup */
    fin.open("resource/mssConfig.json", std::ios::in);
    if(!fin.is_open()){
        printf("Fail to open config file\n");
        exit(0);
    }
    jsin.clear();
    fin >> jsin;
    int simN = jsin["Nsample"].asInt();
    double minRadius = jsin["minRadius"].asDouble();
    int maxTrial = jsin["maxTrial"].asInt();
    int minTrial = jsin["mintrial"].asInt();
    VX initX = dT->x0;  // copy initial state
    // read obstacle information
    VX obslb(4), obsub(4);
    for(int i = 0; i < 4; i++){
        obslb(i) = jsin["obslb"][i].asDouble();
        obsub(i) = jsin["obsub"][i].asDouble();
    }
    fin.close();
    // laod the collision-free model
    std::string mdlfnm("/home/motion/GAO/ExplicitMPC/QuadCopter/Script/models/exportModel.json");
    mlpModel mdl(mdlfnm);
    int numVar = dT->numVar;
    // evaluate and get solution
    VX Sol(dT->numVar);
    mdl.eval(initX.data(), Sol.data());

    /* Construct SNOPT problem */
    SNPSetting SNP(dT);
    traj tj = dT->ranGentj();
    tj.copyfrom(Sol.data());
    SNP.SNPinit(tj);
    /* Prepare for files to record everything, basically, we need what states we tried and what results we got */
    std::string fnm, append;
    if(argc > 1){
        append = std::string(argv[1]);
    }
    char savenames[10];
    fnm = std::string("Result/varyObs") + append + std::string(".npz");
    cnpy::npz_save(fnm, "N", &N, {1}, "w");
    cnpy::npz_save(fnm, "dimx", &dimx, {1}, "a");
    cnpy::npz_save(fnm, "dimu", &dimu, {1}, "a");
    cnpy::npz_save(fnm, "x0", initX.data(), {dimx}, "a");
    cnpy::npz_save(fnm, "sol0", Sol.data(), {numVar}, "a");
    /* Loop over random obstacles */
    int numFail = 0, numOK = 0;
    double *bestx = new double[dT->numVar];
    double *sol = new double[dT->numVar];
    double *tmpx = new double[numVar]; //store the result from model evaluation
    double minradius = obslb(3), maxradius = obsub(3);
    double rdobs[4];
    // allocate space for storing results, conservative enough
    double *tjf = new double[dT->numVar * simN];
    double *obsf = new double[4 * simN];
    double *tjJ = new double[simN];
    double obstol = 0.2;  // not too close to initial and final
    int storeIndex = 0;  // uses to control memory
    for (int simn = 0; simn < simN; simn++) {
        // find the memory address to write results
        double *tjNow = tjf + storeIndex * numVar;
        double *obsNow = obsf + 4 * storeIndex;
        tj.copyfrom(Sol.data());
        // randomly place the obstacle
        while(1){
            int rdind = genRandN(1, N - 1);
            double rdradius = genRand(minradius, maxradius);
            double distance = genRand(0, rdradius);
            rdobs[3] = rdradius;
            genRandDirWDisCenter(tj.X.col(rdind).data(), distance, rdobs);
            // jump out if obs is not in collision with initial and final points
            obsCon.update(rdobs);  //Change information of obstacle
            if(obsCon.violate(dT->x0.data(), obstol) || obsCon.violate(dT->xf.data(), obstol))
                continue;
            else
                break;
        }
        V_Copy(obsNow, rdobs, 4);
        // Try to solve this problem
        int flag = 0;
        int succeed = 0;
        // directly solve without any modification
        flag = SNP.SNPsolve(tj);
        if(flag == 1){
            succeed = 1;
        }
        else{
            /*If sol not found, we try maxTrial times and adding perturbation to previous one*/
            std::vector<int> vioIndex;
            for(int i = 0; i < N; i++){
                if(obsCon.violate(tj.X.col(i).data()))
                    vioIndex.push_back(i);
            }
            if(vioIndex.size() > 0){
                for(auto i : vioIndex){
                    obsCon.lazyRepair(tj.X.col(i).data());
                    tj.U.col(i) += VX::Random(dimu) * 0.3;
                    tj.X.col(i) += VX::Random(dimx) * 0.2;
                }
                tj.copyto(sol);
            } // now we get a basic solution, then we add perturbation to it
            double bestJ = 1e10;
            for (int j = 0; j < maxTrial; ++j) {
                V_Copy(tmpx, sol, dT->numVar);
                for(int i = 0; i < N*dimx; i++){
                    tmpx[i] += ((double)rand()/RAND_MAX - 0.5) * 2 * 0.2;
                }
                flag = SNP.SNPsolve(tmpx);  //solve using random, it might be bad
                if (flag == 1) {
                    succeed++;
                    if(SNP.getObj() < bestJ){
                        bestJ = SNP.getObj();
                        V_Copy(bestx, SNP.getX(), dT->numVar);
                    }
                    if(succeed >= minTrial)
                        break;
                }
                else {
                    flag = 0;
                }
            }
            V_Copy(SNP.getX(), bestx, numVar);
            SNP.getF()[0] = bestJ;
        }
        // store the data if a solution is obtained.
        if(succeed > 0){
            numOK++;
            //copy sol stored there
            V_Copy(tjNow, SNP.getX(), numVar);
            tjJ[storeIndex] = SNP.getObj();
            storeIndex += 1;
            printf("%d out of %d at %s", numOK, simN, append.c_str());
        }
        else{
            numFail++;
        }
    }
    // save results for current episode into the npz file
    sprintf(savenames, "tjf");
    cnpy::npz_save(fnm, savenames, tjf, {storeIndex, numVar}, "a");
    sprintf(savenames, "obs");
    cnpy::npz_save(fnm, savenames, obsf, {storeIndex, 4}, "a");
    sprintf(savenames, "Jf");
    cnpy::npz_save(fnm, savenames, tjJ, {storeIndex, 1}, "a");
    printf("Succeed : %d, Fail : %d\n", numOK, numFail);
    delete dT;
    delete[] bestx;
    delete[] tmpx;
    delete[] sol;
    delete[] tjf;
    delete[] tjJ;
    delete[] obsf;
    //fclose(freefp);
    return 1;
}

// randomly draw a trajectory from that file
void genTraj(traj &tj, FILE *fp, int numTraj, int solLen, int N, int dimx, int dimu){
    int index = (int)floor((double)rand()/RAND_MAX * numTraj);
    fseek(fp, index*solLen*sizeof(double), SEEK_SET);
    int lenSol = N*dimx + (N - 1) * dimu + 1;
    int lenHead = 1 + 2 * dimx;
    fseek(fp, lenHead * sizeof(double), SEEK_CUR);
    double *x = new double[lenSol];
    fread(x, sizeof(double), lenSol, fp);
    tj.copyfrom(x);
    delete[] x;
}

void genTraj(traj &tj, double *sol, int numTraj, int solLen){
    int index = genRandN(0, numTraj);
    tj.copyfrom(sol + index * solLen);
    return;
}
