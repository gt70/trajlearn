//
// Created by Gao Tang on Mon 01 Jan 2018 09:26:29 AM EST
//

/* Warm start mode */

#include "SNPSolve.h"
#include "TrajOpt.h"
#include "stdio.h"
#include "math.h"
#include "json/json.h"
#include "utilityClass.h"
#include "utilityFun.h"
#include <vector>
#include <fstream>
#include "modelLoader.h"
#include "cnpy.h"
#include "QueryClass.h"
#include <vector>
#include <thread>
#include <chrono>

//Declare global variables
dirTranODE *dT;

//Declare several functions
void genTraj(traj &tj, FILE *fp, int numTraj, int solLen, int N, int dimx, int dimu);
void genTraj(traj &tj, double *sol, int numTraj, int solLen);
void checkPointer(void *pnt);
void saveFile(FILE *fp, int N, std::vector<double*> vec, std::vector<int> size);
void Fwrite(const void *ptr, size_t size, size_t count, FILE *stream){
    bool succeed = false;
    long fpos = ftell(stream);
    while(1){
        size_t realcount = fwrite(ptr, size, count, stream);
        fflush(stream);
        if(realcount != count){
            printf("fwrite error\n");
            std::this_thread::sleep_for(std::chrono::milliseconds(200));
            fseek(stream, fpos, SEEK_SET);
        }
        else{
            break;
        }
    }
}

void recordInfo(const char *msg){
    FILE *fp = NULL;
    while(1){
        fp = fopen("log.txt", "wt");
        if(fp)
            break;
        else
            std::this_thread::sleep_for(std::chrono::milliseconds(200));
    }
    fprintf(fp, "%s\n", msg);
    fflush(fp);
    fclose(fp);
}

//define the function to be integrated here
void funwrapper(const double t, cRefV y, cRefV p, RefV dy, RefM J){
    dT->RKDynFun(t, y, p, dy, J);
}

int main(int argc, char *argv[]){
    srand((unsigned int) time(0));
    char msg[100];
    //Initialize dirTranODE instance
    Rotor sys;
    //The obstacle avoidance constraint
    avoidConstraint obsCon;
    //Optionally read the json file that user provided
    std::ifstream fin("resource/droneConfig.json", std::ios::in);
    if(!fin.is_open()){
        printf("Fail to open config file\n");
        exit(0);
    }
    Json::Value jsin;
    fin >> jsin;
    dT = new dirTranODE(jsin);
    dT->system = &sys;
    double tfmin = dT->tfmin;
    int N = dT->N, dimx = dT->dimx, dimu = dT->dimu;
    /* Setting of obstacles, not necessary here, but is required for constructing problem*/
    obsCon.init(jsin);
    dT->conFun = &obsCon;
    fin.close();
    /* Read the database file for warm-start */
    std::string datafnm("data/ObsData.npz");
    cnpy::npz_t my_npz2 = cnpy::npz_load(datafnm);
    cnpy::NpyArray arr_tjf = my_npz2["tjf"];
    cnpy::NpyArray arr_tj0 = my_npz2["tj0"];
    cnpy::NpyArray arr_obs = my_npz2["obs"];
    cnpy::NpyArray arr_index = my_npz2["index"];
    int nData = arr_tjf.shape[0], lenSol = arr_tjf.shape[1];
    std::cout << "nData is " << nData << " lenSol is " << lenSol << std::endl;
    double *tjf_ = arr_tjf.data<double>();
    double *tj0_ = arr_tj0.data<double>();
    double *obs_ = arr_obs.data<double>();
    int *index_ = arr_index.data<int>();
    double *x = new double[nData * 7];  // pos and obs
    checkPointer(x);
    for(int i = 0; i < nData; i++){
        V_Copy(x + 7*i, tj0_ + (index_[i]*lenSol), 3);  //copy pos
        V_Copy(x + 7*i + 3, obs_ + 4*i, 4);  //copy obs
    }
    int querynum = 1, nn_ = 3;
    queryClass Query(7, lenSol, nData, querynum, nn_, x, tjf_);  // from x to tjf
    Matrix<int> indMat(new int[nn_], querynum, nn_);
    /* read mssConfig file for running setup */
    fin.open("resource/mssConfig.json", std::ios::in);
    if(!fin.is_open()){
        printf("Fail to open config file\n");
        exit(0);
    }
    jsin.clear();
    fin >> jsin;
    int simN = jsin["Nsample"].asInt();
    int saveFreq = 100;  // save whenever this number of major iteration is finished
    if(jsin.isMember("saveFreq")){
        saveFreq = jsin["saveFreq"].asInt();
    }
    int obsN = 10;
    if(jsin.isMember("obsN")){
        saveFreq = jsin["obsN"].asInt();
    }
    double minRadius = jsin["minRadius"].asDouble();
    int maxTrial = jsin["maxTrial"].asInt();
    int minTrial = jsin["mintrial"].asInt();
    VX xlb(dimx), xub(dimx), dx(dimx);
    for (int i = 0; i < dimx; i++) {
        xlb(i) = jsin["xlb"][i].asDouble();
        xub(i) = jsin["xub"][i].asDouble();
    }
    dx = (xub - xlb);
    // read obstacle information
    VX obslb(4), obsub(4);
    for(int i = 0; i < 4; i++){
        obslb(i) = jsin["obslb"][i].asDouble();
        obsub(i) = jsin["obsub"][i].asDouble();
    }
    fin.close();
    /* read obstacle-free trajectories*/
    cnpy::npz_t my_npz = cnpy::npz_load("data/allTraj.npz");
    cnpy::NpyArray arr_sol = my_npz["Sol"];
    int numTraj = arr_sol.shape[0], solLen = arr_sol.shape[1];
    double *vec_sol =arr_sol.data<double>();

    /* Construct SNOPT problem */
    SNPSetting SNP(dT);
    traj tj = dT->ranGentj();
    SNP.SNPinit(tj);
    /* Prepare for files to record everything, basically, we need what states we tried and what results we got */
    std::string fnm, append;
    if(argc > 1){
        append = std::string(argv[1]);
    }
    char savenames[10];
    fnm = std::string("Result/obs") + append + std::string(".dat");
    FILE *fpout = fopen(fnm.c_str(), "wb");
    checkPointer(fpout);
    Fwrite(&N, sizeof(int), 1, fpout);
    Fwrite(&dimx, sizeof(int), 1, fpout);
    Fwrite(&dimu, sizeof(int), 1, fpout);
    Fwrite(&saveFreq, sizeof(int), 1, fpout);
    /*
    cnpy::npz_save(fnm, "N", &N, {1}, "w");
    cnpy::npz_save(fnm, "dimx", &dimx, {1}, "a");
    cnpy::npz_save(fnm, "dimu", &dimu, {1}, "a");
    */
    /* Loop over random states */
    int numFail = 0, numOK = 0;
    double *bestx = new double[dT->numVar];
    double *sol = new double[dT->numVar];
    double *tmpx = new double[dT->numVar]; //store the result from model evaluation
    double minradius = obslb(3), maxradius = obsub(3);
    double rdobs[4];
    // three variables for storing obstacle perturbation
    double *tj0 = new double[dT->numVar];
    double *tjf = new double[dT->numVar * saveFreq];
    double *tjJ = new double[saveFreq];
    double *obsf = new double[4 * saveFreq];
    std::vector<double*> vecPnt = {tjf, tjJ, obsf};
    std::vector<int> vecSz = {lenSol, 1, 4};
    checkPointer(bestx);
    checkPointer(sol);
    checkPointer(tmpx);
    checkPointer(tj0);
    checkPointer(tjf);
    checkPointer(tjJ);
    checkPointer(obsf);
    int succeedObs = 0;
    for (int simn = 0; simn < simN; simn++) {
        // randomly load a trajectory that is collision free
        genTraj(tj, vec_sol, numTraj, solLen);
        tj.copyto(tj0);
        // emphasis that x0 and xf are never changed
        dT->xf.setZero();
        dT->x0 = tj.X.col(0);  //so we have fixed target and varying initial
        // randomly place new obstacles
        for(int obsn = 0; obsn < obsN; obsn++){
            // randomly select a point other than first and last
            while(1){
                int rdind = genRandN(1, N - 1);
                double rdradius = genRand(minradius, maxradius);
                double distance = genRand(0, rdradius);
                rdobs[3] = rdradius;
                genRandDirWDisCenter(tj.X.col(rdind).data(), distance, rdobs);
                // jump out if obs is not in collision with initial and final points
                obsCon.update(rdobs);  //Change information of obstacle
                if(obsCon.violate(dT->x0.data()) || obsCon.violate(dT->xf.data()))
                    continue;
                else
                    break;
            }
            V_Copy(obsf + succeedObs * 4, rdobs, 4);
            // do the query and solve the problem to optimum
            double x0[7];
            V_Copy(x0, dT->x0.data(), 3);
            V_Copy(x0 + 3, rdobs, 4);
            Query.query(x0, indMat);
            // loop over neighbors only
            int flag = 0;
            int succeed = 0;
            double bestJ = 1e10;
            int everyTrial = maxTrial / nn_;
            for(int tryind = 0; tryind < nn_; tryind++){
                V_Copy(sol, tjf_ + indMat[0][tryind]*lenSol, lenSol);  //copy neighbor results
                flag = SNP.SNPsolve(sol);
                if(flag == 1){  // if warm start gives a solution, we accept that
                    succeed += 1;
                    if(SNP.getObj() < bestJ){
                        bestJ = SNP.getObj();
                        V_Copy(bestx, SNP.getX(), dT->numVar);
                    }
                }
                else{
                    std::vector<int> vioIndex;
                    for(int i = 0; i < N; i++){
                        if(obsCon.violate(tj.X.col(i).data()))
                            vioIndex.push_back(i);
                    }
                    if(vioIndex.size() > 0){
                        for(auto i : vioIndex){
                            obsCon.lazyRepair(tj.X.col(i).data());
                            tj.U.col(i) += VX::Random(dimu) * 0.3;
                        }
                        tj.copyto(sol);
                    } // now we get a basic solution, then we add perturbation to it
                    for (int j = 0; j < everyTrial; ++j) {
                        V_Copy(tmpx, sol, dT->numVar);
                        for(int i = 0; i < N*dimx; i++){
                            tmpx[i] += ((double)rand()/RAND_MAX - 0.5) * 2 * 0.2;
                        }
                        flag = SNP.SNPsolve(tmpx);  //solve using random, it might be bad
                        if (flag == 1) {
                            succeed++;
                            if(SNP.getObj() < bestJ){
                                bestJ = SNP.getObj();
                                V_Copy(bestx, SNP.getX(), dT->numVar);
                            }
                            if(succeed >= minTrial)
                                break;
                        }
                        else {
                            flag = 0;
                        }
                    }
                }
                if(succeed >= minTrial)
                    break;
            }
            // store the data if a solution is obtained.
            if(succeed > 0){
                numOK++;
                V_Copy(tjf + succeedObs * dT->numVar, bestx, dT->numVar);
                tjJ[succeedObs] = bestJ;
                succeedObs += 1;
                if(succeedObs == saveFreq){
                    saveFile(fpout, saveFreq, vecPnt, vecSz);
                    succeedObs = 0;  //reset
                }
            }
            else{
                numFail++;
            }
        }
    }
    //final save
    saveFile(fpout, succeedObs, vecPnt, vecSz);
    /*
    printf("Entering saving\n");
    int storeIndex = succeedObs;  // since I am lazy
    sprintf(savenames, "tjf");
    cnpy::npz_save(fnm, savenames, tjf, {storeIndex, solLen}, "a");
    sprintf(savenames, "obs");
    cnpy::npz_save(fnm, savenames, obsf, {storeIndex, 4}, "a");
    sprintf(savenames, "Jf");
    cnpy::npz_save(fnm, savenames, tjJ, {storeIndex}, "a");
    */
    sprintf(msg, "Process %s Succeed : %d, Fail : %d", append.c_str(), numOK, numFail);
    recordInfo(msg);
    fclose(fpout);
    delete dT;
    delete[] bestx;
    delete[] tmpx;
    delete[] sol;
    delete[] tj0;
    delete[] tjf;
    delete[] obsf;
    delete[] tjJ;
    delete[] x;
    return 1;
}

// randomly draw a trajectory from that file
void genTraj(traj &tj, FILE *fp, int numTraj, int solLen, int N, int dimx, int dimu){
    int index = (int)floor((double)rand()/RAND_MAX * numTraj);
    fseek(fp, index*solLen*sizeof(double), SEEK_SET);
    int lenSol = N*dimx + (N - 1) * dimu + 1;
    int lenHead = 1 + 2 * dimx;
    fseek(fp, lenHead * sizeof(double), SEEK_CUR);
    double *x = new double[lenSol];
    checkPointer(x);
    fread(x, sizeof(double), lenSol, fp);
    tj.copyfrom(x);
    delete[] x;
}

void genTraj(traj &tj, double *sol, int numTraj, int solLen){
    int index = genRandN(0, numTraj);
    tj.copyfrom(sol + index * solLen);
    return;
}

void checkPointer(void *pnt){
    if(!pnt){
        while(1)
            printf("Memory allocation error occurs\n");
    }
}

void saveFile(FILE *fp, int N, std::vector<double*> vec, std::vector<int> size){
    if(N == 0)
        return;
    size_t numArray = vec.size();
    for(size_t i = 0; i < numArray; i++){
        int sz = size[i];
        double *v = vec[i];
        Fwrite(v, sizeof(double), sz*N, fp);
    }
}
