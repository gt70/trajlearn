/*
 * solverwrapper.cpp
 * Copyright (C) 2017 Gao <gao.tang@duke.edu>
 *
 * Distributed under terms of the  license.
 */

/* Write a python wrapper for the problem */

#include "solveServer.h"
#include "utilityClass.h"
#include "pybind11/eigen.h"


dirTranODE *dT = NULL;
tigerSys *sys = NULL;


//define the function to be integrated here
void funwrapper(const double t, cRefV y, cRefV p, RefV dy, RefM J){
    dT->RKDynFun(t, y, p, dy, J);
}

class pyServer: public solveServer{
public:
    pyServer(){
        sys = new Rotor();
    }
    pyServer(const pyServer &ps): solveServer(ps){
        sys = new Rotor();
        dTran->system = sys;
    }
    void initfnm(const std::string &fnm){
        init(fnm);
    }
    ~pyServer(){
        if(sys){
            delete sys;
            sys = NULL;
        }
    }
};


namespace py = pybind11;
PYBIND11_MODULE(libserver, m){
    py::class_<config>(m, "config")
        .def(py::init<>())
        .def("readjson", &config::readjson)
        .def("setN", &config::setN)
        .def("settf", &config::settf)
        .def("settfweight", &config::settfweight)
        .def("setX0Xf", &config::setX0Xf)
        .def("setObs", &config::setObs)
        .def("setFQR", &config::setFQR)
        .def("setXlbXubUlbUub", &config::setXlbXubUlbUub)
        .def("setXUbase", &config::setXUbase)
        .def("setfixxf", &config::setfixxf)
        .def_readonly("tfweight", &config::tfweight)
        .def_readonly("x0", &config::x0)
        .def_readonly("xf", &config::xf)
        .def_readonly("N", &config::N)
        .def_readonly("F", &config::F)
        .def_readonly("Q", &config::Q)
        .def_readonly("R", &config::R)
        .def_readonly("obs", &config::obs)
        .def_readonly("fixxf", &config::fixxf);

    py::class_<result>(m, "result")
        .def(py::init<>())
        .def("readjson", &result::readjson)
        .def("getObj", &result::getObj)
        .def("getFlag", &result::getFlag)
        .def_readonly("x", &result::x)
        .def_readonly("Fmul", &result::Fmul)
        .def_readonly("x0", &result::x0)
        .def_readonly("xf", &result::xf)
        .def_readonly("flag", &result::flag)
        .def_readonly("Mu", &result::Mu)
        .def_readonly("obj", &result::obj);

    py::class_<solveServer>(m, "server")
        .def(py::init<>())
        .def("init", (void (solveServer::*)(const config &)) &solveServer::init, "set by config data")
        .def(py::init<solveServer const &>()) // works fine
        .def("setOptTol", &solveServer::setOptTol)
        .def("setFeaTol", &solveServer::setFeaTol)
        .def("setRefPenalty", &solveServer::setRefPenalty)
        .def("setRefTraj", &solveServer::setRefTraj)
        .def("setIntOption", &solveServer::setIntOption)
        .def("setMajorIter", &solveServer::setMajorIter)
        .def("setMinorIter", &solveServer::setMinorIter)
        .def("setIter", &solveServer::setIter)
        .def("update", &solveServer::updatedTbyCfg, "update cfg")
        .def("constrEval", &solveServer::constrEval, "evaluate constraint")
        .def("solveStraightRand", &solveServer::solveStraightRand)
        .def("solveWithGuess", &solveServer::solveWithGuess)
        .def("updateX0Xf", &solveServer::updateX0Xf)
        .def("updateObstacle", &solveServer::updateObstacle)
        .def("massSim", &solveServer::massSim, "solve many times")
        .def("showObs", &solveServer::showConstr, "show the obstacle")
        .def_readonly("rst", &solveServer::rst)
        .def_readwrite("cfg", &solveServer::cfg);

    py::class_<pyServer, solveServer>(m, "pysolver")
        .def(py::init<>(), "constructor")
        .def("initfnm", &pyServer::initfnm, "init by file");
}
