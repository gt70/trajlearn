/*
 * pybind.h
 * Copyright (C) 2017 Gao <gao.tang@duke.edu>
 *
 * Distributed under terms of the  license.
 */

#include "TigerTools/TigerEigen.h"
#include <string>
#include "json/json.h"
#include <fstream>
#include "pybind11/eigen.h"

#include "SNPSolve.h"
#include "TrajOpt.h"
#include "Tools/VecMat.h"
#include "stdio.h"
#include "math.h"
#include "utilityClass.h"

/* Define several classes and functions so we can call the solver from Python, sounds great */
class config{
public:
    int N, dimx, dimu, fixxf;
    double tfweight;
    VX tf, ulb, uub, ubase, F, Q, R, xbase, xf, x0, xlb, xub, guess;
    bool warm = false;

    config(){}
    void readjson(const std::string &fnm){
        std::ifstream fin(fnm, std::ios::in);
        Json::Value jsin;
        fin >> jsin;
        //read those variables
        N = jsin["N"].asInt();
        tf.resize(2);
        tf(0) = jsin["tf"][0].asDouble();
        tf(1) = jsin["tf"][1].asDouble();
        tfweight = jsin["tfweight"].asDouble();
        dimx = jsin["dimx"].asInt();
        dimu = jsin["dimu"].asInt();
        x0.resize(dimx);
        xf.resize(dimx);
        xlb.setZero(dimx);
        xub.setZero(dimx);
        xbase.setZero(dimx);
        ulb.setZero(dimu);
        uub.setZero(dimu);
        ubase.setZero(dimu);
        F.resize(dimx);
        Q.resize(dimx);
        R.resize(dimu);
        for (int i = 0; i < dimx; i++) {
            F(i) = jsin["F"][i].asDouble();
            Q(i) = jsin["Q"][i].asDouble();
            x0(i) = jsin["x0"][i].asDouble();
            xf(i) = jsin["xf"][i].asDouble();
            xlb(i) = jsin["xlb"][i].asDouble();
            xub(i) = jsin["xub"][i].asDouble();
            if(jsin.isMember("xbase")){
                xbase(i) = jsin["xbase"][i].asDouble();
            }
        }
        for (int i = 0; i < dimu; i++) {
            R(i) = jsin["R"][i].asDouble();
            ulb(i) = jsin["ulb"][i].asDouble();
            uub(i) = jsin["uub"][i].asDouble();
            ubase(i) = jsin["ubase"][i].asDouble();
        }
        fixxf = jsin["fixxf"].asInt();
    }

    //setters
    void setN(int n){
        N = n;
    }
    void settf(cRefV t0){
        tf = t0;
    }
    void settfweight(double t){
        tfweight = t;
    }
    void setX0Xf(cRefV x0_, cRefV xf_){
        x0 = x0_;
        xf = xf_;
    }
    void setFQR(cRefV F_, cRefV Q_, cRefV R_){
        F = F_; Q = Q_; R = R_;
    }
    void setXlbXubUlbUub(cRefV xlb_, cRefV xub_, cRefV ulb_, cRefV uub_){
        xlb = xlb_; xub = xub_; ulb = ulb_; uub = uub_;
    }
    void setXUbase(cRefV xbs, cRefV ubs){
        xbase = xbs; ubase = ubs;
    }
    void setfixxf(int fix){
        fixxf = fix;
    }
    void setGuess(cRefV guess0){
        // check the size of guess
        int numVar = N * dimx + (N - 1) * dimu + 1;
        if(guess.size() != numVar){
            std::cout << "Size not right, no change.\n";
            return;
        }
        warm = true;
        guess = guess0;
    }
};


class result{
public:
    VX x, Fmul;
    VX x0, xf;
    double obj;
    int flag;
    void readjson(const std::string &fnm){
        std::ifstream fin(fnm, std::ios::in);
        Json::Value jsin;
        fin >> jsin;
        obj = jsin["obj"].asDouble();
        flag = jsin["flag"].asInt();
        int xlen = jsin['x'].size();
        x.resize(xlen);
        for (int i = 0; i < xlen; i++) {
            x(i) = jsin['x'][i].asDouble();
        }
        int flen = jsin["Fmul"].size();
        Fmul.resize(flen);
        for (int i = 0; i < flen; i++) {
            Fmul(i) = jsin["Fmul"][i].asDouble();
        }
    }
    double getObj(){
        return obj;
    }
    int getFlag(){
        return flag;
    }
};


//Declare global variables
dirTranODE *dT;

//define the function to be integrated here
void funwrapper(const double t, cRefV y, cRefV p, RefV dy, RefM J){
    dT->RKDynFun(t, y, p, dy, J);
}
/* The function that takes in config and return result */
result bvpSolve(config &cfg){
    srand(time(NULL));
    //Initialize dirTranODE instance
    Rotor sys;
    avoidConstraint obsCon;
    /* Initialize the dieTranODE instance */
    dT = new dirTranODE(&sys, cfg.N, cfg.tf(0), cfg.tf(1));
    dT->Q = cfg.Q;
    dT->R = cfg.R;
    dT->F = cfg.F;
    dT->x0 = cfg.x0;
    dT->xf = cfg.xf;
    dT->xlb = cfg.xlb;
    dT->xub = cfg.xub;
    dT->xbase = cfg.xbase;
    dT->ulb = cfg.ulb;
    dT->uub = cfg.uub;
    dT->ubase = cfg.ubase;
    dT->tfweight = cfg.tfweight;

    int N = dT->N;
    double tfmin = dT->tfmin, tmax = dT->tfmax;
    //Construct SNOPT problem
    bool warmstart = cfg.warm;
    SNPSetting SNP(dT);
    traj tj(dT->N, dT->dimx, dT->dimu);
    if(cfg.warm){
        tj.copyfrom(cfg.guess.data());
    }
    else{
        tj.X.setRandom();
        tj.U.setRandom();
        VX tmp = VX::LinSpaced(N, 0, tfmin);
        tj.t.array() = tmp.array();
    }
    SNP.SNPinit(tj);
    /*** Start solving ***/
    int flag = 0;
    int succeed = 0;
    int numFmul = dT->dimx * (N - 1);
    if(!warmstart)
        flag = SNP.SNPsolve();
    else
        flag = SNP.SNPsolve(tj);
    if(flag == 1){
        succeed += 1;
    }
    /* Write the result file into a json and store it */
    result rst;
    rst.flag = flag;
    rst.obj = SNP.getObj();
    rst.x0 = dT->x0;
    rst.xf = dT->xf;
    if(rst.flag == 1){
        MapV sol(SNP.getX(), dT->numVar);
        rst.x = sol;
    }
    delete dT;
    return rst;
}


namespace py = pybind11;
PYBIND11_MODULE(libclass, m){
    py::class_<config>(m, "config")
        .def(py::init<>())
        .def("readjson", &config::readjson)
        .def("setN", &config::setN)
        .def("settf", &config::settf)
        .def("settfweight", &config::settfweight)
        .def("setX0Xf", &config::setX0Xf)
        .def("setFQR", &config::setFQR)
        .def("setXlbXubUlbUub", &config::setXlbXubUlbUub)
        .def("setXUbase", &config::setXUbase)
        .def("setfixxf", &config::setfixxf)
        .def_readonly("tfweight", &config::tfweight)
        .def_readonly("x0", &config::x0)
        .def_readonly("xf", &config::xf)
        .def_readonly("N", &config::N)
        .def_readonly("F", &config::F)
        .def_readonly("Q", &config::Q)
        .def_readonly("R", &config::R);

    py::class_<result>(m, "result")
        .def(py::init<>())
        .def("readjson", &result::readjson)
        .def("getObj", &result::getObj)
        .def("getFlag", &result::getFlag)
        .def_readonly("x", &result::x)
        .def_readonly("Fmul", &result::Fmul)
        .def_readonly("x0", &result::x0)
        .def_readonly("xf", &result::xf)
        .def_readonly("flag", &result::flag)
        .def_readonly("obj", &result::obj);

    m.def("bvpSolve", &bvpSolve, "solve TPBVP");
}

