/* Test the new written class and compare with Matlab implementation */
//Modified Aug 14 2017 so that it takes into json file as settings and solve
#include "TrajOpt.h"
#include "SNPSolve.h"
#include "stdio.h"
#include "math.h"
#include "json/json.h"
#include <fstream>

/* Define a class for system dynamics */
class Rotor : public tigerSys {
private:
    double g = 9.81;
public:
    Rotor():tigerSys(6, 2){};
    virtual void dyn(const double t, cRefV x, cRefV u, RefV f, RefM df){
        double sta = sin(x(2)), cta = cos(x(2));
        f.segment(0, 3).array() = x.segment(3, 3).array();
        f(3) = u(0) * sta;
        f(4) = u(0) * cta - g;
        f(5) = u(1);
        df.setZero();
        df.block(0, 4, 3, 3).setIdentity();
        df(3, 3) = u(0) * cta;
        df(4, 3) = -u(0) * sta;
        df(3, 7) = sta;
        df(4, 7) = cta;
        df(5, 8) = 1;
    }
};

//Declare global variables
dirTranODE *dT;

//define the function to be integrated here
void funwrapper(const double t, cRefV y, cRefV p, RefV dy, RefM J){
    dT->RKDynFun(t, y, p, dy, J);
}

int main(int argc, char *argv[]){
    srand(time(NULL));
    //Initialize dirTranODE instance
    Rotor sys;
    int N = 20;
    double tfmin = 3, tfmax = 5;
    dT = new dirTranODE(&sys, N, tfmin, tfmax);
    dT->tfweight = 10;
    dT->F *= 100;
    dT->R *= 5;
    dT->Q *= 0;
    dT->x0.setZero(6);
    dT->x0(0)= -10;
    dT->x0(1) = 10;
    dT->xf.setZero(6);
    dT->fixxf = true;
    dT->ubase.resize(2);
    dT->ubase(0) = 9.81;
    dT->ubase(1) = 0;
    dT->ulb.resize(2);
    dT->ulb(0) = 1;
    dT->ulb(1) = -10;
    dT->uub.resize(2);
    dT->uub(0) = 20;
    dT->uub(1) = 10;
    //Optionally read the json file that user provided
    if(argc > 1){
        std::ifstream fin(argv[1], std::ios::in);
        if(!fin.is_open()){
            printf("Fail to open config file\n");
            exit(0);
        }
        Json::Value jsin;
        fin >> jsin;
        dT->N = jsin["N"].asInt();
        dT->tfmin = jsin["tf"][0].asDouble();
        dT->tfmax = jsin["tf"][1].asDouble();
        dT->tfweight = jsin["tfweight"].asDouble();
        int dimx = jsin["dimx"].asInt();
        int dimu = jsin["dimu"].asInt();
        dT->x0.setZero(dimx);
        dT->xf.setZero(dimx);
        dT->ubase.setZero(dimu);
        bool hasxbd = false, hasubd = false, hasubase = false;
        if(jsin.isMember("xlb")){
            hasxbd = true;
            dT->xlb.setZero(dimx);
            dT->xub.setZero(dimx);
        }
        if(jsin.isMember("ulb")){
            hasubd = true;
            dT->ulb.setZero(dimu);
            dT->uub.setZero(dimu);
        }
        if(jsin.isMember("ubase")){
            hasubase = true;
            dT->ubase.setZero(dimu);
        }
        for (int i = 0; i < dimx; i++) {
            dT->F(i) = jsin["F"][i].asDouble();
            dT->Q(i) = jsin["Q"][i].asDouble();
            dT->x0(i) = jsin["x0"][i].asDouble();
            dT->xf(i) = jsin["xf"][i].asDouble();
            if(hasxbd){
                dT->xlb(i) = jsin["xlb"][i].asDouble();
                dT->xub(i) = jsin["xub"][i].asDouble();
            }
        }
        for (int i = 0; i < dimu; i++) {
            dT->R(i) = jsin["R"][i].asDouble();
            if(hasubd){
                dT->ulb(i) = jsin["ulb"][i].asDouble();
                dT->uub(i) = jsin["uub"][i].asDouble();
            }
            if(hasubase)
                dT->ubase(i) = jsin["ubase"][i].asDouble();
        }
        if(jsin["fixxf"].asInt() > 0)
            dT->fixxf = true;
        else
            dT->fixxf = false;
        fin.close();
    }
    //Construct SNOPT problem
    SNPSetting SNP(dT);
    traj tj(dT->N, dT->dimx, dT->dimu);
    tj.X.setRandom();
    tj.U.setRandom();
    VX tmp = VX::LinSpaced(N, 0, tfmin);
    tj.t.array() = tmp.array();
    SNP.SNPinit(tj);
    //solve
    int flag = SNP.SNPsolve();
    printf("Flag is %d\n", flag);
    /***Output module, write the result into something else***/
    if(argc > 2){
        std::ofstream fout(argv[2], std::ios::out);
        if(!fout.is_open()){
            printf("Fail to open output file\n");
            exit(0);
        }
        Json::Value jsout;
        jsout["flag"] = flag;
        Json::Value jv;
        for(int i = 0; i < dT->numVar; i++)
            jv.append(SNP.getX()[i]);
        jsout["x"] = jv;
        fout << jsout;
        fout.close();
    }
    /*
    printf("Solving flag is %d\n", flag);
    //print final state
    printf("Final state\n");
    double *x = SNP.getX();
    for(int i = 0; i < dT->dimx; i++){
        printf("%lf\n", x[(dT->N - 1)*dT->dimx + i]);
    }
    printf("Initial control\n");
    for(int i = 0; i < dT->dimu; i++){
        printf("%lf\n", x[dT->N*dT->dimx + i]);
    }
    if(flag == 1){
        SNP.tofile("Result/tmp.txt");
    }
    */
    delete dT;
    return 1;
}
