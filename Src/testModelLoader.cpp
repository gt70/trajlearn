/*
 * testModelLoader.cpp
 * Copyright (C) 2017 Gao Tang <gt70@duke.edu>
 *
 * Distributed under terms of the MIT license.
 */

/* test if we can successfully load the model and make inference */

#include "modelLoader.h"
#include <string>

int main(){
    std::string fnm("/home/motion/GAO/ExplicitMPC/QuadCopter/Script/models/exportModel.json");
    mlpModel mdl(fnm);
    double xin[12] = {0};
    xin[0] = 10;
    xin[1] = 10;
    xin[2] = 10;
    VX y = mdl.eval(xin);
    std::cout << y << std::endl;
    return 0;
}
