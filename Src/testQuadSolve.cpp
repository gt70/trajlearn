//
// Created by Gao Tang at
// Fri 15 Dec 2017 08:44:04 PM EST
//
// Designed to solve drone trajectory optimization problem with obstacles
// Currently the obstacle is chosen as a sphere

#include "SNPSolve.h"
#include "TrajOpt.h"
#include "Tools/VecMat.h"
#include "stdio.h"
#include "math.h"
#include "json/json.h"
#include <fstream>
#include "utilityClass.h"

//Declare global variables
dirTranODE *dT;

//define the function to be integrated here
void funwrapper(const double t, cRefV y, cRefV p, RefV dy, RefM J){
    dT->RKDynFun(t, y, p, dy, J);
}

int main(int argc, char *argv[]){
    if(argc != 3){
        printf("Need config in and out file!\n");
        exit(0);
    }
    srand(time(NULL));
    //Initialize dirTranODE instance
    Rotor sys;
    avoidConstraint obsCon;
    std::ifstream fin(argv[1], std::ios::in);
    Json::Value jsin;
    fin >> jsin;
    fin.close();
    dT = new dirTranODE(jsin);
    dT->system = &sys;
    obsCon.init(jsin);
    printf("obs con has %d spheres\n", obsCon.obs.size());
    dT->conFun = &obsCon;
    int N = dT->N;
    double tfmin = dT->tfmin, tmax = dT->tfmax;
    //Construct SNOPT problem
    bool warmstart = false;
    SNPSetting SNP(dT);
    traj tj(dT->N, dT->dimx, dT->dimu);
    tj.X.setRandom();
    tj.U.setRandom();
    VX tmp = VX::LinSpaced(N, 0, tfmin);
    tj.t.array() = tmp.array();
    SNP.SNPinit(tj);
    /*** Update Sep 14 to include trail and guess ***/
    //solve for several times until convergence
    int maxTrial = 1, minTrial = 1;
    if(jsin.isMember("maxTrial")){
        maxTrial = jsin["maxTrial"].asInt();
    }
    if(jsin.isMember("minTrial")){
        minTrial = jsin["minTrial"].asInt();
    }

    /* Make it accept initial guess of trajectory */
    if(jsin.isMember("Guess")){
        // Copy into tj.X
        int ind = 0;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < dT->dimx; j++) {
                tj.X(j, i) = jsin["Guess"][ind].asDouble();
                ind++;
            }
        }
        // Copy into tj.U
        for (int i = 0; i < N - 1; i++) {
            for (int j = 0; j < dT->dimu; j++) {
                tj.U(j, i) = jsin["Guess"][ind].asDouble();
                ind++;
            }
        }
        // Set tj.t
        double tf = jsin["Guess"][ind].asDouble();
        tj.t.setLinSpaced(N, 0, tf);
        warmstart = true;
    }
    /*** Start solving ***/
    double *bestx = new double[dT->numVar];
    double bestJ = 1e10;
    int flag = 0;
    if(warmstart){
        flag = SNP.SNPsolve(tj);
        bestJ = SNP.getObj();
        V_Copy(bestx, SNP.getX(), dT->numVar);
    }
    else{
        int succeed = 0;
        int numFmul = dT->dimx * (N - 1);
        //double *bestFmul = new double[numFmul];
        for (int i = 0; i < maxTrial; i++) {
            flag = SNP.SNPsolve();
            if(flag == 1){
                succeed += 1;
                if(SNP.getObj() < bestJ){
                    bestJ = SNP.getObj();
                    V_Copy(bestx, SNP.getX(), dT->numVar);
                    //V_Copy(bestFmul, SNP.getFmul(), numFmul);
                }
                if(succeed >= minTrial)
                    break;
            }
        }
        if(succeed > 0){
            flag = 1;
        }
    }
    /* Write the result file into a json and store it */
    Json::Value Js;
    Js["flag"] = flag;
    if(flag == 1){
        /* Write several key variables such as x */
        Json::Value tmp;
        //Write x
        for (int i = 0; i < dT->numVar; i++) {
            tmp.append(bestx[i]);
        }
        Js["x"] = tmp;
        tmp.clear();
        //Write obj
        Js["obj"] = bestJ;
        //Write Fmul
        //for (int i = 0; i < (N - 1) * dT->dimx; i++) {
        //    tmp.append(bestFmul[i]);
        //}
        //Js["Fmul"] = tmp;
    }
    std::ofstream fout(argv[2]);
    fout << Js;
    fout.close();
    delete dT;
    delete[] bestx;
    //delete[] bestFmul;
    return 1;
}

