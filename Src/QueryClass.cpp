/*
It might be better if I build my own class to better manage memories
Users do not have to care too much about interface, they simply load file and query

As a start, some filenames are hard coded. Later release may make it free
*/
#include "QueryClass.h"
#include "Tools/VecMat.h"

queryClass::queryClass(int dimx, int dimy, int N, int querynum_, int nn_, double *x, double *y):dimX(dimx), dimY(dimy), nn(nn_){
    deleteXY = false;
    querynum = querynum_;
    if(dimy > 0)
        useY = true;
    else
        useY = false;
    numData = N;
    xSet = new Matrix<double>(x, N, dimx);
    if(useY)
        ySet = new Matrix<double>(y, N, dimx);
    indices = new Matrix<int>(new int[querynum*nn], querynum, nn);
    dists = new Matrix<double>(new double[querynum*nn], querynum, nn);
    index = new Index<DistFunctor<double> >(*xSet, flann::KDTreeIndexParams(4));
    index->buildIndex();
    printf("Build a database of size %d succeed!\n", numData);
}

queryClass::queryClass(int dimx, int dimy, int _querynum, int nn_, FILE *fp, baseReader &reader, int maxSize):dimX(dimx), dimY(dimy), nn(nn_){
    if(dimy > 0)
        useY = true;
    else
        useY = false;
    numData = 0;
    querynum = _querynum;
    //Allocate a large space, we might want to reduce it if necessary
    xSet = new Matrix<double>(new double[maxSize*dimX], maxSize, dimX);
    if(useY)
        ySet = new Matrix<double>(new double[maxSize*dimY], maxSize, dimY);
    //Allocate two spaces for reading X and Y
    double *xin = new double[dimx], *yin = new double[dimY];
    while(1){
        bool readok = reader(fp, xin, readContent::x);
        if(!readok)
            break;
        //Copy data to xSet
        V_Copy((*xSet)[numData], xin, dimx);
        if(useY){
            readok = reader(fp, yin, readContent::y);
            V_Copy((*ySet)[numData], yin, dimY);
        }
        numData++;
    }
    //change the size for xSet and ySet
    xSet->rows = numData;
    if(useY)
        ySet->rows = numData;
    indices = new Matrix<int>(new int[querynum*nn], querynum, nn);
    dists = new Matrix<double>(new double[querynum*nn], querynum, nn);
    index = new Index<DistFunctor<double> >(*xSet, flann::KDTreeIndexParams(4));
    index->buildIndex();
    printf("Build a database of size %d succeed!\n", numData);
    delete[] xin;
    delete[] yin;
}

//Make a query, return all that's necessary
void queryClass::query(double *x, Matrix<int> &ind){
    Matrix<double> query(x, querynum, dimX);
    index->knnSearch(query, *indices, *dists, nn, flann::SearchParams(128));
    //Copy first row of indices to indices since we are assuming this
    for (int j = 0; j < querynum; j++) {
        for (int i = 0; i < nn; i++) {
            ind[j][i] = (*indices)[j][i];
        }
    }
}

void queryClass::append(double *x, double *y, int num){
    //Create Matrix instance for x
    Matrix<double> MatX(x, num, dimX);
    index->addPoints(MatX, 1.1);
    //Deep copy the original data
    int newnumData = numData + num;
    double *newx = new double[dimX*newnumData];
    double *newy = new double[dimY*newnumData];
    for(int i = 0; i < dimX*numData; i++)
        newx[i] = *(xSet->ptr() + i);
    for(int i = 0; i < dimX*num; i++)
        newx[i + dimX*numData] = x[i];
    for(int i = 0; i < dimY*numData; i++)
        newy[i] = *(ySet->ptr() + i);
    for(int i = 0; i < dimY*num; i++)
        newy[i + dimY*numData] = y[i];
    numData = newnumData;
    delete[] xSet->ptr();
    delete[] ySet->ptr();
    delete xSet;
    delete ySet;
    xSet = new Matrix<double>(newx, numData, dimX);
    ySet = new Matrix<double>(newy, numData, dimY);
}

