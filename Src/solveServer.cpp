//
// Created by Gao Tang on 9/29/17.
//

/* Design a solver that can take into several configs and solve and store best result.
 * Provide several APIs to python.
 * For a new problem, previous structure can be used so reallocating is unnecessary. */

#include "solveServer.h"
#include "SNPSolve.h"
#include "TrajOpt.h"
#include "Tools/VecMat.h"
#include "stdio.h"
#include "math.h"
#include "json/json.h"
#include <fstream>
#include "utilityClass.h"
#include <iostream>

#define NO_DEBUG

//Declare global variables
extern dirTranODE *dT;
extern tigerSys *sys;

/* methods for initialization */
solveServer::solveServer(const std::string &jsnm){
    cfg.readjson(jsnm);
    init(cfg);
}

solveServer::solveServer(const config &cfgin){
    cfg = cfgin;
    init(cfg);
}

void solveServer::init(const config &cfgin){
    cfg = cfgin;
#ifdef DEBUG
    printf("copy config file\n");
#endif
#ifdef DEBUG
    std::cout << sys << std::endl;
    std::cout << cfg.N << std::endl;
    std::cout << cfg.tf << std::endl;
#endif
    if(sys == NULL){
        sys = new Rotor();
        std::cout << "create sys" << std::endl;
    }
    dTran = new dirTranODE(sys, cfg.N, cfg.tf(0), cfg.tf(1));
#ifdef DEBUG
    printf("create dTran\n");
#endif
    dT = dTran;
#ifdef DEBUG
    printf("begin update dTran\n");
#endif
    updatedTbyCfg();
    //init SNOPT
    tj = dT->ranGentj();
#ifdef DEBUG
    printf("begin create SNP\n");
#endif
    SNP = new SNPSetting(dT);
#ifdef DEBUG
    printf("create SNP\n");
#endif
    SNP->SNPinit(tj);
    N = dT->N;
    dimx = dT->dimx;
    dimu = dT->dimu;
    numX = N * dimx;
    numU = (N - 1) * dimu;
    numVar = numX + numU + 1;
    bestx.resize(numVar);
    bestFmul.resize((N-1)*dimx);
    guess.resize(numVar);
    maxIter = 20;
    minIter = 2;
}

void solveServer::init(const std::string &jsnm){
    cfg.readjson(jsnm);
    init(cfg);
}

solveServer::solveServer(const solveServer &ss){
    init(ss.cfg);
    maxIter = ss.maxIter;
    minIter = ss.minIter;
}

void solveServer::setIter(int max, int min){
    maxIter = max;
    minIter = min;
}

void solveServer::setOptTol(double tol){
    SNP->setOptTol(tol);
}
void solveServer::setFeaTol(double tol){
    SNP->setFeaTol(tol);
}
void solveServer::setIntOption(std::string &key, int val){
    SNP->setIntOption(key, val);
}
void solveServer::setMajorIter(int val){
    SNP->setMajorIter(val);
}
void solveServer::setMinorIter(int val){
    SNP->setMinorIter(val);
}
/* Update dirTranODE if config changes */
void solveServer::updatedTbyCfg(){
    dT->Q = cfg.Q;
    dT->R = cfg.R;
    dT->F = cfg.F;
    dT->x0 = cfg.x0;
    dT->xf = cfg.xf;
    dT->xlb = cfg.xlb;
    dT->xub = cfg.xub;
    dT->xbase = cfg.xbase;
    dT->ulb = cfg.ulb;
    dT->uub = cfg.uub;
    dT->ubase = cfg.ubase;
    dT->tfweight = cfg.tfweight;
    dT->fixxf = cfg.fixxf;
    // update the constraint function
    if(cfg.obs.size() == 0){
        dT->conFun = NULL;
    }
    else{
#ifdef DEBUG
    printf("update obstacle\n");
    std::cout << cfg.obs << std::endl;
#endif
        obsCon.updateN(cfg.obs.rows());
        for(int i = 0; i < cfg.obs.rows(); i++)
            obsCon.update(cfg.obs.row(i).data(), i);
#ifdef DEBUG
    printf("update obstacle finish\n");
#endif
        dT->conFun = &obsCon;  //just in case not initialized yet
    }
#ifdef DEBUG
    printf("create SNP\n");
#endif
}

/* set penalty on ref_traj */
void solveServer::setRefPenalty(double penalty){
    dT->traj_penalty = penalty;
    if(penalty > 0)
        dT->penalize_traj = true;
    else
        dT->penalize_traj = false;
}

/* set the ref_traj */
void solveServer::setRefTraj(cRefV traj){
    dT->ref_traj = traj;
}

/* solve with a straight line  */
int solveServer::solveStraightRand(){
    srand(time(NULL));
    tj = dT->ranGentj();
    SNP->SNPsolve(tj);
    copyResult();
    return rst.flag;
}

// given a solution, evaluate its violation of constraints
VX solveServer::constrEval(RefV sol){
    return SNP->constrEval(sol);
}

/* solve with guess */
int solveServer::solveWithGuess(cRefV x){
    if(numVar != x.size()){
        std::cout << "Size not equal\n";
        return 0;
    }
    SNP->SNPsolve(x.data(), NULL);
    copyResult();
    return rst.flag;
}

/* massive simulation */
int solveServer::massSim(){
    int solved = 0;
    bestJ = 1e10;
    for (int i = 0; i < maxIter; i++) {
        int flag = solveStraightRand();
        if(flag == 1){
            solved++;
            double newobj = SNP->getObj();
            if(newobj < bestJ){
                bestJ = newobj;
                V_Copy(bestx.data(), SNP->getX(), numVar);
            }
            if(solved >= minIter && minIter > 0){
                break;
            }
        }
    }
    copyBestResult(solved);
    return rst.flag;
}

/* Copy results to rst */
void solveServer::copyResult(){
    rst.flag = SNP->getInfo();
    rst.obj = SNP->getObj();
    rst.x0 = dT->x0;
    rst.xf = dT->xf;
    if(rst.flag == 1){
        MapV sol(SNP->getX(), dT->numVar);
        MapV lmd(SNP->getFmul(), SNP->ndyncon);
        MapV mu(SNP->getMu(), SNP->npathcon);
        rst.x = sol;
        rst.Fmul = lmd;
        rst.Mu = mu;
    }
    else
        rst.x.setZero();
}

/* copy best result to rst */
void solveServer::copyBestResult(int solved){
    if(solved > 0)
        rst.flag = 1;
    else{
        rst.flag = 0;
        return;
    }
    rst.obj = bestJ;
    rst.x0 = dT->x0;
    rst.xf = dT->xf;
    rst.x = bestx;
}

/* Update initial and final states */
void solveServer::updateX0Xf(cRefV x0, cRefV xf){
    // we update cfg directly, and then update dT
    cfg.x0 = x0;
    cfg.xf = xf;
    dT->x0 = x0;
    dT->xf = xf;
}

void solveServer::updateObstacle(cRefV obs, int i){
    obsCon.update(obs.data(), i);
    obsCon.print();
}

/* set guess */
void solveServer::setGuess(cRefV x){
    if(numVar != x.size()){
        std::cout << "Size not equal\n";
        return;
    }
    guess = x;
}

void solveServer::showConstr(){
    ((avoidConstraint*)dTran->conFun)->print();
}

//if fnm exists and fp is not null, write to file
void solveServer::dump(const std::string &fnm, FILE *fp){
    if(fnm.size() > 0){
        Json::Value Js;
        Js["flag"] = rst.flag;
        if(rst.flag == 1){
            /* Write several key variables such as x */
            Json::Value tmp;
            //Write x
            for (int i = 0; i < dT->numVar; i++) {
                tmp.append(rst.x(i));
            }
            Js["x"] = tmp;
            tmp.clear();
            //Write obj
            Js["obj"] = bestJ;
            if(rst.Fmul.size() > 0){
                //Write Fmul
                for (int i = 0; i < (N - 1) * dT->dimx; i++) {
                    tmp.append(rst.Fmul(i));
                }
                Js["Fmul"] = tmp;
            }
        }
        std::ofstream fout(fnm);
        fout << Js;
        fout.close();
    }
    if(fp != NULL){
        SNP->basicSaveAll(fp, true);
    }
}

solveServer::~solveServer(){
    if(dTran){
        delete dTran;
        dTran = NULL;
    }
    if(SNP){
        delete SNP;
        SNP = NULL;
    }
    if(sys){
        delete sys;
        sys = NULL;
    }
}
