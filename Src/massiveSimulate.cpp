//
// Created by Gao Tang on Sun 17 Dec 2017 09:39:06 AM EST
//

/* do massive simulation with changing initial state and obstacle */

#include "SNPSolve.h"
#include "TrajOpt.h"
#include "stdio.h"
#include "math.h"
#include "json/json.h"
#include "utilityClass.h"
#include <vector>
#include <fstream>
#include "modelLoader.h"
#include <vector>

//Declare global variables
dirTranODE *dT;

//define the function to be integrated here
void funwrapper(const double t, cRefV y, cRefV p, RefV dy, RefM J){
    dT->RKDynFun(t, y, p, dy, J);
}

int main(int argc, char *argv[]){
    srand((unsigned int) time(0));
    //Initialize dirTranODE instance
    Rotor sys;
    std::string mdlfnm("/home/motion/GAO/ExplicitMPC/QuadCopter/Script/models/exportModel.json");
    mlpModel mdl(mdlfnm);
    //The obstacle avoidance constraint
    avoidConstraint obsCon;
    //Optionally read the json file that user provided
    std::ifstream fin("resource/droneConfig.json", std::ios::in);
    if(!fin.is_open()){
        printf("Fail to open config file\n");
        exit(0);
    }
    Json::Value jsin;
    fin >> jsin;
    dT = new dirTranODE(jsin);
    dT->system = &sys;
    double tfmin = dT->tfmin;
    int N = dT->N;
    /* Setting of obstacles, not necessary here, but is required for constructing problem*/
    obsCon.init(jsin);
    dT->conFun = &obsCon;
    fin.close();
    /* read mssConfig file for running setup */
    fin.open("resource/mssConfig.json", std::ios::in);
    if(!fin.is_open()){
        printf("Fail to open config file\n");
        exit(0);
    }
    jsin.clear();
    fin >> jsin;
    int simN = jsin["Nsample"].asInt();
    double minRadius = jsin["minRadius"].asDouble();
    int maxTrial = jsin["maxTrial"].asInt();
    int minTrial = jsin["mintrial"].asInt();
    int dimx = sys.getdimx(), dimu = sys.getdimu();
    VX xlb(dimx), xub(dimx), dx(dimx);
    for (int i = 0; i < dimx; i++) {
        xlb(i) = jsin["xlb"][i].asDouble();
        xub(i) = jsin["xub"][i].asDouble();
    }
    dx = (xub - xlb);
    // read obstacle information
    VX obslb(4), obsub(4);
    for(int i = 0; i < 4; i++){
        obslb(i) = jsin["obslb"][i].asDouble();
        obsub(i) = jsin["obsub"][i].asDouble();
    }
    fin.close();
    /* Construct SNOPT problem */
    SNPSetting SNP(dT);
    traj tj = dT->ranGentj();
    SNP.SNPinit(tj);
    /* Prepare for files to record everything, basically, we need what states we tried and what results we got */
    std::string fnm, append;
    if(argc > 1){
        append = std::string(argv[1]);
    }
    fnm = std::string("Result/failStates") + append + std::string(".dat");
    FILE *fpfail = fopen(fnm.c_str(), "wb");
    fnm = std::string("Result/detailResults") + append + std::string(".dat");
    FILE *fpdtl = fopen(fnm.c_str(), "wb");
    if(!fpfail || !fpdtl){
        exit(0);
    }
    /* Loop over random states */
    int numFail = 0, numActive = 0, numOK = 0;
    double *bestx = new double[dT->numVar];
    double *sol = new double[dT->numVar];
    double *tmpx = new double[dT->numVar]; //store the result from model evaluation
    for (int simn = 0; simn < simN; simn++) {
        //Randomly generate xf since we fix x0
        VX randx0(dimx);
        for (int i = 0; i < dimx; i++) {
            randx0(i) = (double)rand()/RAND_MAX * dx(i) + xlb(i);
        }
        double dis0 = randx0.norm();
        if(dis0 < minRadius){
            simn--;
            continue;
        }
        dT->xf.setZero();
        dT->x0 = randx0;  //so we have fixed target and varying initial
        //Randomly generate obstacle
        double obs[4];
        while(1){
            for (int i = 0; i < 4; i++){
                obs[i] = (double)rand()/RAND_MAX*(obsub(i) - obslb(i)) + obslb(i);
            }
            // jump out if obs is not in collision with initial and final points
            obsCon.update(obs);  //Change information of obstacle
            if(obsCon.violate(dT->x0.data()) || obsCon.violate(dT->xf.data()))
                continue;
            else
                break;
        }
        int flag = 0;
        int succeed = 0;
        // use the model to generate an initial guess
        mdl.eval(dT->x0.data(), sol);
        flag = SNP.SNPsolve(sol);
        if(flag == 1){
            succeed = 1;
        }
        else{
            /*If sol not found, we try maxTrial times and adding perturbation to previous one*/
            // check how many points violates constraints
            tj.copyfrom(sol);
            std::vector<int> vioIndex;
            for(int i = 0; i < N; i++){
                if(obsCon.violate(tj.X.col(i).data()))
                    vioIndex.push_back(i);
            }
            if(vioIndex.size() > 0){
                for(auto i : vioIndex){
                    obsCon.lazyRepair(tj.X.col(i).data());
                    tj.U.col(i) += VX::Random(dimu) * 0.3;
                }
                tj.copyto(sol);
            } // now we get a basic solution, then we add perturbation to it
            double bestJ = 1e10;
            for (int j = 0; j < maxTrial; ++j) {
                V_Copy(tmpx, sol, dT->numVar);
                for(int i = 0; i < N*dimx; i++){
                    tmpx[i] += ((double)rand()/RAND_MAX - 0.5) * 2 * 0.5;
                }
                flag = SNP.SNPsolve(tmpx);  //solve using random, it might be bad
                if (flag == 1) {
                    succeed++;
                    if(SNP.getObj() < bestJ){
                        bestJ = SNP.getObj();
                        V_Copy(bestx, SNP.getX(), dT->numVar);
                    }
                    if(succeed >= minTrial)
                        break;
                }
                else {
                    flag = 0;
                }
            }
            V_Copy(SNP.getX(), bestx, dT->numVar);
        }
        // store the data if a solution is obtained.
        if(succeed > 0){
            numOK++;
            tj.copyfrom(SNP.getX());
            //check activeness of constraints given a function
            double tol = 1e-6;
            double maxVal = -1e10;
            for(int i = 0; i < N; i++){
                double val = obsCon.evalConstr(tj.X.col(i).data());
                if(val > maxVal)
                    maxVal = val;
            }
            printf("MaxVal is %f\n", maxVal);
            if(maxVal > -1e-6)
                numActive += 1;
            SNP.basicSaveAll(fpdtl, true);  //save x0, xf, J, sol
            fwrite(obs, sizeof(double), 4, fpdtl); //save obstacle location information, too
        }
        else{
            numFail++;
            fwrite(dT->x0.data(), sizeof(double), dimx, fpfail);
            fwrite(dT->xf.data(), sizeof(double), dimx, fpfail);
            fwrite(obs, sizeof(double), 4, fpfail);
        }
    }
    printf("Succeed : %d, Active %d, Fail : %d\n", numOK, numActive, numFail);
    delete dT;
    delete[] bestx;
    delete[] tmpx;
    delete[] sol;
    fclose(fpfail);
    fclose(fpdtl);
    return 1;
}
