//
// Created by Gao Tang on Sun 17 Dec 2017 11:05:24 PM EST
//

/* read optimal trajectories we already have, adding constraints, learn how the new trajectory should be */

#include "SNPSolve.h"
#include "TrajOpt.h"
#include "stdio.h"
#include "math.h"
#include "json/json.h"
#include "utilityClass.h"
#include "utilityFun.h"
#include <vector>
#include <fstream>
#include "modelLoader.h"
#include "cnpy.h"
#include <vector>

//Declare global variables
dirTranODE *dT;

//Declare several functions
void genTraj(traj &tj, FILE *fp, int numTraj, int solLen, int N, int dimx, int dimu);
void genTraj(traj &tj, double *sol, int numTraj, int solLen);

//define the function to be integrated here
void funwrapper(const double t, cRefV y, cRefV p, RefV dy, RefM J){
    dT->RKDynFun(t, y, p, dy, J);
}

int main(int argc, char *argv[]){
    srand((unsigned int) time(0));
    //Initialize dirTranODE instance
    Rotor sys;
    //The obstacle avoidance constraint
    avoidConstraint obsCon;
    //Optionally read the json file that user provided
    std::ifstream fin("resource/droneConfig.json", std::ios::in);
    if(!fin.is_open()){
        printf("Fail to open config file\n");
        exit(0);
    }
    Json::Value jsin;
    fin >> jsin;
    dT = new dirTranODE(jsin);
    dT->system = &sys;
    double tfmin = dT->tfmin;
    int N = dT->N, dimx = dT->dimx, dimu = dT->dimu;
    /* Setting of obstacles, not necessary here, but is required for constructing problem*/
    obsCon.init(jsin);
    dT->conFun = &obsCon;
    fin.close();
    /* read mssConfig file for running setup */
    fin.open("resource/mssConfig.json", std::ios::in);
    if(!fin.is_open()){
        printf("Fail to open config file\n");
        exit(0);
    }
    jsin.clear();
    fin >> jsin;
    int simN = jsin["Nsample"].asInt();
    double minRadius = jsin["minRadius"].asDouble();
    int maxTrial = jsin["maxTrial"].asInt();
    int minTrial = jsin["mintrial"].asInt();
    VX xlb(dimx), xub(dimx), dx(dimx);
    for (int i = 0; i < dimx; i++) {
        xlb(i) = jsin["xlb"][i].asDouble();
        xub(i) = jsin["xub"][i].asDouble();
    }
    dx = (xub - xlb);
    // read obstacle information
    VX obslb(4), obsub(4);
    for(int i = 0; i < 4; i++){
        obslb(i) = jsin["obslb"][i].asDouble();
        obsub(i) = jsin["obsub"][i].asDouble();
    }
    fin.close();
    /* configurate the database without collision */
    /*
    FILE *freefp = fopen("Result/alldetail.dat", "rb");
    fseek(freefp, 0, SEEK_END);
    long nbyte = ftell(freefp);
    fseek(freefp, 0, SEEK_SET);
    int solLen = dT->numVar;  //Sol is what we have
    if(nbyte % (solLen * sizeof(double)) != 0){
        printf("File size incorrect");
        exit(0);
    }
    int numTraj = ftell(freefp) / solLen / sizeof(double);
    */
    cnpy::npz_t my_npz = cnpy::npz_load("data/allTraj.npz");
    cnpy::NpyArray arr_sol = my_npz["Sol"];
    int numTraj = arr_sol.shape[0], solLen = arr_sol.shape[1];
    double *vec_sol =arr_sol.data<double>();

    /* Construct SNOPT problem */
    SNPSetting SNP(dT);
    traj tj = dT->ranGentj();
    SNP.SNPinit(tj);
    /* Prepare for files to record everything, basically, we need what states we tried and what results we got */
    std::string fnm, append;
    if(argc > 1){
        append = std::string(argv[1]);
    }
    char savenames[10];
    fnm = std::string("Result/obs") + append + std::string(".npz");
    cnpy::npz_save(fnm, "N", &N, {1}, "w");
    cnpy::npz_save(fnm, "dimx", &dimx, {1}, "a");
    cnpy::npz_save(fnm, "dimu", &dimu, {1}, "a");
    /* Loop over random states */
    int numFail = 0, numOK = 0;
    double *bestx = new double[dT->numVar];
    double *sol = new double[dT->numVar];
    double *tmpx = new double[dT->numVar]; //store the result from model evaluation
    double minradius = obslb(3), maxradius = obsub(3);
    double rdobs[4];
    int obsN = 10;
    // three variables for storing obstacle perturbation
    double *tj0 = new double[dT->numVar];
    double *tjf = new double[dT->numVar * obsN];
    double *obsf = new double[4 * obsN];
    for (int simn = 0; simn < simN; simn++) {
        // randomly load a trajectory that is collision free
        //genTraj(tj, freefp, numTraj, solLen, N, dimx, dimu);
        genTraj(tj, vec_sol, numTraj, solLen);
        tj.copyto(tj0);
        // emphasis that x0 and xf are never changed
        dT->xf.setZero();
        dT->x0 = tj.X.col(0);  //so we have fixed target and varying initial
        int succeedObs = 0;
        // randomly place new obstacles
        for(int obsn = 0; obsn < obsN; obsn++){
            // randomly select a point other than first and last
            while(1){
                int rdind = genRandN(1, N - 1);
                double rdradius = genRand(minradius, maxradius);
                double distance = genRand(0, rdradius);
                rdobs[3] = rdradius;
                genRandDirWDisCenter(tj.X.col(rdind).data(), distance, rdobs);
                // jump out if obs is not in collision with initial and final points
                obsCon.update(rdobs);  //Change information of obstacle
                if(obsCon.violate(dT->x0.data()) || obsCon.violate(dT->xf.data()))
                    continue;
                else
                    break;
            }
            V_Copy(obsf + succeedObs * 4, rdobs, 4);
            // Try to solve this problem
            int flag = 0;
            int succeed = 0;
            // directly solve without any modification
            flag = SNP.SNPsolve(tj);
            if(flag == 1){
                succeed = 1;
            }
            else{
                /*If sol not found, we try maxTrial times and adding perturbation to previous one*/
                std::vector<int> vioIndex;
                for(int i = 0; i < N; i++){
                    if(obsCon.violate(tj.X.col(i).data()))
                        vioIndex.push_back(i);
                }
                if(vioIndex.size() > 0){
                    for(auto i : vioIndex){
                        obsCon.lazyRepair(tj.X.col(i).data());
                        tj.U.col(i) += VX::Random(dimu) * 0.3;
                        tj.X.col(i) += VX::Random(dimx) * 0.2;
                    }
                    tj.copyto(sol);
                } // now we get a basic solution, then we add perturbation to it
                double bestJ = 1e10;
                for (int j = 0; j < maxTrial; ++j) {
                    V_Copy(tmpx, sol, dT->numVar);
                    for(int i = 0; i < N*dimx; i++){
                        tmpx[i] += ((double)rand()/RAND_MAX - 0.5) * 2 * 0.2;
                    }
                    flag = SNP.SNPsolve(tmpx);  //solve using random, it might be bad
                    if (flag == 1) {
                        succeed++;
                        if(SNP.getObj() < bestJ){
                            bestJ = SNP.getObj();
                            V_Copy(bestx, SNP.getX(), dT->numVar);
                        }
                        if(succeed >= minTrial)
                            break;
                    }
                    else {
                        flag = 0;
                    }
                }
                V_Copy(SNP.getX(), bestx, dT->numVar);
            }
            // store the data if a solution is obtained.
            if(succeed > 0){
                numOK++;
                //copy sol stored there
                V_Copy(tjf + succeedObs * dT->numVar, SNP.getX(), dT->numVar);
                succeedObs += 1;
            }
            else{
                numFail++;
            }
        }
        // save results for current episode into the npz file
        sprintf(savenames, "tj0_%d", simn);
        cnpy::npz_save(fnm, savenames, tj0, {dT->numVar}, "a");
        sprintf(savenames, "tjf_%d", simn);
        cnpy::npz_save(fnm, savenames, tjf, {succeedObs, dT->numVar}, "a");
        sprintf(savenames, "obsf_%d", simn);
        cnpy::npz_save(fnm, savenames, obsf, {succeedObs, 4}, "a");
    }
    printf("Succeed : %d, Fail : %d\n", numOK, numFail);
    delete dT;
    delete[] bestx;
    delete[] tmpx;
    delete[] sol;
    delete[] tj0;
    delete[] tjf;
    delete[] obsf;
    //fclose(freefp);
    return 1;
}

// randomly draw a trajectory from that file
void genTraj(traj &tj, FILE *fp, int numTraj, int solLen, int N, int dimx, int dimu){
    int index = (int)floor((double)rand()/RAND_MAX * numTraj);
    fseek(fp, index*solLen*sizeof(double), SEEK_SET);
    int lenSol = N*dimx + (N - 1) * dimu + 1;
    int lenHead = 1 + 2 * dimx;
    fseek(fp, lenHead * sizeof(double), SEEK_CUR);
    double *x = new double[lenSol];
    fread(x, sizeof(double), lenSol, fp);
    tj.copyfrom(x);
    delete[] x;
}

void genTraj(traj &tj, double *sol, int numTraj, int solLen){
    int index = genRandN(0, numTraj);
    tj.copyfrom(sol + index * solLen);
    return;
}
