#include <stdio.h>
#include "stdlib.h"
#include <string.h>
#include "TrajOpt.h"
#include "toyfunction.hh"
#include "snoptProblem.hh"
#include "Tools/VecMat.h"

extern dirTranODE *dT;

/**External variables**/

inline void MatCopy(double *G, MX &mat, int row, int col, int ldg){
    int accu = 0;
    for (int i = 0; i < row; ++i){
        for (int j = 0; j < col; ++j){
            G[accu + j] = mat(i, j);
        }
        accu += ldg;
    }
}

void toyusrf_(integer    *Status, integer *n,    doublereal x[],
	     integer    *needF,  integer *neF,  doublereal F[],
	     integer    *needG,  integer *neG,  doublereal G[],
	     char       *cu,     integer *lencu,
	     integer    iu[],    integer *leniu,
	     doublereal ru[],    integer *lenru )
{
    //convert x to traj
    traj tj(x, dT->N, dT->dimx, dT->dimu);
    MapV c(F, *neF);
    MapV value(G, *neG);
    MapM g(G, 1, dT->numVar);
    dT->objfun(tj, F[0], g);
    //F[0] = 0;
    //g.setZero();
#ifdef __APPLE__
    typedef Eigen::Matrix<long, -1, 1> VXi;
#endif
    VXi row(1), col(1);
    int rowAdd = 1, nGadd = dT->numVar;
    dT->confun(tj, c, row, col, value, rowAdd, nGadd, false);
}
