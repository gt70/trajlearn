/*  
 *  quadDyn.cpp
 *  Write a wrapper for quadCopter to boost calling
 */
#include "TigerTools/TigerEigen.h"
#include "pybind11/stl.h"
#include "pybind11/pybind11.h"
#include "pybind11/eigen.h"
#include <vector>


class Rotor{
public:
    int dimx = 12, dimu = 4;
    double m = 0.5, g = 9.81, kF = 1., kM = 0.0245, L = 0.175;
    VX In;
    double cg[12] = {0.0};
    Rotor(){
        In.setZero(3);
        In(0) = 0.0023;
        In(1) = 0.0023;
        In(2) = 0.004;
    };
    MapV advDyn(cRefV x, cRefV u){
        dronedyn(0, x.data(), u.data());
        MapV dx(cg, dimx);
        return dx;
    }
    void dyn(cRefV x, cRefV u, RefV f){
        //First we call dronedyn to flush cg0
        dronedyn(0, x.data(), u.data());
        MapV dx(cg, dimx);
        f = dx;
    }
    void dronedyn(const double t, const double *x, const double *u){
        //extract variables
        double phi = x[3], theta = x[4], psi = x[5], xd = x[6], yd = x[7], zd = x[8], p = x[9], q = x[10], r = x[11];
        double t1 = cos(theta);
        double t2 = sin(theta);
        double t3 = sin(phi);
        double t4 = cos(phi);
        double t5 = 0.1e1 / t4;
        t5 = t5 * (p * t2 - r * t1);
        double t6 = cos(psi);
        double t7 = sin(psi);
        double t8 = t1 * t3;
        double t9 = (u[0] + u[1] + u[2] + u[3]) * kF;
        double t10 = 0.1e1 / m;
        cg[0] = xd;
        cg[1] = yd;
        cg[2] = zd;
        cg[3] = p * t1 + r * t2;
        cg[4] = t5 * t3 + q;
        cg[5] = -t5;
        cg[6] = t10 * (t2 * t6 + t8 * t7) * t9;
        cg[7] = t10 * (t2 * t7 - t8 * t6) * t9;
        cg[8] = t10 * t4 * t1 * t9 - g;
        cg[9] = 0.1e1 / In[0] * ((u[1] - u[3]) * kF * L + (In[1] - In[2]) * r * q);
        cg[10] = -0.1e1 / In[1] * (L * kF * (u[0] - u[2]) + p * r * (In[0] - In[2]));
        cg[11] = 0.1e1 / In[2] * ((u[0] - u[1] + u[2] - u[3]) * kM + (In[0] - In[1]) * q * p);
    }
};

namespace py = pybind11;
PYBIND11_MODULE(libquad, m){
    py::class_<Rotor>(m, "quad")
        .def(py::init<>())
        .def("dyn", &Rotor::dyn)
        .def("dyn", &Rotor::advDyn)
        .def_readonly("dimx", &Rotor::dimx)
        .def_readonly("dimu", &Rotor::dimu)
        .def_readwrite("m", &Rotor::m)
        .def_readwrite("g", &Rotor::g)
        .def_readwrite("kF", &Rotor::kF)
        .def_readwrite("kM", &Rotor::kM)
        .def_readwrite("L", &Rotor::kM)
        .def_readwrite("In", &Rotor::In);
}
