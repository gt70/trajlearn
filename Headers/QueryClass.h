/*
It might be better if I build my own class to better manage memories
Users do not have to care too much about interface, they simply load file and query

As a start, some filenames are hard coded. Later release may make it free
*/
#ifndef QUERYCLASS_H
#define QUERYCLASS_H
#include <flann/flann.hpp>
#include <iostream>
#include <fstream>
#include "TigerTools/TigerEigen.h"
using namespace flann;

template<class T>
struct DistFunctor
{
	typedef bool is_kdtree_distance;
	typedef T ElementType;
	typedef typename Accumulator<T>::Type ResultType;
	template <typename Iterator1, typename Iterator2>
	ResultType operator()(Iterator1 a, Iterator2 b, size_t size,
	ResultType /*worst_dist*/ = -1) const
	{
		ResultType result = ResultType();
		ResultType diff;
		for(size_t i = 0; i < size; ++i ) {
			diff = *a++ - *b++;
			result += diff*diff;
		}
		return result;
	}
	template <typename U, typename V>
	inline ResultType accum_dist(const U& a, const V& b, int) const
	{
		return (a-b)*(a-b);
	}
};

//A functor to read file, return an array, and initialize the QueryClass
enum readContent{
    x, y
};
class baseReader{
    public:
        virtual bool operator()(FILE *fp, double *x, readContent ctt) = 0;
};

//A Query Class with sensitivity analysis matrix option
//I shall also add other properties, but basically I have to read the 126 doubles in side and deal with them
class queryClass{
public:
    //store dimension data, here I only store states, do not directly copy data
    const int dimX, dimY;
    int numData, querynum;
    const int nn; //number of points to be queried
    bool useY; //If it is false, we do not have information of Y, we only return indices and X
    bool deleteXY = true;
    Matrix<int> *indices; //Store indices of nearest points
    Matrix<double> *dists;//Store distances
    Index<DistFunctor<double> > *index;//Used for querying
    Matrix<double> *xSet; //The set of X
    Matrix<double> *ySet; //The set of Y
    queryClass(int dimx, int dimy, int querynum, int nn_, FILE *fp, baseReader &reader, int maxSize);
    queryClass(int dimx, int dimy, int N, int querynum, int nn_, double *x, double *y);
    void setuseY(){useY = true;}
    void setunuseY(){useY = false;}
    //Function to query a database and return the results stored in a Matrix provided by the user
    void query(double *x, Matrix<int> &ind);
    void append(double *x, double *y, int);
    ~queryClass(){
        if(deleteXY){
            delete[] xSet->ptr();
            if(useY)
                delete[] ySet->ptr();
        }
        delete[] indices->ptr();
        delete[] dists->ptr();
        delete xSet;
        if(useY)
            delete ySet;
        delete indices;
        delete dists;
        delete index;
    };
};
#endif
