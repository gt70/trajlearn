#ifndef READER_H
#define READER_H
#include "QueryClass.h"
#include "stdio.h"
//define the functor of reading good and bad results
class rdfunc : public baseReader{
    public:
        bool readOK = true;
        bool binaryMode = false;
        int numFlag = 1, numData = 12;
        //Basically, I want to read
        bool operator()(FILE *fp, double *x, readContent ctt){
            int solveflag = 0;
            double tmp = 0;
            //Read until we found one or eof
            if(!binaryMode){
                while(1){
                    int rdflag = fscanf(fp, "%d", &solveflag);
                    if(rdflag <= 0)
                        return false;
                    if((solveflag == 1 && readOK) || (solveflag == 0 && !readOK)){//If we found the correct flag, read and return
                        for (int i = 0; i < numData; i++) {
                            size_t t = fscanf(fp, "%lf", x + i);
                        }
                        return true;
                    }
                    else{//otherwise we skip this line
                        for (int i = 0; i < numData; i++) {
                            size_t t = fscanf(fp, "%lf", &tmp);
                        }
                    }
                }
            }
            else{
                while(1){//This is aggressive, since we know we are reading what we really want
                    int rdlen = fread(x, sizeof(double), numData, fp);
                    if(rdlen <= 0)
                        return false;
                    else
                        return true;
                }
            }
        }

};
//define the functor of reading good and bad results
class detailRead : public baseReader{
    public:
        bool binaryMode = false;
        int numData = 12;
        detailRead(int N, int dimx, int dimu){
            numData = N * dimx + (N - 1) * dimu + 1 + (N - 1) * dimx;
        }
        //Basically, I want to read all the data
        bool operator()(FILE *fp, double *x, readContent ctt){
            //Read until we found one or eof
            if(!binaryMode){
                for (int i = 0; i < numData; i++) {
                    size_t t = fscanf(fp, "%lf", x + i);
                }
            }
            else{
                if(feof(fp))
                    return false;
                size_t t = fread(x, sizeof(double), numData, fp);
            }
        }
};
#endif
