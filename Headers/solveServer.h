/*
 * solveServer.h
 * Copyright (C) 2017 Gao <gao.tang@duke.edu>
 *
 * Distributed under terms of the  license.
 */

#ifndef SOLVESERVER_H
#define SOLVESERVER_H

#include "TigerTools/TigerEigen.h"
#include <iostream>
#include "utilityClass.h"
#include <string>
#include "json/json.h"
#include <fstream>
#include "SNPSolve.h"
#include "TrajOpt.h"


/* the class for defining inputs to the server */
class config{
public:
    int N, dimx, dimu, fixxf;
    double tfweight;
    VX tf, ulb, uub, ubase, F, Q, R, xbase, xf, x0, xlb, xub, guess;
    rMX obs;  // a N by 4 matrix
    bool warm = false;

    config(){}
    void readjson(const std::string &fnm){
        std::ifstream fin(fnm, std::ios::in);
        Json::Value jsin;
        fin >> jsin;
        //read those variables
        N = jsin["N"].asInt();
        tf.resize(2);
        tf(0) = jsin["tf"][0].asDouble();
        tf(1) = jsin["tf"][1].asDouble();
        tfweight = jsin["tfweight"].asDouble();
        dimx = jsin["dimx"].asInt();
        dimu = jsin["dimu"].asInt();
        x0.resize(dimx);
        xf.resize(dimx);
        xlb.setZero(dimx);
        xub.setZero(dimx);
        xbase.setZero(dimx);
        ulb.setZero(dimu);
        uub.setZero(dimu);
        ubase.setZero(dimu);
        F.resize(dimx);
        Q.resize(dimx);
        R.resize(dimu);
        for (int i = 0; i < dimx; i++) {
            F(i) = jsin["F"][i].asDouble();
            Q(i) = jsin["Q"][i].asDouble();
            x0(i) = jsin["x0"][i].asDouble();
            xf(i) = jsin["xf"][i].asDouble();
            xlb(i) = jsin["xlb"][i].asDouble();
            xub(i) = jsin["xub"][i].asDouble();
            if(jsin.isMember("xbase")){
                xbase(i) = jsin["xbase"][i].asDouble();
            }
        }
        for (int i = 0; i < dimu; i++) {
            R(i) = jsin["R"][i].asDouble();
            ulb(i) = jsin["ulb"][i].asDouble();
            uub(i) = jsin["uub"][i].asDouble();
            ubase(i) = jsin["ubase"][i].asDouble();
        }
        fixxf = jsin["fixxf"].asInt();
        //read obstacle
        obs.resize(0, 4);
        if(jsin.isMember("center")){
            int N = jsin["center"].size();
            std::cout << N << " obstacles\n";
            obs.resize(N, 4);
            for(int j = 0; j < N; j++){
                for(int i = 0; i < 3; i++)
                    obs(j, i) = jsin["center"][j][i].asDouble();
                obs(j, 3) = jsin["radius"][j].asDouble();
            }
        }
    }

    //setters
    void setN(int n){
        N = n;
    }
    void settf(cRefV t0){
        tf = t0;
    }
    void settfweight(double t){
        tfweight = t;
    }
    void setX0Xf(cRefV x0_, cRefV xf_){
        x0 = x0_;
        xf = xf_;
    }
    void setObs(cRefV obs_){
        obs = obs_;
    }
    void setFQR(cRefV F_, cRefV Q_, cRefV R_){
        F = F_; Q = Q_; R = R_;
    }
    void setXlbXubUlbUub(cRefV xlb_, cRefV xub_, cRefV ulb_, cRefV uub_){
        xlb = xlb_; xub = xub_; ulb = ulb_; uub = uub_;
    }
    void setXUbase(cRefV xbs, cRefV ubs){
        xbase = xbs; ubase = ubs;
    }
    void setfixxf(int fix){
        fixxf = fix;
    }
    void setGuess(cRefV guess0){
        // check the size of guess
        int numVar = N * dimx + (N - 1) * dimu + 1;
        if(guess.size() != numVar){
            std::cout << "Size not right, no change.\n";
            return;
        }
        warm = true;
        guess = guess0;
    }
};

/* The class for storing outputst */
class result{
public:
    VX x, Fmul, Mu;
    VX x0, xf;
    double obj;
    int flag;
    void readjson(const std::string &fnm){
        std::ifstream fin(fnm, std::ios::in);
        Json::Value jsin;
        fin >> jsin;
        obj = jsin["obj"].asDouble();
        flag = jsin["flag"].asInt();
        int xlen = jsin['x'].size();
        x.resize(xlen);
        for (int i = 0; i < xlen; i++) {
            x(i) = jsin['x'][i].asDouble();
        }
        int flen = jsin["Fmul"].size();
        Fmul.resize(flen);
        for (int i = 0; i < flen; i++) {
            Fmul(i) = jsin["Fmul"][i].asDouble();
        }
    }
    double getObj(){
        return obj;
    }
    int getFlag(){
        return flag;
    }
};


class solveServer{
private:
    int N, dimx, dimu;
    int numVar, numX, numU;
public:
    // basic structure necessary for solving a problem
    dirTranODE *dTran;
    SNPSetting *SNP;
    avoidConstraint obsCon;
    // tigerSys *sys;
    config cfg;
    result rst;
    // utilities
    traj tj; //might be useful
    VX bestx, bestFmul;
    VX guess;
    double bestJ;
    // for solving
    int maxIter, minIter;

    /* methods for initialization */
    solveServer():obsCon(1){};
    solveServer(const std::string &jsnm);

    solveServer(const config &cfg);

    /* Copy constructor */
    solveServer(const solveServer &);

    void init(const std::string &jsnm);
    void init(const config &cfg);

    /* about ref traj */
    void setRefPenalty(double);
    /* set the ref_traj */
    void setRefTraj(cRefV traj);

    /* set tol for fea and opt */
    void setOptTol(double tol);
    void setFeaTol(double tol);
    void setIter(int max, int min);
    void setIntOption(std::string &name, int val);
    void setMajorIter(int val);
    void setMinorIter(int val);
    /* Update dirTranODE if config changes */
    void updatedTbyCfg();
    /* solve with a straight line  */
    int solveStraightRand();
    /* solve with guess */
    int solveWithGuess(cRefV x);
    /* massive simulation */
    int massSim();

    void showConstr();

    /* Copy results to rst */
    void copyResult();
    /* copy best result to rst */
    void copyBestResult(int solved);
    /* Update initial and final states */
    void updateX0Xf(cRefV x0, cRefV xf);
    /* Update obstacle information */
    void updateObstacle(cRefV obs, int i);
    // evaluate constraints given a solution candidate
    VX constrEval(RefV sol);
    /* set guess */
    void setGuess(cRefV x);
    /* dump result to file */
    void dump(const std::string &fnm, FILE *fp);
    ~solveServer();
};
#endif /* !SOLVESERVER_H */
