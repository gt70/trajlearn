/*
 * utilityFun.h
 * Copyright (C) 2017 Gao Tang <gt70@duke.edu>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef UTILITYFUN_H
#define UTILITYFUN_H

#include "stdlib.h"
#include "stdio.h"
#include "math.h"

inline int genRandN(int i0, int i1){
    // generate random number in [i0, i1)
    int diff = i1 - i0;
    int rd = rand() % diff + i0;
    return rd;
}

inline double genRand(double d0, double d1){
    // generate random number in [d0, d1]
    return (double)rand()/RAND_MAX * (d1 - d0) + d0;
}

inline void genRandDirWDisCenter(const double *ctr, double dis, double *rst){
    // randomly generate a point that has distance w.r.t center and random direction
    double theta = genRand(0, 2*M_PI);
    double z = genRand(-1, 1);
    double dir[3] = {sqrt(1-z*z)*cos(theta), sqrt(1-z*z)*sin(theta), z};
    for(int i = 0; i < 3; i++){
        rst[i] = ctr[i] + dir[i] * dis;
    }
}

#endif /* !UTILITYFUN_H */
