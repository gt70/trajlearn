/* Construct the SNOPT problem
 */
#ifndef SNPSOLVE_H
#define SNPSOLVE_H

#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <cstdio>
#include "toyfunction.hh"
#include "snoptProblem.hh"
#include "TrajOpt.h" //Admittedly, this is not good. A better choice is to abstract everything
#include "TigerTools/TigerTools.h"

/**External Variables**/

class SNPSetting{
private:
    integer n, neF, lenA, lenG, rowA, numVar;
    integer nc0con, ncfcon, nxbd, nubd;
    integer *iAfun, *jAvar, *iGfun, *jGvar, *xstate, *Fstate;
    doublereal *mA, *G, *x, *xlow, *xupp, *xmul, *F, *Flow, *Fupp, *Fmul;
    char *xnames, *Fnames;
public:
    int dimy, dimu, dimpath, N, ndyncon, npathcon;
    TrajOpt::dirTranODE *dT;
    snoptProblem2 ToyProb;
	double *getFmul() const {return Fmul+1;};
    double *getMu() const {return Fmul + 1 + ndyncon;}
    double *getMuc0con() const {return Fmul + 1 + ndyncon + npathcon;}
    double *getMucfcon() const {return getMuc0con() + nc0con;}
	double *getX() const {return x;};
    int getVarNum() const {return numVar;}
    int getFNum() const {return neF - 1;}
    double getObj() const {return F[0];};
    double *getF() const{return F;}
    double *getFlow() const{return Flow;}
    double *getFupp() const{return Fupp;}
    int getneF() const{return neF;}
    //Construction function
    SNPSetting(TrajOpt::dirTranODE *dT_): dT(dT_){}

    void setX(const double *xin){
        for (int i = 0; i < n; ++i)
            x[i] = xin[i];
    }
    void copyX(double *xout) const{
        for (int i = 0; i < n; ++i)
            xout[i] = x[i];
    }
    void copyFmul(double *Mul) const{
        for (int i = 0; i < ndyncon; ++i)
            Mul[i] = Fmul[1 + i];
    }
    void setFmul(const double *Mul){
        for (int i = 0; i < ndyncon; ++i)
            Fmul[1 + i] = Mul[i];
    }
    void copyMu(double *mu) const {  // for path constraint
        for (int i = 0; i < npathcon; ++i)
            mu[i] = Fmul[1 + ndyncon + i];
    }
    //As this could change many times, we use it as a separate function
    void setxbound(){
        // Set the upper and lower bounds.
        for (int i = 0; i < numVar; ++i){
            xlow[i] = -1e20; xupp[i] = 1e20; xstate[i] = 0;
        }
        //bounds on x
        if(nxbd > 0){
            for (int i = 0; i < N; ++i){
                for (int j = 0; j < dimy; ++j){
                    int index = dimy * i + j;
                    xlow[index] = dT->xlb(j);
                    xupp[index] = dT->xub(j);
                }
            }
        }
        //bounds on control u
        if(nubd > 0){
            for (int i = 0; i < N - 1; ++i) {
                for (int j = 0; j < dimu; ++j){
                    int index = dimy * N + i*dimu + j;
                    xlow[index] = dT->ulb(j);
                    xupp[index] = dT->uub(j);
                }
            }
        }
        //bounds on final time
        xlow[numVar - 1] = dT->tfmin;
        xupp[numVar - 1] = dT->tfmax;
        if(dT->fixx0){
            for (int i = 0; i < dimy; ++i){
                xlow[i] = dT->x0(i);
                xupp[i] = dT->x0(i);
            }
        }
        if(dT->fixxf){
            for (int i = 0; i < dimy; ++i){
                xlow[i + (N-1)*dimy] = dT->xf(i);
                xupp[i + (N-1)*dimy] = dT->xf(i);
            }
        }
    }

    void SNPinit(traj &tj){ //Initialize the problem
        //Simple calculation to get n, neF, lenA, lenG, etc
        N = dT->N, dimy = dT->dimx, dimu = dT->dimu;
        int nnzpath = 0;
        if(dT->conFun){
            dimpath = dT->conFun->dimc;
            nnzpath = dT->conFun->nnz;
        }
        else
            dimpath = 0;
        printf("conFun has %d constr and %d nnz\n", dimpath, nnzpath);
        ndyncon = (N - 1) * dimy;
        npathcon = (N - 1) * dimpath;
        nc0con = 0; ncfcon = 0;
        nxbd = dT->xlb.size();
        nubd = dT->ulb.size();
        numVar = dT->numVar;
        int numY = dT->numX;
        int numF = 1 + ndyncon + npathcon + nc0con + ncfcon; //row num of F

        int estlenA = 0;//linear part in system dynamics, not including itself
        //Modified Aug 13 2017, I do not use pseudospectral methods, assuming linear part in dynamics is 0

        // Allocate and initialize;
        n     =  numVar;
        neF   = numF;
        lenA  = estlenA;//Note on Aug 13 2017, can lenA be zero? Needs verification

        iAfun = new integer[lenA];
        jAvar = new integer[lenA];
        mA  = new doublereal[lenA];
        iAfun[0] = 0; jAvar[0] = 0; mA[0] = 0;
        //G include: numVar(objective)+nc0con*(dimy+dimu)+ncfcon*(dimy+dimu)+(dimy*dimy+dimy*(N-1))*(N-1)
        int everydyncon = nnzpath + dimy*(dimy + dimu) + dimy + dimy;//Modified Aug 14, do not forget tf
        lenG  = numVar + (N-1)*everydyncon + (nc0con + ncfcon)*(dimy + dimu);
        iGfun = new integer[lenG];
        jGvar = new integer[lenG];
        G     = new doublereal[lenG];

        x      = new doublereal[n];
        xlow   = new doublereal[n];
        xupp   = new doublereal[n];
        xmul   = new doublereal[n];
        xstate = new    integer[n];

        F      = new doublereal[neF];
        Flow   = new doublereal[neF];
        Fupp   = new doublereal[neF];
        Fmul   = new doublereal[neF];
        Fstate = new integer[neF];

        integer nxnames = 1;
        integer nFnames = 1;
        xnames = new char[nxnames*8];
        Fnames = new char[nFnames*8];

        integer    ObjRow = 0;
        doublereal ObjAdd = 0;

        //For objective function
        int neG = 0;
        Flow[0] = -1e20; Fupp[0] = 1e20;
        for (int i = 0; i < numVar; ++i){
            iGfun[neG] = 0;
            jGvar[neG] = i;
            neG++;
        }

        /***set bound on x***/
        setxbound();
        /***ssign mA, iAfun, jAvar, handles bound constraints on x and u***/
        int neA = 0;
        //Modify Aug 13 2017, currently I do not need A

        /***Set nondyn constraints***/
        //For the N-1 equality constraints about dynamics
        int appendrow = 1;
        int appendcol = 0;
        for (int i = 0; i < (N-1)*dimy; ++i){
            Flow[appendrow + i] = 0;
            Fupp[appendrow + i] = 0;
        }
        //set N - 1 path constraint
        appendrow = (N - 1) * dimy + 1;
        if(dimpath > 0){
            for (int i = 0; i < N - 1; i++) {
                for (int j = 0; j < dimpath; j++) {
                    Flow[appendrow + j] = dT->conFun->clb(j);
                    Fupp[appendrow + j] = dT->conFun->cub(j);
                }
                appendrow += dimpath;
            }
        }
        //set this by calling the function once since we are returning those variables
        MapV mV(F, neF);
#ifdef __APPLE__
        typedef Eigen::Map<Eigen::Matrix<long, -1, 1> > MapVi;
#endif
        MapVi row(iGfun, lenG), col(jGvar, lenG);
        MapV Gvalue(G, lenG);
        dT->confun(tj, mV, row, col, Gvalue, 1, numVar, true);
        neG = lenG;
        /***Set c0 and cf constraint***/
        //Modfied Aug 13 2017, no need to do this, assume all are known through confun
        // Set the problem, what's left is the initial guess
        ToyProb.initialize("", 1);
        ToyProb.setPrintFile("Toy0.out");
        ToyProb.setProbName("Toy0");
        ToyProb.setNeA         ( neA );
        ToyProb.setNeG         ( neG );
        ToyProb.setProblemSize( n, neF );
        ToyProb.setObjective  ( ObjRow, ObjAdd );
        ToyProb.setA          ( lenA, iAfun, jAvar, mA );
        ToyProb.setG          ( lenG, iGfun, jGvar );
        ToyProb.setX          ( x, xlow, xupp, xmul, xstate );
        ToyProb.setF          ( F, Flow, Fupp, Fmul, Fstate );
        ToyProb.setXNames     ( xnames, nxnames );
        ToyProb.setFNames     ( Fnames, nFnames );
        ToyProb.setProbName   ( "Toy0" );
        ToyProb.setUserFun    ( toyusrf_ );
        // snopta will compute the Jacobian by finite-differences.
        // The user has the option of calling  snJac  to define the
        // coordinate arrays (iAfun,jAvar,A) and (iGfun, jGvar).
        //ToyProb.computeJac    ();
        ToyProb.setIntParameter( "Derivative option", 1 );
        ToyProb.setIntParameter( "Verify level", 0 );
	    ToyProb.setIntParameter( "Major print level", 0 );
	    ToyProb.setIntParameter( "Major iterations limit", 3000);
        ToyProb.setIntParameter( "Minor print level", 0 );
        ToyProb.setRealParameter("Major optimality tolerance", 1e-4);
        ToyProb.setRealParameter("Major feasibility tolerance", 1e-6);
    }
    void setOptTol(double tol){
        ToyProb.setRealParameter("Major optimality tolerance", tol);
    }
    void setFeaTol(double tol){
        ToyProb.setRealParameter("Major feasibility tolerance", tol);
    }
    void setIntOption(std::string &fnm, int opt){
        ToyProb.setIntParameter(fnm.c_str(), opt);
    }
    void setMajorIter(int val){
	    ToyProb.setIntParameter( "Major iterations limit", val);
    }
    void setMinorIter(int val){
	    ToyProb.setIntParameter( "Minor iterations limit", val);
    }
    void tofile(const std::string fnm){
        //Print result stored in x into a file
        std::ofstream fout(fnm, std::ios::out);
        fout.precision(10);
        int numVar = n;
        Eigen::Map<VX> rstx(x, numVar);
        fout << rstx;
        fout << "\n";
        new (&rstx) Eigen::Map<VX>(Fmul+1, ndyncon);
        fout << rstx;
        fout.close();
    }
    //Here is how I save this, in binary mode, save everything including both xmul and Fmul, xstate, Fstate
    void saveall(FILE *fp){
        fwrite(&N, sizeof(int), 1, fp);
        fwrite(&dimy, sizeof(int), 1, fp);
        fwrite(&dimu, sizeof(int), 1, fp);
        fwrite(&numVar, sizeof(int), 1, fp);
        int numf = neF - 1;
        fwrite(&numf, sizeof(int), 1, fp);
        //write x0 //Modified Aug 13 2017, there is no x0 now
        //fwrite(MPP.ptr->x0.data(), sizeof(double), dimy, fp);
        //write x
        fwrite(x, sizeof(double), numVar, fp);
        fwrite(xmul, sizeof(double), numVar, fp);
        fwrite(xstate, sizeof(int), numVar, fp);
        //write Fmul
        fwrite(Fmul + 1, sizeof(double), numf, fp);
        fwrite(Fstate + 1, sizeof(int), numf, fp);
    }
    void basicSaveAll(FILE *fp, bool binary = false){
        int numf = neF - 1;
        //write x0 //Modified Aug 13 2017, there is no x0 now
        if(!binary){
            WriteRow(fp, x, dT->numX, "%15.6e", "", "\n");
            WriteRow(fp, x + dT->numX, dT->numU, "%15.6e", "", "\n");
            fprintf(fp, "%15.6e\n", x[dT->numVar - 1]);
            double J = getObj();
            //write J
            fprintf(fp, "%15.6e\n", J);
        }
        else{
            fwrite(dT->x0.data(), sizeof(double), dT->dimx, fp);
            fwrite(dT->xf.data(), sizeof(double), dT->dimx, fp);
            double J = getObj();
            fwrite(&J, sizeof(double), 1, fp);
            fwrite(x, sizeof(double), dT->numVar, fp);
            //fwrite(Fmul + 1, sizeof(double), numf, fp);
        }
    }
    /*  Calculate the distance of actual and desired final state   */
    double finalXfDiff(){
        MapV xf(x + dT->numX - dimy, dimy);
        VX diff = xf - dT->xf;
        return diff.norm();
    }
    //Solve the problem. Modified Aug 13 2017, no need to warm start
    int SNPsolve(){
        int appendcol = 0;
        //change constraints, here
        setxbound();
        for (int i = 0; i < neF; ++i){
            F[i] = 0;
            Fstate[i] = 0;
            Fmul[i] = 0;
        }
        for (int i = 0; i < n; ++i){
            xstate[i] = 0;
            xmul[i] = 0;
        }
        //Generate initial guess using the method in TrajOpt.h
        dT->ranGenGuess(x);
        ToyProb.setF          ( F, Flow, Fupp, Fmul, Fstate );
        ToyProb.setX          ( x, xlow, xupp, xmul, xstate );
        ToyProb.solve( 0 );
        return ToyProb.getInfo();
    };
    //Solve the problem. Given a trajectory
    int SNPsolve(traj &tj){
        //change constraints, here
        setxbound();
        for (int i = 0; i < neF; ++i){
            F[i] = 0;
            Fstate[i] = 0;
            Fmul[i] = 0;
        }
        for (int i = 0; i < n; ++i){
            xstate[i] = 0;
            xmul[i] = 0;
        }
        tj.copyto(x);
        ToyProb.setF          ( F, Flow, Fupp, Fmul, Fstate );
        ToyProb.setX          ( x, xlow, xupp, xmul, xstate );
        ToyProb.solve( 0 );
        return ToyProb.getInfo();
    };

    //Solve the problem. given the solution, lmdF
    int SNPsolve(const double *_x, double *_Fmul = NULL){
        setxbound();//As always, do this
        for (int i = 0; i < neF; ++i){
            F[i] = 0;
            Fstate[i] = 0;
            if(_Fmul)
                Fmul[i] = _Fmul[i];
            else
                Fmul[i] = 0;
        }
        for (int i = 0; i < n; ++i){
            xstate[i] = 0;
            xmul[i] = 0;
            x[i] = _x[i];
        }
        ToyProb.setF          ( F, Flow, Fupp, Fmul, Fstate );
        ToyProb.setX          ( x, xlow, xupp, xmul, xstate );
        ToyProb.solve( 0 );
        return ToyProb.getInfo();
    };

    int getInfo(){
        return ToyProb.getInfo();
    }

    /* Given a guess of solution, evaluate violation of constraints */
    void constrEval(traj &tj){
        MapVi row(iGfun, lenG), col(jGvar, lenG);
        MapV Gvalue(G, lenG);
        MapV mV(F, neF);
        dT->confun(tj, mV, row, col, Gvalue, 1, numVar, false);
    }

    // Given a guess of solution, evaluate violation of constraints
    // compared with previous one, its function argument is not traj
    VX constrEval(RefV sol){
        traj tj(sol.data(), dT->N, dT->dimx, dT->dimu);
        constrEval(tj);
        MapV mV(F, neF);
        VX out(neF);
        out = mV;
        return out;
    }

    ~SNPSetting(){
        delete []iAfun;  delete []jAvar;  delete []mA;
        delete []iGfun;  delete []jGvar;  delete []G;

        delete []x;      delete []xlow;   delete []xupp;
        delete []xmul;   delete []xstate;

        delete []F;      delete []Flow;   delete []Fupp;
        delete []Fmul;   delete []Fstate;

        delete []xnames; delete []Fnames;
    };
};
#endif
