//
// Created by Gao Tang on 8/22/17.
//

#ifndef ROTORDB_UTILITYCLASS_H
#define ROTORDB_UTILITYCLASS_H

#include "TrajOpt.h"
#include "TigerTools/TigerEigen.h"
#include <vector>
#include "json/json.h"

/* Define a class for system dynamics */
class Rotor : public tigerSys {
private:
    int dimx = 12, dimu = 4;
    double m = 0.5, g = 9.81, kF = 1., kM = 0.0245, L = 0.175;
    double In[3] = {0.0023, 0.0023, 0.004};
    double cg0[204] = {0.0};
public:
    Rotor():tigerSys(12, 4){};
    virtual void dyn(const double t, cRefV x, cRefV u, RefV f, RefM df){
        //First we call dronedyn to flush cg0
        dronedyn(t, x.data(), u.data());
        df.setZero();
        MapV dx(cg0, dimx);
        f = dx;
        MapM J(cg0 + dimx, dimx, dimx + dimu);
        df.middleCols(1, dimx + dimu) = J;
    }
    void dronedyn(const double t, const double *x, const double *u){
        //extract variables
        double phi = x[3], theta = x[4], psi = x[5], xd = x[6], yd = x[7], zd = x[8], p = x[9], q = x[10], r = x[11];
        double t1 = cos(theta);
        double t2 = sin(theta);
        double t3 = p * t1 + r * t2;
        double t4 = sin(phi);
        double t5 = cos(phi);
        double t6 = 0.1e1 / t5;
        double t7 = t1 * r;
        double t8 = t2 * p;
        double t9 = t8 - t7;
        double t10 = t6 * t9;
        double t11 = cos(psi);
        double t12 = sin(psi);
        double t13 = t1 * t12;
        double t14 = t11 * t2;
        double t15 = (u[0] + u[1] + u[2] + u[3]) * kF;
        t11 = t11 * t1;
        t12 = t12 * t2;
        double t16 = -t11 * t4 + t12;
        double t17 = 0.1e1 / m;
        t5 = t17 * t5;
        double t18 = t5 * t1;
        double t19 = -In[1] + In[2];
        double t20 = q * t19;
        double t21 = In[0] - In[2];
        double t22 = p * t21;
        double t23 = L * kF * (u[0] - u[2]) + t22 * r;
        double t24 = In[0] - In[1];
        double t25 = p * t24;
        double t26 = (u[0] - u[1] + u[2] - u[3]) * kM + t25 * q;
        double t27 = pow(t6, 0.2e1);
        double t28 = t27 * pow(t4, 0.2e1) + 0.1e1;
        t7 = -t7 * t28 + t8 * t28;
        t8 = t6 * t3;
        t28 = 0.1e1 / In[1];
        double t29 = 0.1e1 / In[0];
        double t30 = 0.1e1 / In[2];
        double t31 = t18 * kF;
        double t32 = t17 * (t13 * t4 + t14);
        double t33 = t32 * t15;
        t32 = t32 * kF;
        double t34 = t17 * t16;
        double t35 = t34 * kF;
        double t36 = t28 * L * kF;
        double t37 = t29 * L * kF;
        double t38 = t30 * kM;
        double t39 = t1 * t6;
        t6 = t2 * t6;
        cg0[0] = xd;
        cg0[1] = yd;
        cg0[2] = zd;
        cg0[3] = t3;
        cg0[4] = t10 * t4 + q;
        cg0[5] = -t10;
        cg0[6] = t33;
        cg0[7] = t34 * t15;
        cg0[8] = t18 * t15 - g;
        cg0[9] = -t29 * (-L * kF * (u[1] - u[3]) + t20 * r);
        cg0[10] = -t28 * t23;
        cg0[11] = t30 * t26;
        cg0[52] = t7;
        cg0[53] = -t27 * t4 * t9;
        cg0[54] = t5 * t13 * t15;
        cg0[55] = -t5 * t11 * t15;
        cg0[56] = -t17 * t4 * t1 * t15;
        cg0[63] = -t9;
        cg0[64] = t8 * t4;
        cg0[65] = -t8;
        cg0[66] = t17 * (-t12 * t4 + t11) * t15;
        cg0[67] = t17 * (t14 * t4 + t13) * t15;
        cg0[68] = -t5 * t2 * t15;
        cg0[78] = -t17 * t16 * t15;
        cg0[79] = t33;
        cg0[84] = 1;
        cg0[97] = 1;
        cg0[110] = 1;
        cg0[123] = t1;
        cg0[124] = t6 * t4;
        cg0[125] = -t6;
        cg0[130] = -t28 * r * t21;
        cg0[131] = t30 * q * t24;
        cg0[136] = 1;
        cg0[141] = -t29 * r * t19;
        cg0[143] = t30 * t25;
        cg0[147] = t2;
        cg0[148] = -t39 * t4;
        cg0[149] = t39;
        cg0[153] = -t29 * t20;
        cg0[154] = -t28 * t22;
        cg0[162] = t32;
        cg0[163] = t35;
        cg0[164] = t31;
        cg0[166] = -t36;
        cg0[167] = t38;
        cg0[174] = t32;
        cg0[175] = t35;
        cg0[176] = t31;
        cg0[177] = t37;
        cg0[179] = -t38;
        cg0[186] = t32;
        cg0[187] = t35;
        cg0[188] = t31;
        cg0[190] = t36;
        cg0[191] = t38;
        cg0[198] = t32;
        cg0[199] = t35;
        cg0[200] = t31;
        cg0[201] = -t37;
        cg0[203] = -t38;
    }
};

class geometry{
public:
    bool out = true;  // means we want to be outside
    int ngrad = 0;
    geometry(bool out_=true):out(out_){};
    virtual void evalConstr(const double *s, double *val, double *grad)=0;
    virtual void print() = 0;
};
//class circles
class circle : public geometry{
public:
    double x, y, r;
    circle(){};
    circle(double _x, double _y, double _r): x(_x), y(_y), r(_r){ngrad=2;}
    void evalConstr(const double *s, double *val, double *grad){
        double sx = s[0], sy = s[1];
        *val = pow(sx - x, 2) + pow(sy - y, 2) - r * r;
        grad[0] = 2 * (sx - x);
        grad[1] = 2 * (sy - y);
    }
    void print(){
        std::cout << "x = " << x << " y = " << y << " r = " << r << std::endl;
    }
};
//class sphere
class sphere : public geometry{
public:
    double x, y, z, r;
    sphere(){};
    sphere(double _x, double _y, double _z, double _r): x(_x), y(_y), z(_z), r(_r){ngrad=3;}
    void update(double _x, double _y, double _z, double _r){
        x = _x;
        y = _y;
        z = _z;
        r = _r;
    }
    void evalConstr(const double *s, double *val, double *grad){
        double sx = s[0], sy = s[1], sz = s[2];
        *val = -(pow(sx - x, 2) + pow(sy - y, 2) + pow(sz - z, 2)) + r * r;  // assume we want to be outside
        grad[0] = -2 * (sx - x);
        grad[1] = -2 * (sy - y);
        grad[2] = -2 * (sz - z);
        if(!out){
            *val *= -1.0;
            grad[0] *= -1.0;
            grad[1] *= -1.0;
            grad[2] *= -1.0;
        }
    }
    void print(){
        std::cout << "x=" << x << " y=" << y << " z=" << z <<  " r=" << r << std::endl;
    }
    double evalConstr(const double *s){
        double sx = s[0], sy = s[1], sz = s[2];
        double val = -(pow(sx - x, 2) + pow(sy - y, 2) + pow(sz - z, 2)) + r * r;  // assume we want to be outside
        return val;
    }
    bool violate(const double *s, double tol=0){
        double val = -(pow(s[0] - x, 2) + pow(s[1] - y, 2) + pow(s[2] - z, 2)) + r * r;  // assume we want to be outside
        if((out && val < -tol) || (!out && val > tol)){
            return false;
        }
        else{
            return true;
        }
    }
    void lazyRepair(double *xin){  // repair a point by moving to the closest point on surface
        if(!violate(xin))
            return;
        VX dir(3);
        dir(0) = xin[0] - x;
        dir(1) = xin[1] - y;
        dir(2) = xin[2] - z;
        double dirnorm = dir.norm();
        if(dirnorm < 1e-6){
            dir.setRandom();
        }
        dir.array() /= dir.norm();
        xin[0] = x + r * dir(0);
        xin[1] = y + r * dir(1);
        xin[2] = z + r * dir(2);
    }
};
//Declare the class for collision avoidance of sphere
class avoidConstraint : public TrajOpt::constraintFunctor {
public:
    std::vector<geometry*> obs;
    avoidConstraint(int N=1){
        dimstate = 12;
        dimctrl = 4;
        dimc = N;
        nnz = 3 * N;
        cub = VX::Zero(N);
        clb = -1e10 * VX::Ones(N);
        for(int i = 0; i < N; i++)
            obs.push_back(new sphere(0, 0, 0, 0));
    }
    void init(Json::Value &js){
        int N = 0;
        if(js.isMember("radius")){
            N = js["radius"].size();
        }
        std::cout << "N = " << N << std::endl;
        // update some integers, if necessary
        if(obs.size() != N){
            updateN(N);
        }
        // update center, radius, etc
        for (int i = 0; i < N; i++) {
            double x = js["center"][i][0].asDouble(), y = js["center"][i][1].asDouble(), z = js["center"][i][2].asDouble();
            double r = js["radius"][i].asDouble();
            ((sphere*)(obs[i]))->update(x, y, z, r);
        }
    }
    void updateN(int N){
        if(N != obs.size()){
            dimc = N;
            nnz = 3 * N;
            cub = VX::Zero(N);
            clb = -1e10 * VX::Ones(N);
            obs.clear();
            for(int i = 0; i < N; i++)
                obs.push_back(new sphere(0, 0, 0, 0));
        }
    }
    // update one obstacle at a time
    void update(const double *ob, int obsn=0){
        ((sphere*)(obs[obsn]))->update(ob[0], ob[1], ob[2], ob[3]);
    }
    void print(){
        std::cout << dimc << " obstacles\n";
        for(int i = 0; i < dimc; i++){
            obs[i]->print();
        }
    }
    // evaluate constraint function values
    void eval(const double t, cRefV x, cRefV u, RefV f, SpMX &fx) {
        const double *s = x.data();
        double val;
        for (int i = 0; i < obs.size(); i++) {
            int ngrad = obs[i]->ngrad;
            VX Egrad(obs[i]->ngrad);
            double *grad = Egrad.data();
            obs[i]->evalConstr(s, &val, grad);
            f(i) = val;
            for(int j = 0; j < ngrad; j++){
                fx.coeffRef(i, j) = grad[j];
            }
        }
    }
    bool violate(const double *x, double tol=0, int obsn=0){
        // determine if a given state in x violates constraints
        return ((sphere*)(obs[obsn]))->violate(x, tol);
    }
    void lazyRepair(double *x, int obsn=0){
        return ((sphere*)(obs[obsn]))->lazyRepair(x);
    }
    double evalConstr(const double *x, int obsn=0){
        return ((sphere*)(obs[obsn]))->evalConstr(x);
    }
    ~avoidConstraint(){
        for(auto i : obs)
            delete i;
    }
};
#endif //ROTORDB_UTILITYCLASS_H
